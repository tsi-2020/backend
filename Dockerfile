FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY ./Udelar.sln .
COPY ./Udelar.API/Udelar.API.csproj ./Udelar.API/
COPY ./Udelar.Common/Udelar.Common.csproj ./Udelar.Common/
COPY ./Udelar.Model/Udelar.Model.csproj ./Udelar.Model/
COPY ./Udelar.Persistence/Udelar.Persistence.csproj ./Udelar.Persistence/
COPY ./Udelar.Service/Udelar.Service.csproj ./Udelar.Service/
COPY ./Udelar.Service.Tests/Udelar.Service.Tests.csproj ./Udelar.Service.Tests/

RUN dotnet restore

COPY . .

RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out .
RUN ls -la
ENTRYPOINT ["dotnet", "Udelar.API.dll"]
