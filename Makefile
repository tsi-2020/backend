setup: hooks restore clean build migrate

hooks:
	npm i 

clean:
	dotnet clean

build:
	dotnet build

restore:
	dotnet restore

migrate:
	dotnet ef database update -s Udelar.API -p Udelar.Persistence -v

watch:
	dotnet watch -p Udelar.API/ run

run:
	dotnet run -p Udelar.API

test:
	dotnet test --no-build -v=normal