﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Notice.Request;
using Udelar.Common.Dtos.Notice.Response;

namespace Udelar.Services
{
  public interface INoticeService
  {
    Task CreateNotice(CreateNoticeDto createNoticeDto, Guid universityId, Guid? subjectId = null);
    Task<List<ReadNoticeDto>> ListNotices(Guid universityId, Guid? subjectId = null);
    Task<ReadNoticeDto> ReadNotice(Guid noticeId);
  }
}
