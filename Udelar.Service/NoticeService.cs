﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Notice.Request;
using Udelar.Common.Dtos.Notice.Response;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class NoticeService : INoticeService
  {
    private readonly IMapper _mapper;
    private readonly ISubjectRepository _subjectRepo;
    private readonly INoticeRepository _noticeRepo;
    private readonly IUniversitiesRepository _universityRepo;

    public NoticeService(IMapper mapper, ISubjectRepository iSubjectRepository, INoticeRepository iNoticeRepository,
      IUniversitiesRepository iUniversitiesRepository)
    {
      _subjectRepo = iSubjectRepository;
      _mapper = mapper;
      _noticeRepo = iNoticeRepository;
      _universityRepo = iUniversitiesRepository;
    }

    public async Task CreateNotice(CreateNoticeDto createNoticeDto, Guid universityId, Guid? subjectId)
    {
      if (subjectId != null)
      {
        var subject = await FindSubjectByIdAndUniversity(universityId, subjectId.Value);
        var notice = new Notice { Message = createNoticeDto.Message, Title = createNoticeDto.Title };
        subject.Notices.Add(notice);
        await _subjectRepo.SaveChanges(subject);
      }
      else
      {
        var university = await _universityRepo.FindUniversityById(universityId);
        var notice = new Notice { Message = createNoticeDto.Message, Title = createNoticeDto.Title };
        university.Notices.Add(notice);
        await _universityRepo.Save(university);
      }
    }

    private async Task<Subject> FindSubjectByIdAndUniversity(Guid universityId, Guid subjectId)
    {
      var subject = await _subjectRepo.FindByIdAndUniversity(subjectId, universityId);
      if (subject == null)
      {
        throw new EntityNotFoundException("Asignatura no encontrada.");
      }

      return subject;
    }

    public async Task<List<ReadNoticeDto>> ListNotices(Guid universityId, Guid? subjectId)
    {
      List<Notice> notices;
      if (subjectId != null)
      {
        var subject = await FindSubjectByIdAndUniversity(universityId, subjectId.Value);
        notices = await _noticeRepo.ListNotices(subject.Id);
        return _mapper.Map<List<ReadNoticeDto>>(notices);
      }
      notices = await _noticeRepo.ListUniversityNotices(universityId);
      return _mapper.Map<List<ReadNoticeDto>>(notices);
    }

    public async Task<ReadNoticeDto> ReadNotice(Guid noticeId)
    {
      var notice = await _noticeRepo.GetNoticeById(noticeId);
      if (notice == default)
      {
        throw new EntityNotFoundException("Comunicado no enocntrado.");
      }
      return _mapper.Map<ReadNoticeDto>(notice);
    }
  }
}
