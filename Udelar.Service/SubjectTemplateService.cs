﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Component.Request;
using Udelar.Common.Dtos.Component.Response;
using Udelar.Common.Dtos.SubjectTemplate.request;
using Udelar.Common.Dtos.SubjectTemplate.response;
using Udelar.Model;
using Udelar.Model.Component;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class SubjectTemplateService : ISubjectTemplateService
  {
    private readonly IMapper _mapper;
    private readonly ISubjectTemplateRepository _templateRepository;
    private readonly IUniversitiesRepository _universitiesRepository;

    public SubjectTemplateService(IMapper mapper, IUniversitiesRepository universitiesRepository,
      ISubjectTemplateRepository templateRepository)
    {
      _mapper = mapper;
      _universitiesRepository = universitiesRepository;
      _templateRepository = templateRepository;
    }

    public async Task CreateSubjectTemplate(Guid universityId, CreateSubjectTemplateDto newSubjectTemplate)
    {
      var university = await _universitiesRepository.FindUniversityById(universityId);

      var subjectTemplate = _mapper.Map<CreateSubjectTemplateDto, SubjectTemplate>(newSubjectTemplate);
      subjectTemplate.University = university;
      university.SubjectTemplates.Add(subjectTemplate);
      foreach (var educationalUnit in newSubjectTemplate.EducationalUnits)
      {
        subjectTemplate.Components.Add(NewEducationalUnit(educationalUnit));
      }

      await _templateRepository.AddSubjectTemplate(subjectTemplate);
    }

    public async Task<List<ReadSubjectTemplateDto>> GetAllSubjectTemplates(Guid universityId)
    {
      var subjectTemplates = await _templateRepository.GetAllSubjectTemplates(universityId);

      return _mapper.Map<List<ReadSubjectTemplateDto>>(subjectTemplates);
    }

    public async Task<object> GetSubjectTemplateById(Guid universityId, Guid templateId)
    {
      var subjectTemplate = await _templateRepository.GetSubjectTemplateById(universityId, templateId);
      var templateDto = _mapper.Map<ReadSubjectTemplateDto>(subjectTemplate);
      foreach (var educationalUnit in subjectTemplate.Components)
      {
        var educationalUnitDto = _mapper.Map<ReadEducationalUnitDto>(educationalUnit);
        foreach (var component in educationalUnit.Components)
        {
          if (component.GetType() == typeof(AssignmentComponent))
          {
            educationalUnitDto.AssigmentComponents
              .Add(_mapper.Map<ReadAssigmentComponentDto>((AssignmentComponent)component));
          }
          else if (component.GetType() == typeof(DocumentComponent))
          {
            educationalUnitDto.DocumentComponents
              .Add(_mapper.Map<ReadDocumentComponentDto>((DocumentComponent)component));
          }
          else if (component.GetType() == typeof(ForumComponent))
          {
            educationalUnitDto.ForumComponents
              .Add(_mapper.Map<ReadForumComponentDto>((ForumComponent)component));
          }
          else if (component.GetType() == typeof(LinkComponent))
          {
            educationalUnitDto.LinkComponents
              .Add(_mapper.Map<ReadLinkComponentDto>((LinkComponent)component));
          }
          else if (component.GetType() == typeof(PollComponent))
          {
            educationalUnitDto.PollComponents
              .Add(_mapper.Map<ReadPollComponentDto>((PollComponent)component));
          }
          else if (component.GetType() == typeof(QuizComponent))
          {
            educationalUnitDto.QuizComponents
              .Add(_mapper.Map<ReadQuizComponentDto>((QuizComponent)component));
          }
          else if (component.GetType() == typeof(TextComponent))
          {
            educationalUnitDto.TextComponents
              .Add(_mapper.Map<ReadTextComponentDto>((TextComponent)component));
          }
          else if (component.GetType() == typeof(VideoComponent))
          {
            educationalUnitDto.VideoComponents
              .Add(_mapper.Map<ReadVideoComponentDto>((VideoComponent)component));
          }
          else if (component.GetType() == typeof(VirtualMeetingComponent))
          {
            educationalUnitDto.VirtualMeetingComponents
              .Add(_mapper.Map<ReadVirtualMeetingComponentDto>((VirtualMeetingComponent)component));
          }
        }

        templateDto.EducationalUnits.Add(educationalUnitDto);
      }

      return templateDto;
    }

    private EducationalUnit NewEducationalUnit(CreateEducationalUnitDto newEducationalUnit)
    {
      var educationalUnit = _mapper.Map<CreateEducationalUnitDto, EducationalUnit>(newEducationalUnit);

      foreach (var newComponent in newEducationalUnit.AssigmentComponents)
      {
        var component = _mapper.Map<CreateAssigmentComponentDto, AssignmentComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.DocumentComponents)
      {
        var component = _mapper.Map<CreateDocumentComponentDto, DocumentComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.ForumComponents)
      {
        var component = _mapper.Map<CreateForumComponentDto, ForumComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.LinkComponents)
      {
        var component = _mapper.Map<CreateLinkComponentDto, LinkComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.PollComponents)
      {
        var component = _mapper.Map<CreatePollComponentDto, PollComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.QuizComponents)
      {
        var component = _mapper.Map<CreateQuizComponentDto, QuizComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.TextComponents)
      {
        var component = _mapper.Map<CreateTextComponentDto, TextComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.VideoComponents)
      {
        var component = _mapper.Map<CreateVideoComponentDto, VideoComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.VirtualMeetingComponents)
      {
        var component = _mapper.Map<CreateVirtualMeetingComponentDto, VirtualMeetingComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      return educationalUnit;
    }
  }
}
