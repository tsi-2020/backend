﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Report;
using Udelar.Common.Dtos.User.Request;
using Udelar.Common.Dtos.User.Response;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using Udelar.Services.utils;

namespace Udelar.Services
{
  public class UserService : IUserService
  {
    private readonly IAuthService _authService;
    private readonly IMapper _mapper;
    private readonly IUniversitiesRepository _universitiesRepository;
    private readonly IUserRepository _userRepository;
    private readonly IEmailSender _emailSender;

    public UserService(IMapper mapper, IUserRepository userRepository, IAuthService authService,
      IUniversitiesRepository universitiesRepository, IEmailSender emailSender)
    {
      _emailSender = emailSender;
      _universitiesRepository = universitiesRepository;
      _authService = authService;
      _userRepository = userRepository;
      _mapper = mapper;
    }

    public async Task<ReadUserDto> CreateUser(CreateUserDto createStudentDto, List<Role> userRole, Guid tenantId)
    {
      if (await IsEmailTaken(createStudentDto.Email, tenantId))
      {
        throw new ConflictException("Email ya se encuentra en uso.");
      }

      var university = await _universitiesRepository.FindUniversityById(tenantId);
      var user = await _userRepository.FindByCI(createStudentDto.CI, tenantId);
      if (user != null)
      {
        throw new ConflictException("Ya existe un usuario igual.");
      }

      var userRoles = userRole.Select(role => new UserRole { Role = role }).ToList();

      user = new User
      {
        FirstName = createStudentDto.FirstName,
        LastName = createStudentDto.LastName,
        Email = createStudentDto.Email,
        CI = createStudentDto.CI,
        University = university,
        Roles = userRoles,
        IsActive = false,
        ResetPasswordCode = Guid.NewGuid().ToString(),
      };
      await _userRepository.CreateUser(user);
      _ = _emailSender.SendMail(user.Email, "Alta de usuario",
        $"https://udelar.web.elasticloud.uy/frontoffice/resetPassword/{user.ResetPasswordCode}");
      return _mapper.Map<ReadUserDto>(user);
    }

    public async Task<PaginatedUsersDto> GetUsers(Guid tenantId, FilterUserDto userFilter)
    {
      Tuple<int, List<User>> paginatedUsers;
      if (!string.IsNullOrEmpty(userFilter.RoleFilter))
      {
        paginatedUsers = await _userRepository.GetUsersByRole(tenantId,
          userFilter.Page,
          userFilter.Limit,
          Enum.Parse<Role>(userFilter.RoleFilter),
          userFilter.FullNameFilter);
      }
      else
      {
        paginatedUsers =
          await _userRepository.GetUsers(tenantId, userFilter.Page, userFilter.Limit, userFilter.FullNameFilter);
      }

      var dtoList = _mapper.Map<List<ReadUserDto>>(paginatedUsers.Item2);
      return new PaginatedUsersDto { Count = paginatedUsers.Item1, UserList = dtoList };
    }

    public async Task<ReadUserDto> GetUserById(Guid tenantId, Guid userId)
    {
      var user = await _userRepository.GetUserById(tenantId, userId);

      if (user == default)
      {
        throw new EntityNotFoundException("El usuario no existe.");
      }

      var dto = _mapper.Map<ReadUserDto>(user);
      dto.Roles.RemoveAll(r => true);
      foreach (var userRole in user.Roles)
      {
        dto.Roles.Add(userRole.Role.ToString());
      }

      return dto;
    }

    public async Task UpdateUserData(Guid tenantId, Guid userId, UpdateUserDto updateUserData)
    {
      var user = await _userRepository.GetUserById(tenantId, userId);
      if (user == default)
      {
        throw new EntityNotFoundException("Usuario no encontrado.");
      }

      if (await IsEmailTaken(updateUserData.Email, tenantId, userId))
      {
        throw new ConflictException("Email ya se encuentra en uso.");
      }

      user.FirstName = updateUserData.FirstName;
      user.LastName = updateUserData.LastName;
      user.Email = updateUserData.Email;

      await _userRepository.SaveChanges(user);
    }

    private async Task<bool> IsEmailTaken(string email, Guid universityId, Guid userId = default)
    {
      return await _userRepository.FindByEmail(email, universityId, userId) != null;
    }

    public async Task PatchUserRole(Guid tenantId, Guid userId, PatchUserRoleDto patchUserRoleDto)
    {
      var user = await _userRepository.GetUserById(tenantId, userId);
      if (user == default)
      {
        throw new EntityNotFoundException("Usuario no encontrado.");
      }

      var roleList = patchUserRoleDto.Roles.Select(Enum.Parse<Role>).ToList();

      var userRolesToRemove = new List<UserRole>();
      foreach (var userRole in user.Roles)
      {
        if (roleList.Contains(userRole.Role))
        {
          roleList.Remove(userRole.Role);
        }
        else
        {
          userRolesToRemove.Add(userRole);
        }
      }

      foreach (var role in roleList)
      {
        user.Roles.Add(new UserRole { User = user, Role = role });
      }

      foreach (var userRole in userRolesToRemove)
      {
        user.Roles.Remove(userRole);
      }

      await _userRepository.SaveChanges(user);
    }

    public async Task<ReadUserDto> FindByResetToken(string token)
    {
      var user = await _userRepository.FindByResetToken(token);
      if (user == default)
      {
        throw new EntityNotFoundException("Token not found");
      }

      return _mapper.Map<ReadUserDto>(user);
    }

    public async Task<ReadUserDto> ResetPassword(string token, ResetPasswordDto resetPasswordDto)
    {
      var user = await _userRepository.FindByResetToken(token);
      if (user == default)
      {
        throw new EntityNotFoundException("Token not found");
      }

      user.Password = _authService.HashPassword(resetPasswordDto.Password);
      user.IsActive = true;
      user.ResetPasswordCode = null;
      await _userRepository.SaveChanges(user);
      return _mapper.Map<ReadUserDto>(user);
    }

    public async Task DeleteUser(Guid tenantId, Guid userId)
    {
      var user = await _userRepository.FindUserInUniversityById(userId, tenantId);
      if (user == default)
      {
        throw new EntityNotFoundException("User not found");
      }

      user.DeletedOn = new DateTime();
      await _userRepository.SaveChanges(user);
    }

    public async Task<List<ReadUserEnrollmentsDto>> GetUserEnrollments(Guid userId)
    {
      var user = await _userRepository.FindUserEnrollments(userId);
      if (user == default)
      {
        throw new EntityNotFoundException("Usuario no encontrado");
      }

      var userEnrollmentsDtos = user.Inscriptions.Select(userInscription =>
        _mapper.Map<ReadUserEnrollmentsDto>(userInscription)).ToList();
      return userEnrollmentsDtos;
    }

    public async Task<List<UniversityUsersReportDto>> GetUniversitiesUsersQuantityReport()
    {
      var users = await _userRepository.GetAllWithUniversityAndRoles();

      var count = new Dictionary<Guid, UniversityUsersCount>();

      foreach (var user in users)
      {
        if (!count.TryGetValue(user.University.Id, out var universityCount))
        {
          universityCount = new UniversityUsersCount
          {
            University = user.University,
            ProfessorsQuantity = 0,
            StudentsQuantity = 0
          };
          count.Add(user.University.Id, universityCount);
        }

        foreach (var userRole in user.Roles)
        {
          if (userRole.Role == Role.Professor)
          {
            universityCount.ProfessorsQuantity++;
          }
          else
          {
            universityCount.StudentsQuantity++;
          }
        }
      }


      var report = new List<UniversityUsersReportDto>();

      foreach (var universityUsersCount in count)
      {
        report.Add(new UniversityUsersReportDto
        {
          Id = universityUsersCount.Value.University.Id,
          Name = universityUsersCount.Value.University.Name,
          ProfessorsQuantity = universityUsersCount.Value.ProfessorsQuantity,
          StudentsQuantity = universityUsersCount.Value.StudentsQuantity
        });
      }

      return report;
    }
  }
}
