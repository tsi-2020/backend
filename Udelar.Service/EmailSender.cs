﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace Udelar.Services
{
  public class EmailSender : IEmailSender
  {
    private const string From = "admin@udelar.uy";
    private const string Domain = "mail.viruscontrol.tech";
    private readonly HttpClient _mailgunHttpClient;
    private readonly ILogger<EmailSender> _logger;

    private string html = @"
<html>
	<head>
		<style>
            .v-card {
                border-width: thin;
                display: block;
                max-width: 100%;
                outline: none;
                text-decoration: none;
                transition-property: box-shadow, opacity;
                overflow-wrap: break-word;
                position: relative;
                white-space: normal;
                box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12);
                border-radius: 4px;
                padding: 10px;
            }
            .v-card__title {
                align-items: center;
                display: flex;
                flex-wrap: wrap;
                font-size: 1.25rem;
                font-weight: 500;
                letter-spacing: 0.0125em;
                line-height: 2rem;
                word-break: break-all;
            }
            .v-card__subtitle {
                font-size: 0.875rem;
                font-weight: 400;
                line-height: 1.375rem;
                letter-spacing: 0.0071428571em;
                color: #00000099;
            }
            .v-card__subtitle {
                font-size: 0.875rem;
                font-weight: 400;
                line-height: 1.375rem;
                letter-spacing: 0.0071428571em;
                color: #00000099;
            }
            .v-btn {
                height: 36px;
                min-width: 64px;
                padding: 0 16px;
                text-decoration: none;
                color: #fff;
                padding: 10px;
                border-radius: 4px;
                box-shadow: 0px 3px 1px -2px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12);
            }
        </style>
	</head>
	<body>
		<div class='v-card v-sheet theme--light' style='width: 400px; border-top: 2px solid rgb(0, 188, 212);'>
			<div class='v-card__title'>Buenas,</div>
			<div class='v-card__subtitle'>Para completar el proceso haz click en el siguiente boton:</div>
			<div class='v-card__text'>
				<div style='text-align: center;margin: 40px;'>
					<a href='#UrlPlaceholder' target='_blank' class='v-btn' style='background-color: rgb(0, 188, 212); border-color: rgb(0, 188, 212);'>
						<span class='v-btn__content'>Completar</span>
					</a>
				</div>
				<hr class='my-8' style='border: 1px solid rgb(247, 247, 247);'>
					<p class='mb-0'>O copia este link y pegalo en tu navegador</p>
					<a href='#UrlPlaceholder' style='color: rgb(0, 188, 212);'>#UrlPlaceholder</a>
					<p class='mt-5'>Saludos,</p>
					<p style='font-weight: bold;'>Udelar</p>
				</div>
			</div>
		</body>
	</html>
<html>";

    public EmailSender(HttpClient mailgunHttpClient, ILogger<EmailSender> logger)
    {
      this._mailgunHttpClient = mailgunHttpClient;
      this._logger = logger;
    }


    public async Task SendMail(string to, string subject, string body)
    {
      var html = this.html.Replace("#UrlPlaceholder", body);

      var form = new Dictionary<string, string>
      {
        ["from"] = "admin@udelar.uy",
        ["to"] = to,
        ["subject"] = subject,
        ["html"] = html,
      };

      var response = await _mailgunHttpClient.PostAsync($"v3/{Domain}/messages",
        new FormUrlEncodedContent(form));

      if (!response.IsSuccessStatusCode)
      {
        _logger.LogError(
          "Error when trying to send mail. mailFrom: {mailFrom}, emailTo: {emailTo}, body: {body}, subject: {subject}, response: {@response}",
          From, to, body, subject, response);
      }
    }
  }
}
