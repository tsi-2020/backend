﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Assigment.request;
using Udelar.Common.Dtos.Assigment.response;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class AssignmentService : IAssignmentService
  {
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly ISubjectRepository _subjectRepository;
    private readonly IMapper _mapper;

    public AssignmentService(IAssignmentRepository assignmentRepository, ISubjectRepository subjectRepository, IMapper mapper)
    {
      _assignmentRepository = assignmentRepository;
      _subjectRepository = subjectRepository;
      _mapper = mapper;
    }

    public async Task<ReadAssignmentDto> GetSubjectAssignmentById(Guid tenantId, Guid subjectId, Guid assignmentId,
      Guid requesterUserId)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollments(tenantId, subjectId);

      var enrollment = ValidateUserIsEnrolled(subject, requesterUserId);
      var assignmentComponent = await _assignmentRepository.GetAssignmentComponentByAssignmentId(assignmentId);

      ReadAssignmentDto assignmentDto;
      if (enrollment.GetType() == typeof(ProfessorEnrollment))
      {
        var assignment = await _assignmentRepository.FindAssignmentById(tenantId, subjectId, assignmentId);
        assignmentDto = _mapper.Map<ReadAssignmentDto>(assignment);
      }
      else
      {
        var assignment = await _assignmentRepository.FindAssignmentByIdWithDeliveries(tenantId, subjectId, assignmentId);
        assignmentDto = _mapper.Map<ReadAssignmentDto>(assignment);

        var delivery = assignment.DeliveredAssignments
          .FirstOrDefault(d => d.GetType() == typeof(IndividualDelivery)
                               && ((IndividualDelivery)d).User.Id == requesterUserId);
        if (delivery != null)
        {
          assignmentDto.DeliveredAssigment = _mapper.Map<ReadDeliveredAssigmentDto>(delivery);
        }
      }

      assignmentDto.Title = assignmentComponent.Title;
      assignmentDto.Text = assignmentComponent.Text;
      return assignmentDto;
    }

    public async Task<ReadAssignmentDto> CreateAssignmentDelivery(Guid tenantId, Guid subjectId, Guid assignmentId, Guid requesterUserId, CreateAssignmentDeliveryDto deliveryDto)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollments(tenantId, subjectId);

      var enrollment = ValidateUserIsEnrolled(subject, requesterUserId);

      if (enrollment.GetType() != typeof(StudentEnrollment))
      {
        throw new AuthorizationException("El usuario no es estudiante del curso");
      }

      var assignment = await _assignmentRepository.FindAssignmentByIdWithDeliveries(tenantId, subjectId, assignmentId);

      if (assignment.AvailableUntil < DateTime.Now)
      {
        throw new AuthorizationException("El plazo de entrega ha finalizado.");
      }

      var assignmentDto = _mapper.Map<ReadAssignmentDto>(assignment);

      var delivery = assignment.DeliveredAssignments
        .FirstOrDefault(d => d.GetType() == typeof(IndividualDelivery)
                             && ((IndividualDelivery)d).User.Id == requesterUserId);
      if (delivery == null)
      {
        delivery = new IndividualDelivery { User = enrollment.User };
        assignment.DeliveredAssignments.Add(delivery);
      }

      delivery.FileName = deliveryDto.FileName;
      delivery.FileUrl = deliveryDto.FileUrl;

      await _assignmentRepository.SaveChanges();

      assignmentDto.DeliveredAssigment = _mapper.Map<ReadDeliveredAssigmentDto>(delivery);

      return assignmentDto;
    }

    public async Task<ReadAssignmentDto> UpdateSubjectAssignment(Guid tenantId, Guid subjectId, Guid assignmentId, UpdateAssignmentDto assignmentDto,
      Guid requesterUserId)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollments(tenantId, subjectId);

      var enrollment = ValidateUserIsEnrolled(subject, requesterUserId);

      if (enrollment.GetType() != typeof(ProfessorEnrollment))
      {
        throw new AuthorizationException("El usuario no es docente del curso");
      }

      var assignment = await _assignmentRepository.FindAssignmentById(tenantId, subjectId, assignmentId);

      assignment.AvailableFrom = assignmentDto.AvailableFrom;
      assignment.AvailableUntil = assignmentDto.AvailableUntil;
      assignment.MaxScore = assignmentDto.MaxScore;
      assignment.IsInitialized = true;

      await _assignmentRepository.SaveChanges();

      return _mapper.Map<ReadAssignmentDto>(assignment);

    }

    /// <summary>
    /// Verifica si un usuario esta inscripto a un curso:
    /// - Si esta inscripto devuelve  la inscripcion
    /// - Sino tira excepcion
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="requesterUserId"></param>
    /// <returns></returns>
    private Enrollment ValidateUserIsEnrolled(Subject subject, Guid requesterUserId)
    {
      var enrollment = subject.Enrollments.FirstOrDefault(e => e.User.Id == requesterUserId);

      if (enrollment == default)
      {
        throw new AuthorizationException("El usuario no esta inscripto al curso");
      }

      return enrollment;
    }
  }
}
