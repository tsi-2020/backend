﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Calendar.Request;
using Udelar.Common.Dtos.Calendar.Response;

namespace Udelar.Services
{
  public interface ICalendarService
  {
    Task<ReadEventDto> CreateEvent(Guid tenantId, Guid subjectId, CreateEventDto createEventDto);
    Task<List<ReadEventDto>> FilterEvents(Guid tenantId, Guid subjectId, FilterEventsDto filterEventsDto);
  }
}
