﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Udelar.Common.Dtos.Auth;
using Udelar.Common.Dtos.Auth.Response;
using Udelar.Common.Dtos.Auth.Rquest;
using Udelar.Common.Dtos.User.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using BC = BCrypt.Net.BCrypt;

namespace Udelar.Services
{
  public class AuthService : IAuthService
  {
    private readonly IAdminRepository _adminRepository;
    private readonly IConfiguration _configuration;
    private readonly IUserRepository _userRepository;
    private readonly IEmailSender _emailSender;

    public AuthService(IConfiguration configuration, IAdminRepository adminRepository, IUserRepository userRepository,
      IEmailSender emailSender)
    {
      _configuration = configuration;
      _adminRepository = adminRepository;
      _userRepository = userRepository;
      _emailSender = emailSender;
    }

    public bool VerifyPassword(string plainPassword, string hashedPassword)
    {
      try
      {
        return BC.Verify(plainPassword, hashedPassword);
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }

    public async Task RequestForgotPasswordLink(RequestForgotPasswordDto requestForgotPassword, Guid universityId)
    {
      var user = await _userRepository.FindByCI(requestForgotPassword.CI, universityId);
      if (user != default)
      {
        user.ResetPasswordCode = Guid.NewGuid().ToString();
        await _userRepository.SaveChanges(user);
        _ = _emailSender.SendMail(user.Email, "Recuperacion de contraseña",
          $"https://udelar.web.elasticloud.uy/frontoffice/resetPassword/{user.ResetPasswordCode}");
      }
    }

    public string HashPassword(string password)
    {
      return BC.HashPassword(password);
    }

    public async Task<CredentialsDto> AdminLogin(BackOfficeLoginDto backOfficeLoginDto)
    {
      var administrator = await _adminRepository.FindByEmail(backOfficeLoginDto.Email);

      if (administrator == null)
      {
        throw new AuthenticationException("Credenciales Incorrectas");
      }

      var isValid = VerifyPassword(backOfficeLoginDto.Password, administrator.Password);
      if (!isValid)
      {
        throw new AuthenticationException("Credenciales Incorrectas");
      }

      var createTokenDto = new CreateTokenDto
      {
        Email = administrator.Email,
        FirstName = administrator.FirstName,
        LastName = administrator.LastName,
        Roles = new[] { "UdelarAdmin" },
        Id = administrator.Id.ToString()
      };

      if (administrator is UniversityAdministrator universityAdministrator)
      {
        createTokenDto.Tenant = universityAdministrator.University.Id.ToString();
        createTokenDto.Roles = new[] { "UniversityAdmin" };
      }

      var accessToken = CreateAccessToken(createTokenDto);

      if (administrator.RefreshToken == null || !ValidateToken(administrator.RefreshToken))
      {
        var refreshToken = CreateRefreshToken(createTokenDto);
        administrator.RefreshToken = refreshToken;
        await _adminRepository.SaveChanges(administrator);
      }

      return new CredentialsDto { RefreshToken = administrator.RefreshToken, AccessToken = accessToken };
    }

    public async Task<CredentialsDto> RefreshToken(RefreshTokenDto refreshTokenDto)
    {
      var role = this.GetRoleFromRefreshToken(refreshTokenDto.RefreshToken);
      if (role == "UdelarAdmin" || role == "UniversityAdmin")
      {
        var admin = await _adminRepository.FindByRefreshToken(refreshTokenDto.RefreshToken);
        if (admin != null && ValidateToken(admin.RefreshToken))
        {
          return HandleAdminRefreshToken(admin);
        }
      }
      else
      {
        var user = await _userRepository.FindByRefreshToken(refreshTokenDto.RefreshToken);
        if (user != null && ValidateToken(user.RefreshToken))
        {
          return HandleUserRefreshToken(user);
        }
      }

      throw new AuthenticationException("Credenciales invalidas");
    }

    private CredentialsDto HandleUserRefreshToken(User user)
    {
      var createTokenDto = new CreateTokenDto
      {
        Email = user.Email,
        FirstName = user.FirstName,
        LastName = user.LastName,
        Roles = user.Roles.Select(userRole => Enum.GetName(typeof(Role), userRole.Role)),
        Id = user.Id.ToString(),
        Tenant = user.University.Id.ToString()
      };
      return new CredentialsDto { AccessToken = CreateAccessToken(createTokenDto), RefreshToken = user.RefreshToken };
    }

    private CredentialsDto HandleAdminRefreshToken(Administrator admin)
    {
      var createTokenDto = new CreateTokenDto
      {
        Email = admin.Email,
        FirstName = admin.FirstName,
        LastName = admin.LastName,
        Roles = new[] { "UdelarAdmin" },
        Id = admin.Id.ToString()
      };

      if (admin is UniversityAdministrator)
      {
        createTokenDto.Tenant = (admin as UniversityAdministrator).University.Id.ToString();
        createTokenDto.Roles = new[] { "UniversityAdmin" };
      }

      return new CredentialsDto { AccessToken = CreateAccessToken(createTokenDto), RefreshToken = admin.RefreshToken };
    }

    public async Task<CredentialsDto> UserLogin(FrontOfficeLoginDto frontOfficeLoginDto, Guid universityId)
    {
      var user = await _userRepository.FindByCI(frontOfficeLoginDto.CI, universityId);

      if (user == null)
      {
        throw new AuthenticationException("Credenciales Incorrectas");
      }

      var isValid = VerifyPassword(frontOfficeLoginDto.Password, user.Password);
      if (!isValid)
      {
        throw new AuthenticationException("Credenciales Incorrectas");
      }

      var createTokenDto = new CreateTokenDto
      {
        Email = user.Email,
        FirstName = user.FirstName,
        LastName = user.LastName,
        Roles = user.Roles.Select(userRole => Enum.GetName(typeof(Role), userRole.Role)),
        Id = user.Id.ToString(),
        Tenant = user.University.Id.ToString()
      };

      var accessToken = CreateAccessToken(createTokenDto);

      if (user.RefreshToken == null || !ValidateToken(user.RefreshToken))
      {
        var refreshToken = CreateRefreshToken(createTokenDto);
        user.RefreshToken = refreshToken;
        await _userRepository.SaveChanges(user);
      }

      return new CredentialsDto { RefreshToken = user.RefreshToken, AccessToken = accessToken };
    }

    private string CreateToken(CreateTokenDto createTokenDto, DateTime expirationDate)
    {
      var tokenSecret = _configuration.GetSection("AppSettings:Token").Value;
      var claims = new List<Claim>
      {
        new Claim(ClaimTypes.GivenName, $"{createTokenDto.FirstName} {createTokenDto.LastName}"),
        new Claim(ClaimTypes.Email, createTokenDto.Email),
        new Claim(ClaimTypes.NameIdentifier, createTokenDto.Id),
        new Claim("Tenant", string.IsNullOrEmpty(createTokenDto.Tenant) ? "" : createTokenDto.Tenant)
      };

      claims.AddRange(createTokenDto.Roles.Select(role => new Claim(ClaimTypes.Role, role)));

      var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(tokenSecret));
      var cred = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
      var tokenDescriptor = new SecurityTokenDescriptor
      {
        Subject = new ClaimsIdentity(claims),
        Expires = expirationDate,
        SigningCredentials = cred
      };
      var tokenHandler = new JwtSecurityTokenHandler();
      var token = tokenHandler.CreateToken(tokenDescriptor);
      return tokenHandler.WriteToken(token);
    }

    public bool ValidateToken(string jwtToken)
    {
      var tokenHandler = new JwtSecurityTokenHandler();
      var validationParameters = GetValidationParameters();
      try
      {
        tokenHandler.ValidateToken(jwtToken, validationParameters, out _);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    private TokenValidationParameters GetValidationParameters()
    {
      var tokenSecret = _configuration.GetSection("AppSettings:Token").Value;
      return new TokenValidationParameters
      {
        ValidateAudience = false,
        ValidateIssuer = false,
        IssuerSigningKey =
          new SymmetricSecurityKey(
            Encoding.UTF8.GetBytes(tokenSecret))
      };
    }

    private string GetRoleFromRefreshToken(string token)
    {
      var handler = new JwtSecurityTokenHandler();

      if (!handler.CanReadToken(token))
      {
        throw new ValidationException("Invalid JWT");
      }

      var decodedToken = handler.ReadJwtToken(token);
      return decodedToken.Claims.First(claim => claim.Type == "role").Value;
    }

    private string CreateAccessToken(CreateTokenDto createTokenDto)
    {
      return CreateToken(createTokenDto, DateTime.Now.AddHours(8));
    }

    private string CreateRefreshToken(CreateTokenDto createTokenDto)
    {
      return CreateToken(createTokenDto, DateTime.Now.AddDays(7));
    }
  }
}
