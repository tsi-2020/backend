﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Enrollment.request;
using Udelar.Common.Dtos.Enrollment.response;

namespace Udelar.Services
{
  public interface IEnrollmentService
  {
    Task EnrollStudent(Guid tenantId, Guid subjectId, Guid enrollmentDtoUserId,
      Guid responsibleUserId);
    Task<ReadEnrollmentListDto> GetSubjectEnrollments(Guid tenantId, Guid subjectId, Guid userId);
    Task UpdateProfessorRole(Guid tenantId, Guid subjectId, Guid enrollmentId, bool isResponsible,
      Guid responsibleUserId);
    Task DeleteEnrollment(Guid tenantId, Guid subjectId, Guid enrollmentId, Guid responsibleUserId);
    Task UpdateProfessorEnrollments(Guid tenantId, Guid subjectId, List<CreateProfessorEnrollmentDto> enrollmentDtoList, Guid responsibleUserId);
    Task AutoEnrollStudent(Guid tenantId, Guid subjectId, Guid userToEnroll);
  }
}
