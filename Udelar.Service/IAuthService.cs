﻿using System;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Auth.Response;
using Udelar.Common.Dtos.Auth.Rquest;
using Udelar.Common.Dtos.User.Request;

namespace Udelar.Services
{
  public interface IAuthService
  {
    Task<CredentialsDto> AdminLogin(BackOfficeLoginDto backOfficeLoginDto);
    Task<CredentialsDto> RefreshToken(RefreshTokenDto refreshTokenDto);
    Task<CredentialsDto> UserLogin(FrontOfficeLoginDto frontOfficeLoginDto, Guid universityId);
    string HashPassword(string administratorPassword);
    bool VerifyPassword(string passwordDtoOldPassword, string adminPassword);
    Task RequestForgotPasswordLink(RequestForgotPasswordDto requestForgotPassword, Guid universityId);
  }
}
