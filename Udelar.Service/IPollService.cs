﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Poll.Request;
using Udelar.Common.Dtos.Poll.Response;

namespace Udelar.Services
{
  public interface IPollService
  {
    public Task CreateUdelarPoll(CreatePollDto createPollDto);
    Task<List<ReadPollDto>> ReadUdelarPolls();
    public Task CreateUniversityPoll(CreatePollDto createPollDto, Guid universityId);
    Task<List<ReadPollDto>> ReadUniversityPolls(Guid id);
    Task ReplySubjectPoll(Guid universityId, Guid subjectId, Guid pollId, Guid userId, ReplyPollDto replyPollDto);
    Task CreateSubjectPoll(CreatePollDto createPollDto, Guid subjectId, Guid universityId);
    Task PublishPollIntoUniversity(Guid universityId, Guid pollId);
    Task PublishPollIntoSubjectAsProfessor(Guid universityId, Guid subjectId, Guid pollId);
    Task PublishPollIntoSubjectAsUniversityAdmin(Guid universityId, Guid subjectId, Guid pollId);
    Task<List<ReadPollDto>> ReadSubjectPolls(Guid subjectId);
    Task ReplyUniversityPoll(Guid universityId, Guid pollId, Guid userId, ReplyPollDto replyPollDto);
    Task<List<ReadPublishPollDto>> ListPublishedSubjectPolls(Guid subjectId, Guid userId);
    Task<List<ReadPublishPollDto>> ListPublishedUniversityPolls(Guid universityId, Guid userId);
    Task<ReadPublishPollDto> GetPublishedPollById(Guid pollId);
    Task<ReadPaginatedPollAnswerDto> GetPollAnswers(Guid pollId, int page, int limit);
  }
}
