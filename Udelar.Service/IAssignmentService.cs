﻿using System;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Assigment.request;
using Udelar.Common.Dtos.Assigment.response;

namespace Udelar.Services
{
  public interface IAssignmentService
  {
    Task<ReadAssignmentDto> GetSubjectAssignmentById(Guid tenantId, Guid subjectId, Guid assignmentId,
      Guid requesterUserId);

    Task<ReadAssignmentDto> CreateAssignmentDelivery(Guid tenantId, Guid subjectId, Guid assignmentId, Guid requesterUserId,
      CreateAssignmentDeliveryDto deliveryDto);

    Task<ReadAssignmentDto> UpdateSubjectAssignment(Guid tenantId, Guid subjectId, Guid assignmentId, UpdateAssignmentDto assignmentDto, Guid requesterUserId);
  }
}
