﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Report;
using Udelar.Common.Dtos.University.Request;
using Udelar.Common.Dtos.University.Response;

namespace Udelar.Services
{
  public interface IUniversitiesService
  {
    Task<ReadUniversityDto> CreateUniversity(CreateUniversityDto createUniversity);
    Task<List<ReadUniversityDto>> GetUniversities();
    Task<ReadUniversityDto> EditUniversity(UpdateUniversityDto updateUniversity);
    Task DeleteUniversity(Guid id);
    Task<ReadUniversityDto> GetUniversityById(Guid id);
    Task<List<ReadUniversityDto>> GetUniversityByPath(string path);
  }
}
