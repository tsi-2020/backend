﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Udelar.Common.Dtos.Bedelias.Request;
using Udelar.Common.Dtos.Bedelias.Response;

namespace Udelar.Services
{
  public class BedeliasService : IBedeliasService
  {
    private readonly IConfiguration _configuration;

    public BedeliasService(IConfiguration configuration)
    {
      _configuration = configuration;
    }

    public async Task<BedeliasResponseDto> ValidateInscription(ValidateInscriptionDto validateInscriptionDto)
    {
      try
      {
        var json = JsonConvert.SerializeObject(validateInscriptionDto);
        var data = new StringContent(json, Encoding.UTF8, "application/json");
        var url = _configuration["AppSettings:BedeliasUri"];
        Console.WriteLine(url);
        using var client = new HttpClient();
        var response = await client.PostAsync(url, data);
        if (!response.IsSuccessStatusCode)
        {
          return new BedeliasResponseDto { Approved = false };
        }

        var result = response.Content.ReadAsStringAsync().Result;
        var bedeliasResponseDto = JsonConvert.DeserializeObject<BedeliasResponseDto>(result);
        return bedeliasResponseDto;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return new BedeliasResponseDto { Approved = false };
      }
    }
  }
}
