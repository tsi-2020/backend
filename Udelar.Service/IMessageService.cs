﻿using System;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Message.Request;
using Udelar.Common.Dtos.Message.Response;

namespace Udelar.Services
{
  public interface IMessageService
  {
    Task<ReadMessageDto> CreateMessage(CreateMessageDto createMessageDto, string threadId, Guid userId, string userFullName, Guid tenantId);
    Task EditMessage(EditMessageDto editMessageDto, string messageId, Guid tenantId, Guid userId);
    Task<PaginatedMessageDto> ListMessages(Guid tenantId, string threadId, FilterMessageDto filterMessageDto);

  }
}
