﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.UniversityAdministrator.Reponse;
using Udelar.Common.Dtos.UniversityAdministrator.Request;

namespace Udelar.Services
{
  public interface IUniversityAdminService
  {
    Task<ReadUniversityAdminDto> CreateAdmin(CreateUniversityAdminDto createUniversityAdminDto, Guid universityId);
    Task<ReadUniversityAdminDto> GetAdminById(Guid userId, Guid tenantId);
    Task<List<ReadUniversityAdminDto>> ListAdmins(Guid tenantId);
    Task ChangePassword(UpdateUniversityAdminPasswordDto passwordDto, Guid userId);
    Task DeleteUniversityAdmin(Guid tenantId, Guid userId);

    Task UpdateUniversityAdminPersonalData(Guid tenantId, Guid userId,
      ChangePersonalDataUniversityAdminDto adminData);
  }
}
