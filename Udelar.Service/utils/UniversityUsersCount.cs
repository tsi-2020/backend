﻿using Udelar.Model;

namespace Udelar.Services.utils
{
  public class UniversityUsersCount
  {
    public University University { get; set; }

    public int ProfessorsQuantity { get; set; }

    public int StudentsQuantity { get; set; }
  }
}
