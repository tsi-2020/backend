﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Component.Request;
using Udelar.Common.Dtos.Component.Response;
using Udelar.Common.Dtos.Report;
using Udelar.Common.Dtos.Subject.Request;
using Udelar.Common.Dtos.Subject.Response;

namespace Udelar.Services
{
  public interface ISubjectService
  {
    Task<ReadSubjectLiteDto> CreateSubject(Guid universityId, CreateSubjectDto createSubjectDto);
    Task<List<ReadSubjectLiteDto>> GetAllSubjects(string subjectName, Guid tenantId);
    Task<ReadSubjectDto> GetSubjectById(Guid tenantId, Guid subjectId);
    Task RemoveSubject(Guid tenantId, Guid id);
    Task<ReadSubjectLiteDto> UpdateSubject(Guid tenantId, Guid subjectId, UpdateSubjectDto updateSubjectDto);
    Task<ReadEducationalUnitDto> AddNewEducationalUnit(Guid tenantId, Guid subjectId, CreateEducationalUnitDto unitDto, Guid responsibleUserId);
    Task RemoveEducationalUnit(Guid tenantId, Guid subjectId, Guid unitId, Guid responsibleUserId);
    Task<ReadEducationalUnitDto> UpdateEducationalUnit(Guid tenantId, Guid subjectId, Guid unitId, UpdateEducationalUnitDto unitDto,
      Guid responsibleUserId);
    Task<List<SubjectEnrollmentsReportDto>> GetSubjectEnrollmentsReportFilterByUniversityId(Guid tenantId);
  }
}
