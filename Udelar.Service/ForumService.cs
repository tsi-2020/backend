﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Forum.Request;
using Udelar.Common.Dtos.Forum.Response;
using Udelar.Common.Dtos.Thread.Response;
using Udelar.Common.Exceptions;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class ForumService : IForumService
  {
    private readonly IForumRepository _forumRepository;
    private readonly IMapper _mapper;
    private readonly ISubjectRepository _subjectRepository;

    public ForumService(ISubjectRepository subjectRepository, IForumRepository forumRepository, IMapper mapper)
    {
      _subjectRepository = subjectRepository;
      _forumRepository = forumRepository;
      _mapper = mapper;
    }

    public async Task CreateForum(CreateForumDto createForumDto, Guid tenantId)
    {
      var subject = await _subjectRepository.FindByIdAndUniversity(createForumDto.SubjectId, tenantId);
      if (subject == null)
      {
        throw new EntityNotFoundException("Asignatura no encontrada.");
      }

      await _forumRepository.CreateForum(createForumDto, tenantId);
    }

    public async Task EditForum(EditForumDto editForumDto, Guid tenantId, string forumId)
    {
      var persistedForum = await _forumRepository.GetById(forumId, tenantId);

      if (persistedForum == null)
      {
        throw new EntityNotFoundException("Foro no encontrado.");
      }

      persistedForum.Name = editForumDto.Name;

      await _forumRepository.Save(persistedForum);
    }

    public async Task DeleteForum(Guid tenantId, string forumId)
    {
      var persistedForum = await _forumRepository.GetById(forumId, tenantId);

      if (persistedForum == null)
      {
        throw new EntityNotFoundException("Foro no encontrado.");
      }

      persistedForum.DeletedOn = DateTime.Now;

      await _forumRepository.Save(persistedForum);
    }

    public async Task<PaginatedForumDto> ListForum(Guid tenantId, FilterForumDto filterForum)
    {

      var paginatedForum = await _forumRepository.List(tenantId, filterForum.Page, filterForum.Limit);

      var dtoList = paginatedForum.Item2.Select(forum => _mapper.Map<ReadForumDto>(forum)).ToList();

      return new PaginatedForumDto { Count = paginatedForum.Item1, ForumList = dtoList };

    }

    public async Task<ViewForumDto> ViewForum(Guid tenantId, string forumId)
    {

      var forums = await _forumRepository.GetById(forumId, tenantId);

      if (forums == null)
      {
        throw new EntityNotFoundException("Foro no encontrado.");
      }

      var threads = await _forumRepository.GetForumThreads(forums);

      var forumDto = _mapper.Map<ViewForumDto>(forums);

      var threadDto = _mapper.Map<List<ReadThreadDto>>(threads);

      forumDto.Threads = threadDto;

      return forumDto;

    }

  }
}
