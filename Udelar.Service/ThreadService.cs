﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Message.Response;
using Udelar.Common.Dtos.Thread.Request;
using Udelar.Common.Dtos.Thread.Response;
using Udelar.Common.Exceptions;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class ThreadService : IThreadService
  {
    private readonly IForumRepository _forumRepository;
    private readonly IMapper _mapper;
    private readonly IThreadRepository _threadRepository;

    public ThreadService(IThreadRepository threadRepository, IForumRepository forumRepository, IMapper mapper)
    {
      _threadRepository = threadRepository;
      _forumRepository = forumRepository;
      _mapper = mapper;
    }

    public async Task<ReadThreadDto> CreateThread(CreateThreadDto createThreadDto, string forumId, Guid tenantId)
    {
      var forum = await _forumRepository.GetById(forumId, tenantId);

      if (forum == null)
      {
        throw new EntityNotFoundException("Foro no encontrado.");
      }

      var thread = await _threadRepository.Create(createThreadDto, forum, tenantId);

      return new ReadThreadDto { Id = thread.ID, Title = thread.Title };
    }

    public async Task<PaginatedThreadDto> ListThreads(Guid tenantId, string forumId, FilterThreadDto filterThread)
    {
      var forum = await _forumRepository.GetById(forumId, tenantId);

      if (forum == null)
      {
        throw new EntityNotFoundException("Foro no encontrado.");
      }

      var paginatedThread = await _threadRepository.List(tenantId, forum, filterThread.Page, filterThread.Limit);

      var dtoList = paginatedThread.Item2.Select(thread => _mapper.Map<ReadThreadDto>(thread)).ToList();

      return new PaginatedThreadDto { Count = paginatedThread.Item1, ThreadList = dtoList };
    }

    public async Task EditThread(EditThreadDto editThreadDto, string threadId, Guid tenantId)
    {
      var persistedThread = await _threadRepository.GetById(threadId, tenantId);

      if (persistedThread == null)
      {
        throw new EntityNotFoundException("Hilo no encontrado.");
      }

      persistedThread.Title = editThreadDto.Title;

      await _threadRepository.Save(persistedThread);
    }

    public async Task DeleteThread(string threadId, Guid tenantId)
    {
      var persistedThread = await _threadRepository.GetById(threadId, tenantId);

      if (persistedThread == null)
      {
        throw new EntityNotFoundException("Hilo no encontrado.");
      }

      persistedThread.DeletedOn = DateTime.Now;

      await _threadRepository.Save(persistedThread);
    }

    public async Task<PaginatedViewThreadDto> ViewThread(Guid tenantId, string threadId, FilterThreadDto filterThread)
    {
      var persistedThread = await _threadRepository.GetById(threadId, tenantId);

      if (persistedThread == null)
      {
        throw new EntityNotFoundException("Hilo no encontrado.");
      }

      var paginatedThreadMessages =
        await _threadRepository.GetThreadMessages(persistedThread, filterThread.Page, filterThread.Limit);

      var listThreadMessageDto = _mapper.Map<List<ReadMessageDto>>(paginatedThreadMessages.Item2);

      return new PaginatedViewThreadDto { Count = paginatedThreadMessages.Item1, MessageList = listThreadMessageDto };
    }
  }
}
