﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Calendar.Request;
using Udelar.Common.Dtos.Component.Request;
using Udelar.Common.Dtos.Component.Response;
using Udelar.Common.Dtos.Forum.Request;
using Udelar.Common.Dtos.Notice.Request;
using Udelar.Common.Dtos.Report;
using Udelar.Common.Dtos.Subject.Request;
using Udelar.Common.Dtos.Subject.Response;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Model.Component;
using Udelar.Model.Poll;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class SubjectService : ISubjectService
  {
    private readonly IMapper _mapper;
    private readonly ISubjectRepository _subjectRepository;
    private readonly ISubjectTemplateRepository _templateRepository;
    private readonly IUniversitiesRepository _universitiesRepository;
    private readonly IUserRepository _userRepository;
    private readonly IForumRepository _forumRepository;
    private readonly ICalendarService _calendarService;

    public SubjectService(IUniversitiesRepository universitiesRepository, ISubjectTemplateRepository templateRepository,
      ISubjectRepository subjectRepository, IUserRepository userRepository, IForumRepository forumRepository,
      ICalendarService iCalendarService,
      IMapper mapper)
    {
      _universitiesRepository = universitiesRepository;
      _templateRepository = templateRepository;
      _subjectRepository = subjectRepository;
      _userRepository = userRepository;
      _forumRepository = forumRepository;
      _calendarService = iCalendarService;
      _mapper = mapper;
    }

    public async Task<ReadSubjectLiteDto> CreateSubject(Guid universityId, CreateSubjectDto createSubjectDto)
    {
      var university = await _universitiesRepository.FindUniversityById(universityId);

      var subject = new Subject
      {
        Name = createSubjectDto.Name,
        University = university,
        ValidateWithBedelia = createSubjectDto.ValidateWithBedelia,
        IsRemote = createSubjectDto.IsRemote
      };


      Enrollment newEnrollment = new ProfessorEnrollment
      {
        Subject = subject,
        User = await _userRepository.GetUserById(universityId, createSubjectDto.ResponsibleTeacherId),
        IsResponsible = true
      };

      subject.Enrollments.Add(newEnrollment);

      if (createSubjectDto.SubjectTemplateId != null)
      {
        var template =
          await _templateRepository.GetSubjectTemplateById(universityId, (Guid)createSubjectDto.SubjectTemplateId);
        await ApplyTemplate(subject, template);
      }

      await _subjectRepository.CreateSubject(subject);
      return _mapper.Map<ReadSubjectLiteDto>(subject);
    }

    public async Task<List<ReadSubjectLiteDto>> GetAllSubjects(string subjectName, Guid tenantId)
    {
      await _universitiesRepository.FindUniversityById(tenantId);
      var subjects = await _subjectRepository.FindAllSubjects(subjectName, tenantId);
      return _mapper.Map<List<ReadSubjectLiteDto>>(subjects);
    }

    public async Task<ReadSubjectDto> GetSubjectById(Guid tenantId, Guid subjectId)
    {
      var subject = await _subjectRepository.FindSubjectById(tenantId, subjectId);
      var dto = new ReadSubjectDto
      {
        Id = subject.Id.ToString(),
        Name = subject.Name,
        ValidateWithBedelia = subject.ValidateWithBedelia,
        IsRemote = subject.IsRemote
      };
      foreach (var educationalUnit in subject.Components)
      {
        dto.Components.Add(MapEducationalUnitToDto(educationalUnit));
      }

      return dto;
    }

    private ReadEducationalUnitDto MapEducationalUnitToDto(EducationalUnit educationalUnit)
    {
      var educationalUnitDto = _mapper.Map<ReadEducationalUnitDto>(educationalUnit);
      foreach (var component in educationalUnit.Components)
      {
        if (component.GetType() == typeof(AssignmentComponent))
        {
          educationalUnitDto.AssigmentComponents
            .Add(_mapper.Map<ReadAssigmentComponentDto>((AssignmentComponent)component));
        }
        else if (component.GetType() == typeof(DocumentComponent))
        {
          educationalUnitDto.DocumentComponents
            .Add(_mapper.Map<ReadDocumentComponentDto>((DocumentComponent)component));
        }
        else if (component.GetType() == typeof(ForumComponent))
        {
          educationalUnitDto.ForumComponents
            .Add(_mapper.Map<ReadForumComponentDto>((ForumComponent)component));
        }
        else if (component.GetType() == typeof(LinkComponent))
        {
          educationalUnitDto.LinkComponents
            .Add(_mapper.Map<ReadLinkComponentDto>((LinkComponent)component));
        }
        else if (component.GetType() == typeof(PollComponent))
        {
          educationalUnitDto.PollComponents
            .Add(_mapper.Map<ReadPollComponentDto>((PollComponent)component));
        }
        else if (component.GetType() == typeof(QuizComponent))
        {
          educationalUnitDto.QuizComponents
            .Add(_mapper.Map<ReadQuizComponentDto>((QuizComponent)component));
        }
        else if (component.GetType() == typeof(TextComponent))
        {
          educationalUnitDto.TextComponents
            .Add(_mapper.Map<ReadTextComponentDto>((TextComponent)component));
        }
        else if (component.GetType() == typeof(VideoComponent))
        {
          educationalUnitDto.VideoComponents
            .Add(_mapper.Map<ReadVideoComponentDto>((VideoComponent)component));
        }
        else if (component.GetType() == typeof(VirtualMeetingComponent))
        {
          educationalUnitDto.VirtualMeetingComponents
            .Add(_mapper.Map<ReadVirtualMeetingComponentDto>((VirtualMeetingComponent)component));
        }
      }

      return educationalUnitDto;
    }

    public async Task RemoveSubject(Guid tenantId, Guid subjectId)
    {
      var subject = await _subjectRepository.FindSubjectById(tenantId, subjectId);
      await _subjectRepository.RemoveSubject(subject);
    }

    public async Task<ReadSubjectLiteDto> UpdateSubject(Guid tenantId, Guid subjectId,
      UpdateSubjectDto updateSubjectDto)
    {
      var subject = await _subjectRepository.FindSubjectById(tenantId, subjectId);
      subject.Name = updateSubjectDto.Name;
      subject.IsRemote = updateSubjectDto.IsRemote;
      subject.ValidateWithBedelia = updateSubjectDto.ValidateWithBedelia;

      await _subjectRepository.SaveChanges(subject);

      return _mapper.Map<ReadSubjectLiteDto>(subject);
    }

    public async Task<ReadEducationalUnitDto> AddNewEducationalUnit(Guid tenantId, Guid subjectId,
      CreateEducationalUnitDto unitDto, Guid professorUserId)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollments(tenantId, subjectId);

      bool userIsProfessor = subject.Enrollments.Any(enrollment =>
        enrollment.User.Id == professorUserId && enrollment.GetType() == typeof(ProfessorEnrollment));

      if (!userIsProfessor)
      {
        throw new AuthorizationException("El usuario no es docente del curso");
      }

      var unit = await NewEducationalUnit(subject, unitDto);

      subject.Components.Add(unit);

      await _subjectRepository.SaveChanges(subject);

      return MapEducationalUnitToDto(unit);
    }

    public async Task RemoveEducationalUnit(Guid tenantId, Guid subjectId, Guid unitId, Guid professorUserId)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollmentsAndComponents(tenantId, subjectId);
      bool userIsProfessor = false;
      foreach (var enrollment in subject.Enrollments)
      {
        if (enrollment.User.Id == professorUserId && enrollment.GetType() == typeof(ProfessorEnrollment))
        {
          userIsProfessor = true;
          break;
        }
      }

      if (!userIsProfessor)
      {
        throw new AuthorizationException("El usuario no es docente del curso");
      }

      var unit = subject.Components.FirstOrDefault(c => c.Id == unitId);

      if (unit == default)
      {
        throw new EntityNotFoundException("No se encontro la unidad");
      }

      subject.Components.Remove(unit);

      await _subjectRepository.SaveChanges(subject);
    }

    public async Task<ReadEducationalUnitDto> UpdateEducationalUnit(Guid tenantId, Guid subjectId, Guid unitId,
      UpdateEducationalUnitDto unitDto, Guid professorUserId)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollmentsAndComponents(tenantId, subjectId);
      bool userIsProfessor = false;
      foreach (var enrollment in subject.Enrollments)
      {
        if (enrollment.User.Id == professorUserId && enrollment.GetType() == typeof(ProfessorEnrollment))
        {
          userIsProfessor = true;
          break;
        }
      }

      if (!userIsProfessor)
      {
        throw new AuthorizationException("El usuario no es docente del curso");
      }

      var unit = subject.Components.FirstOrDefault(c => c.Id == unitId);

      if (unit == default)
      {
        throw new EntityNotFoundException("No se encontro la unidad");
      }

      /* Modificacion de cabezales*/

      unit.Position = unitDto.Position;
      unit.Text = unitDto.Text;
      unit.Title = unitDto.Title;

      /* Modificacion de  componentes */

      var componentsToRemove = new List<Component>();

      foreach (var unitComponent in unit.Components)
      {
        if (unitComponent.GetType() == typeof(AssignmentComponent))
        {
          var componentDto = unitDto.AssigmentComponents.FirstOrDefault(c => c.Id == unitComponent.Id);

          if (componentDto == default)
          {
            componentsToRemove.Add(unitComponent);
          }
          else
          {
            var castedComponent = (AssignmentComponent)unitComponent;

            castedComponent.Text = componentDto.Text;
            castedComponent.Title = componentDto.Title;
            castedComponent.Position = componentDto.Position;

            var assignment = subject.Assignments.FirstOrDefault(a => a.Id == componentDto.AssignmentId);
            if (assignment == default)
            {
              throw new ValidationException("El trabajo entregable no existe");
            }

            castedComponent.Assignment = assignment;

            unitDto.AssigmentComponents.Remove(componentDto);
          }
        }
        else if (unitComponent.GetType() == typeof(DocumentComponent))
        {
          var componentDto = unitDto.DocumentComponents.FirstOrDefault(c => c.Id == unitComponent.Id);

          if (componentDto == default)
          {
            componentsToRemove.Add(unitComponent);
          }
          else
          {
            var castedComponent = (DocumentComponent)unitComponent;

            castedComponent.Url = componentDto.Url;
            castedComponent.Title = componentDto.Title;
            castedComponent.Position = componentDto.Position;

            unitDto.DocumentComponents.Remove(componentDto);
          }
        }
        else if (unitComponent.GetType() == typeof(ForumComponent))
        {
          var componentDto = unitDto.ForumComponents.FirstOrDefault(c => c.Id == unitComponent.Id);

          if (componentDto == default)
          {
            componentsToRemove.Add(unitComponent);
          }
          else
          {
            var castedComponent = (ForumComponent)unitComponent;

            castedComponent.Title = componentDto.Title;
            castedComponent.Position = componentDto.Position;

            var forum = await _forumRepository.GetById(componentDto.ForumId, subject.University.Id);
            if (forum == null || forum.SubjectId != subject.Id)
            {
              throw new ValidationException("El foro no existe");
            }

            castedComponent.ForumId = componentDto.ForumId;

            unitDto.ForumComponents.Remove(componentDto);
          }
        }
        else if (unitComponent.GetType() == typeof(LinkComponent))
        {
          var componentDto = unitDto.LinkComponents.FirstOrDefault(c => c.Id == unitComponent.Id);

          if (componentDto == default)
          {
            componentsToRemove.Add(unitComponent);
          }
          else
          {
            var castedComponent = (LinkComponent)unitComponent;

            castedComponent.Url = componentDto.Url;
            castedComponent.Title = componentDto.Title;
            castedComponent.Position = componentDto.Position;

            unitDto.LinkComponents.Remove(componentDto);
          }
        }
        else if (unitComponent.GetType() == typeof(PollComponent))
        {
          var componentDto = unitDto.PollComponents.FirstOrDefault(c => c.Id == unitComponent.Id);

          if (componentDto == default)
          {
            componentsToRemove.Add(unitComponent);
          }
          else
          {
            var castedComponent = (PollComponent)unitComponent;

            castedComponent.Title = componentDto.Title;
            castedComponent.Position = componentDto.Position;


            // TODO
            // var subjectPoll = subject.SubjectPolls.FirstOrDefault(p => p.Poll.Id == componentDto.PollId);
            // if (subjectPoll == default)
            // {
            // throw new ValidationException("La encuesta no existe");
            // }

            // castedComponent.Poll = subjectPoll.Poll;

            unitDto.PollComponents.Remove(componentDto);
          }
        }
        else if (unitComponent.GetType() == typeof(QuizComponent))
        {
          var componentDto = unitDto.QuizComponents.FirstOrDefault(c => c.Id == unitComponent.Id);

          if (componentDto == default)
          {
            componentsToRemove.Add(unitComponent);
          }
          else
          {
            var castedComponent = (QuizComponent)unitComponent;

            castedComponent.Title = componentDto.Title;
            castedComponent.Position = componentDto.Position;


            var quiz = subject.Quizzes.FirstOrDefault(q => q.Id == componentDto.QuizId);
            if (quiz == default)
            {
              throw new ValidationException("La evaluacion no existe");
            }

            castedComponent.Quiz = quiz;

            unitDto.QuizComponents.Remove(componentDto);
          }
        }
        else if (unitComponent.GetType() == typeof(TextComponent))
        {
          var componentDto = unitDto.TextComponents.FirstOrDefault(c => c.Id == unitComponent.Id);

          if (componentDto == default)
          {
            componentsToRemove.Add(unitComponent);
          }
          else
          {
            var castedComponent = (TextComponent)unitComponent;

            castedComponent.Text = componentDto.Text;
            castedComponent.Position = componentDto.Position;

            unitDto.TextComponents.Remove(componentDto);
          }
        }
        else if (unitComponent.GetType() == typeof(VideoComponent))
        {
          var componentDto = unitDto.VideoComponents.FirstOrDefault(c => c.Id == unitComponent.Id);

          if (componentDto == default)
          {
            componentsToRemove.Add(unitComponent);
          }
          else
          {
            var castedComponent = (VideoComponent)unitComponent;

            castedComponent.Url = componentDto.Url;
            castedComponent.Title = componentDto.Title;
            castedComponent.Position = componentDto.Position;

            unitDto.VideoComponents.Remove(componentDto);
          }
        }
        else if (unitComponent.GetType() == typeof(VirtualMeetingComponent))
        {
          var componentDto = unitDto.VirtualMeetingComponents.FirstOrDefault(c => c.Id == unitComponent.Id);

          if (componentDto == default)
          {
            componentsToRemove.Add(unitComponent);
          }
          else
          {
            var castedComponent = (VirtualMeetingComponent)unitComponent;

            castedComponent.Url = componentDto.Url;
            castedComponent.Title = componentDto.Title;
            castedComponent.Position = componentDto.Position;

            unitDto.VirtualMeetingComponents.Remove(componentDto);
          }
        }
      }

      /* Elimino  los componentes que ya no existen el dto */

      foreach (var component in componentsToRemove)
      {
        unit.Components.Remove(component);
      }

      /*  Agrego componentes  nuevos a la unidad */

      foreach (var newComponent in unitDto.AssigmentComponents)
      {
        var component = _mapper.Map<AssignmentComponent>(newComponent);

        Assignment assignment;
        if (newComponent.AssignmentId != null)
        {
          assignment = subject.Assignments.FirstOrDefault(a => a.Id == newComponent.AssignmentId);
          if (assignment == default)
          {
            throw new ValidationException("El trabajo entregable no existe");
          }
        }
        else
        {
          assignment = new Assignment();
          assignment.Subject = subject;
          assignment.IsInitialized = false;
        }

        component.Assignment = assignment;
        unit.Components.Add(component);
      }

      foreach (var newComponent in unitDto.DocumentComponents)
      {
        var component = _mapper.Map<DocumentComponent>(newComponent);
        unit.Components.Add(component);
      }

      foreach (var newComponent in unitDto.ForumComponents)
      {
        var component = _mapper.Map<ForumComponent>(newComponent);
        if (newComponent.ForumId != null && !newComponent.ForumId.Equals(""))
        {
          var forum = await _forumRepository.GetById(newComponent.ForumId, subject.University.Id);
          if (forum == null || forum.SubjectId != subject.Id)
          {
            throw new ValidationException("El foro no existe");
          }
        }
        else
        {
          var forum = await _forumRepository.CreateForum(
            new CreateForumDto { Name = newComponent.Title, SubjectId = subjectId }, tenantId);
          component.ForumId = forum.ID;
        }

        unit.Components.Add(component);
      }

      foreach (var newComponent in unitDto.LinkComponents)
      {
        var component = _mapper.Map<LinkComponent>(newComponent);
        unit.Components.Add(component);
      }

      foreach (var newComponent in unitDto.PollComponents)
      {
        var component = _mapper.Map<PollComponent>(newComponent);

        // TODO
        // var subjectPoll = subject.SubjectPolls.FirstOrDefault(p => p.Poll.Id == newComponent.PollId);
        // if (subjectPoll == default)
        // {
        // throw new ValidationException("La encuesta no existe");
        // }

        // component.Poll = subjectPoll.Poll;

        unit.Components.Add(component);
      }

      foreach (var newComponent in unitDto.QuizComponents)
      {
        var component = _mapper.Map<QuizComponent>(newComponent);

        var quiz = subject.Quizzes.FirstOrDefault(q => q.Id == newComponent.QuizId);
        if (quiz == default)
        {
          throw new ValidationException("La evaluacion no existe");
        }

        component.Quiz = quiz;

        unit.Components.Add(component);
      }

      foreach (var newComponent in unitDto.TextComponents)
      {
        var component = _mapper.Map<TextComponent>(newComponent);
        unit.Components.Add(component);
      }

      foreach (var newComponent in unitDto.VideoComponents)
      {
        var component = _mapper.Map<VideoComponent>(newComponent);
        unit.Components.Add(component);
      }

      foreach (var newComponent in unitDto.VirtualMeetingComponents)
      {
        var component = _mapper.Map<VirtualMeetingComponent>(newComponent);
        unit.Components.Add(component);
      }

      await _subjectRepository.SaveChanges(subject);

      return MapEducationalUnitToDto(unit);
    }

    public async Task<List<SubjectEnrollmentsReportDto>> GetSubjectEnrollmentsReportFilterByUniversityId(Guid tenantId)
    {
      List<Subject> subjects;
      if (tenantId == Guid.Empty)
      {
        subjects = await _subjectRepository.FindAllSubjectsWithEnrollmentsAndUniversity();
      }
      else
      {
        subjects = await _subjectRepository.FindAllSubjectsWithEnrollmentsByUniversity(tenantId);
      }

      var report = new List<SubjectEnrollmentsReportDto>();

      foreach (var subject in subjects)
      {
        var row = new SubjectEnrollmentsReportDto
        {
          Id = subject.Id,
          Name = subject.Name,
          UniversityName = subject.University.Name,
          StudentsQuantity = 0,
          ProfessorsQuantity = 0
        };
        report.Add(row);
        foreach (var subjectEnrollment in subject.Enrollments)
        {
          if (subjectEnrollment.GetType() == typeof(StudentEnrollment))
          {
            row.StudentsQuantity++;
          }
          else
          {
            row.ProfessorsQuantity++;
          }
        }
      }

      return report;
    }


    private async Task<EducationalUnit> NewEducationalUnit(Subject subject, CreateEducationalUnitDto newEducationalUnit)
    {
      var educationalUnit = _mapper.Map<EducationalUnit>(newEducationalUnit);

      foreach (var newComponent in newEducationalUnit.AssigmentComponents)
      {
        var component = _mapper.Map<CreateAssigmentComponentDto, AssignmentComponent>(newComponent);
        Assignment assignment;
        if (newComponent.AssignmentId != null)
        {
          assignment = subject.Assignments.FirstOrDefault(a => a.Id == newComponent.AssignmentId);
          if (assignment == default)
          {
            throw new ValidationException("El trabajo entregable no existe");
          }
        }
        else
        {
          assignment = new Assignment();
          assignment.Subject = subject;
          assignment.IsInitialized = false;
          assignment.AvailableUntil = newComponent.AvailableUntil;
        }

        _ = _calendarService.CreateEvent(subject.University.Id, subject.Id,
          new CreateEventDto
          {
            Event = component.Title,
            StartDate = assignment.AvailableUntil,
            EndDate = assignment.AvailableUntil
          });

        component.Assignment = assignment;
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.DocumentComponents)
      {
        var component = _mapper.Map<CreateDocumentComponentDto, DocumentComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.ForumComponents)
      {
        var component = _mapper.Map<CreateForumComponentDto, ForumComponent>(newComponent);
        if (newComponent.ForumId != null && !newComponent.ForumId.Equals(""))
        {
          var forum = await _forumRepository.GetById(newComponent.ForumId, subject.University.Id);
          if (forum == null || forum.SubjectId != subject.Id)
          {
            throw new ValidationException("El foro no existe");
          }
        }
        else
        {
          var forum = await _forumRepository.CreateForum(
            new CreateForumDto { Name = newComponent.Title, SubjectId = subject.Id }, subject.University.Id);
          component.ForumId = forum.ID;
        }

        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.LinkComponents)
      {
        var component = _mapper.Map<CreateLinkComponentDto, LinkComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.PollComponents)
      {
        var component = _mapper.Map<CreatePollComponentDto, PollComponent>(newComponent);

        // TODO
        // var subjectPoll = subject.SubjectPolls.FirstOrDefault(p => p.Poll.Id == newComponent.PollId);
        // if (subjectPoll == default)
        // {
        // throw new ValidationException("La encuesta no existe");
        // }

        // component.Poll = subjectPoll.Poll;

        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.QuizComponents)
      {
        var component = _mapper.Map<CreateQuizComponentDto, QuizComponent>(newComponent);

        var quiz = subject.Quizzes.FirstOrDefault(q => q.Id == newComponent.QuizId);
        if (quiz == default)
        {
          throw new ValidationException("La evaluacion no existe");
        }

        component.Quiz = quiz;

        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.TextComponents)
      {
        var component = _mapper.Map<CreateTextComponentDto, TextComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.VideoComponents)
      {
        var component = _mapper.Map<CreateVideoComponentDto, VideoComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      foreach (var newComponent in newEducationalUnit.VirtualMeetingComponents)
      {
        var component = _mapper.Map<CreateVirtualMeetingComponentDto, VirtualMeetingComponent>(newComponent);
        educationalUnit.Components.Add(component);
      }

      return educationalUnit;
    }


    private async Task ApplyTemplate(Subject subject, SubjectTemplate template)
    {
      foreach (var templateEducationalUnit in template.Components)
      {
        var newEducationalUnit = new EducationalUnit(templateEducationalUnit);
        subject.Components.Add(newEducationalUnit);
        foreach (var templateComponent in templateEducationalUnit.Components)
        {
          Component component;
          if (templateComponent.GetType() == typeof(AssignmentComponent))
          {
            component = new AssignmentComponent((AssignmentComponent)templateComponent);

            var assignment = new Assignment();
            assignment.Subject = subject;
            assignment.IsInitialized = false;

            subject.Assignments.Add(assignment);
            ((AssignmentComponent)component).Assignment = assignment;
          }
          else if (templateComponent.GetType() == typeof(DocumentComponent))
          {
            component = new DocumentComponent((DocumentComponent)templateComponent);
          }
          else if (templateComponent.GetType() == typeof(ForumComponent))
          {
            component = new ForumComponent((ForumComponent)templateComponent);
            var forum = await _forumRepository.CreateForum(
              new CreateForumDto { Name = ((ForumComponent)component).Title, SubjectId = subject.Id },
              subject.University.Id);
            ((ForumComponent)component).ForumId = forum.ID;
          }
          else if (templateComponent.GetType() == typeof(LinkComponent))
          {
            component = new LinkComponent((LinkComponent)templateComponent);
          }
          else if (templateComponent.GetType() == typeof(PollComponent))
          {
            component = new PollComponent((PollComponent)templateComponent);

            var poll = new UniversityPoll();
            poll.Title = ((PollComponent)templateComponent).Title;
            poll.University = subject.University;
            poll.IsInitialized = false;

            ((PollComponent)templateComponent).Poll = poll;
          }
          else if (templateComponent.GetType() == typeof(QuizComponent))
          {
            component = new QuizComponent((QuizComponent)templateComponent);
            //TODO Quizz(opcional)
          }
          else if (templateComponent.GetType() == typeof(TextComponent))
          {
            component = new TextComponent((TextComponent)templateComponent);
          }
          else if (templateComponent.GetType() == typeof(VideoComponent))
          {
            component = new VideoComponent((VideoComponent)templateComponent);
          }
          else
          {
            component = new VirtualMeetingComponent((VirtualMeetingComponent)templateComponent);
          }

          newEducationalUnit.Components.Add(component);
        }
      }
    }
  }
}
