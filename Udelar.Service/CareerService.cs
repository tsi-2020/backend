﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos;
using Udelar.Common.Dtos.Career.Request;
using Udelar.Common.Dtos.Career.Response;
using Udelar.Common.Dtos.Subject.Response;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class CareerService : ICareerService
  {
    private readonly ICareerRepository _careerRepository;
    private readonly IMapper _mapper;
    private readonly ISubjectRepository _subjectRepository;

    public CareerService(ICareerRepository careerRepository, IMapper mapper, ISubjectRepository subjectRepository)
    {
      _careerRepository = careerRepository;
      _subjectRepository = subjectRepository;
      _mapper = mapper;
    }

    public async Task<ReadCareerDto> CreateCareer(Guid universityId, CreateCareerDto newCareer)
    {
      var career = _mapper.Map<Career>(newCareer);
      await _careerRepository.AddCareerToUniversity(universityId, career);
      return _mapper.Map<ReadCareerDto>(career);
    }

    public async Task<List<ReadCareerDto>> GetAllUniversityCareers(Guid universityId, string careerName)
    {
      var careers = await _careerRepository.FindUniversityCareers(universityId, careerName);
      return _mapper.Map<List<ReadCareerDto>>(careers);
    }

    public async Task<ReadCareerDto> GetUniversityCareerById(Guid universityId, Guid careerId)
    {
      var career = await _careerRepository.FindUniversityCareerById(universityId, careerId);
      if (career == null)
      {
        throw new EntityNotFoundException("Carreara no encontrada.");
      }

      var careerDto = _mapper.Map<ReadCareerDto>(career);

      var subjects = await _subjectRepository.ListSubjectCareerById(careerId);

      var careerSubjectDto = _mapper.Map<List<ReadSubjectLiteDto>>(subjects);

      careerDto.Subjects = careerSubjectDto;

      return _mapper.Map<ReadCareerDto>(careerDto);
    }

    public async Task EditCareer(Guid universityId, EditCareerDto editCareerDto)
    {
      var persitedCareer = await _careerRepository.FindUniversityCareerById(universityId, editCareerDto.Id);
      if (persitedCareer == null)
      {
        throw new EntityNotFoundException("Carreara no encontrada.");
      }

      persitedCareer.Name = editCareerDto.Name;
      await _careerRepository.UpdateChanges();
    }

    public async Task DeleteCareer(Guid universityId, Guid careerId)
    {
      var persitedCareer = await _careerRepository.FindUniversityCareerById(universityId, careerId);
      if (persitedCareer == null)
      {
        throw new EntityNotFoundException("Carreara no encontrada.");
      }

      await _careerRepository.RemoveUniversityCareer(universityId, careerId);
    }

    public async Task AddSubjects(Guid universityId, Guid careerId, List<Guid> subjectIds)
    {
      var career = await _careerRepository.FindUniversityCareerById(universityId, careerId);
      if (career == null)
      {
        throw new EntityNotFoundException("Carreara no encontrada.");
      }

      var subjects = await _subjectRepository.FindSubjectByIdWithCareers(universityId, subjectIds);

      if (subjectIds.Any(s => !subjects.Any(subject => subject.Id.Equals(s))))
      {
        throw new ValidationException("Alguna de las asginaturas no pudo ser encontrada.");
      }

      foreach (var subject in subjects)
      {
        if (subject.CareerSubjects.FirstOrDefault(cs => cs.Career.Id == careerId) != default)
        {
          throw new ValidationException("La asignatura ya existe en la carrera");
        }

        var careerSubject = new CareerSubject { Career = career, Subject = subject };

        career.CareerSubjects.Add(careerSubject);
      }

      await _careerRepository.UpdateChanges();
    }

    public async Task RemoveSubjects(Guid universityId, Guid careerId, List<Guid> subjectIds)
    {

      var career = await _careerRepository.FindUniversityCareerById(universityId, careerId);

      if (career == null)
      {
        throw new EntityNotFoundException("Carreara no encontrada.");
      }

      var subjects = await _subjectRepository.FindSubjectByIdWithCareers(universityId, subjectIds);

      if (subjectIds.Any(s => !subjects.Any(subject => subject.Id.Equals(s))))
      {
        throw new ValidationException("Alguna de las asginaturas no pudo ser encontrada.");
      }

      foreach (var subject in subjects)
      {

        //Controlo que la asignatura esté en la carrera
        if (subject.CareerSubjects.FirstOrDefault(cs => cs.Career.Id == careerId) == default)
        {
          throw new ValidationException("La asignatura no existe en la carrera");
        }

        //La coloco como deleted now
        await _careerRepository.RemoveSubjectCareer(universityId, careerId, subject.Id);

      }

      await _careerRepository.UpdateChanges();
    }

  }
}
