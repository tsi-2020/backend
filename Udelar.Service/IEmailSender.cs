﻿using System.Threading.Tasks;

namespace Udelar.Services
{
  public interface IEmailSender
  {
    Task SendMail(string to, string subject, string body);
  }
}
