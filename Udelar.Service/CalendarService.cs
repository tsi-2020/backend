﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Calendar.Request;
using Udelar.Common.Dtos.Calendar.Response;
using Udelar.Common.Exceptions;
using Udelar.Model.Mongo;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class CalendarService : ICalendarService
  {
    private readonly ISubjectRepository _subjectRepo;
    private readonly IMapper _mapper;
    private readonly ICalendarRepository _calendarRepo;

    public CalendarService(IMapper iMapper, ISubjectRepository iSubjectRepository,
      ICalendarRepository iCalendarRepository)
    {
      _subjectRepo = iSubjectRepository;
      _calendarRepo = iCalendarRepository;
      _mapper = iMapper;
    }

    public async Task<ReadEventDto> CreateEvent(Guid tenantId, Guid subjectId, CreateEventDto createEventDto)
    {
      var subject = await _subjectRepo.FindByIdAndUniversity(subjectId, tenantId);
      if (subject == default)
      {
        throw new EntityNotFoundException("Curso no encontrado.");
      }

      var calendar = _mapper.Map<Calendar>(createEventDto);
      calendar.SubjectId = subjectId;
      calendar.TenantId = tenantId;
      await _calendarRepo.Save(calendar);
      return _mapper.Map<ReadEventDto>(calendar);
    }

    public async Task<List<ReadEventDto>> FilterEvents(Guid tenantId, Guid subjectId, FilterEventsDto filterEventsDto)
    {
      var subject = await _subjectRepo.FindByIdAndUniversity(subjectId, tenantId);
      if (subject == default)
      {
        throw new EntityNotFoundException("Curso no encontrado.");
      }

      var calendars = await _calendarRepo.FilterByDates(tenantId, subject.Id, filterEventsDto);
      return _mapper.Map<List<ReadEventDto>>(calendars);
    }
  }
}
