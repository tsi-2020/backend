﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos;
using Udelar.Common.Dtos.Career.Request;
using Udelar.Common.Dtos.Career.Response;

namespace Udelar.Services
{
  public interface ICareerService
  {
    Task<ReadCareerDto> CreateCareer(Guid id, CreateCareerDto career);
    Task<List<ReadCareerDto>> GetAllUniversityCareers(Guid universityId, string careerName);
    Task<ReadCareerDto> GetUniversityCareerById(Guid universityId, Guid careerId);
    Task EditCareer(Guid universityId, EditCareerDto career);
    Task DeleteCareer(Guid universityId, Guid careerId);
    Task AddSubjects(Guid universityId, Guid careerId, List<Guid> subjectIds);
    Task RemoveSubjects(Guid universityId, Guid careerId, List<Guid> subjectIds);
  }
}
