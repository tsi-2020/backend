﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using Udelar.Common.Dtos.Thread.Request;
using Udelar.Common.Dtos.Thread.Response;

namespace Udelar.Services
{
  public interface IThreadService
  {
    Task<ReadThreadDto> CreateThread(CreateThreadDto createThreadDto, string forumId, Guid tenantId);

    Task<PaginatedThreadDto> ListThreads(Guid tenantId, string forumId, FilterThreadDto filterThread);

    Task EditThread(EditThreadDto editThreadDto, string threadId, Guid tenantId);

    Task DeleteThread(string threadId, Guid tenantId);

    Task<PaginatedViewThreadDto> ViewThread(Guid tenantId, string threadId, FilterThreadDto filterThread);
  }
}
