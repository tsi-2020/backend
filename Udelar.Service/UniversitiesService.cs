﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Udelar.Common.Dtos.University.Request;
using Udelar.Common.Dtos.University.Response;
using Udelar.Common.Dtos.UniversityAdministrator.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Model.Component;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class UniversitiesService : IUniversitiesService
  {
    private readonly IAuthService _authService;
    private readonly IMapper _mapper;
    private readonly IUniversitiesRepository _universitiesRepository;
    private readonly IConfiguration _config;

    public UniversitiesService(IMapper mapper, IUniversitiesRepository universitiesRepository, IAuthService authService, IConfiguration iConfiguration)
    {
      _universitiesRepository = universitiesRepository;
      _mapper = mapper;
      _authService = authService;
      _config = iConfiguration;
    }

    public async Task<ReadUniversityDto> CreateUniversity(CreateUniversityDto createUniversity)
    {
      if (await IsPathTaken(createUniversity.AccessPath))
      {
        throw new ConflictException("Direccion en uso.");
      }

      var university = _mapper.Map<CreateUniversityDto, University>(createUniversity);
      var admin = _mapper.Map<CreateUniversityAdminDto, UniversityAdministrator>(createUniversity.DefaultAdmin);
      admin.Password = _authService.HashPassword(admin.Password);
      university.UniversityAdministrators.Add(admin);
      AssignInitialTemplates(university);
      await _universitiesRepository.AddUniversity(university);
      return _mapper.Map<University, ReadUniversityDto>(university);
    }


    private static void AssignInitialTemplates(University university)
    {
      //TEMPLATE 1
      var forum1 = new ForumComponent { Position = 0, Title = "Foro General" };

      var forum2 = new ForumComponent { Position = 1, Title = "Noticias" };


      var educationalUnits1 = new List<EducationalUnit>();

      educationalUnits1.Add(new EducationalUnit
      {
        Position = 0,
        Title = "General",
        Text = "Aqui va una descripcion del curso",
        Components = new List<Component> { forum1, forum2 }
      });

      for (var i = 1; i <= 8; i++)
      {
        educationalUnits1.Add(new EducationalUnit { Position = i, Title = "Modulo " + i, Text = "Descripcion opcional" });
      }


      var template1 = new SubjectTemplate
      {
        University = university,
        Title = "Basico",
        Description = "Crea un Foro general y uno de noticias",
        Components = educationalUnits1
      };

      university.SubjectTemplates.Add(template1);

      //TEMPLATE 2

      var forum3 = new ForumComponent { Position = 0, Title = "Foro General" };

      var forum4 = new ForumComponent { Position = 1, Title = "Noticias" };


      var educationalUnits2 = new List<EducationalUnit>();

      educationalUnits2.Add(new EducationalUnit
      {
        Position = 0,
        Title = "General",
        Text = "Aqui va una descripcion del curso",
        Components = new List<Component> { forum3, forum4 }
      });

      for (var i = 1; i <= 4; i++)
      {
        educationalUnits2.Add(new EducationalUnit { Position = i, Title = "Modulo " + i, Text = "Descripcion opcional" });
      }

      var assignment = new AssignmentComponent { Position = 0, Title = "Trabajo final" };

      educationalUnits2.Add(new EducationalUnit
      {
        Position = 5,
        Title = "Entregas finales",
        Components = new List<Component> { assignment }
      });


      var template2 = new SubjectTemplate
      {
        University = university,
        Title = "Taller",
        Description = "Con trabajo final",
        Components = educationalUnits2
      };

      university.SubjectTemplates.Add(template2);

      //TEMPLATE 3

      var forum5 = new ForumComponent { Position = 0, Title = "Foro General" };

      var forum6 = new ForumComponent { Position = 1, Title = "Noticias" };

      var virtualMetting = new VirtualMeetingComponent { Position = 2, Title = "Salon virual", Url = "https://zoom.us/" };


      var educationalUnits3 = new List<EducationalUnit>();

      educationalUnits3.Add(new EducationalUnit
      {
        Position = 0,
        Title = "General",
        Text = "Aqui va una descripcion del curso",
        Components = new List<Component> { forum5, forum6, virtualMetting }
      });

      for (var i = 1; i <= 8; i++)
      {
        educationalUnits3.Add(new EducationalUnit { Position = i, Title = "Modulo " + i, Text = "Descripcion opcional" });
      }


      var template3 = new SubjectTemplate
      {
        University = university,
        Title = "Con salon virtual",
        Description = "Con salon virtual",
        Components = educationalUnits3
      };
      university.SubjectTemplates.Add(template3);
    }

    public async Task<List<ReadUniversityDto>> GetUniversities()
    {
      var universities = await _universitiesRepository.FindUniversityAll();
      return _mapper.Map<List<University>, List<ReadUniversityDto>>(universities);
    }

    public async Task<ReadUniversityDto> EditUniversity(UpdateUniversityDto updateUniversity)
    {
      var university = await _universitiesRepository.FindUniversityById(updateUniversity.Id);
      if (university.AccessPath != updateUniversity.AccessPath)
      {
        if (await IsPathTaken(updateUniversity.AccessPath))
        {
          throw new ConflictException("Direccion en uso.");
        }
      }
      university.Name = updateUniversity.Name;
      university.AccessPath = updateUniversity.AccessPath;
      university.Logo = updateUniversity.Logo;
      university.PrimaryColor = updateUniversity.PrimaryColor;
      university.Description = updateUniversity.Description;
      university.SecondaryColor = updateUniversity.SecondaryColor;
      await _universitiesRepository.UpdateUniversity(university);
      return _mapper.Map<University, ReadUniversityDto>(university);
    }

    public Task DeleteUniversity(Guid id)
    {
      return _universitiesRepository.RemoveUniversity(id);
    }

    public async Task<ReadUniversityDto> GetUniversityById(Guid id)
    {
      var university = await _universitiesRepository.FindUniversityById(id);
      return _mapper.Map<University, ReadUniversityDto>(university);
    }

    public async Task<List<ReadUniversityDto>> GetUniversityByPath(string path)
    {
      var university = await _universitiesRepository.FindByPath(path);
      if (university == default)
      {
        throw new EntityNotFoundException("Universidad no encontrada");
      }

      return _mapper.Map<List<ReadUniversityDto>>(new List<University> { university });
    }


    private async Task<bool> IsPathTaken(string path)
    {
      return await _universitiesRepository.FindByPath(path) != null;
    }
  }
}
