﻿using System;
using System.Linq;
using AutoMapper;
using Udelar.Common.Dtos.Assigment.response;
using Udelar.Common.Dtos.Calendar.Response;
using Udelar.Common.Dtos.Career.Response;
using Udelar.Common.Dtos.Component.Response;
using Udelar.Common.Dtos.Forum.Response;
using Udelar.Common.Dtos.Message.Response;
using Udelar.Common.Dtos.Notice.Response;
using Udelar.Common.Dtos.Poll.Response;
using Udelar.Common.Dtos.Subject.Response;
using Udelar.Common.Dtos.SubjectTemplate.response;
using Udelar.Common.Dtos.Thread.Response;
using Udelar.Common.Dtos.UdelarAdministrator.Reponse;
using Udelar.Common.Dtos.University.Response;
using Udelar.Common.Dtos.UniversityAdministrator.Reponse;
using Udelar.Common.Dtos.User.Response;
using Udelar.Model;
using Udelar.Model.Component;
using Udelar.Model.Mongo;
using Udelar.Model.Poll;

namespace Udelar.Services.MapperProfiles
{
  public class ModelToDtoProfile : Profile
  {
    public ModelToDtoProfile()
    {
      //Calendar
      CreateMap<Calendar, ReadEventDto>();

      //Notice
      CreateMap<Notice, ReadNoticeDto>();

      //Poll 
      CreateMap<Poll, ReadPollDto>();

      //Forum 
      CreateMap<Forum, ReadForumDto>();
      CreateMap<Forum, ViewForumDto>();

      //Threads
      CreateMap<ForumThread, ReadThreadDto>();
      CreateMap<ForumThread, ViewThreadDto>();

      //Messages
      CreateMap<Message, ReadMessageDto>();

      //Udelar Admin
      CreateMap<Administrator, ReadUdelarAdminDto>();

      //Universities
      CreateMap<University, ReadUniversityDto>()
        .ForMember(
          source => source.Administrators,
          dest =>
            dest.MapFrom(m => m.UniversityAdministrators)
        );

      //UniversityAdministrators
      CreateMap<UniversityAdministrator, ReadUniversityAdminDto>();

      //SubjectTemplates
      CreateMap<SubjectTemplate, ReadSubjectTemplateDto>();

      //Components
      CreateMap<EducationalUnit, ReadEducationalUnitDto>();
      CreateMap<AssignmentComponent, ReadAssigmentComponentDto>();
      CreateMap<DocumentComponent, ReadDocumentComponentDto>();
      CreateMap<ForumComponent, ReadForumComponentDto>();
      CreateMap<LinkComponent, ReadLinkComponentDto>();
      CreateMap<PollComponent, ReadPollComponentDto>();
      CreateMap<QuizComponent, ReadQuizComponentDto>();
      CreateMap<TextComponent, ReadTextComponentDto>();
      CreateMap<VideoComponent, ReadVideoComponentDto>();
      CreateMap<VirtualMeetingComponent, ReadVirtualMeetingComponentDto>();

      //Careers
      CreateMap<Career, ReadCareerDto>();

      //Subject
      CreateMap<Subject, ReadSubjectLiteDto>();
      CreateMap<Subject, ReadSubjectDto>();

      //Users
      CreateMap<User, ReadUserDto>()
        .ForMember(dto => dto.Roles,
          op => op.MapFrom(user => user.Roles.Select(role => Enum.GetName(typeof(Role), role.Role))));

      //Assignments
      CreateMap<Assignment, ReadAssignmentDto>();
      CreateMap<DeliveredAssignment, ReadDeliveredAssigmentDto>();

      CreateMap<Enrollment, ReadUserEnrollmentsDto>()
        .ForMember(dto => dto.IsProfessorEnrollment,
          op => op.MapFrom(enrollment => enrollment is ProfessorEnrollment)
        )
        .ForMember(dto => dto.IsResponsible,
          op => op.MapFrom<CustomResolver>()
        )
        .ForMember(dto => dto.Score,
          op => op.MapFrom<CustomResolverScore>()
        )
        .ForMember(dto => dto.Id,
          op => op.MapFrom(enrollment => enrollment.Subject.Id)
        )
        .ForMember(dto => dto.Name,
          op => op.MapFrom(enrollment => enrollment.Subject.Name)
        );
    }
  }

  public class CustomResolverScore : IValueResolver<Enrollment, ReadUserEnrollmentsDto, float>
  {
    public float Resolve(Enrollment source, ReadUserEnrollmentsDto destination, float destMember,
      ResolutionContext context)
    {
      return source is StudentEnrollment enrollment ? enrollment.Score : 0;
    }
  }


  public class CustomResolver : IValueResolver<Enrollment, ReadUserEnrollmentsDto, bool>
  {
    public bool Resolve(Enrollment source, ReadUserEnrollmentsDto destination, bool destMember,
      ResolutionContext context)
    {
      return source is ProfessorEnrollment enrollment && enrollment.IsResponsible;
    }
  }
}
