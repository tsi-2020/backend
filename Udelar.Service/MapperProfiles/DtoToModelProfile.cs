﻿using AutoMapper;
using Udelar.Common.Dtos.Calendar.Request;
using Udelar.Common.Dtos.Calendar.Response;
using Udelar.Common.Dtos.Career.Request;
using Udelar.Common.Dtos.Component.Request;
using Udelar.Common.Dtos.Poll.Request;
using Udelar.Common.Dtos.SubjectTemplate.request;
using Udelar.Common.Dtos.UdelarAdministrator.Request;
using Udelar.Common.Dtos.University.Request;
using Udelar.Common.Dtos.UniversityAdministrator.Request;
using Udelar.Model;
using Udelar.Model.Component;
using Udelar.Model.Mongo;
using Udelar.Model.Poll.Questions;

namespace Udelar.Services.MapperProfiles
{
  public class DtoToModelProfile : Profile
  {
    public DtoToModelProfile()
    {
      //Calendar
      CreateMap<CreateEventDto, Calendar>();

      //Polls
      CreateMap<CreatePollMultipleChoiceQuestionDto, MultipleChoicePollQuestion>().ForMember(
        source => source.Choices,
        dest => dest.MapFrom(m => m.Choices)
      );
      CreateMap<CreatePollOpenQuestionDto, OpenPollQuestion>();
      CreateMap<CreatePollChoiceDto, PollChoice>();

      //Udelar Admin
      CreateMap<CreateUdelarAdminDto, UdelarAdministrator>();

      //University
      CreateMap<CreateUniversityDto, University>();
      CreateMap<UpdateUniversityDto, University>();

      //UniversityAdministrator
      CreateMap<CreateUniversityAdminDto, UniversityAdministrator>();

      //SubjectTemplates
      CreateMap<CreateSubjectTemplateDto, SubjectTemplate>();

      //Components
      CreateMap<CreateEducationalUnitDto, EducationalUnit>();
      CreateMap<CreateAssigmentComponentDto, AssignmentComponent>();
      CreateMap<CreateDocumentComponentDto, DocumentComponent>();
      CreateMap<CreateForumComponentDto, ForumComponent>();
      CreateMap<CreateLinkComponentDto, LinkComponent>();
      CreateMap<CreatePollComponentDto, PollComponent>();
      CreateMap<CreateQuizComponentDto, QuizComponent>();
      CreateMap<CreateTextComponentDto, TextComponent>();
      CreateMap<CreateVideoComponentDto, VideoComponent>();
      CreateMap<CreateVirtualMeetingComponentDto, VirtualMeetingComponent>();

      CreateMap<UpdateEducationalUnitDto, EducationalUnit>();
      CreateMap<UpdateAssigmentComponentDto, AssignmentComponent>();
      CreateMap<UpdateDocumentComponentDto, DocumentComponent>();
      CreateMap<UpdateForumComponentDto, ForumComponent>();
      CreateMap<UpdateLinkComponentDto, LinkComponent>();
      CreateMap<UpdatePollComponentDto, PollComponent>();
      CreateMap<UpdateQuizComponentDto, QuizComponent>();
      CreateMap<UpdateTextComponentDto, TextComponent>();
      CreateMap<UpdateVideoComponentDto, VideoComponent>();
      CreateMap<UpdateVirtualMeetingComponentDto, VirtualMeetingComponent>();

      //Careers
      CreateMap<CreateCareerDto, Career>();
      CreateMap<EditCareerDto, Career>();
    }
  }
}
