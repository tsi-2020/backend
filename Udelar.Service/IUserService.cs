﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Report;
using Udelar.Common.Dtos.User.Request;
using Udelar.Common.Dtos.User.Response;
using Udelar.Model;

namespace Udelar.Services
{
  public interface IUserService
  {
    Task<ReadUserDto> CreateUser(CreateUserDto createStudentDto, List<Role> role, Guid tenantId);
    Task<PaginatedUsersDto> GetUsers(Guid tenantId, FilterUserDto userFilter);
    Task<ReadUserDto> GetUserById(Guid tenantId, Guid userId);
    Task UpdateUserData(Guid tenantId, Guid userId, UpdateUserDto updateUserData);
    Task PatchUserRole(Guid tenantId, Guid userId, PatchUserRoleDto patchUserRoleDto);
    Task<ReadUserDto> FindByResetToken(string token);
    Task<ReadUserDto> ResetPassword(string token, ResetPasswordDto resetPasswordDto);
    Task DeleteUser(Guid tenantId, Guid userId);
    Task<List<ReadUserEnrollmentsDto>> GetUserEnrollments(Guid userId);
    Task<List<UniversityUsersReportDto>> GetUniversitiesUsersQuantityReport();
  }
}
