﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Poll.Request;
using Udelar.Common.Dtos.Poll.Response;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Model.Poll;
using Udelar.Model.Poll.Answers;
using Udelar.Model.Poll.Publications;
using Udelar.Model.Poll.Questions;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class PollService : IPollService
  {
    private readonly IMapper _mapper;
    private readonly IPollRepository _pollRepository;
    private readonly IUniversitiesRepository _universityRepository;
    private readonly ISubjectRepository _subjectRepository;
    private readonly IUserRepository _userRepository;

    public PollService(IPollRepository pollRepository,
      IUniversitiesRepository universitiesRepository,
      ISubjectRepository subjectRepository,
      IUserRepository userRepository,
      IMapper iMapper)
    {
      _mapper = iMapper;
      _pollRepository = pollRepository;
      _universityRepository = universitiesRepository;
      _subjectRepository = subjectRepository;
      _userRepository = userRepository;
    }

    public async Task CreateSubjectPoll(CreatePollDto createPollDto, Guid subjectId, Guid universityId)
    {
      var subject = await _subjectRepository.FindSubjectById(universityId, subjectId);
      Poll poll = new SubjectPoll
      {
        Title = createPollDto.Title,
        Questions = new HashSet<PollQuestion>(),
        Subject = subject
      };
      CreateNewPoll(createPollDto, ref poll);
      await _pollRepository.CreateSubjectPoll(poll as SubjectPoll);
    }

    public async Task CreateUniversityPoll(CreatePollDto createPollDto, Guid universityId)
    {
      var university = await _universityRepository.FindUniversityById(universityId);
      Poll poll = new UniversityPoll
      {
        Title = createPollDto.Title,
        Questions = new HashSet<PollQuestion>(),
        University = university
      };
      CreateNewPoll(createPollDto, ref poll);
      await _pollRepository.CreateUniversityPoll(poll as UniversityPoll);
    }

    public Task CreateUdelarPoll(CreatePollDto createPollDto)
    {
      Poll poll = new UdelarPoll { Title = createPollDto.Title, Questions = new HashSet<PollQuestion>() };
      CreateNewPoll(createPollDto, ref poll);
      return _pollRepository.CreateUdelarPoll(poll as UdelarPoll);
    }

    private static void CreateNewPoll(CreatePollDto createPollDto, ref Poll poll)
    {
      foreach (var createPollOpenQuestionDto in createPollDto.OpenQuestions)
      {
        var openPollQuestion = new OpenPollQuestion
        {
          Text = createPollOpenQuestionDto.Text,
          Position = createPollOpenQuestionDto.Position
        };
        poll.Questions.Add(openPollQuestion);
      }

      foreach (var multipleChoiceQuestion in createPollDto.MultipleChoiceQuestions)
      {
        var multipleChoicePollQuestion = new MultipleChoicePollQuestion
        {
          IsMultipleSelect = multipleChoiceQuestion.IsMultipleSelect,
          Position = multipleChoiceQuestion.Position,
          Text = multipleChoiceQuestion.Text,
          Choices = new HashSet<PollChoice>()
        };
        foreach (var choice in multipleChoiceQuestion.Choices)
        {
          multipleChoicePollQuestion.Choices.Add(new PollChoice { Text = choice.Text });
        }

        poll.Questions.Add(multipleChoicePollQuestion);
      }
    }

    public async Task<List<ReadPollDto>> ReadUdelarPolls()
    {
      var polls = await _pollRepository.ReadUdelarPolls();
      return _mapper.Map<List<ReadPollDto>>(polls);
    }

    public async Task<List<ReadPollDto>> ReadUniversityPolls(Guid id)
    {
      var polls = await _pollRepository.ReadUniversityPolls(id);
      return _mapper.Map<List<ReadPollDto>>(polls);
    }

    public async Task<List<ReadPollDto>> ReadSubjectPolls(Guid subjectId)
    {
      var polls = await _pollRepository.ReadSubjectPolls(subjectId);
      return _mapper.Map<List<ReadPollDto>>(polls);
    }

    public async Task PublishPollIntoUniversity(Guid universityId, Guid pollId)
    {
      var university = await _universityRepository.FindUniversityById(universityId);
      var poll = await _pollRepository.FindUdelarPollById(pollId);
      if (poll.UniversityPollPublications.Any(p => p.Poll.Id.Equals(pollId)))
      {
        throw new ConflictException("La encuesta ya se encuentra publicada en la universidad.");
      }

      var universityPoll = new UniversityPollPublication { Poll = poll, University = university };
      university.UniversityPollPublications.Add(universityPoll);
      await _universityRepository.Save(university);
    }

    public async Task PublishPollIntoSubjectAsProfessor(Guid universityId, Guid subjectId, Guid pollId)
    {
      var subject = await _subjectRepository.FindByIdAndUniversity(subjectId, universityId);
      var poll = await _pollRepository.FindSubjectPoll(pollId, subjectId);
      if (poll.SubjectPollPublications.Any(p => p.Poll.Id.Equals(pollId)))
      {
        throw new ConflictException("La encuesta ya se encuentra publicada en el curso.");
      }

      var subjectPollPublication = new SubjectPollPublication { Poll = poll, Subject = subject };
      subject.SubjectPollPublications.Add(subjectPollPublication);
      await _subjectRepository.SaveChanges(subject);
    }

    public async Task PublishPollIntoSubjectAsUniversityAdmin(Guid universityId, Guid subjectId, Guid pollId)
    {
      var subject = await _subjectRepository.FindByIdAndUniversity(subjectId, universityId);
      var poll = await _pollRepository.FindUniversityPollById(pollId, universityId);
      if (poll.SubjectPollPublications.Any(p => p.Poll.Id.Equals(pollId)))
      {
        throw new ConflictException("La encuesta ya se encuentra publicada en el curso.");
      }

      var subjectPollPublication = new SubjectPollPublication { Poll = poll, Subject = subject };
      subject.SubjectPollPublications.Add(subjectPollPublication);
      await _subjectRepository.SaveChanges(subject);
    }

    public async Task ReplySubjectPoll(Guid universityId, Guid subjectId, Guid pollId, Guid userId,
      ReplyPollDto replyPollDto)
    {
      if (await IsPollAnswered(pollId, userId))
      {
        throw new ConflictException("ya respondio esta encuesta");
      }

      var subject = await _subjectRepository.FindSubjectWithEnrollments(universityId, subjectId);

      var user = await FindUser(universityId, userId);

      if (!subject.Enrollments.Any(enrollment => enrollment.User.Id.Equals(userId)))
      {
        throw new AuthorizationException("No puede responder esta encuesta");
      }

      var poll = await _pollRepository.FindPollById(pollId);
      if (poll == default)
        throw new EntityNotFoundException("Encuesta no encontrada");

      await ReplyPoll(replyPollDto, poll, user);
    }

    public async Task ReplyUniversityPoll(Guid universityId, Guid pollId, Guid userId, ReplyPollDto replyPollDto)
    {
      if (await IsPollAnswered(pollId, userId))
      {
        throw new ConflictException("Ya respondio esta encuesta");
      }

      var user = await _userRepository.FindUserInUniversityById(userId, universityId);
      if (user == default)
        throw new EntityNotFoundException("Usuario no encontrada");


      var poll = await _pollRepository.FindPollById(pollId);
      if (poll == default)
        throw new EntityNotFoundException("Encuesta no encontrada");

      await ReplyPoll(replyPollDto, poll, user);
    }

    private async Task ReplyPoll(ReplyPollDto replyPollDto, Poll poll, User user)
    {
      var pollResponse = new PollResponse();

      foreach (var openQuestionDto in replyPollDto.OpenQuestions)
      {
        var question = poll.Questions.FirstOrDefault(question => question.Id.Equals(openQuestionDto.OpenQuestionId));
        if (question == default)
        {
          throw new EntityNotFoundException("Pregunta no encontrada");
        }

        pollResponse.Answers.Add(new OpenPollAnswer
        {
          Text = openQuestionDto.Response,
          OpenPollQuestion = question as OpenPollQuestion,
        });
      }

      foreach (var multipleChoiceQuestionDto in replyPollDto.MultipleChoiceQuestions)
      {
        var question = poll.Questions.FirstOrDefault(pollQuestion =>
          pollQuestion.Id.Equals(multipleChoiceQuestionDto.MultipleChoiceQuestionId)) as MultipleChoicePollQuestion;
        if (question == default)
        {
          throw new EntityNotFoundException("Pregunta no encontrada");
        }

        var answer = new MultipleChoicePollAnswer
        {
          MultipleChoicePollQuestion = question,
          Choices = new List<PollResponseChoice>()
        };
        multipleChoiceQuestionDto.Choices.ForEach(choiceId =>
        {
          var choice = question.Choices.FirstOrDefault(pollChoice => pollChoice.Id.Equals(choiceId));
          answer.Choices.Add(new PollResponseChoice { Choice = choice, MultipleChoicePollAnswer = answer });
        });
        pollResponse.Answers.Add(answer);
      }

      pollResponse.User = user;
      pollResponse.Poll = poll;

      await _pollRepository.CreatePollResponse(pollResponse);
    }

    public async Task<ReadPublishPollDto> GetPublishedPollById(Guid pollId)
    {
      var poll = await _pollRepository.FindPollById(pollId);
      if (poll == default)
      {
        throw new EntityNotFoundException("Encuesta no encontrada");
      }

      return new ReadPublishPollDto
      {
        Id = poll.Id,
        Title = poll.Title,
        CreatedOn = poll.CreatedOn,
        MultipleChoiceQuestions = SelectWhere(poll.Questions, question => question,
            question => question is MultipleChoicePollQuestion)
          .Select(question =>
            new ListPollMultipleChoiceQuestionDto
            {
              Id = question.Id,
              Text = question.Text,
              Position = question.Position,
              Choices = ((MultipleChoicePollQuestion)question).Choices.Select(choice =>
                new ListPollQuestionChoicesDto { Id = choice.Id, Text = choice.Text }).ToList()
            }
          ).ToList(),
        OpenQuestions =
          SelectWhere(poll.Questions, question => question,
              question => question is OpenPollQuestion)
            .Select(question => new ListPollOpenQuestionDto
            {
              Id = question.Id,
              Text = question.Text,
              Position = question.Position,
            }).ToList()
      };
    }

    public async Task<ReadPaginatedPollAnswerDto> GetPollAnswers(Guid pollId, int page, int limit)
    {
      var (item1, item2) = await _pollRepository.GetPollAnswers(pollId, page, limit);
      var paginatedPollAnswerDto =
        new ReadPaginatedPollAnswerDto { ReadPollAnswerDtos = new List<ReadPollAnswerDto>(), Count = item1 };
      foreach (var response in item2)
      {
        var answerDto = new ReadPollAnswerDto();
        answerDto.Title = response.Poll.Title;
        answerDto.CreatedOn = response.CreatedOn;
        answerDto.MultipleChoiceAnswerDtos = new List<ListPollMultipleChoiceAnswerDto>();
        answerDto.OpenAnswerDtos = new List<ListPollOpenAnswerDto>();

        foreach (var responseAnswer in response.Answers)
        {
          if (responseAnswer is MultipleChoicePollAnswer)
          {
            var multipleChoicePollAnswer = responseAnswer as MultipleChoicePollAnswer;
            var multipleChoiceAnswerDto = new ListPollMultipleChoiceAnswerDto();
            multipleChoiceAnswerDto.Question = multipleChoicePollAnswer.MultipleChoicePollQuestion.Text;
            multipleChoiceAnswerDto.Position = multipleChoicePollAnswer.MultipleChoicePollQuestion.Position;
            multipleChoiceAnswerDto.Choices = new List<string>();

            foreach (var pollResponseChoice in multipleChoicePollAnswer.Choices)
            {
              multipleChoiceAnswerDto.Choices.Add(pollResponseChoice.Choice.Text);
            }

            answerDto.MultipleChoiceAnswerDtos.Add(multipleChoiceAnswerDto);
          }
          else
          {
            var openPollAnswer = responseAnswer as OpenPollAnswer;

            var pollOpenAnswerDto = new ListPollOpenAnswerDto();
            pollOpenAnswerDto.Position = openPollAnswer.OpenPollQuestion.Position;
            pollOpenAnswerDto.Response = openPollAnswer.Text;
            pollOpenAnswerDto.Question = openPollAnswer.OpenPollQuestion.Text;
            answerDto.OpenAnswerDtos.Add(pollOpenAnswerDto);
          }
        }

        paginatedPollAnswerDto.ReadPollAnswerDtos.Add(answerDto);
      }

      return paginatedPollAnswerDto;
    }

    public async Task<List<ReadPublishPollDto>> ListPublishedSubjectPolls(Guid subjectId, Guid userId)
    {
      var polls = await _pollRepository.FindSubjectPublishedPolls(subjectId);

      var readPublishPollDtos = polls.Item1.Select(SubjectPollToReadPublishPollDto(userId)).ToList();
      readPublishPollDtos.AddRange(polls.Item2.Select(SubjectPollToReadPublishPollDto(userId)).ToList());

      return readPublishPollDtos;
    }

    private static Func<Poll, ReadPublishPollDto> SubjectPollToReadPublishPollDto(Guid userId)
    {
      return subjectPoll => new ReadPublishPollDto
      {
        Id = subjectPoll.Id,
        Title = subjectPoll.Title,
        CreatedOn = subjectPoll.CreatedOn,
        IsAnswered = subjectPoll.PollResponses.Any(pr => pr.User.Id.Equals(userId))
      };
    }

    public async Task<List<ReadPublishPollDto>> ListPublishedUniversityPolls(Guid subjectId, Guid userId)
    {
      var polls = await _pollRepository.FindUniversityPublishedPolls(subjectId);
      var readPublishPollDtos = polls.Select(SubjectPollToReadPublishPollDto(userId)).ToList();
      return readPublishPollDtos;
    }

    private async Task<bool> IsPollAnswered(Guid pollId, Guid userId)
    {
      var pollResponse = await _pollRepository.FindUserPollResponse(pollId, userId);
      return pollResponse != default;
    }

    private async Task<User> FindUser(Guid universityId, Guid userId)
    {
      var user = await _userRepository.FindUserInUniversityById(userId, universityId);
      if (user == default)
      {
        throw new EntityNotFoundException("Usuario no encontrado");
      }

      return user;
    }

    private static IEnumerable<TResult> SelectWhere<TSource, TResult>(IEnumerable<TSource> source,
      Func<TSource, TResult> selector, Func<TSource, bool> predicate)
    {
      return from item in source where predicate(item) select selector(item);
    }
  }
}
