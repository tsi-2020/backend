﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Califications.response;
using Udelar.Common.Dtos.User.Response;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class CalificationService : ICalificationService
  {
    private ISubjectRepository _repository;
    private readonly IMapper _mapper;

    public CalificationService(ISubjectRepository repository, IMapper mapper)
    {
      _repository = repository;
      _mapper = mapper;
    }

    public async Task<List<ReadCalificationDto>> GetSubjectCalifications(Guid tenantId, Guid subjectId, Guid requesterUserId)
    {
      var subject = await _repository.FindSubjectWithEnrollments(tenantId, subjectId);

      if (requesterUserId != Guid.Empty)
      {
        var requesterEnrollment = ValidateUserIsEnrolled(subject, requesterUserId);
        if (requesterEnrollment.GetType() != typeof(ProfessorEnrollment))
        {
          throw new AuthorizationException("El usuario no es docente del curso");
        }
      }

      var califications = new List<ReadCalificationDto>();

      foreach (var enrollment in subject.Enrollments)
      {
        if (enrollment.GetType() == typeof(StudentEnrollment))
        {
          califications.Add(new ReadCalificationDto
          {
            Student = _mapper.Map<ReadUserDto>(enrollment.User),
            Score = ((StudentEnrollment)enrollment).Score
          });
        }
      }

      return califications;
    }

    public async Task<List<ReadCalificationDto>> GenerateSubjectCalifications(Guid tenantId, Guid subjectId, Guid requesterUserId)
    {
      var subject = await _repository.FindSubjectWithEnrollments(tenantId, subjectId);

      if (requesterUserId != Guid.Empty)
      {
        var requesterEnrollment = ValidateUserIsEnrolled(subject, requesterUserId);
        if (requesterEnrollment.GetType() != typeof(ProfessorEnrollment))
        {
          throw new AuthorizationException("El usuario no es docente del curso");
        }
      }

      var califications = new List<ReadCalificationDto>();

      foreach (var enrollment in subject.Enrollments)
      {
        if (enrollment.GetType() != typeof(StudentEnrollment))
        {
          continue;
        }

        ((StudentEnrollment)enrollment).Score = (float)(Math.Round(new Random().NextDouble() * (100 - 1) + 1));
        califications.Add(new ReadCalificationDto
        {
          Student = _mapper.Map<ReadUserDto>(enrollment.User),
          Score = ((StudentEnrollment)enrollment).Score
        });
      }

      await _repository.SaveChanges(subject);

      return califications;
    }


    /// <summary>
    /// Verifica si un usuario esta inscripto a un curso:
    /// - Si esta inscripto devuelve  la inscripcion
    /// - Sino tira excepcion
    /// </summary>
    /// <param name="subject"></param>
    /// <param name="requesterUserId"></param>
    /// <returns></returns>
    private Enrollment ValidateUserIsEnrolled(Subject subject, Guid requesterUserId)
    {
      var enrollment = subject.Enrollments.FirstOrDefault(e => e.User.Id == requesterUserId);

      if (enrollment == default)
      {
        throw new AuthorizationException("El usuario no esta inscripto al curso");
      }

      return enrollment;
    }
  }
}
