﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Califications.response;

namespace Udelar.Services
{
  public interface ICalificationService
  {
    Task<List<ReadCalificationDto>> GetSubjectCalifications(Guid tenantId, Guid subjectId, Guid requesterUserId);
    Task<List<ReadCalificationDto>> GenerateSubjectCalifications(Guid tenantId, Guid subjectId, Guid userId);
  }
}
