﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.Message.Request;
using Udelar.Common.Dtos.Message.Response;
using Udelar.Common.Exceptions;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class MessageService : IMessageService
  {
    private readonly IMessageRepository _messageRepository;
    private readonly IThreadRepository _threadRepository;
    private readonly IMapper _mapper;

    public MessageService(IMessageRepository messageRepository, IThreadRepository threadRepository, IMapper mapper)
    {
      _messageRepository = messageRepository;
      _threadRepository = threadRepository;
      _mapper = mapper;
    }

    public async Task<ReadMessageDto> CreateMessage(CreateMessageDto createMessageDto, string threadId, Guid userId, string userFullName, Guid tenantId)
    {
      var thread = await _threadRepository.GetById(threadId, tenantId);
      if (thread == null)
      {
        throw new EntityNotFoundException("Hilo no encontrado.");
      }

      var message = await _messageRepository.Create(createMessageDto, userFullName, userId, thread, tenantId);

      return new ReadMessageDto
      {
        Id = message.ID,
        UserId = message.UserId.ToString(),
        Text = message.Text,
        FullName = message.FullName,
        CreatedOn = message.CreatedOn
      };
    }

    public async Task EditMessage(EditMessageDto editMessageDto, string messageId, Guid tenantId, Guid userId)
    {
      var persistedMessage = await _messageRepository.GetById(messageId, tenantId);
      if (persistedMessage == null)
      {
        throw new EntityNotFoundException("Mensaje no encontrado.");
      }

      //chequear que el user id es el mismo
      if (persistedMessage.UserId != userId)
      {
        throw new EntityNotFoundException("El usuario no es quien creo el mensaje.");
      }

      persistedMessage.Text = editMessageDto.Text;

      await _messageRepository.Save(persistedMessage);

    }

    public async Task<PaginatedMessageDto> ListMessages(Guid tenantId, string threadId, FilterMessageDto filterMessageDto)
    {
      var thread = await _threadRepository.GetById(threadId, tenantId);
      if (thread == null)
      {
        throw new EntityNotFoundException("Hilo no encontrado.");
      }

      var paginatedMessages = await _messageRepository.List(thread, filterMessageDto.Page, filterMessageDto.Limit);

      var dtoList = paginatedMessages.Item2.Select(forum => _mapper.Map<ReadMessageDto>(forum)).ToList();

      return new PaginatedMessageDto { Count = paginatedMessages.Item1, MessageList = dtoList };

    }

  }
}
