﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Udelar.Common.Dtos.Bedelias.Request;
using Udelar.Common.Dtos.Enrollment.request;
using Udelar.Common.Dtos.Enrollment.response;
using Udelar.Common.Dtos.User.Response;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class EnrollmentService : IEnrollmentService
  {
    private readonly IMapper _mapper;
    private readonly ISubjectRepository _subjectRepository;
    private readonly IUserRepository _userRepository;
    private readonly IBedeliasService _bedeliasService;

    public EnrollmentService(ISubjectRepository subjectRepository, IUserRepository userRepository, IBedeliasService iBedeliasService, IMapper mapper)
    {
      _subjectRepository = subjectRepository;
      _userRepository = userRepository;
      _mapper = mapper;
      _bedeliasService = iBedeliasService;
      JsonConvert.DefaultSettings =
        () => new JsonSerializerSettings()
        {
          ContractResolver = new CamelCasePropertyNamesContractResolver(),
          Converters = { new StringEnumConverter() }
        };
    }

    public async Task UpdateProfessorEnrollments(Guid tenantId, Guid subjectId,
      List<CreateProfessorEnrollmentDto> enrollmentDtoList, Guid responsibleUserId)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollments(tenantId, subjectId);

      if (responsibleUserId != Guid.Empty)
      {
        Enrollment responsibleUserEnrollment = subject.Enrollments.FirstOrDefault(e => e.User.Id == responsibleUserId);
        if (responsibleUserEnrollment == default || responsibleUserEnrollment.GetType() != typeof(ProfessorEnrollment)
                                                 || !((ProfessorEnrollment)responsibleUserEnrollment).IsResponsible)
        {
          throw new AuthorizationException("El usuario no es responsable del curso");
        }
      }


      var enrollmentsToRemove = new List<Enrollment>();


      foreach (var enrollment in subject.Enrollments)
      {
        if (enrollment.GetType() == typeof(ProfessorEnrollment))
        {
          var enrollmentDto = enrollmentDtoList.Find(e =>
            e.UserId == enrollment.User.Id);
          if (enrollmentDto == null)
          {
            enrollmentsToRemove.Add(enrollment);
          }
          else
          {
            ((ProfessorEnrollment)enrollment).IsResponsible = enrollmentDto.IsResponsible;
            enrollmentDtoList.Remove(enrollmentDto);
          }
        }
      }

      foreach (var enrollment in enrollmentsToRemove)
      {
        subject.Enrollments.Remove(enrollment);
      }

      foreach (var enrollmentDto in enrollmentDtoList)
      {
        var enrollment = subject.Enrollments.FirstOrDefault(e =>
          e.GetType() == typeof(ProfessorEnrollment) && e.User.Id == enrollmentDto.UserId);

        if (enrollment != default)
        {
          if (enrollmentDto.IsResponsible)
          {
            ((ProfessorEnrollment)enrollment).IsResponsible = true;
          }
        }
        else
        {
          User user = await _userRepository.GetUserById(tenantId, enrollmentDto.UserId);
          if (user == null)
          {
            throw new ConflictException("El usuario " + enrollmentDto.UserId + " no existe");
          }

          var hasRequiredRole =
            user.Roles.Aggregate(false, (current, userRole) => current || userRole.Role == Role.Professor);

          if (!hasRequiredRole)
          {
            throw new ConflictException("El usuario " + enrollmentDto.UserId + " no tiene rol Profesor asociado");
          }

          Enrollment newEnrollment = new ProfessorEnrollment
          {
            Subject = subject,
            User = user,
            IsResponsible = enrollmentDto.IsResponsible
          };

          subject.Enrollments.Add(newEnrollment);
        }
      }

      if (subject.Enrollments.FirstOrDefault(e =>
        e.GetType() == typeof(ProfessorEnrollment) && ((ProfessorEnrollment)e).IsResponsible) == default)
      {
        throw new ConflictException("El curso debe tener un docente responsable");
      }

      await _subjectRepository.SaveChanges(subject);
    }


    public async Task AutoEnrollStudent(Guid tenantId, Guid subjectId, Guid userToEnroll)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollments(tenantId, subjectId);
      var isUserEnrolled = subject.Enrollments.Any(enrollment =>
        enrollment.User.Id.Equals(userToEnroll) && enrollment is StudentEnrollment);
      if (isUserEnrolled)
      {
        throw new ConflictException("El usuario ya esta inscripto al curso");
      }

      var user = await _userRepository.GetUserById(tenantId, userToEnroll);
      if (user == null)
      {
        throw new ConflictException("El usuario no existe");
      }

      if (subject.ValidateWithBedelia)
      {
        await ValidateInscriptionWithBedelias(user);
      }

      Enrollment newEnrollment = new StudentEnrollment { Subject = subject, User = user };

      subject.Enrollments.Add(newEnrollment);

      await _subjectRepository.SaveChanges(subject);
    }

    public async Task EnrollStudent(Guid tenantId, Guid subjectId, Guid enrollmentDtoUserId, Guid responsibleUserId)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollments(tenantId, subjectId);


      Enrollment responsibleUserEnrollment = null;
      var alreadyEnrolled = false;
      foreach (var enrollment in subject.Enrollments)
      {
        if (responsibleUserEnrollment == null && enrollment.User.Id == responsibleUserId)
        {
          responsibleUserEnrollment = enrollment;
        }

        if (enrollment.User.Id == enrollmentDtoUserId)
        {
          alreadyEnrolled = true;
        }
      }

      if (responsibleUserId != null && (responsibleUserEnrollment == null ||
                                        responsibleUserEnrollment.GetType() != typeof(ProfessorEnrollment)))
      {
        throw new AuthorizationException("El usuario no es docente del curso");
      }

      if (alreadyEnrolled)
      {
        throw new ConflictException("El usuario ya esta inscripto al curso");
      }

      var user = await _userRepository.GetUserById(tenantId, enrollmentDtoUserId);
      if (user == null)
      {
        throw new ConflictException("El usuario no existe");
      }


      if (subject.ValidateWithBedelia)
      {
        await ValidateInscriptionWithBedelias(user);
      }

      var hasRequiredRole =
        user.Roles.Aggregate(false, (current, userRole) => current || userRole.Role == Role.Student);

      if (!hasRequiredRole)
      {
        throw new ConflictException("El usuario no tiene rol Estudiante asociado");
      }

      Enrollment newEnrollment = new StudentEnrollment { Subject = subject, User = user };

      subject.Enrollments.Add(newEnrollment);

      await _subjectRepository.SaveChanges(subject);
    }

    private async Task ValidateInscriptionWithBedelias(User user)
    {
      var bedeliasResponseDto = await _bedeliasService.ValidateInscription(new ValidateInscriptionDto { CI = user.CI.ToString() });
      if (!bedeliasResponseDto.Approved)
      {
        throw new ValidationException("Bedelias rechazo su inscripcion.");
      }
    }

    public async Task<ReadEnrollmentListDto> GetSubjectEnrollments(Guid tenantId, Guid subjectId, Guid userId)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollments(tenantId, subjectId);

      // if (!userId.Equals(Guid.Empty))
      // {
      //   CheckIfUserIsEnrolled(userId, subject);
      // }

      var readEnrollmentListDto = new ReadEnrollmentListDto();

      foreach (var subjectEnrollment in subject.Enrollments)
      {
        if (subjectEnrollment.GetType() == typeof(ProfessorEnrollment))
        {
          var readProfessorEnrollmentDto = new ReadProfessorEnrollmentDto
          {
            Id = subjectEnrollment.Id.ToString(),
            User = _mapper.Map<ReadUserDto>(subjectEnrollment.User),
            IsResponsible = ((ProfessorEnrollment)subjectEnrollment).IsResponsible
          };
          readEnrollmentListDto.Professors.Add(readProfessorEnrollmentDto);
        }
        else
        {
          var readStudentEnrollmentDto = new ReadStudentEnrollmentDto
          {
            Id = subjectEnrollment.Id.ToString(),
            User = _mapper.Map<ReadUserDto>(subjectEnrollment.User)
          };
          readEnrollmentListDto.Students.Add(readStudentEnrollmentDto);
        }
      }

      return readEnrollmentListDto;
    }

    private static void CheckIfUserIsEnrolled(Guid userId, Subject subject)
    {
      var isEnrolled = subject.Enrollments.Any(enrollment => enrollment.User.Id.Equals(userId));
      if (!isEnrolled)
      {
        throw new AuthorizationException("El usuario no esta inscripto al curso");
      }
    }

    public async Task UpdateProfessorRole(Guid tenantId, Guid subjectId, Guid enrollmentId,
      bool isResponsible, Guid responsibleUserId)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollments(tenantId, subjectId);
      Enrollment responsibleUserEnrollment = null;
      Enrollment enrollmentToUpdate = null;
      foreach (var enrollment in subject.Enrollments)
      {
        if (responsibleUserEnrollment == null && enrollment.User.Id == responsibleUserId)
        {
          responsibleUserEnrollment = enrollment;
        }

        if (enrollment.Id == enrollmentId)
        {
          enrollmentToUpdate = enrollment;
        }
      }

      if (responsibleUserEnrollment == null || responsibleUserEnrollment.GetType() != typeof(ProfessorEnrollment)
                                            || !((ProfessorEnrollment)responsibleUserEnrollment).IsResponsible)
      {
        throw new AuthorizationException("El usuario no es responsable del curso");
      }

      if (enrollmentToUpdate == null || enrollmentToUpdate.GetType() != typeof(ProfessorEnrollment))
      {
        throw new ConflictException("No existe la inscripcion de profesor");
      }

      ((ProfessorEnrollment)enrollmentToUpdate).IsResponsible = isResponsible;

      await _subjectRepository.SaveChanges(subject);
    }

    public async Task DeleteEnrollment(Guid tenantId, Guid subjectId, Guid enrollmentId, Guid responsibleUserId)
    {
      var subject = await _subjectRepository.FindSubjectWithEnrollments(tenantId, subjectId);
      Enrollment responsibleUserEnrollment = null;
      Enrollment enrollmentToDelete = null;
      foreach (var enrollment in subject.Enrollments)
      {
        if (responsibleUserEnrollment == null && enrollment.User.Id == responsibleUserId)
        {
          responsibleUserEnrollment = enrollment;
        }

        if (enrollment.Id == enrollmentId)
        {
          enrollmentToDelete = enrollment;
        }
      }

      if (responsibleUserEnrollment == null || responsibleUserEnrollment.GetType() != typeof(ProfessorEnrollment)
                                            || !((ProfessorEnrollment)responsibleUserEnrollment).IsResponsible)
      {
        throw new AuthorizationException("El usuario no es responsable del curso");
      }

      if (enrollmentToDelete == null)
      {
        throw new ConflictException("No existe la inscripcion");
      }

      subject.Enrollments.Remove(enrollmentToDelete);

      await _subjectRepository.SaveChanges(subject);
    }
  }
}
