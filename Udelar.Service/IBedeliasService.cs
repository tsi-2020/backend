﻿using System.Threading.Tasks;
using Udelar.Common.Dtos.Bedelias.Request;
using Udelar.Common.Dtos.Bedelias.Response;

namespace Udelar.Services
{
  public interface IBedeliasService
  {
    Task<BedeliasResponseDto> ValidateInscription(ValidateInscriptionDto validateInscriptionDto);
  }
}
