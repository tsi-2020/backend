﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Forum.Request;
using Udelar.Common.Dtos.Forum.Response;

namespace Udelar.Services
{
  public interface IForumService
  {
    Task CreateForum(CreateForumDto createForumDto, Guid tenantId);

    Task EditForum(EditForumDto editForumDto, Guid tenantId, string forumId);

    Task DeleteForum(Guid tenantId, string forumId);

    Task<PaginatedForumDto> ListForum(Guid tenantId, FilterForumDto filterForumDto);

    Task<ViewForumDto> ViewForum(Guid tenantId, string forumId);

  }
}
