﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.SubjectTemplate.request;
using Udelar.Common.Dtos.SubjectTemplate.response;

namespace Udelar.Services
{
  public interface ISubjectTemplateService
  {
    Task CreateSubjectTemplate(Guid universityId, CreateSubjectTemplateDto newSubjectTemplate);
    Task<List<ReadSubjectTemplateDto>> GetAllSubjectTemplates(Guid universityId);
    Task<object> GetSubjectTemplateById(Guid id, Guid templateId);
  }
}
