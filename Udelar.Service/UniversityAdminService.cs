﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AutoMapper;
using Udelar.Common.Dtos.UniversityAdministrator.Reponse;
using Udelar.Common.Dtos.UniversityAdministrator.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;

namespace Udelar.Services
{
  public class UniversityAdminService : IUniversityAdminService
  {
    private readonly IAdminRepository _adminRepo;
    private readonly IAuthService _authService;
    private readonly IMapper _mapper;
    private readonly IUniversitiesRepository _universityRepo;

    public UniversityAdminService(IMapper mapper, IAuthService authService,
      IAdminRepository adminRepository, IUniversitiesRepository universitiesRepository)
    {
      _mapper = mapper;
      _authService = authService;
      _adminRepo = adminRepository;
      _universityRepo = universitiesRepository;
    }

    public async Task<ReadUniversityAdminDto> CreateAdmin(CreateUniversityAdminDto createUniversityAdminDto,
      Guid universityId)
    {
      if (await IsEmailTaken(createUniversityAdminDto.Email, universityId))
      {
        throw new ConflictException("Email en uso.");
      }

      var university = await _universityRepo.FindUniversityById(universityId);

      if (university == null)
      {
        throw new EntityNotFoundException("Universidad no encontrada.");
      }

      var administrator = _mapper.Map<UniversityAdministrator>(createUniversityAdminDto);
      administrator.University = university;
      administrator.Password = _authService.HashPassword(administrator.Password);
      return _mapper.Map<ReadUniversityAdminDto>(await _adminRepo.AddUniversityAdministrator(administrator));
    }

    public async Task<ReadUniversityAdminDto> GetAdminById(Guid userId, Guid universityId)
    {
      var admin = await _adminRepo.FindByIdAndUniversity(userId, universityId);
      if (admin == null)
      {
        throw new EntityNotFoundException("Usuario no encontrado.");
      }

      return _mapper.Map<ReadUniversityAdminDto>(admin);
    }

    public async Task<List<ReadUniversityAdminDto>> ListAdmins(Guid tenantId)
    {
      return _mapper.Map<List<ReadUniversityAdminDto>>(await _adminRepo.ListUniversityAdministrators(tenantId));
    }

    public async Task ChangePassword(UpdateUniversityAdminPasswordDto passwordDto, Guid userId)
    {
      var admin = await _adminRepo.FindById(userId);
      if (admin == null)
      {
        throw new EntityNotFoundException("Usuario no encontrado.");
      }

      admin.Password = _authService.HashPassword(passwordDto.NewPassword);
      await _adminRepo.SaveChanges(admin);
    }

    public async Task DeleteUniversityAdmin(Guid tenantId, Guid userId)
    {
      var admin = await _adminRepo.FindByIdAndUniversity(userId, tenantId);
      if (admin == null)
      {
        throw new EntityNotFoundException("Administrador no encontrado");
      }
      if ((await _adminRepo.ListUniversityAdministrators(tenantId)).Count == 1)
      {
        throw new ConflictException("No se puede eliminar el unico Administrador");
      }
      admin.DeletedOn = DateTime.Now;
      await _adminRepo.SaveChanges(admin);
    }

    public async Task UpdateUniversityAdminPersonalData(Guid tenantId, Guid userId,
      ChangePersonalDataUniversityAdminDto adminData)
    {
      var admin = await _adminRepo.FindByIdAndUniversity(userId, tenantId);
      if (admin == null)
      {
        throw new EntityNotFoundException("Usuario no encontrado.");
      }

      admin.FirstName = adminData.FirstName;
      admin.LastName = adminData.LastName;
      await _adminRepo.SaveChanges(admin);
    }

    private async Task<bool> IsEmailTaken(string email, Guid universityId)
    {
      return await _adminRepo.FindByEmailAndUniversityId(email, universityId) != null;
    }
  }
}
