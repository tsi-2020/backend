﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Udelar.Common.Dtos.UniversityAdministrator.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;

namespace Uderlar.Service.Tests
{
  public class UniversityAdminServiceTest
  {
    public UniversityAdminServiceTest()
    {
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));
      _mockedAdminRepo = new Mock<IAdminRepository>();
      _mockedUniversityRepo = new Mock<IUniversitiesRepository>();
      _mockAuthService = new Mock<IAuthService>();

      _universityAdminService = new UniversityAdminService(
        mapper,
        _mockAuthService.Object,
        _mockedAdminRepo.Object,
        _mockedUniversityRepo.Object);
    }

    private readonly UniversityAdminService _universityAdminService;
    private readonly Mock<IAdminRepository> _mockedAdminRepo;
    private readonly Mock<IUniversitiesRepository> _mockedUniversityRepo;
    private readonly Mock<IAuthService> _mockAuthService;
    private readonly Fixture _fixture = new Fixture();

    [Fact]
    public async Task Should_ChangePassword_When_CorrectPasswordProvided()
    {
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      _mockedAdminRepo
        .Setup(x => x.FindById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<UniversityAdministrator>());
      _mockAuthService
        .Setup(x => x.VerifyPassword(It.IsAny<string>(), It.IsAny<string>()))
        .Returns(true);


      await _universityAdminService.ChangePassword(_fixture.Create<UpdateUniversityAdminPasswordDto>(),
        _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_Fail_When_UserNotFound()
    {
      _mockedAdminRepo
        .Setup(x => x.FindById(It.IsAny<Guid>()))
        .ReturnsAsync((UniversityAdministrator)null);

      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _universityAdminService.ChangePassword(_fixture.Create<UpdateUniversityAdminPasswordDto>(),
          _fixture.Create<Guid>()));
    }


    [Fact]
    public async Task Should_FailToFindUser_When_UserNotFound()
    {
      _mockedAdminRepo
        .Setup(x => x.FindById(It.IsAny<Guid>()))
        .ReturnsAsync((UniversityAdministrator)null);
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _universityAdminService.GetAdminById(_fixture.Create<Guid>(), _fixture.Create<Guid>()));
    }


    [Fact]
    public async Task Should_ReturnAdminDto_When_UserIsFound()
    {
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      _mockedAdminRepo
        .Setup(x => x.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<UniversityAdministrator>());
      Assert.NotNull(await _universityAdminService.GetAdminById(_fixture.Create<Guid>(), _fixture.Create<Guid>()));
    }


    [Fact]
    public async Task Should_ReturnEmptyListOfDto_When_NoUsersAreFound()
    {
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      _mockedAdminRepo
        .Setup(x => x.ListUniversityAdministrators(It.IsAny<Guid>()))
        .ReturnsAsync(
          new List<UniversityAdministrator>()
        );
      Assert.Empty(await _universityAdminService.ListAdmins(_fixture.Create<Guid>()));
    }


    [Fact]
    public async Task Should_ReturnListOfDto_Always()
    {
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      _mockedAdminRepo
        .Setup(x => x.ListUniversityAdministrators(It.IsAny<Guid>()))
        .ReturnsAsync(
          _fixture.Create<List<UniversityAdministrator>>()
        );
      Assert.NotEmpty(await _universityAdminService.ListAdmins(_fixture.Create<Guid>()));
    }

    [Fact]
    public async Task Should_ReturnListOfDto_When_UserCreated()
    {
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      _fixture.Customize<University>(c =>
        c.Without(p => p.Careers)
          .Without(p => p.Notices)
          .Without(p => p.UniversityAdministrators)
          .Without(p => p.SubjectTemplates)
          .Without(p => p.UniversityPollPublications)
      );
      _mockAuthService
        .Setup(x => x.HashPassword(It.IsAny<string>()))
        .Returns(_fixture.Create<string>());
      _mockedUniversityRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());
      _mockedAdminRepo
        .Setup(x => x.AddUniversityAdministrator(It.IsAny<UniversityAdministrator>()))
        .ReturnsAsync(_fixture.Create<UniversityAdministrator>());
      _mockedAdminRepo
        .Setup(x => x.FindByEmailAndUniversityId(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync((UniversityAdministrator)null);
      var result =
        await _universityAdminService.CreateAdmin(_fixture.Create<CreateUniversityAdminDto>(),
          _fixture.Create<Guid>());
      Assert.NotNull(result);
    }

    [Fact]
    public async Task Should_Throw_When_CiIsTaken()
    {
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      _mockedAdminRepo
        .Setup(x => x.FindByEmailAndUniversityId(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          _fixture.Create<UniversityAdministrator>
        );
      await Assert.ThrowsAsync<ConflictException>(() =>
        _universityAdminService.CreateAdmin(_fixture.Create<CreateUniversityAdminDto>(), _fixture.Create<Guid>()));
    }


    [Fact]
    public async Task Should_Throw_When_UniversityNotFound()
    {
      _mockedUniversityRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync((University)default);
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _universityAdminService.CreateAdmin(_fixture.Create<CreateUniversityAdminDto>(), _fixture.Create<Guid>()));
    }


    [Fact]
    public async Task Should_Throw_When_DeletingANonFoundUniversity()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _universityAdminService.DeleteUniversityAdmin(_fixture.Create<Guid>(), _fixture.Create<Guid>()));
    }

    [Fact]
    public async Task Should_DeleteAdmin_When_AdminFound()
    {
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      _mockedAdminRepo
        .Setup(x => x.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          _fixture.Create<UniversityAdministrator>
        );
      _mockedAdminRepo
        .Setup(x => x.ListUniversityAdministrators(It.IsAny<Guid>()))
        .ReturnsAsync(
          _fixture.Create<List<UniversityAdministrator>>()
        );
      await _universityAdminService.DeleteUniversityAdmin(_fixture.Create<Guid>(), _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_FailDeleteAdmin_When_OnlyAdmin()
    {
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      _mockedAdminRepo
        .Setup(x => x.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          _fixture.Create<UniversityAdministrator>
        );
      _mockedAdminRepo
        .Setup(x => x.ListUniversityAdministrators(It.IsAny<Guid>()))
        .ReturnsAsync(new List<UniversityAdministrator> { new UniversityAdministrator() }
        );
      await Assert.ThrowsAsync<ConflictException>(() => _universityAdminService.DeleteUniversityAdmin(_fixture.Create<Guid>(), _fixture.Create<Guid>()));
    }


    [Fact]
    public async Task Should_Throw_When_UpdatingANonFoundUniversity()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _universityAdminService.UpdateUniversityAdminPersonalData(_fixture.Create<Guid>(), _fixture.Create<Guid>(), _fixture.Create<ChangePersonalDataUniversityAdminDto>()));
    }


    [Fact]
    public async Task Should_UpdateAdminPersonalData_When_AdminFound()
    {
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      _mockedAdminRepo
        .Setup(x => x.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          _fixture.Create<UniversityAdministrator>
        );
      await _universityAdminService.UpdateUniversityAdminPersonalData(_fixture.Create<Guid>(), _fixture.Create<Guid>(), _fixture.Create<ChangePersonalDataUniversityAdminDto>());
    }

  }
}
