﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Udelar.Common.Dtos.Component.Request;
using Udelar.Common.Dtos.SubjectTemplate.request;
using Udelar.Model;
using Udelar.Model.Component;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class SubjectTemplateServiceTest
  {
    private readonly Mock<ISubjectTemplateRepository> _mockedTemplateRepo;
    private readonly Mock<IUniversitiesRepository> _mockedUniversitiesRepo;
    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly ISubjectTemplateService _service;

    public SubjectTemplateServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));

      _mockedTemplateRepo = new Mock<ISubjectTemplateRepository>();
      _mockedUniversitiesRepo = new Mock<IUniversitiesRepository>();

      _service = new SubjectTemplateService(mapper, _mockedUniversitiesRepo.Object, _mockedTemplateRepo.Object);
    }


    [Fact]
    public async Task Should_Create_When_ValidData()
    {
      _mockedUniversitiesRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(new University { SubjectTemplates = new List<SubjectTemplate>() });

      await _service.CreateSubjectTemplate(_fixture.Create<Guid>(), new CreateSubjectTemplateDto
      {
        EducationalUnits = new List<CreateEducationalUnitDto>
        {
          new CreateEducationalUnitDto
          {
            AssigmentComponents = new List<CreateAssigmentComponentDto> {new CreateAssigmentComponentDto()},
            DocumentComponents = new List<CreateDocumentComponentDto> {new CreateDocumentComponentDto()},
            ForumComponents = new List<CreateForumComponentDto> {new CreateForumComponentDto()},
            LinkComponents = new List<CreateLinkComponentDto> {new CreateLinkComponentDto()},
            PollComponents = new List<CreatePollComponentDto> {new CreatePollComponentDto()},
            QuizComponents = new List<CreateQuizComponentDto> {new CreateQuizComponentDto()},
            TextComponents = new List<CreateTextComponentDto> {new CreateTextComponentDto()},
            VideoComponents = new List<CreateVideoComponentDto> {new CreateVideoComponentDto()},
            VirtualMeetingComponents =
              new List<CreateVirtualMeetingComponentDto> {new CreateVirtualMeetingComponentDto()},

          }
        }
      });
    }

    [Fact]
    public async Task Should_ReturnAll_When_ValidData()
    {
      _mockedTemplateRepo
        .Setup(x => x.GetAllSubjectTemplates((It.IsAny<Guid>())))
        .ReturnsAsync(new List<SubjectTemplate> { new SubjectTemplate() });

      await _service.GetAllSubjectTemplates(_fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_ReturnById_When_ValidData()
    {
      _mockedTemplateRepo
        .Setup(x => x.GetSubjectTemplateById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new SubjectTemplate
        {
          Components = new List<EducationalUnit>{ new EducationalUnit{Components = new List<Component>
          {
            new AssignmentComponent(),
            new DocumentComponent(),
            new ForumComponent(),
            new LinkComponent(),
            new PollComponent(),
            new QuizComponent(),
            new TextComponent(),
            new VideoComponent(),
            new VirtualMeetingComponent()
          }}}
        });

      await _service.GetSubjectTemplateById(_fixture.Create<Guid>(), _fixture.Create<Guid>());
    }


  }
}
