﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using Microsoft.Extensions.Configuration;
using Moq;
using Udelar.Common.Dtos.Auth.Rquest;
using Udelar.Common.Dtos.User.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Xunit;

namespace Uderlar.Service.Tests
{
  public class AuthServiceTest
  {
    public AuthServiceTest()
    {
      _mockConfiguration = new Mock<IConfiguration>();
      _mockedUserRepo = new Mock<IUserRepository>();
      _mockedAdminRepo = new Mock<IAdminRepository>();
      _emailMock = new Mock<IEmailSender>();
      _authService = new AuthService(_mockConfiguration.Object, _mockedAdminRepo.Object, _mockedUserRepo.Object, _emailMock.Object);
    }

    private readonly Mock<IConfiguration> _mockConfiguration;
    private readonly Mock<IAdminRepository> _mockedAdminRepo;
    private readonly Mock<IUserRepository> _mockedUserRepo;
    private readonly AuthService _authService;
    private readonly Fixture _fixture = new Fixture();
    private readonly Mock<IEmailSender> _emailMock;

    [Fact]
    public async Task Should_CreateCredentials_When_ValidPassword()
    {
      _mockConfiguration
        .Setup(x => x.GetSection("AppSettings:Token").Value)
        .Returns("Super Secret Key");
      _mockedAdminRepo
        .Setup(x => x.FindByEmail(It.IsAny<string>()))
        .ReturnsAsync(new UniversityAdministrator
        {
          Password = BCrypt.Net.BCrypt.HashPassword("123"),
          University = new University { Id = new Guid() },
          Email = "random@random.com",
          FirstName = "Some Name",
          LastName = "Some Name"
        });
      var credentialsDto =
        await _authService.AdminLogin(new BackOfficeLoginDto { Password = "123", Email = "random@random.com" });
      Assert.NotEmpty(credentialsDto.AccessToken);
      Assert.NotEmpty(credentialsDto.RefreshToken);
    }


    [Fact]
    public async Task Should_CreateCredentials_When_ValidPassword_AtUserLogin()
    {
      _mockConfiguration
        .Setup(x => x.GetSection("AppSettings:Token").Value)
        .Returns("Super Secret Key");
      _mockedUserRepo
        .Setup(x => x.FindByCI(It.IsAny<int>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User
        {
          Password = BCrypt.Net.BCrypt.HashPassword("123"),
          University = new University { Id = new Guid() },
          Email = "random@random.com",
          FirstName = "Some Name",
          LastName = "Some Name",
          Roles = new List<UserRole> { new UserRole { Role = Role.Professor }, new UserRole { Role = Role.Student } }
        });
      var credentialsDto =
        await _authService.UserLogin(new FrontOfficeLoginDto { Password = "123", CI = _fixture.Create<int>() },
          _fixture.Create<Guid>());
      Assert.NotEmpty(credentialsDto.AccessToken);
      Assert.NotEmpty(credentialsDto.RefreshToken);
    }

    [Fact]
    public void Should_Fail_When_InvalidHashIsPassed()
    {
      var hash = _authService.HashPassword("hassh");
      Assert.True(_authService.VerifyPassword("hassh", hash));
    }

    [Fact]
    public async Task Should_FailToUpdateToken_When_RefreshTokenIsBad()
    {
      _mockConfiguration
        .Setup(x => x.GetSection("AppSettings:Token").Value)
        .Returns("Super Secret Key");
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      _mockedAdminRepo
        .Setup(x => x.FindByRefreshToken(It.IsAny<string>()))
        .ReturnsAsync(_fixture.Create<UniversityAdministrator>());

      await Assert.ThrowsAsync<ValidationException>(() =>
        _authService.RefreshToken(_fixture.Create<RefreshTokenDto>()));
    }

    [Fact]
    public async Task Should_FailToUpdateToken_When_UserNotFound()
    {
      _mockedAdminRepo
        .Setup(x => x.FindByRefreshToken(It.IsAny<string>()))
        .ReturnsAsync((UniversityAdministrator)null);

      await Assert.ThrowsAsync<ValidationException>(() =>
        _authService.RefreshToken(_fixture.Create<RefreshTokenDto>()));
    }

    [Fact]
    public void Should_ReturnFalse_When_GeneratedTokenIsValid()
    {
      _mockConfiguration
        .Setup(x => x.GetSection("AppSettings:Token").Value)
        .Returns("Super Secret Key");
      Assert.False(_authService.ValidateToken("invalidToken"));
    }

    [Fact]
    public async Task Should_Throw_When_InvalidPassword()
    {
      _mockedAdminRepo
        .Setup(x => x.FindByEmail(It.IsAny<string>()))
        .ReturnsAsync(new UniversityAdministrator { Password = BCrypt.Net.BCrypt.HashPassword("123") });
      await Assert.ThrowsAsync<AuthenticationException>(() =>
        _authService.AdminLogin(new BackOfficeLoginDto { Password = "fake", Email = "random@random.com" }));
    }

    [Fact]
    public async Task Should_Throw_When_InvalidPassword_AtUserLogin()
    {
      _mockConfiguration
        .Setup(x => x.GetSection("AppSettings:Token").Value)
        .Returns("Super Secret Key");
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
          .Without(p => p.Roles)
      );
      _mockedUserRepo
        .Setup(x => x.FindByCI(It.IsAny<int>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<User>());
      await Assert.ThrowsAsync<AuthenticationException>(() =>
        _authService.UserLogin(_fixture.Create<FrontOfficeLoginDto>(), _fixture.Create<Guid>()));
    }

    [Fact]
    public async Task Should_Throw_When_UserNotFound()
    {
      _mockedAdminRepo
        .Setup(x => x.FindByEmail(It.IsAny<string>()))
        .ReturnsAsync((UniversityAdministrator)null);
      await Assert.ThrowsAsync<AuthenticationException>(() =>
        _authService.AdminLogin(_fixture.Create<BackOfficeLoginDto>()));
    }


    [Fact]
    public async Task Should_Throw_When_UserNotFound_AtUserLogin()
    {
      _mockedUserRepo
        .Setup(x => x.FindByCI(It.IsAny<int>(), It.IsAny<Guid>()))
        .ReturnsAsync((User)null);
      await Assert.ThrowsAsync<AuthenticationException>(() =>
        _authService.UserLogin(_fixture.Create<FrontOfficeLoginDto>(), _fixture.Create<Guid>()));
    }

    [Fact]
    public async Task Should_UpdateToken_When_RefreshTokenIsOk()
    {
      _fixture.Customize<UniversityAdministrator>(c =>
        c.Without(p => p.University)
      );
      var administrator = new UniversityAdministrator
      {
        Password = BCrypt.Net.BCrypt.HashPassword("123"),
        University = new University { Id = new Guid() },
        Email = "random@random.com",
        FirstName = "Some Name",
        LastName = "Some Name"
      };
      _mockConfiguration
        .Setup(x => x.GetSection("AppSettings:Token").Value)
        .Returns("Super Secret Key");
      _mockedAdminRepo
        .Setup(x => x.FindByEmail(It.IsAny<string>()))
        .ReturnsAsync(administrator);
      var credentialsDto =
        await _authService.AdminLogin(new BackOfficeLoginDto { Password = "123", Email = "random@random.com" });

      _mockedAdminRepo
        .Setup(x => x.FindByRefreshToken(It.IsAny<string>()))
        .ReturnsAsync(administrator);
      var mock = new Mock<AuthService>(MockBehavior.Strict,
        new AuthService(_mockConfiguration.Object, _mockedAdminRepo.Object, _mockedUserRepo.Object, _emailMock.Object));
      Assert.NotNull(
        await _authService.RefreshToken(new RefreshTokenDto { RefreshToken = credentialsDto.RefreshToken }));
    }


    [Fact]
    public async Task Should_UpdateToken_When_RefreshTokenIsOk2()
    {
      _fixture.Customize<User>(c =>
        c
          .Without(p => p.University)
          .Without(p => p.Roles)
      );
      var user = new User
      {
        Password = BCrypt.Net.BCrypt.HashPassword("123"),
        University = new University { Id = new Guid() },
        Email = "random@random.com",
        FirstName = "Some Name",
        LastName = "Some Name",
        CI = 1234568,
        Roles = new List<UserRole> { new UserRole { Role = Role.Professor } }
      };
      _mockConfiguration
        .Setup(x => x.GetSection("AppSettings:Token").Value)
        .Returns("Super Secret Key");
      _mockedUserRepo
        .Setup(x => x.FindByCI(It.IsAny<int>(), It.IsAny<Guid>()))
        .ReturnsAsync(user);
      _mockedUserRepo
        .Setup(x => x.FindByRefreshToken(It.IsAny<string>()))
        .ReturnsAsync(user);
      var credentialsDto =
        await _authService.UserLogin(new FrontOfficeLoginDto { Password = "123", CI = 12345678 }, _fixture.Create<Guid>());

      var mock = new Mock<AuthService>(MockBehavior.Strict,
        new AuthService(_mockConfiguration.Object, _mockedAdminRepo.Object, _mockedUserRepo.Object, _emailMock.Object));
      Assert.NotNull(
        await _authService.RefreshToken(new RefreshTokenDto { RefreshToken = credentialsDto.RefreshToken }));
    }


    [Fact]
    public void Should_VerifyHash_When_ValidHashIsPassed()
    {
      var hash = _authService.HashPassword("hassh");
      Assert.True(_authService.VerifyPassword("hassh", hash));
    }

    [Fact]
    public void Should_RequestLinkToRestPassword()
    {

      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
          .ForEach(b => _fixture.Behaviors.Remove(b));
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());

      _fixture.Customize<User>(c =>
        c
          .Without(p => p.University)
          .Without(p => p.Roles)
          .Without(p => p.Assignments)
          .Without(p => p.Inscriptions)
          .Without(p => p.QuizResponses)
          .Without(p => p.PollsResponses)
          .Without(p => p.UserGroups)
      );
      _mockedUserRepo
        .Setup(x => x.FindByCI(It.IsAny<int>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<User>());
      _authService.RequestForgotPasswordLink(_fixture.Create<RequestForgotPasswordDto>(), _fixture.Create<Guid>());
    }
  }
}
