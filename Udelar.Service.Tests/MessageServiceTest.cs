﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Udelar.Common.Dtos.Message.Request;
using Udelar.Common.Dtos.Thread.Request;
using Udelar.Common.Exceptions;
using Udelar.Model.Mongo;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class MessageServiceTest
  {
    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly MessageService _messageService;
    private readonly Mock<IThreadRepository> _threadRepoMock;
    private readonly Mock<IMessageRepository> _messageRepoMock;

    public MessageServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));

      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
        .ForEach(b => _fixture.Behaviors.Remove(b));
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _threadRepoMock = new Mock<IThreadRepository>();
      _messageRepoMock = new Mock<IMessageRepository>();
      _messageService = new MessageService(_messageRepoMock.Object, _threadRepoMock.Object, mapper);
    }

    [Fact]
    public async Task Should_CreateMessage()
    {
      _threadRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new ForumThread(true));

      _messageRepoMock
        .Setup(r => r.Create(It.IsAny<CreateMessageDto>(), It.IsAny<string>(), It.IsAny<Guid>(), It.IsAny<ForumThread>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Message());

      await
        _messageService.CreateMessage(_fixture.Create<CreateMessageDto>(), _fixture.Create<string>(),
          _fixture.Create<Guid>(), _fixture.Create<string>(), _fixture.Create<Guid>()
        );
    }


    [Fact]
    public async Task Should_Fail_CreateThread_When_ForumNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _messageService.CreateMessage(_fixture.Create<CreateMessageDto>(), _fixture.Create<string>(),
          _fixture.Create<Guid>(), _fixture.Create<string>(), _fixture.Create<Guid>())
      );
    }

    [Fact]
    public async Task Should_Fail_ListMessages_When_ThreadNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _messageService.ListMessages(_fixture.Create<Guid>(),
          _fixture.Create<string>()
          , _fixture.Create<FilterMessageDto>())
      );
    }

    [Fact]
    public async Task Should_ListThreads()
    {
      _threadRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new ForumThread(true));
      _messageRepoMock
        .Setup(r => r.List(It.IsAny<ForumThread>(),
          It.IsAny<int>(), It.IsAny<int>()))
        .ReturnsAsync(() =>
          new Tuple<long, List<Message>>(1, new List<Message> { new Message() })
        );
      await _messageService.ListMessages(_fixture.Create<Guid>(), _fixture.Create<string>(),
        _fixture.Create<FilterMessageDto>());
    }

    [Fact]
    public async Task Should_EditMessage()
    {
      _messageRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Message { UserId = Guid.Empty });
      await _messageService.EditMessage(_fixture.Create<EditMessageDto>(), _fixture.Create<string>(),
        _fixture.Create<Guid>(), Guid.Empty);
    }

    [Fact]
    public async Task Should_Fail_EditMessage_When_UserIsNotOwner()
    {
      _messageRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Message { UserId = Guid.Empty });
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _messageService.EditMessage(_fixture.Create<EditMessageDto>(), _fixture.Create<string>(),
          _fixture.Create<Guid>(), Guid.NewGuid())
      );
    }

    [Fact]
    public async Task Should_Fail_EditMessage_When_MessageNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _messageService.EditMessage(_fixture.Create<EditMessageDto>(), _fixture.Create<string>(),
          _fixture.Create<Guid>(), Guid.NewGuid())
      );
    }

    //
    //     [Fact]
    //     public async Task Should_ViewThread()
    //     {
    //       _threadRepoMock
    //         .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
    //         .ReturnsAsync(new ForumThread(true));
    //       _threadRepoMock
    //         .Setup(r => r.GetThreadMessages(It.IsAny<ForumThread>(), It.IsAny<int>(), It.IsAny<int>()))
    //         .ReturnsAsync(
    //           new Tuple<long, List<Message>>(1, new List<Message>
    //           {
    //             new Message()
    //           })
    // );
    //       await _messageService.ViewThread(_fixture.Create<Guid>(),
    //         _fixture.Create<string>(), _fixture.Create<FilterThreadDto>());
    //     }
    //
    //     [Fact]
    //     public async Task Should_Fail_ViewThread_When_ThreadNotFound()
    //     {
    //       await Assert.ThrowsAsync<EntityNotFoundException>(() =>
    //         _messageService.ViewThread(_fixture.Create<Guid>(),
    //         _fixture.Create<string>(), _fixture.Create<FilterThreadDto>())
    //       );
    //     }
  }
}
