﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.Kernel;
using AutoMapper;
using MongoDB.Entities;
using Moq;
using Udelar.Common.Dtos.Forum.Request;
using Udelar.Common.Dtos.User.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Model.Mongo;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class ForumServiceTest
  {
    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly ForumService _forumService;
    private readonly Mock<IForumRepository> _forumRepoMock;
    private readonly Mock<ISubjectRepository> _subjectRepoMock;

    public ForumServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));

      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
        .ForEach(b => _fixture.Behaviors.Remove(b));
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _forumRepoMock = new Mock<IForumRepository>();
      _subjectRepoMock = new Mock<ISubjectRepository>();
      _forumService = new ForumService(_subjectRepoMock.Object, _forumRepoMock.Object, mapper);
    }

    [Fact]
    public async Task Should_CreateForum()
    {
      _fixture.Customize<Subject>(c =>
        c
          .Without(p => p.CareerSubjects)
          .Without(p => p.Enrollments)
          .Without(p => p.Quizzes)
          .Without(p => p.SubjectPollPublications)
          .Without(p => p.Assignments)
          .Without(p => p.Components)
          .Without(p => p.Forums)
          .Without(p => p.Notices)
          .Without(p => p.University)
      );
      _subjectRepoMock
        .Setup(r => r.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<Subject>());
      await _forumService.CreateForum(_fixture.Create<CreateForumDto>(), _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_Fail_CreateForum_When_SubjectNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _forumService.CreateForum(_fixture.Create<CreateForumDto>(), _fixture.Create<Guid>())
      );
    }


    [Fact]
    public async Task Should_Fail_EditForum_When_ForumNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _forumService.EditForum(_fixture.Create<EditForumDto>(), _fixture.Create<Guid>(), _fixture.Create<string>())
      );
    }

    [Fact]
    public async Task Should_EditForum()
    {
      _forumRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Forum(true));
      await _forumService.EditForum(_fixture.Create<EditForumDto>(), _fixture.Create<Guid>(),
        _fixture.Create<string>());
    }


    [Fact]
    public async Task Should_Fail_DeleteForum_When_ForumNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _forumService.DeleteForum(_fixture.Create<Guid>(), _fixture.Create<string>())
      );
    }

    [Fact]
    public async Task Should_DeleteForum()
    {
      _forumRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Forum(true));
      await _forumService.DeleteForum(_fixture.Create<Guid>(), _fixture.Create<string>());
    }


    [Fact]
    public async Task Should_ListForums()
    {
      _forumRepoMock
        .Setup(r => r.List(It.IsAny<Guid>(),
          It.IsAny<int>(), It.IsAny<int>()))
        .ReturnsAsync(() =>
          new Tuple<long, List<Forum>>(1, new List<Forum>
          {
            new Forum(true)
          })
        );
      await _forumService.ListForum(_fixture.Create<Guid>(), _fixture.Create<FilterForumDto>());
    }

    [Fact]
    public async Task Should_Fail_ViewForum_When_ForumNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _forumService.ViewForum(_fixture.Create<Guid>(), _fixture.Create<string>())
      );
    }

    [Fact]
    public async Task Should_ViewForum()
    {
      _forumRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Forum(true));

      _forumRepoMock
        .Setup(r => r.GetForumThreads(It.IsAny<Forum>()))
        .ReturnsAsync(new List<ForumThread> { new ForumThread(true) });

      await _forumService.ViewForum(_fixture.Create<Guid>(), _fixture.Create<string>());
    }
  }
}
