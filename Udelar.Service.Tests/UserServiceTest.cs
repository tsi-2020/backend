﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Udelar.Common.Dtos.User.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class UserServiceTest
  {
    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly Mock<IUserRepository> _mockedUserRepo;
    private readonly UserService _userService;

    public UserServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));

      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
        .ForEach(b => _fixture.Behaviors.Remove(b));
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());

      _mockedUserRepo = new Mock<IUserRepository>();
      var mockAuthService = new Mock<IAuthService>();
      var mockUniversityRepo = new Mock<IUniversitiesRepository>();
      var mockEmailSender = new Mock<IEmailSender>();
      _userService = new UserService(mapper, _mockedUserRepo.Object, mockAuthService.Object,
        mockUniversityRepo.Object, mockEmailSender.Object);
    }

    [Fact]
    public async Task Should_CreateUser()
    {
      await _userService.CreateUser(_fixture.Create<CreateUserDto>(), _fixture.Create<List<Role>>(), Guid.NewGuid());
    }

    [Fact]
    public async Task Should_Fail_ToCreateUser_When_EmailTaken()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
          .Without(p => p.Roles)
      );
      _mockedUserRepo
        .Setup(r => r.FindByEmail(It.IsAny<string>(), It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<User>());
      await Assert.ThrowsAsync<ConflictException>(() =>
        _userService.CreateUser(_fixture.Create<CreateUserDto>(), _fixture.Create<List<Role>>(), Guid.NewGuid())
      );
    }

    [Fact]
    public async Task Should_Fail_ToCreateUser_When_CITaken()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
          .Without(p => p.Roles)
      );
      _mockedUserRepo
        .Setup(r => r.FindByCI(It.IsAny<int>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<User>());
      await Assert.ThrowsAsync<ConflictException>(() =>
        _userService.CreateUser(_fixture.Create<CreateUserDto>(), _fixture.Create<List<Role>>(), Guid.NewGuid())
      );
    }

    [Fact]
    public async Task Should_ListUsers()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
          .Without(p => p.Roles)
      );
      _mockedUserRepo
        .Setup(r => r.GetUsersByRole(It.IsAny<Guid>(), It.IsAny<int>(),
          It.IsAny<int>(),
          It.IsAny<Role>(),
          It.IsAny<string>()
        ))
        .ReturnsAsync(_fixture.Create<Tuple<int, List<User>>>());
      var filterUserDto = _fixture.Create<FilterUserDto>();
      filterUserDto.RoleFilter = "Student";
      await _userService.GetUsers(_fixture.Create<Guid>(), filterUserDto);
    }

    [Fact]
    public async Task Should_ListUsersWithOutRoleFilter()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
          .Without(p => p.Roles)
      );
      _mockedUserRepo
        .Setup(r => r.GetUsers(It.IsAny<Guid>(), It.IsAny<int>(),
          It.IsAny<int>(),
          It.IsAny<string>()
        ))
        .ReturnsAsync(_fixture.Create<Tuple<int, List<User>>>());
      var filterUserDto = _fixture.Create<FilterUserDto>();
      filterUserDto.RoleFilter = "";
      await _userService.GetUsers(_fixture.Create<Guid>(), filterUserDto);
    }

    [Fact]
    public async Task Should_GetUserById()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
      );
      _mockedUserRepo
        .Setup(r => r.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<User>());
      await _userService.GetUserById(_fixture.Create<Guid>(), _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_Fail_ToGetUserById_When_UserDoesNotExist()
    {
      _mockedUserRepo
        .Setup(r => r.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(() => default);

      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _userService.GetUserById(_fixture.Create<Guid>(), _fixture.Create<Guid>())
      );
    }

    [Fact]
    public async Task Should_Fail_ToUpdateUser_When_UserDoesNotExist()
    {
      _mockedUserRepo
        .Setup(r => r.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(() => default);

      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _userService.UpdateUserData(_fixture.Create<Guid>(), _fixture.Create<Guid>(), _fixture.Create<UpdateUserDto>())
      );
    }


    [Fact]
    public async Task Should_Fail_ToUpdateUser_When_EmailIsTaken()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
      );
      _mockedUserRepo
        .Setup(r => r.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<User>());

      _mockedUserRepo
        .Setup(r => r.FindByEmail(It.IsAny<string>(), It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<User>());
      await Assert.ThrowsAsync<ConflictException>(() =>
        _userService.UpdateUserData(_fixture.Create<Guid>(), _fixture.Create<Guid>(), _fixture.Create<UpdateUserDto>())
      );
    }

    [Fact]
    public async Task Should_UpdateUserData()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
      );
      _mockedUserRepo
        .Setup(r => r.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<User>());

      _mockedUserRepo
        .Setup(r => r.FindByEmail(It.IsAny<string>(), It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(() => default);
      await _userService.UpdateUserData(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
        _fixture.Create<UpdateUserDto>());
    }

    [Fact]
    public async Task Should_PathUserRoles()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
      );
      var user = _fixture.Create<User>();
      user.Roles = new List<UserRole> { new UserRole { Role = Role.Professor } };
      _mockedUserRepo
        .Setup(r => r.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(user);
      var patchUserRoleDto = _fixture.Create<PatchUserRoleDto>();
      patchUserRoleDto.Roles = new List<string> { "Student", "Professor" };

      await _userService.PatchUserRole(_fixture.Create<Guid>(),
        _fixture.Create<Guid>(), patchUserRoleDto);
    }


    [Fact]
    public async Task Should_Fail_PathUserRoles_When_UserDoesNotExist()
    {
      _mockedUserRepo
        .Setup(r => r.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(() => default);
      var patchUserRoleDto = _fixture.Create<PatchUserRoleDto>();
      patchUserRoleDto.Roles = new List<string> { "Student", "Professor" };

      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _userService.PatchUserRole(_fixture.Create<Guid>(),
          _fixture.Create<Guid>(), patchUserRoleDto)
      );
    }

    [Fact]
    public async Task Should_Fail_FindUserByRefreshToken_When_UserNotFound()
    {
      _mockedUserRepo
        .Setup(r => r.FindByResetToken(It.IsAny<string>()))
        .ReturnsAsync(() => default);
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _userService.FindByResetToken(_fixture.Create<string>())
      );
    }

    [Fact]
    public async Task Should_FindUserByRefreshToken()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
      );
      _mockedUserRepo
        .Setup(r => r.FindByResetToken(It.IsAny<string>()))
        .ReturnsAsync(_fixture.Create<User>());
      await _userService.FindByResetToken(_fixture.Create<string>());
    }

    [Fact]
    public async Task Should_ResetPassword()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.University).Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
      );
      _mockedUserRepo
        .Setup(r => r.FindByResetToken(It.IsAny<string>()))
        .ReturnsAsync(_fixture.Create<User>());
      await _userService.ResetPassword(_fixture.Create<string>(), _fixture.Create<ResetPasswordDto>());
    }


    [Fact]
    public async Task Should_Fail_ResetPassword_When_TokenNotFound()
    {
      _mockedUserRepo
        .Setup(r => r.FindByResetToken(It.IsAny<string>()))
        .ReturnsAsync(() => default);
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _userService.ResetPassword(_fixture.Create<string>(), _fixture.Create<ResetPasswordDto>())
      );
    }


    [Fact]
    public async Task Should_Fail_DeleteUser_NotFound()
    {
      _mockedUserRepo
        .Setup(r => r.FindUserInUniversityById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(() => default);
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _userService.DeleteUser(_fixture.Create<Guid>(), _fixture.Create<Guid>())
      );
    }

    [Fact]
    public async Task Should_DeleteUser()
    {
      _mockedUserRepo
        .Setup(r => r.FindUserInUniversityById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(() => new User());
      await _userService.DeleteUser(_fixture.Create<Guid>(), _fixture.Create<Guid>());
    }


    [Fact]
    public async Task Should_Fail_ListUserEnrollments_NotFound()
    {
      _mockedUserRepo
        .Setup(r => r.FindUserEnrollments(It.IsAny<Guid>()))
        .ReturnsAsync(() => default);
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _userService.GetUserEnrollments(_fixture.Create<Guid>())
      );
    }

    [Fact]
    public async Task Should_GetUserEnrollments()
    {
      _mockedUserRepo
        .Setup(r => r.FindUserEnrollments(It.IsAny<Guid>()))
        .ReturnsAsync(() => new User
        {
          Inscriptions = new List<Enrollment>
          {
            new ProfessorEnrollment()
            {
              Id = new Guid(),
              Subject = new Subject {Name = _fixture.Create<string>(), Id = _fixture.Create<Guid>()}
            }
          }
        });
      var dto = await _userService.GetUserEnrollments(_fixture.Create<Guid>());
      Assert.Single(dto);
      Assert.True(dto[0].IsProfessorEnrollment);
    }

    [Fact]
    public async Task Should_GetUniversitiesUsersQuantityReport()
    {
      _fixture.Customize<User>(c =>
        c.Without(p => p.Assignments)
          .Without(p => p.Inscriptions).Without(p => p.PollsResponses)
          .Without(p => p.QuizResponses).Without(p => p.UserGroups)
      );
      _fixture.Customize<University>(c =>
        c.Without(p => p.Careers)
          .Without(p => p.UniversityAdministrators)
          .Without(p => p.Notices)
          .Without(p => p.SubjectTemplates)
          .Without(u => u.UniversityPollPublications)
      );
      _mockedUserRepo
        .Setup(r => r.GetAllWithUniversityAndRoles())
        .ReturnsAsync(_fixture.Create<List<User>>());
      var dto = await _userService.GetUniversitiesUsersQuantityReport();
    }
  }
}
