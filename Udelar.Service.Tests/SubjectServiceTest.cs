﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.Kernel;
using AutoMapper;
using MongoDB.Entities;
using Moq;
using Udelar.Common.Dtos.Component.Request;
using Udelar.Common.Dtos.Forum.Request;
using Udelar.Common.Dtos.Subject.Request;
using Udelar.Common.Dtos.User.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Model.Component;
using Udelar.Model.Mongo;
using Udelar.Model.Poll;
using Udelar.Model.Poll.Publications;
using Udelar.Model.Quiz;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class SubjectServiceTest
  {
    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly SubjectService _subjectService;
    private readonly Mock<IForumRepository> _forumRepoMock;
    private readonly Mock<ISubjectRepository> _subjectRepoMock;
    private readonly Mock<ISubjectTemplateRepository> _subjectTemplateRepoMock;
    private readonly Mock<IUniversitiesRepository> _universityRepoMock;
    private readonly Mock<IUserRepository> _userRepoMock;
    private readonly Mock<ICalendarService> _calendarService;

    public SubjectServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));

      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
        .ForEach(b => _fixture.Behaviors.Remove(b));
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());

      _forumRepoMock = new Mock<IForumRepository>();
      _userRepoMock = new Mock<IUserRepository>();
      _subjectRepoMock = new Mock<ISubjectRepository>();
      _subjectTemplateRepoMock = new Mock<ISubjectTemplateRepository>();
      _universityRepoMock = new Mock<IUniversitiesRepository>();
      _calendarService = new Mock<ICalendarService>();

      _subjectService = new SubjectService(
        _universityRepoMock.Object,
        _subjectTemplateRepoMock.Object,
        _subjectRepoMock.Object,
        _userRepoMock.Object,
        _forumRepoMock.Object,
        _calendarService.Object,
        mapper
      );

      _fixture.Customize<AssignmentComponent>(c =>
        c
          .Without(p => p.Assignment)
      );
      _fixture.Customize<SubjectTemplate>(c =>
        c
          .Without(p => p.Components)
      );

      _fixture.Customize<University>(c =>
        c.Without(p => p.Careers)
          .Without(p => p.UniversityAdministrators)
          .Without(p => p.SubjectTemplates)
          .Without(p => p.UniversityPollPublications)
      );

      _fixture.Customize<EducationalUnit>(c =>
        c
          .Without(p => p.Components)
      );
      _fixture.Customize<PollComponent>(c =>
        c
          .Without(p => p.Poll)
      );
      _fixture.Customize<QuizComponent>(c =>
        c
          .Without(p => p.Quiz)
      );

      _fixture.Customize<Subject>(c =>
        c
          .Without(p => p.CareerSubjects)
          .Without(p => p.Enrollments)
          .Without(p => p.Quizzes)
          .Without(p => p.SubjectPollPublications)
          .Without(p => p.Assignments)
          .Without(p => p.Forums)
      );
    }

    [Fact]
    public async Task Should_CreateSubject()
    {
      var subjectTemplate = _fixture.Create<SubjectTemplate>();
      subjectTemplate.Components = _fixture.Create<List<EducationalUnit>>();
      foreach (var subjectTemplateComponent in subjectTemplate.Components)
      {
        subjectTemplateComponent.Components = new List<Component>
        {
          _fixture.Create<LinkComponent>(),
          _fixture.Create<PollComponent>(),
          _fixture.Create<QuizComponent>(),
          _fixture.Create<DocumentComponent>(),
          _fixture.Create<AssignmentComponent>(),
          _fixture.Create<VirtualMeetingComponent>(),
          _fixture.Create<VideoComponent>(),
          _fixture.Create<ForumComponent>(),
          _fixture.Create<TextComponent>(),
        };
      }


      _subjectTemplateRepoMock.Setup(
        x => x.GetSubjectTemplateById(It.IsAny<Guid>(), It.IsAny<Guid>())
      ).ReturnsAsync(subjectTemplate);

      _universityRepoMock.Setup(
        x => x.FindUniversityById(It.IsAny<Guid>())
      ).ReturnsAsync(_fixture.Create<University>());

      _forumRepoMock.Setup(
        x => x.CreateForum(It.IsAny<CreateForumDto>(), It.IsAny<Guid>())
      ).ReturnsAsync(new Forum(true));


      await _subjectService.CreateSubject(_fixture.Create<Guid>(), _fixture.Create<CreateSubjectDto>());
    }

    [Fact]
    public async Task Should_ListSubjects()
    {
      _universityRepoMock.Setup(e => e.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());
      await _subjectService.GetAllSubjects(_fixture.Create<string>(), _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_GetSubjectById()
    {
      _subjectRepoMock.Setup(e => e.FindSubjectById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<Subject>());
      await _subjectService.GetSubjectById(_fixture.Create<Guid>(), _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_RemoveById()
    {
      _subjectRepoMock.Setup(e => e.FindSubjectById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<Subject>());
      await _subjectService.RemoveSubject(_fixture.Create<Guid>(), _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_UpdateSubject()
    {
      _subjectRepoMock.Setup(e => e.FindSubjectById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<Subject>());
      await _subjectService.UpdateSubject(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
        _fixture.Create<UpdateSubjectDto>());
    }


    [Fact]
    public async Task Should_AddNewEducationUnit()
    {
      var subject = _fixture.Create<Subject>();

      var user = new User { Id = Guid.NewGuid(), Roles = new List<UserRole> { new UserRole { Role = Role.Professor } } };

      var forum = new Forum(true);
      forum.SubjectId = subject.Id;

      subject.Assignments = new List<Assignment> { new Assignment { Id = Guid.NewGuid() } };
      subject.SubjectPollPublications = new List<SubjectPollPublication> { new SubjectPollPublication { Poll = new UniversityPoll { Id = Guid.NewGuid() } } };
      subject.Quizzes = new List<Quiz> { new Quiz { Id = Guid.NewGuid() } };
      subject.Forums = new List<Forum> { forum };
      subject.Enrollments.Add(new ProfessorEnrollment { User = user });

      var createEducationalUnitDto = _fixture.Create<CreateEducationalUnitDto>();

      createEducationalUnitDto.AssigmentComponents[0].AssignmentId = subject.Assignments.First().Id;
      createEducationalUnitDto.AssigmentComponents[1].AssignmentId = subject.Assignments.First().Id;
      createEducationalUnitDto.AssigmentComponents[2].AssignmentId = subject.Assignments.First().Id;

      createEducationalUnitDto.PollComponents[0].PollId = subject.SubjectPollPublications.First().Poll.Id;
      createEducationalUnitDto.PollComponents[1].PollId = subject.SubjectPollPublications.First().Poll.Id;
      createEducationalUnitDto.PollComponents[2].PollId = subject.SubjectPollPublications.First().Poll.Id;

      createEducationalUnitDto.QuizComponents[0].QuizId = subject.Quizzes.First().Id;
      createEducationalUnitDto.QuizComponents[1].QuizId = subject.Quizzes.First().Id;
      createEducationalUnitDto.QuizComponents[2].QuizId = subject.Quizzes.First().Id;


      _forumRepoMock.Setup(e =>
          e.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(forum);

      _subjectRepoMock.Setup(e =>
          e.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(subject);
      await _subjectService.AddNewEducationalUnit(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
        createEducationalUnitDto, user.Id);
    }


    [Fact]
    public async Task Should_Fail_AddNewEducationUnit_InvalidProfessor()
    {
      var subject = _fixture.Create<Subject>();

      var user = new User { Id = Guid.NewGuid(), Roles = new List<UserRole> { new UserRole { Role = Role.Professor } } };

      var forum = new Forum(true);
      forum.SubjectId = subject.Id;

      var createEducationalUnitDto = _fixture.Create<CreateEducationalUnitDto>();

      _forumRepoMock.Setup(e =>
          e.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(forum);

      _subjectRepoMock.Setup(e =>
          e.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(subject);
      await Assert.ThrowsAsync<AuthorizationException>(() =>
        _subjectService.AddNewEducationalUnit(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
          createEducationalUnitDto, user.Id)
      );
    }


    [Fact]
    public async Task Should_Fail_RemoveEducationUnit_InvalidProfessor()
    {
      var subject = _fixture.Create<Subject>();

      var forum = new Forum(true) { SubjectId = subject.Id };

      _forumRepoMock.Setup(e =>
          e.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(forum);

      _subjectRepoMock.Setup(e =>
          e.FindSubjectWithEnrollmentsAndComponents(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(subject);
      await Assert.ThrowsAsync<AuthorizationException>(() =>
        _subjectService.RemoveEducationalUnit(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
          _fixture.Create<Guid>(), _fixture.Create<Guid>())
      );
    }


    [Fact]
    public async Task Should_RemoveEducationUnit()
    {
      var subject = _fixture.Create<Subject>();

      var user = new User { Id = Guid.NewGuid(), Roles = new List<UserRole> { new UserRole { Role = Role.Professor } } };

      var forum = new Forum(true);
      forum.SubjectId = subject.Id;

      subject.Components = new List<EducationalUnit> { new EducationalUnit { Id = Guid.Empty } };
      subject.Enrollments.Add(new ProfessorEnrollment { User = user });

      _forumRepoMock.Setup(e =>
          e.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(forum);

      _subjectRepoMock.Setup(e =>
          e.FindSubjectWithEnrollmentsAndComponents(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(subject);
      await _subjectService.RemoveEducationalUnit(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
        Guid.Empty, user.Id);
    }


    [Fact]
    public async Task Should_UpdateEducationalUnit()
    {
      var subject = _fixture.Create<Subject>();

      var user = new User { Id = Guid.NewGuid(), Roles = new List<UserRole> { new UserRole { Role = Role.Professor } } };

      var forum = new Forum(true);
      forum.SubjectId = subject.Id;

      subject.Assignments = new List<Assignment> { new Assignment { Id = Guid.NewGuid() } };
      subject.SubjectPollPublications = new List<SubjectPollPublication> { new SubjectPollPublication { Poll = new UniversityPoll { Id = Guid.NewGuid() } } };
      subject.Quizzes = new List<Quiz> { new Quiz { Id = Guid.NewGuid() } };
      subject.Forums = new List<Forum> { forum };
      subject.Enrollments.Add(new ProfessorEnrollment { User = user });

      foreach (var subjectComponent in subject.Components)
      {
        subjectComponent.Id = user.Id;

        subjectComponent.Components.Add(_fixture.Create<LinkComponent>());
        subjectComponent.Components.Add(_fixture.Create<PollComponent>());
        subjectComponent.Components.Add(_fixture.Create<QuizComponent>());
        subjectComponent.Components.Add(_fixture.Create<DocumentComponent>());
        subjectComponent.Components.Add(_fixture.Create<AssignmentComponent>());
        subjectComponent.Components.Add(_fixture.Create<VirtualMeetingComponent>());
        subjectComponent.Components.Add(_fixture.Create<VideoComponent>());
        subjectComponent.Components.Add(_fixture.Create<ForumComponent>());
        subjectComponent.Components.Add(_fixture.Create<TextComponent>());
      }

      var updateEducationalUnitDto = _fixture.Create<UpdateEducationalUnitDto>();

      updateEducationalUnitDto.AssigmentComponents[0].AssignmentId = subject.Assignments.First().Id;
      updateEducationalUnitDto.AssigmentComponents[1].AssignmentId = subject.Assignments.First().Id;
      updateEducationalUnitDto.AssigmentComponents[2].AssignmentId = subject.Assignments.First().Id;

      updateEducationalUnitDto.AssigmentComponents[0].Id = user.Id;
      updateEducationalUnitDto.AssigmentComponents[1].Id = user.Id;
      updateEducationalUnitDto.AssigmentComponents[2].Id = user.Id;

      updateEducationalUnitDto.PollComponents[0].PollId = subject.SubjectPollPublications.First().Poll.Id;
      updateEducationalUnitDto.PollComponents[1].PollId = subject.SubjectPollPublications.First().Poll.Id;
      updateEducationalUnitDto.PollComponents[2].PollId = subject.SubjectPollPublications.First().Poll.Id;

      updateEducationalUnitDto.QuizComponents[0].QuizId = subject.Quizzes.First().Id;
      updateEducationalUnitDto.QuizComponents[1].QuizId = subject.Quizzes.First().Id;
      updateEducationalUnitDto.QuizComponents[2].QuizId = subject.Quizzes.First().Id;

      _forumRepoMock.Setup(e =>
          e.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(forum);

      _subjectRepoMock.Setup(e =>
          e.FindSubjectWithEnrollmentsAndComponents(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(subject);
      await _subjectService.UpdateEducationalUnit(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
        user.Id, updateEducationalUnitDto, user.Id);
    }

    [Fact]
    public async Task Should_UpdateEducationalUnit2()
    {
      var subject = _fixture.Create<Subject>();

      var user = new User { Id = Guid.NewGuid(), Roles = new List<UserRole> { new UserRole { Role = Role.Professor } } };

      var forum = new Forum(true);
      forum.SubjectId = subject.Id;

      subject.Assignments = new List<Assignment> { new Assignment { Id = Guid.NewGuid() } };

      subject.SubjectPollPublications = new List<SubjectPollPublication> { new SubjectPollPublication { Poll = new UniversityPoll { Id = Guid.NewGuid() } } };
      subject.Quizzes = new List<Quiz> { new Quiz { Id = Guid.NewGuid() } };
      subject.Forums = new List<Forum> { forum };
      subject.Enrollments.Add(new ProfessorEnrollment { User = user });

      foreach (var subjectComponent in subject.Components)
      {
        subjectComponent.Id = user.Id;
        subjectComponent.Components.Add(_fixture.Create<LinkComponent>());
        subjectComponent.Components.Add(_fixture.Create<PollComponent>());
        subjectComponent.Components.Add(_fixture.Create<QuizComponent>());
        subjectComponent.Components.Add(_fixture.Create<DocumentComponent>());
        subjectComponent.Components.Add(_fixture.Create<AssignmentComponent>());
        subjectComponent.Components.Add(_fixture.Create<VirtualMeetingComponent>());
        subjectComponent.Components.Add(_fixture.Create<VideoComponent>());
        subjectComponent.Components.Add(_fixture.Create<ForumComponent>());
        subjectComponent.Components.Add(_fixture.Create<TextComponent>());
        foreach (var subjectComponentComponent in subjectComponent.Components)
        {
          subjectComponentComponent.Id = user.Id;
        }
      }

      var updateEducationalUnitDto = _fixture.Create<UpdateEducationalUnitDto>();

      updateEducationalUnitDto.AssigmentComponents[0].AssignmentId = subject.Assignments.First().Id;
      updateEducationalUnitDto.AssigmentComponents[1].AssignmentId = subject.Assignments.First().Id;
      updateEducationalUnitDto.AssigmentComponents[2].AssignmentId = subject.Assignments.First().Id;

      updateEducationalUnitDto.AssigmentComponents[0].Id = user.Id;
      updateEducationalUnitDto.AssigmentComponents[1].Id = user.Id;
      updateEducationalUnitDto.AssigmentComponents[2].Id = Guid.NewGuid();

      updateEducationalUnitDto.DocumentComponents[0].Id = user.Id;
      updateEducationalUnitDto.DocumentComponents[1].Id = user.Id;
      updateEducationalUnitDto.DocumentComponents[2].Id = Guid.NewGuid();

      updateEducationalUnitDto.ForumComponents[0].Id = user.Id;
      updateEducationalUnitDto.ForumComponents[1].Id = user.Id;
      updateEducationalUnitDto.ForumComponents[2].Id = Guid.NewGuid();

      updateEducationalUnitDto.LinkComponents[0].Id = user.Id;
      updateEducationalUnitDto.LinkComponents[1].Id = user.Id;
      updateEducationalUnitDto.LinkComponents[2].Id = Guid.NewGuid();


      updateEducationalUnitDto.PollComponents[0].PollId = subject.SubjectPollPublications.First().Poll.Id;
      updateEducationalUnitDto.PollComponents[1].PollId = subject.SubjectPollPublications.First().Poll.Id;
      updateEducationalUnitDto.PollComponents[2].PollId = subject.SubjectPollPublications.First().Poll.Id;
      updateEducationalUnitDto.PollComponents[0].Id = user.Id;
      updateEducationalUnitDto.PollComponents[1].Id = user.Id;
      updateEducationalUnitDto.PollComponents[2].Id = Guid.NewGuid();

      updateEducationalUnitDto.QuizComponents[0].QuizId = subject.Quizzes.First().Id;
      updateEducationalUnitDto.QuizComponents[1].QuizId = subject.Quizzes.First().Id;
      updateEducationalUnitDto.QuizComponents[2].QuizId = subject.Quizzes.First().Id;
      updateEducationalUnitDto.QuizComponents[0].Id = user.Id;
      updateEducationalUnitDto.QuizComponents[1].Id = user.Id;
      updateEducationalUnitDto.QuizComponents[2].Id = Guid.NewGuid();

      updateEducationalUnitDto.TextComponents[0].Id = user.Id;
      updateEducationalUnitDto.TextComponents[1].Id = user.Id;
      updateEducationalUnitDto.TextComponents[2].Id = Guid.NewGuid();

      updateEducationalUnitDto.VideoComponents[0].Id = user.Id;
      updateEducationalUnitDto.VideoComponents[1].Id = user.Id;
      updateEducationalUnitDto.VideoComponents[2].Id = Guid.NewGuid();

      updateEducationalUnitDto.VirtualMeetingComponents[0].Id = user.Id;
      updateEducationalUnitDto.VirtualMeetingComponents[1].Id = user.Id;
      updateEducationalUnitDto.VirtualMeetingComponents[2].Id = Guid.NewGuid();

      _forumRepoMock.Setup(e =>
          e.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(forum);

      _subjectRepoMock.Setup(e =>
          e.FindSubjectWithEnrollmentsAndComponents(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(subject);
      await _subjectService.UpdateEducationalUnit(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
        user.Id, updateEducationalUnitDto, user.Id);
    }
  }
}
