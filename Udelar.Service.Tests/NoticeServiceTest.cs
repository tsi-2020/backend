﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Udelar.Common.Dtos.Notice.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class NoticeServiceTest
  {
    private readonly ITestOutputHelper _output;
    private readonly Mock<ISubjectRepository> _mockSubjectRepo;
    private readonly Mock<INoticeRepository> _mockNoticeRepo;
    private readonly NoticeService _noticeService;
    private readonly Fixture _fixture = new Fixture();
    private readonly Mock<IUniversitiesRepository> _mockUniversityRepo;

    public NoticeServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));

      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
        .ForEach(b => _fixture.Behaviors.Remove(b));
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());

      _mockSubjectRepo = new Mock<ISubjectRepository>();
      _mockNoticeRepo = new Mock<INoticeRepository>();
      _mockUniversityRepo = new Mock<IUniversitiesRepository>();
      _noticeService = new NoticeService(mapper, _mockSubjectRepo.Object, _mockNoticeRepo.Object,
        _mockUniversityRepo.Object);

      _fixture.Customize<Subject>(c =>
        c
          .Without(p => p.CareerSubjects)
          .Without(p => p.Enrollments)
          .Without(p => p.Quizzes)
          .Without(p => p.SubjectPollPublications)
          .Without(p => p.Assignments)
          .Without(p => p.Components)
          .Without(p => p.Forums)
          .Without(p => p.Notices)
          .Without(p => p.University)
      );
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityAdministrators)
        .Without(u => u.SubjectTemplates)
        .Without(p => p.Notices)
        .Without(u => u.UniversityPollPublications)
      );
    }

    [Fact]
    public async Task Should_ThrowNotFound_When_InvalidSubject()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _noticeService.CreateNotice(
          _fixture.Create<CreateNoticeDto>(),
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>()
        )
      );
    }

    [Fact]
    public async Task Should_CreateANotice_When_ValidSubjectProvided()
    {

      _mockSubjectRepo
        .Setup(x => x.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<Subject>());

      await _noticeService.CreateNotice(
        _fixture.Create<CreateNoticeDto>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>()
      );
    }


    [Fact]
    public async Task Should_CreateANotice_When_ValidUniveristy()
    {
      _mockUniversityRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());

      await _noticeService.CreateNotice(_fixture.Create<CreateNoticeDto>(), _fixture.Create<Guid>(), null);
    }

    [Fact]
    public async Task Should_ThrowNotFound_When_InvalidSubjectProvided()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _noticeService.ListNotices(_fixture.Create<Guid>(), _fixture.Create<Guid>())
      );
    }

    [Fact]
    public async Task Should_ListNotices_When_ValidSubjectProvided()
    {

      _mockSubjectRepo
        .Setup(x => x.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<Subject>());

      _mockNoticeRepo
        .Setup(x => x.ListNotices(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<List<Notice>>());

      var result = await _noticeService.ListNotices(_fixture.Create<Guid>(), _fixture.Create<Guid>());
      Assert.NotEmpty(result);
    }

    [Fact]
    public async Task Should_ListNotices_When_ValidUniversityProvided()
    {

      _mockNoticeRepo
        .Setup(x => x.ListUniversityNotices(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<List<Notice>>());

      var result = await _noticeService.ListNotices(_fixture.Create<Guid>(), null);
      Assert.NotEmpty(result);
    }


    [Fact]
    public async Task Should_GetNoticeById()
    {
      _mockNoticeRepo
        .Setup(x => x.GetNoticeById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<Notice>());

      var result = await _noticeService.ReadNotice(Guid.NewGuid());
      Assert.NotNull(result);
    }



    [Fact]
    public async Task Should_Fail_GetNoticeById_When_IdNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
      _noticeService.ReadNotice(Guid.NewGuid())
      );
    }
  }
}
