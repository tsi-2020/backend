﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.Kernel;
using AutoMapper;
using AutoMapper.Internal;
using Moq;
using Udelar.Common.Dtos.Poll.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Model.Poll;
using Udelar.Model.Poll.Answers;
using Udelar.Model.Poll.Publications;
using Udelar.Model.Poll.Questions;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Uderlar.Service.Tests
{
  public class PollServiceTest
  {
    public PollServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));
      _mockedPollRepo = new Mock<IPollRepository>();
      _mockeUniRepo = new Mock<IUniversitiesRepository>();
      _mockedSubjRepo = new Mock<ISubjectRepository>();
      _userRepo = new Mock<IUserRepository>();
      _pollService = new PollService(_mockedPollRepo.Object, _mockeUniRepo.Object, _mockedSubjRepo.Object,
        _userRepo.Object, mapper);

      _fixture.Customize<University>(c =>
        c.Without(p => p.Careers)
          .Without(p => p.UniversityAdministrators)
          .Without(p => p.Notices)
          .Without(p => p.SubjectTemplates)
          .Without(u => u.UniversityPollPublications)
      );

      _fixture.Customize<Subject>(c =>
        c
          .Without(p => p.CareerSubjects)
          .Without(p => p.Enrollments)
          .Without(p => p.Quizzes)
          .Without(p => p.SubjectPollPublications)
          .Without(p => p.Assignments)
          .Without(p => p.Forums)
      );
      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
        .ForEach(b => _fixture.Behaviors.Remove(b));
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
    }

    private readonly ITestOutputHelper _output;
    private readonly PollService _pollService;
    private readonly Mock<IPollRepository> _mockedPollRepo;
    private readonly Fixture _fixture = new Fixture();
    private readonly Mock<IUniversitiesRepository> _mockeUniRepo;
    private readonly Mock<ISubjectRepository> _mockedSubjRepo;
    private readonly Mock<IUserRepository> _userRepo;


    [Fact]
    public async Task Should_CreateUdelarPoll_When_ValidData()
    {
      await _pollService.CreateUdelarPoll(_fixture.Create<CreatePollDto>());
    }


    [Fact]
    public async Task Should_CreateUniversityPoll_When_ValidData()
    {
      _fixture.Customizations.Add(
        new TypeRelay(
          typeof(Poll),
          typeof(UdelarPoll)));
      _mockeUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());
      await _pollService.CreateUniversityPoll(_fixture.Create<CreatePollDto>(), _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_CreateSubjectPoll_When_ValidData()
    {
      _fixture.Customizations.Add(
        new TypeRelay(
          typeof(Poll),
          typeof(SubjectPoll)));
      _mockeUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());

      _mockedSubjRepo
        .Setup(x => x.FindSubjectById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject());
      await _pollService.CreateSubjectPoll(_fixture.Create<CreatePollDto>(), _fixture.Create<Guid>(),
        _fixture.Create<Guid>());
    }


    [Fact]
    public async Task Should_NotReturnEmpty_When_PollsAreCreated()
    {
      _fixture.Customize<UdelarPoll>(c =>
        c.Without(p => p.Questions)
          .Without(p => p.PollResponses)
          .Without(p => p.SubjectPollPublications)
      );
      _fixture.Customizations.Add(
        new TypeRelay(
          typeof(Poll),
          typeof(UdelarPoll)));
      _mockedPollRepo
        .Setup(x => x.ReadUdelarPolls())
        .ReturnsAsync(_fixture.Create<List<UdelarPoll>>());
      Assert.NotEmpty(await _pollService.ReadUdelarPolls());
    }


    [Fact]
    public async Task Should_ReadSubjectPolls()
    {
      _fixture.Customize<UdelarPoll>(c =>
        c.Without(p => p.Questions)
          .Without(p => p.PollResponses)
          .Without(p => p.SubjectPollPublications)
      );
      _fixture.Customizations.Add(
        new TypeRelay(
          typeof(Poll),
          typeof(UdelarPoll)));
      _mockedPollRepo
        .Setup(x => x.ReadUdelarPolls())
        .ReturnsAsync(_fixture.Create<List<UdelarPoll>>());
      await _pollService.ReadSubjectPolls(_fixture.Create<Guid>());
    }


    [Fact]
    public async Task Should_PublishPollIntoUniversity()
    {
      _fixture.Customize<UdelarPoll>(c =>
        c.Without(p => p.Questions)
          .Without(p => p.PollResponses)
          .Without(p => p.SubjectPollPublications)
      );
      _fixture.Customizations.Add(
        new TypeRelay(
          typeof(Poll),
          typeof(UdelarPoll)));

      _mockeUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());

      var udelarPoll = _fixture.Create<UdelarPoll>();
      udelarPoll.UniversityPollPublications.ForAll(publication => publication.Poll = new UniversityPoll());

      _mockedPollRepo
        .Setup(x => x.FindUdelarPollById(It.IsAny<Guid>()))
        .ReturnsAsync(udelarPoll);

      await _pollService.PublishPollIntoUniversity(_fixture.Create<Guid>(), _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_Fail_PublishPollIntoUniversity()
    {
      _fixture.Customize<UdelarPoll>(c =>
        c.Without(p => p.Questions)
          .Without(p => p.PollResponses)
          .Without(p => p.SubjectPollPublications)
      );
      _fixture.Customizations.Add(
        new TypeRelay(
          typeof(Poll),
          typeof(UdelarPoll)));

      _mockeUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());

      var pollId = Guid.NewGuid();
      var udelarPoll = _fixture.Create<UdelarPoll>();
      udelarPoll.UniversityPollPublications.ForAll(publication => publication.Poll = new UniversityPoll { Id = pollId });

      _mockedPollRepo
        .Setup(x => x.FindUdelarPollById(It.IsAny<Guid>()))
        .ReturnsAsync(udelarPoll);

      await Assert.ThrowsAsync<ConflictException>(() =>
        _pollService.PublishPollIntoUniversity(_fixture.Create<Guid>(), pollId));
    }


    [Fact]
    public async Task Should_PublishPollIntoSubjectAsProfessor()
    {
      _fixture.Customize<SubjectPoll>(c =>
        c.Without(p => p.Questions)
          .Without(p => p.PollResponses)
          .Without(p => p.SubjectPollPublications)
          .Without(p => p.Subject)
          .Without(p => p.UniversityPollPublications)
      );

      _mockeUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());

      _mockedSubjRepo
        .Setup(x => x.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject());

      var udelarPoll = _fixture.Create<SubjectPoll>();
      udelarPoll.SubjectPollPublications = new HashSet<SubjectPollPublication> { new SubjectPollPublication() };
      udelarPoll.SubjectPollPublications.ForAll(publication => publication.Poll = new SubjectPoll());

      _mockedPollRepo
        .Setup(x => x.FindSubjectPoll(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(udelarPoll);

      await _pollService.PublishPollIntoSubjectAsProfessor(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
        _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_Fail_PublishPollIntoSubjectAsProfessor()
    {
      _fixture.Customize<SubjectPoll>(c =>
        c.Without(p => p.Questions)
          .Without(p => p.PollResponses)
          .Without(p => p.SubjectPollPublications)
          .Without(p => p.Subject)
          .Without(p => p.UniversityPollPublications)
      );

      _mockeUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());

      var pollId = Guid.NewGuid();
      var udelarPoll = _fixture.Create<SubjectPoll>();
      udelarPoll.SubjectPollPublications = new HashSet<SubjectPollPublication> { new SubjectPollPublication() };
      udelarPoll.SubjectPollPublications.ForAll(publication => publication.Poll = new SubjectPoll { Id = pollId });

      _mockedPollRepo
        .Setup(x => x.FindSubjectPoll(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(udelarPoll);

      await Assert.ThrowsAsync<ConflictException>(() =>
        _pollService.PublishPollIntoSubjectAsProfessor(_fixture.Create<Guid>(), _fixture.Create<Guid>(), pollId));
    }


    [Fact]
    public async Task Should_PublishPollIntoSubjectAsUniversityAdmin()
    {
      _fixture.Customize<UniversityPoll>(c =>
        c.Without(p => p.Questions)
          .Without(p => p.PollResponses)
          .Without(p => p.SubjectPollPublications)
          .Without(p => p.UniversityPollPublications)
      );

      _mockeUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());

      _mockedSubjRepo
        .Setup(x => x.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject());

      var udelarPoll = _fixture.Create<UniversityPoll>();
      udelarPoll.SubjectPollPublications = new HashSet<SubjectPollPublication> { new SubjectPollPublication() };
      udelarPoll.SubjectPollPublications.ForAll(publication => publication.Poll = new UniversityPoll());

      _mockedPollRepo
        .Setup(x => x.FindUniversityPollById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(udelarPoll);

      await _pollService.PublishPollIntoSubjectAsUniversityAdmin(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
        _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_Fail_PublishPollIntoSubjectAsUniversityAdmin()
    {
      _fixture.Customize<UniversityPoll>(c =>
        c.Without(p => p.Questions)
          .Without(p => p.PollResponses)
          .Without(p => p.SubjectPollPublications)
          .Without(p => p.UniversityPollPublications)
      );

      _mockeUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(_fixture.Create<University>());

      var pollId = Guid.NewGuid();
      var udelarPoll = _fixture.Create<UniversityPoll>();
      udelarPoll.SubjectPollPublications = new HashSet<SubjectPollPublication> { new SubjectPollPublication() };
      udelarPoll.SubjectPollPublications.ForAll(publication => publication.Poll = new UniversityPoll { Id = pollId });

      _mockedPollRepo
        .Setup(x => x.FindUniversityPollById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(udelarPoll);

      await Assert.ThrowsAsync<ConflictException>(() =>
        _pollService.PublishPollIntoSubjectAsUniversityAdmin(_fixture.Create<Guid>(), _fixture.Create<Guid>(), pollId));
    }


    [Fact]
    public async Task Should_ReplySubjectPoll()
    {
      var userId = Guid.NewGuid();
      _mockedSubjRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment> { new StudentEnrollment { User = new User { Id = userId } } }
        });

      _userRepo
        .Setup(x => x.FindUserInUniversityById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User { Id = userId });

      _mockedPollRepo
        .Setup(x => x.FindPollById(It.IsAny<Guid>()))
        .ReturnsAsync(new UniversityPoll());

      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _pollService.ReplySubjectPoll(_fixture.Create<Guid>(), _fixture.Create<Guid>(), Guid.NewGuid(), userId,
          _fixture.Create<ReplyPollDto>()));
    }


    [Fact]
    public async Task Should_ReplyUniversityPoll()
    {
      var userId = Guid.NewGuid();
      _mockedSubjRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment> { new StudentEnrollment { User = new User { Id = userId } } }
        });

      _userRepo
        .Setup(x => x.FindUserInUniversityById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User { Id = userId });

      _mockedPollRepo
        .Setup(x => x.FindPollById(It.IsAny<Guid>()))
        .ReturnsAsync(new UniversityPoll());

      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _pollService.ReplyUniversityPoll(_fixture.Create<Guid>(), _fixture.Create<Guid>(), userId,
          _fixture.Create<ReplyPollDto>()));
    }

    [Fact]
    public async Task Should_GetPublishedPollById()
    {
      _fixture.Customize<UdelarPoll>(c =>
        c
          .Without(p => p.Questions)
          .Without(p => p.PollResponses)
          .Without(p => p.SubjectPollPublications)
          .Without(p => p.UniversityPollPublications)
      );
      var poll = _fixture.Create<UdelarPoll>();
      poll.Questions = new HashSet<PollQuestion>(_fixture.Create<List<MultipleChoicePollQuestion>>());
      poll.Questions.Add(_fixture.Create<OpenPollQuestion>());

      _mockedPollRepo
        .Setup(x => x.FindPollById(It.IsAny<Guid>()))
        .ReturnsAsync(poll);

      await _pollService.GetPublishedPollById(_fixture.Create<Guid>());
    }


    [Fact]
    public async Task Should_GetPollAnswers()
    {
      _fixture.Customize<PollResponse>(c =>
        c
          .Without(p => p.Answers)
          .Without(p => p.Poll)
          .Without(p => p.User)
      );
      _fixture.Customize<UdelarPoll>(c =>
        c
          .Without(p => p.Questions)
          .Without(p => p.PollResponses)
          .Without(p => p.SubjectPollPublications)
          .Without(p => p.UniversityPollPublications)
      );
      var poll = _fixture.Create<PollResponse>();
      poll.Answers.Add(_fixture.Create<OpenPollAnswer>());
      poll.Answers.Add(_fixture.Create<MultipleChoicePollAnswer>());
      poll.Poll = new UdelarPoll();

      _mockedPollRepo
        .Setup(x => x.GetPollAnswers(It.IsAny<Guid>(), It.IsAny<int>(), It.IsAny<int>()))
        .ReturnsAsync(new Tuple<int, List<PollResponse>>(1, new List<PollResponse> { poll }));

      await _pollService.GetPollAnswers(_fixture.Create<Guid>(), 0, 0);
    }

    [Fact]
    public async Task Should_ReturnEmpty_When_NoPollsAreCreated()
    {
      _mockedPollRepo
        .Setup(x => x.ReadUdelarPolls())
        .ReturnsAsync(new List<UdelarPoll>());
      Assert.Empty(await _pollService.ReadUdelarPolls());
    }


    [Fact]
    public async Task Should_ReturnEmpty_When_NoUniversityPollsAreCreated()
    {
      _mockedPollRepo
        .Setup(x => x.ReadUniversityPolls(It.IsAny<Guid>()))
        .ReturnsAsync(new List<UniversityPoll>());
      Assert.Empty(await _pollService.ReadUniversityPolls(_fixture.Create<Guid>()));
    }

    [Fact]
    public async Task Should_Fail_When_IsPollAnswered()
    {
      _mockedPollRepo
        .Setup(x => x.FindUserPollResponse(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new PollResponse());
      await Assert.ThrowsAsync<ConflictException>(() => _pollService.ReplySubjectPoll(_fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(), _fixture.Create<Guid>(), new ReplyPollDto()));
    }

    [Fact]
    public async Task Should_Fail_When_UserNotFound()
    {
      var userId = _fixture.Create<Guid>();

      _mockedPollRepo
        .Setup(x => x.FindUserPollResponse(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((PollResponse)default);

      _mockedSubjRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment> { new StudentEnrollment { User = new User { Id = userId } } }
        });

      _userRepo
        .Setup(x => x.FindUserInUniversityById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((User)default);
      await Assert.ThrowsAsync<EntityNotFoundException>(() => _pollService.ReplySubjectPoll(_fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(), userId, new ReplyPollDto()));
    }

    [Fact]
    public async Task Should_Fail_When_IsNotEnrolled()
    {
      var userId = _fixture.Create<Guid>();

      _mockedPollRepo
        .Setup(x => x.FindUserPollResponse(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((PollResponse)default);

      _mockedSubjRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
          {
            /*new StudentEnrollment {User = new User {Id = userId}}*/
          }
        });

      _userRepo
        .Setup(x => x.FindUserInUniversityById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User());
      await Assert.ThrowsAsync<AuthorizationException>(() => _pollService.ReplySubjectPoll(_fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(), userId,
        new ReplyPollDto
        {
          MultipleChoiceQuestions = new List<ReplyPollMultipleChoiceQuestionDto>(),
          OpenQuestions = new List<ReplyPollOpenQuestionDto>()
        }));
    }

    [Fact]
    public async Task Should_Fail_When_MultipleChoiceQuestionNotFound()
    {
      var userId = _fixture.Create<Guid>();

      _mockedPollRepo
        .Setup(x => x.FindUserPollResponse(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((PollResponse)default);

      _mockedSubjRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment> { new StudentEnrollment { User = new User { Id = userId } } }
        });

      _userRepo
        .Setup(x => x.FindUserInUniversityById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User());

      _mockedPollRepo
        .Setup(x => x.FindUdelarPollById(It.IsAny<Guid>()))
        .ReturnsAsync(new UdelarPoll { Questions = new HashSet<PollQuestion>() });
      await Assert.ThrowsAsync<EntityNotFoundException>(() => _pollService.ReplySubjectPoll(_fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(), userId,
        new ReplyPollDto
        {
          MultipleChoiceQuestions =
            new List<ReplyPollMultipleChoiceQuestionDto> { new Mock<ReplyPollMultipleChoiceQuestionDto>().Object },
          OpenQuestions = new List<ReplyPollOpenQuestionDto>()
        }));
    }

    [Fact]
    public async Task Should_Fail_When_OpenQuestionNotFound()
    {
      var userId = _fixture.Create<Guid>();

      _mockedPollRepo
        .Setup(x => x.FindUserPollResponse(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((PollResponse)default);

      _mockedSubjRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment> { new StudentEnrollment { User = new User { Id = userId } } }
        });

      _userRepo
        .Setup(x => x.FindUserInUniversityById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User());

      _mockedPollRepo
        .Setup(x => x.FindUdelarPollById(It.IsAny<Guid>()))
        .ReturnsAsync(new UdelarPoll { Questions = new HashSet<PollQuestion>() });
      await Assert.ThrowsAsync<EntityNotFoundException>(() => _pollService.ReplySubjectPoll(_fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(), userId,
        new ReplyPollDto
        {
          MultipleChoiceQuestions = new List<ReplyPollMultipleChoiceQuestionDto>(),
          OpenQuestions = new List<ReplyPollOpenQuestionDto> { new ReplyPollOpenQuestionDto() }
        }));
    }

    [Fact]
    public async Task Should_ReplyPoll_When_ValidData()
    {
      var userId = _fixture.Create<Guid>();
      var openQuestionId = _fixture.Create<Guid>();
      var multipleChoiceQuestionId = _fixture.Create<Guid>();
      var choiceId = _fixture.Create<Guid>();

      _mockedPollRepo
        .Setup(x => x.FindUserPollResponse(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((PollResponse)default);

      _mockedSubjRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment> { new StudentEnrollment { User = new User { Id = userId } } }
        });

      _userRepo
        .Setup(x => x.FindUserInUniversityById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User());

      _mockedPollRepo
        .Setup(x => x.FindPollById(It.IsAny<Guid>()))
        .ReturnsAsync(new UdelarPoll
        {
          Questions = new HashSet<PollQuestion>
          {
            new OpenPollQuestion {Id = openQuestionId},
            new MultipleChoicePollQuestion
            {
              Id = multipleChoiceQuestionId, Choices = new HashSet<PollChoice> {new PollChoice {Id = choiceId}}
            }
          },
          PollResponses = new HashSet<PollResponse>()
        });

      var replyDto = new ReplyPollDto
      {
        MultipleChoiceQuestions =
          new List<ReplyPollMultipleChoiceQuestionDto>
          {
            new ReplyPollMultipleChoiceQuestionDto
            {
              MultipleChoiceQuestionId = multipleChoiceQuestionId, Choices = new List<Guid> {choiceId}
            }
          },
        OpenQuestions =
          new List<ReplyPollOpenQuestionDto> { new ReplyPollOpenQuestionDto { OpenQuestionId = openQuestionId } }
      };

      await _pollService.ReplySubjectPoll(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
        _fixture.Create<Guid>(), userId, replyDto);
    }
  }
}
