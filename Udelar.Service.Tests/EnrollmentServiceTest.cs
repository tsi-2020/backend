﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Udelar.Common.Dtos.Bedelias.Request;
using Udelar.Common.Dtos.Bedelias.Response;
using Udelar.Common.Dtos.Enrollment.request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class EnrollmentServiceTest
  {
    public EnrollmentServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));
      _mockedSubjectRepo = new Mock<ISubjectRepository>();
      _mockedUserRepo = new Mock<IUserRepository>();
      _bedeliasService = new Mock<IBedeliasService>();
      _enrollmentService = new EnrollmentService(_mockedSubjectRepo.Object, _mockedUserRepo.Object, _bedeliasService.Object, mapper);
    }

    private readonly IEnrollmentService _enrollmentService;
    private readonly ITestOutputHelper _output;
    private readonly Mock<ISubjectRepository> _mockedSubjectRepo;
    private readonly Mock<IUserRepository> _mockedUserRepo;
    private readonly Fixture _fixture = new Fixture();
    private readonly Mock<IBedeliasService> _bedeliasService;

    [Fact]
    public async Task Should_Fail_When_ResponsibleIsNotEnrolled()
    {
      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject { Enrollments = new List<Enrollment>() });

      await Assert.ThrowsAsync<AuthorizationException>(() => _enrollmentService.UpdateProfessorEnrollments(_fixture.Create<Guid>(), _fixture.Create<Guid>(), new List<CreateProfessorEnrollmentDto>(), _fixture.Create<Guid>()));
    }


    [Fact]
    public async Task Should_Fail_When_UserNotExist()
    {
      var responsibleId = _fixture.Create<Guid>();
      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true},
          new ProfessorEnrollment{ User = new User{Id = _fixture.Create<Guid>()}, IsResponsible = true}
        }
        });

      _mockedUserRepo
        .Setup(x => x.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((User)null);

      await Assert.ThrowsAsync<ConflictException>(() => _enrollmentService.UpdateProfessorEnrollments(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        new List<CreateProfessorEnrollmentDto>
        {
          new CreateProfessorEnrollmentDto { IsResponsible = true, UserId = responsibleId },
          new CreateProfessorEnrollmentDto { UserId = _fixture.Create<Guid>()}
        },
        responsibleId));
    }

    [Fact]
    public async Task Should_Fail_When_UserNotProfessor()
    {
      var responsibleId = _fixture.Create<Guid>();
      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true},
          new ProfessorEnrollment{ User = new User{Id = _fixture.Create<Guid>()}, IsResponsible = true}
        }
        });

      _mockedUserRepo
        .Setup(x => x.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User { Roles = new List<UserRole> { new UserRole { Role = Role.Student } } });

      await Assert.ThrowsAsync<ConflictException>(() => _enrollmentService.UpdateProfessorEnrollments(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        new List<CreateProfessorEnrollmentDto>
        {
          new CreateProfessorEnrollmentDto { IsResponsible = true, UserId = responsibleId },
          new CreateProfessorEnrollmentDto { UserId = _fixture.Create<Guid>()}
        },
        responsibleId));
    }

    [Fact]
    public async Task Should_Fail_When_HasNotResponsible()
    {
      var responsibleId = _fixture.Create<Guid>();
      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true},
          new ProfessorEnrollment{ User = new User{Id = _fixture.Create<Guid>()}, IsResponsible = true}
        }
        });

      _mockedUserRepo
        .Setup(x => x.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User { Roles = new List<UserRole> { new UserRole { Role = Role.Professor } } });

      await Assert.ThrowsAsync<ConflictException>(() => _enrollmentService.UpdateProfessorEnrollments(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        new List<CreateProfessorEnrollmentDto>
        {
          new CreateProfessorEnrollmentDto { IsResponsible = false, UserId = responsibleId },
          new CreateProfessorEnrollmentDto { UserId = _fixture.Create<Guid>()}
        },
        responsibleId));
    }

    [Fact]
    public async Task Should_Enroll_When_ValidData()
    {
      var responsibleId = _fixture.Create<Guid>();
      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true},
          new ProfessorEnrollment{ User = new User{Id = _fixture.Create<Guid>()}, IsResponsible = true}
        }
        });

      _mockedUserRepo
        .Setup(x => x.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User { Roles = new List<UserRole> { new UserRole { Role = Role.Professor } } });

      await _enrollmentService.UpdateProfessorEnrollments(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        new List<CreateProfessorEnrollmentDto>
        {
          new CreateProfessorEnrollmentDto { IsResponsible = true, UserId = responsibleId },
          new CreateProfessorEnrollmentDto { IsResponsible = true, UserId = responsibleId },
          new CreateProfessorEnrollmentDto { UserId = _fixture.Create<Guid>()}
        },
        responsibleId);
    }

    [Fact]
    public async Task Student_Should_Fail_When_NotProfessor()
    {
      var responsibleId = _fixture.Create<Guid>();
      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
          {
            /*new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true}*/
          }
        });


      await Assert.ThrowsAsync<AuthorizationException>(() => _enrollmentService.EnrollStudent(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        responsibleId));
    }

    [Fact]
    public async Task Student_Should_Fail_When_AlreadyEnrolled()
    {
      var responsibleId = _fixture.Create<Guid>();
      var studentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true},
          new StudentEnrollment{User = new User{Id = studentId}}
        }
        });


      await Assert.ThrowsAsync<ConflictException>(() => _enrollmentService.EnrollStudent(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        studentId,
        responsibleId));
    }

    [Fact]
    public async Task Student_Should_Fail_When_UserNotExists()
    {
      var responsibleId = _fixture.Create<Guid>();
      var studentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true}
        }
        });

      _mockedUserRepo
        .Setup(x => x.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((User)null);

      await Assert.ThrowsAsync<ConflictException>(() => _enrollmentService.EnrollStudent(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        studentId,
        responsibleId));
    }

    [Fact]
    public async Task Student_Should_Fail_When_UserNotStudent()
    {
      var responsibleId = _fixture.Create<Guid>();
      var studentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true}
        }
        });

      _mockedUserRepo
        .Setup(x => x.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User { Roles = new List<UserRole> { new UserRole { Role = Role.Professor } } });

      await Assert.ThrowsAsync<ConflictException>(() => _enrollmentService.EnrollStudent(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        studentId,
        responsibleId));
    }

    [Fact]
    public async Task Student_Should_Fail_When_BedeliasReject()
    {
      var responsibleId = _fixture.Create<Guid>();
      var studentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          ValidateWithBedelia = true,
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true}
        }
        });

      _bedeliasService
        .Setup(x => x.ValidateInscription(It.IsAny<ValidateInscriptionDto>()))
        .ReturnsAsync(new BedeliasResponseDto { Approved = false });

      _mockedUserRepo
        .Setup(x => x.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User { CI = 12345678, Roles = new List<UserRole> { new UserRole { Role = Role.Student } } });

      await Assert.ThrowsAsync<ValidationException>(() => _enrollmentService.EnrollStudent(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        studentId,
        responsibleId));
    }

    [Fact]
    public async Task Student_Should_Enroll_When_ValidData()
    {
      var responsibleId = _fixture.Create<Guid>();
      var studentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          ValidateWithBedelia = true,
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true}
        }
        });

      _bedeliasService
        .Setup(x => x.ValidateInscription(It.IsAny<ValidateInscriptionDto>()))
        .ReturnsAsync(new BedeliasResponseDto { Approved = true });

      _mockedUserRepo
        .Setup(x => x.GetUserById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new User { CI = 12345671, Roles = new List<UserRole> { new UserRole { Role = Role.Student } } });

      await _enrollmentService.EnrollStudent(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        studentId,
        responsibleId);
    }

    [Fact]
    public async Task GetEnrollments_Should_Fail_When_NotEnrolled()
    {
      var responsibleId = _fixture.Create<Guid>();
      var studentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true},
          new StudentEnrollment{User = new User{Id = studentId}}
        }
        });

      await Assert.ThrowsAsync<AuthorizationException>(() => _enrollmentService.GetSubjectEnrollments(_fixture.Create<Guid>(), _fixture.Create<Guid>(), _fixture.Create<Guid>()));
    }

    [Fact]
    public async Task GetEnrollments_Should_Return_When_ValidData()
    {
      var responsibleId = _fixture.Create<Guid>();
      var studentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{ User = new User{Id = responsibleId}, IsResponsible = true},
          new StudentEnrollment{User = new User{Id = studentId}}
        }
        });

      await _enrollmentService.GetSubjectEnrollments(_fixture.Create<Guid>(), _fixture.Create<Guid>(), studentId);
    }

    [Fact]
    public async Task DeleteEnrollment_Should_Fail_When_NotResponsible()
    {
      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject { Enrollments = new List<Enrollment>() });

      await Assert.ThrowsAsync<AuthorizationException>(() => _enrollmentService.DeleteEnrollment(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>()));
    }

    [Fact]
    public async Task DeleteEnrollment_Should_Fail_When_EnrollmentNotExists()
    {
      var responsibleId = _fixture.Create<Guid>();
      var enrollmentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{User = new User{Id = responsibleId}, IsResponsible = true}
        }
        });

      await Assert.ThrowsAsync<ConflictException>(() => _enrollmentService.DeleteEnrollment(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        responsibleId));
    }

    [Fact]
    public async Task DeleteEnrollment_Should_Update_When_ValidData()
    {
      var responsibleId = _fixture.Create<Guid>();
      var enrollmentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{Id = enrollmentId, User = new User{Id = responsibleId}, IsResponsible = true},
        }
        });

      await _enrollmentService.DeleteEnrollment(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        enrollmentId,
        responsibleId);
    }



    [Fact]
    public async Task UpdateProffessorRole_Should_Fail_When_NotResponsible()
    {
      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject { Enrollments = new List<Enrollment>() });

      await Assert.ThrowsAsync<AuthorizationException>(() => _enrollmentService.UpdateProfessorRole(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        true,
        _fixture.Create<Guid>()));
    }

    [Fact]
    public async Task UpdateProffessorRole_Should_Fail_When_EnrollmentNotExists()
    {
      var responsibleId = _fixture.Create<Guid>();
      var enrollmentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{User = new User{Id = responsibleId}, IsResponsible = true}
        }
        });

      await Assert.ThrowsAsync<ConflictException>(() => _enrollmentService.UpdateProfessorRole(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        true,
        responsibleId));
    }

    [Fact]
    public async Task UpdateProffessorRole_Should_Update_When_ValidData()
    {
      var responsibleId = _fixture.Create<Guid>();
      var enrollmentId = _fixture.Create<Guid>();

      _mockedSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{Id = enrollmentId, User = new User{Id = responsibleId}, IsResponsible = true},
        }
        });

      await _enrollmentService.UpdateProfessorRole(
        _fixture.Create<Guid>(),
        _fixture.Create<Guid>(),
        enrollmentId,
        true,
        responsibleId);
    }
  }
}
