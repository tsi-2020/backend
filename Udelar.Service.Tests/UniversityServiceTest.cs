﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Moq;
using Udelar.Common.Dtos.Poll.Request;
using Udelar.Common.Dtos.University.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class UniversityServiceTest
  {
    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly Mock<IUniversitiesRepository> _mockUniRepo;
    private readonly Mock<IAuthService> _mockAuthService;
    private readonly Mock<IConfiguration> _mockConfig;
    private readonly IUniversitiesService _service;

    public UniversityServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));

      _mockAuthService = new Mock<IAuthService>();
      _mockUniRepo = new Mock<IUniversitiesRepository>();
      _mockConfig = new Mock<IConfiguration>();
      _service = new UniversitiesService(mapper, _mockUniRepo.Object, _mockAuthService.Object, _mockConfig.Object);
    }

    [Fact]
    public async Task Should_FailCreateUniversity_When_ExistPath()
    {
      _mockUniRepo
        .Setup(x => x.FindByPath(It.IsAny<string>()))
        .ReturnsAsync(new University());

      await Assert.ThrowsAsync<ConflictException>(() => _service.CreateUniversity(_fixture.Create<CreateUniversityDto>()));
    }

    [Fact]
    public async Task Should_CreateUniversity_When_ValidData()
    {
      _mockUniRepo
        .Setup(x => x.FindByPath(It.IsAny<string>()))
        .ReturnsAsync((University)null);
      await _service.CreateUniversity(_fixture.Create<CreateUniversityDto>());
    }

    [Fact]
    public async Task Should_ReturnAllUniversities_When_ValidData()
    {
      _mockUniRepo
        .Setup(x => x.FindUniversityAll())
        .ReturnsAsync(new List<University>());
      await _service.GetUniversities();
    }


    [Fact]
    public async Task Should_FailUpdateUniversity_When_ExistPath()
    {
      _mockUniRepo
        .Setup(x => x.FindByPath(It.IsAny<string>()))
        .ReturnsAsync(new University());

      _mockUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(new University());

      await Assert.ThrowsAsync<ConflictException>(() => _service.EditUniversity(_fixture.Create<UpdateUniversityDto>()));
    }

    [Fact]
    public async Task Should_UpdateUniversity_When_ValidData()
    {
      _mockUniRepo
        .Setup(x => x.FindByPath(It.IsAny<string>()))
        .ReturnsAsync((University)null);

      _mockUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(new University());

      await _service.EditUniversity(_fixture.Create<UpdateUniversityDto>());
    }

    [Fact]
    public async Task Should_DeleteUniversity_When_ValidData()
    {
      await _service.DeleteUniversity(_fixture.Create<Guid>());
    }


    [Fact]
    public async Task Should_GetUniversity_When_ValidData()
    {

      _mockUniRepo
        .Setup(x => x.FindUniversityById(It.IsAny<Guid>()))
        .ReturnsAsync(new University());

      await _service.GetUniversityById(_fixture.Create<Guid>());
    }


    [Fact]
    public async Task Should_GetUniversityByPath_When_ValidData()
    {
      _mockUniRepo
        .Setup(x => x.FindByPath(It.IsAny<string>()))
        .ReturnsAsync(new University());

      await _service.GetUniversityByPath(_fixture.Create<string>());
    }

    [Fact]
    public async Task Should_Fail_When_PathNotFound()
    {
      _mockUniRepo
        .Setup(x => x.FindByPath(It.IsAny<string>()))
        .ReturnsAsync((University)null);

      await Assert.ThrowsAsync<EntityNotFoundException>(() => _service.GetUniversityByPath(_fixture.Create<string>()));
    }
  }


}
