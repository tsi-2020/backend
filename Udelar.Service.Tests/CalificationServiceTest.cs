﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Udelar.Common.Dtos.University.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class CalificationServiceTest
  {
    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly Mock<ISubjectRepository> _mockSubjectRepo;
    private readonly ICalificationService _service;

    public CalificationServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));
      _mockSubjectRepo = new Mock<ISubjectRepository>();
      _service = new CalificationService(_mockSubjectRepo.Object, mapper);
    }

    [Fact]
    public async Task Should_Fail_When_IsNotEnrolled()
    {
      _mockSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject { Enrollments = new List<Enrollment>() });

      await Assert.ThrowsAsync<AuthorizationException>(() => _service.GetSubjectCalifications(_fixture.Create<Guid>(), _fixture.Create<Guid>(), _fixture.Create<Guid>()));
    }

    [Fact]
    public async Task Should_Fail_When_IsNotProfessor()
    {
      var userId = _fixture.Create<Guid>();

      _mockSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject { Enrollments = new List<Enrollment> { new StudentEnrollment { User = new User { Id = userId } } } });

      await Assert.ThrowsAsync<AuthorizationException>(() => _service.GetSubjectCalifications(_fixture.Create<Guid>(), _fixture.Create<Guid>(), userId));
    }


    [Fact]
    public async Task Should_Return_When_ValidData()
    {
      var userId = _fixture.Create<Guid>();

      _mockSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{User = new User{Id = userId}},
          new StudentEnrollment{User = new User(),  Score = 10}
        }
        });

      await _service.GetSubjectCalifications(_fixture.Create<Guid>(), _fixture.Create<Guid>(), userId);
    }


    [Fact]
    public async Task Should_GenerateSubjectCalifications()
    {
      var userId = _fixture.Create<Guid>();

      _mockSubjectRepo
        .Setup(x => x.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject
        {
          Enrollments = new List<Enrollment>
        {
          new ProfessorEnrollment{User = new User{Id = userId}},
          new StudentEnrollment{User = new User(),  Score = 10}
        }
        });

      await _service.GenerateSubjectCalifications(_fixture.Create<Guid>(), _fixture.Create<Guid>(), userId);
    }
  }


}
