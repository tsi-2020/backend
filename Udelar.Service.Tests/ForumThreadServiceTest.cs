﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Udelar.Common.Dtos.Thread.Request;
using Udelar.Common.Exceptions;
using Udelar.Model.Mongo;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class ForumThreadServiceTest
  {
    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly ThreadService _threadService;
    private readonly Mock<IForumRepository> _forumRepoMock;
    private readonly Mock<ISubjectRepository> _subjectRepoMock;
    private readonly Mock<IThreadRepository> _threadRepoMock;

    public ForumThreadServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));

      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
        .ForEach(b => _fixture.Behaviors.Remove(b));
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _forumRepoMock = new Mock<IForumRepository>();
      _subjectRepoMock = new Mock<ISubjectRepository>();
      _threadRepoMock = new Mock<IThreadRepository>();
      _threadService = new ThreadService(_threadRepoMock.Object, _forumRepoMock.Object, mapper);
    }

    [Fact]
    public async Task Should_CreateForum()
    {
      _forumRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Forum(true));

      _threadRepoMock
        .Setup(r => r.Create(It.IsAny<CreateThreadDto>(), It.IsAny<Forum>(), It.IsAny<Guid>()))
        .ReturnsAsync(new ForumThread(true));

      await _threadService.CreateThread(_fixture.Create<CreateThreadDto>(), _fixture.Create<string>(),
        _fixture.Create<Guid>());

    }

    [Fact]
    public async Task Should_Fail_CreateThread_When_ForumNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _threadService.CreateThread(_fixture.Create<CreateThreadDto>(), _fixture.Create<string>(),
          _fixture.Create<Guid>())
      );
    }

    [Fact]
    public async Task Should_Fail_ListThreads_When_ForumNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _threadService.ListThreads(_fixture.Create<Guid>(),
          _fixture.Create<string>()
          , _fixture.Create<FilterThreadDto>())
      );
    }

    [Fact]
    public async Task Should_ListThreads()
    {
      _forumRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Forum(true));
      _threadRepoMock
        .Setup(r => r.List(It.IsAny<Guid>(),
          It.IsAny<Forum>(), It.IsAny<int>(), It.IsAny<int>()))
        .ReturnsAsync(() =>
          new Tuple<long, List<ForumThread>>(1, new List<ForumThread> { new ForumThread(true) })
        );
      await _threadService.ListThreads(_fixture.Create<Guid>(), _fixture.Create<string>(),
        _fixture.Create<FilterThreadDto>());
    }

    [Fact]
    public async Task Should_EditThread()
    {
      _threadRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new ForumThread(true));
      await _threadService.EditThread(_fixture.Create<EditThreadDto>(), _fixture.Create<string>(),
        _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_Fail_EditThread_When_ThreadNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _threadService.EditThread(_fixture.Create<EditThreadDto>(), _fixture.Create<string>(),
          _fixture.Create<Guid>())
      );
    }


    [Fact]
    public async Task Should_DeleteThread()
    {
      _threadRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new ForumThread(true));
      await _threadService.DeleteThread(_fixture.Create<string>(),
        _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_Fail_DeleteThread_When_ThreadNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _threadService.DeleteThread(_fixture.Create<string>(),
          _fixture.Create<Guid>())
      );
    }


    [Fact]
    public async Task Should_ViewThread()
    {
      _threadRepoMock
        .Setup(r => r.GetById(It.IsAny<string>(), It.IsAny<Guid>()))
        .ReturnsAsync(new ForumThread(true));
      _threadRepoMock
        .Setup(r => r.GetThreadMessages(It.IsAny<ForumThread>(), It.IsAny<int>(), It.IsAny<int>()))
        .ReturnsAsync(
          new Tuple<long, List<Message>>(1, new List<Message>
          {
            new Message()
          })
);
      await _threadService.ViewThread(_fixture.Create<Guid>(),
        _fixture.Create<string>(), _fixture.Create<FilterThreadDto>());
    }

    [Fact]
    public async Task Should_Fail_ViewThread_When_ThreadNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _threadService.ViewThread(_fixture.Create<Guid>(),
        _fixture.Create<string>(), _fixture.Create<FilterThreadDto>())
      );
    }
  }
}
