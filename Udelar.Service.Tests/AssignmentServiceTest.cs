﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Udelar.Common.Dtos.Assigment.request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Model.Component;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class AssignmentServiceTest
  {

    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly Mock<IAssignmentRepository> _assigmentRepoMock;
    private readonly Mock<ISubjectRepository> _subjectRepoMock;
    private readonly AssignmentService _assignmentService;


    public AssignmentServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));

      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
        .ForEach(b => _fixture.Behaviors.Remove(b));

      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());

      //Repo mocks
      _assigmentRepoMock = new Mock<IAssignmentRepository>();
      _subjectRepoMock = new Mock<ISubjectRepository>();

      _assignmentService = new AssignmentService(_assigmentRepoMock.Object, _subjectRepoMock.Object, mapper);
    }

    [Fact]
    public async Task Should_GetSubjectAssignmentById_When_StudentEnrollment_IsOk()
    {
      var userId = Guid.NewGuid();

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Subject
          {
            Enrollments = new HashSet<Enrollment>
            {
              new StudentEnrollment
              { User = new User
                {
                  Id = userId
                }
              }
            }
          }
        );

      _assigmentRepoMock
        .Setup(c => c.FindAssignmentByIdWithDeliveries(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Assignment
        {
          DeliveredAssignments = new List<DeliveredAssignment>
            {
              new IndividualDelivery
              {
                User = new User
                {
                  Id = userId
                }
              }
            }
        });

      _assigmentRepoMock
        .Setup(c => c.GetAssignmentComponentByAssignmentId(It.IsAny<Guid>()))
        .ReturnsAsync(new AssignmentComponent
        {
          Title = "asd",
          Text = "asd"
        });

      await _assignmentService.GetSubjectAssignmentById(
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          userId
        );
    }

    [Fact]
    public async Task Should_GetSubjectAssignmentById_When_ProfessorEnrollment_IsOk()
    {
      var userId = Guid.NewGuid();

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Subject
          {
            Enrollments = new HashSet<Enrollment>
            {
              new ProfessorEnrollment
              { User = new User
                {
                  Id = userId
                }
              }
            }
          }
        );

      _assigmentRepoMock
        .Setup(c => c.FindAssignmentById(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Assignment
        {
          DeliveredAssignments = new List<DeliveredAssignment>
            {
              new IndividualDelivery
              {
                User = new User
                {
                  Id = userId
                }
              }
            }
        });

      _assigmentRepoMock
        .Setup(c => c.GetAssignmentComponentByAssignmentId(It.IsAny<Guid>()))
        .ReturnsAsync(new AssignmentComponent
        {
          Title = "asd",
          Text = "asd"
        });

      await _assignmentService.GetSubjectAssignmentById(
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          userId
        );
    }

    [Fact]
    public async Task Should_ThrowAuthorizationException_When_InvalidSubject()
    {
      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject());

      await Assert.ThrowsAsync<AuthorizationException>(() =>
        _assignmentService.GetSubjectAssignmentById(
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>()
        )
      );
    }

    [Fact]
    public async Task Should_ThrowAuthorizationException_CreateAssignmentDelivery_When_Enrollment_IsNotStudentEnrollment()
    {
      var userId = Guid.NewGuid();

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Subject
          {
            Enrollments = new HashSet<Enrollment>
            {
              new ProfessorEnrollment
              { User = new User
                {
                  Id = userId
                }
              }
            }
          }
        );

      await Assert.ThrowsAsync<AuthorizationException>(() =>
        _assignmentService.CreateAssignmentDelivery(
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          userId,
          _fixture.Create<CreateAssignmentDeliveryDto>()
        )
      );
    }

    [Fact]
    public async Task Should_ThrowAuthorizationException_CreateAssignmentDelivery_When_Assigment_IsNotOk()
    {
      var userId = Guid.NewGuid();

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject());

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Subject
          {
            Enrollments = new HashSet<Enrollment>
            {
              new StudentEnrollment
              { User = new User
                {
                  Id = userId
                }
              }
            }
          }
        );

      _assigmentRepoMock
        .Setup(c => c.FindAssignmentByIdWithDeliveries(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Assignment
        {
          AvailableUntil = DateTime.MinValue
        });

      await Assert.ThrowsAsync<AuthorizationException>(() =>
        _assignmentService.CreateAssignmentDelivery(
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          userId,
          _fixture.Create<CreateAssignmentDeliveryDto>()
        )
      );
    }

    [Fact]
    public async Task Should_CreateAssignmentDelivery_When_DataIsOk()
    {
      var userId = Guid.NewGuid();

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject());

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Subject
          {
            Enrollments = new HashSet<Enrollment>
            {
              new StudentEnrollment
              { User = new User
                {
                  Id = userId
                }
              }
            }
          }
        );

      _assigmentRepoMock
        .Setup(c => c.FindAssignmentByIdWithDeliveries(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Assignment
        {
          AvailableUntil = DateTime.MaxValue
        });

      _assigmentRepoMock
        .Setup(c => c.FindAssignmentById(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Assignment
        {
          DeliveredAssignments = new List<DeliveredAssignment>
            {
              new IndividualDelivery
              {
                User = new User
                {
                  Id = userId,
                }
              }
            }
        });

      await _assignmentService.CreateAssignmentDelivery(
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          userId,
          _fixture.Create<CreateAssignmentDeliveryDto>()
      );
    }

    [Fact]
    public async Task Should_ThrowAuthorizationException_UpdateSubjectAssignment_When_Enrollment_IsNotOk()
    {
      var userId = Guid.NewGuid();

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject());

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Subject
          {
            Enrollments = new HashSet<Enrollment>
            {
              new StudentEnrollment
              { User = new User
                {
                  Id = userId
                }
              }
            }
          }
        );

      await Assert.ThrowsAsync<AuthorizationException>(() =>
        _assignmentService.UpdateSubjectAssignment(
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<UpdateAssignmentDto>(),
          userId
        )
      );
    }

    [Fact]
    public async Task Should_UpdateSubjectAssignment_When_Data_IsOk()
    {
      var userId = Guid.NewGuid();

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject());

      _subjectRepoMock
        .Setup(c => c.FindSubjectWithEnrollments(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Subject
          {
            Enrollments = new HashSet<Enrollment>
            {
              new ProfessorEnrollment
              { User = new User
                {
                  Id = userId
                }
              }
            }
          }
        );

      _assigmentRepoMock
        .Setup(c => c.FindAssignmentById(It.IsAny<Guid>(), It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Assignment
        {
          DeliveredAssignments = new List<DeliveredAssignment>
            {
              new IndividualDelivery
              {
                User = new User
                {
                  Id = userId
                }
              }
            }
        });

      _assignmentService.UpdateSubjectAssignment(
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<Guid>(),
          _fixture.Create<UpdateAssignmentDto>(),
          userId
      );
    }

  }
}
