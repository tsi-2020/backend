﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Moq;
using Udelar.Common.Dtos.Calendar.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class CalendarServiceTest
  {
    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly CalendarService _calendarService;
    private Mock<ICalendarRepository> _calendarMockRepo;
    private Mock<ISubjectRepository> _subjectRepoMock;

    public CalendarServiceTest(ITestOutputHelper output)
    {
      _output = output;
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));

      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
        .ForEach(b => _fixture.Behaviors.Remove(b));
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _subjectRepoMock = new Mock<ISubjectRepository>();
      _calendarMockRepo = new Mock<ICalendarRepository>();
      _calendarService = new CalendarService(mapper, _subjectRepoMock.Object, _calendarMockRepo.Object);
    }

    [Fact]
    public async Task Should_CreateEvent()
    {
      _subjectRepoMock
        .Setup(r => r.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject());
      await
        _calendarService.CreateEvent(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
          _fixture.Create<CreateEventDto>()
        );
    }


    [Fact]
    public async Task Should_Fail_CreateEvent_When_SubjectNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _calendarService.CreateEvent(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
          _fixture.Create<CreateEventDto>()
        )
      );
    }

    [Fact]
    public async Task Should_FilterEvent()
    {
      _subjectRepoMock
        .Setup(r => r.FindByIdAndUniversity(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(new Subject());
      await
        _calendarService.FilterEvents(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
          _fixture.Create<FilterEventsDto>()
        );
    }


    [Fact]
    public async Task Should_Fail_FilterEvent_SubjectNotFound()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _calendarService.FilterEvents(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
          _fixture.Create<FilterEventsDto>()
        )
      );
    }
  }
}
