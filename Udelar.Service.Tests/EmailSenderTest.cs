﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using Microsoft.Extensions.Logging;
using Moq;
using Moq.Protected;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Xunit;
using Xunit.Abstractions;

namespace Uderlar.Service.Tests
{
  public class EmailSenderTest
  {
    private readonly ITestOutputHelper _output;
    private readonly Fixture _fixture = new Fixture();
    private readonly EmailSender _emailService;
    private readonly Mock<ILogger<EmailSender>> _loggerMock;
    private readonly Mock<IMessageRepository> _messageRepoMock;
    private Mock<HttpMessageHandler> _mockMessageHandler;

    public EmailSenderTest(ITestOutputHelper output)
    {
      _output = output;

      _fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
        .ForEach(b => _fixture.Behaviors.Remove(b));
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _loggerMock = new Mock<ILogger<EmailSender>>();
      _messageRepoMock = new Mock<IMessageRepository>();

      _mockMessageHandler = new Mock<HttpMessageHandler>();
      _mockMessageHandler.Protected()
        .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
          ItExpr.IsAny<CancellationToken>())
        .ReturnsAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.OK, });
      var httpClient = new HttpClient(_mockMessageHandler.Object);
      httpClient.BaseAddress = new Uri("http://google.com/");
      _emailService = new EmailSender(httpClient, _loggerMock.Object);
    }

    [Fact]
    public async Task Should_SendEmail()
    {
      await _emailService.SendMail(_fixture.Create<string>(), _fixture.Create<string>(), _fixture.Create<string>());
    }

    [Fact]
    public async Task Should_Fail_SendEmail()
    {
      _mockMessageHandler.Protected()
        .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
          ItExpr.IsAny<CancellationToken>())
        .ReturnsAsync(new HttpResponseMessage { StatusCode = HttpStatusCode.BadRequest });
      await _emailService.SendMail(_fixture.Create<string>(), _fixture.Create<string>(), _fixture.Create<string>());
    }
  }
}
