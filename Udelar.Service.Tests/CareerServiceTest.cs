﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using AutoFixture;
using AutoMapper;
using Castle.Components.DictionaryAdapter;
using Moq;
using Udelar.Common.Dtos.Career.Request;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;
using Xunit;

namespace Uderlar.Service.Tests
{
  public class CareerServiceTest
  {
    public CareerServiceTest()
    {
      var mapper = new Mapper(new MapperConfiguration(expression =>
      {
        expression.AddProfile<DtoToModelProfile>();
        expression.AddProfile<ModelToDtoProfile>();
      }));
      _careerRepository = new Mock<ICareerRepository>();
      _subjectRepository = new Mock<ISubjectRepository>();
      _careerService = new CareerService(_careerRepository.Object, mapper, _subjectRepository.Object);
    }

    private readonly Mock<ICareerRepository> _careerRepository;
    private readonly Mock<ISubjectRepository> _subjectRepository;
    private readonly CareerService _careerService;
    private readonly Fixture _fixture = new Fixture();

    [Fact]
    public async Task Should_CreateACareer_When_DataIsOk()
    {
      await _careerService.CreateCareer(_fixture.Create<Guid>(), _fixture.Create<CreateCareerDto>());
    }

    [Fact]
    public async Task Should_DeleteCareer_When_IdDoesExist()
    {
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityPollPublications)
        .Without(u => u.UniversityAdministrators)
        .Without(u => u.SubjectTemplates)
          .Without(p => p.Notices)
      );
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Career { Id = Guid.NewGuid(), Name = _fixture.Create<string>(), University = _fixture.Create<University>() }
        );
      await _careerService.DeleteCareer(_fixture.Create<Guid>(), _fixture.Create<Guid>());
    }

    [Fact]
    public async Task Should_FindCareerById_When_CareerIdIsPassed()
    {
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityPollPublications)
        .Without(u => u.UniversityAdministrators)
          .Without(p => p.Notices)
        .Without(u => u.SubjectTemplates)
        .Without(u => u.UniversityPollPublications)
      );
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Career { Id = Guid.NewGuid(), Name = _fixture.Create<string>(), University = _fixture.Create<University>() }
        );
      var x = await _careerService.GetUniversityCareerById(_fixture.Create<Guid>(), _fixture.Create<Guid>());
      Assert.NotNull(x);
    }

    [Fact]
    public async Task Should_ListAllCareers_When_CareerIdIsPassed()
    {
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityAdministrators)
        .Without(u => u.SubjectTemplates)
        .Without(u => u.UniversityPollPublications)
          .Without(p => p.Notices)
      );
      _careerRepository
        .Setup(repository => repository.FindUniversityCareers(It.IsAny<Guid>(), It.IsAny<string>()))
        .ReturnsAsync(new List<Career>
        {
          new Career
          {
            Id = Guid.NewGuid(), Name = _fixture.Create<string>(), University = _fixture.Create<University>()
          }
        });
      var x = await _careerService.GetAllUniversityCareers(_fixture.Create<Guid>(), _fixture.Create<string>());
      Assert.NotNull(x);
      Assert.NotEmpty(x);
    }


    [Fact]
    public async Task Should_ThorwEntityNotFound_When_CareerIdDoesNotExist()
    {
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((Career)null);
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _careerService.GetUniversityCareerById(_fixture.Create<Guid>(), _fixture.Create<Guid>()));
    }


    [Fact]
    public async Task Should_ThorwEntityNotFound_When_DeletingCareerAndIdDoesNotExist()
    {
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((Career)null);
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _careerService.DeleteCareer(_fixture.Create<Guid>(), _fixture.Create<Guid>()));
    }

    [Fact]
    public async Task Should_ThorwEntityNotFound_When_EditingCareerAndIdDoesNotExist()
    {
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync((Career)null);
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _careerService.EditCareer(_fixture.Create<Guid>(), _fixture.Create<EditCareerDto>()));
    }

    [Fact]
    public async Task Should_UpdateCareer_When_CareerAndIdDoesExist()
    {
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityAdministrators)
        .Without(u => u.UniversityPollPublications)
          .Without(p => p.Notices)
        .Without(u => u.SubjectTemplates)
      );
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Career { Id = Guid.NewGuid(), Name = _fixture.Create<string>(), University = _fixture.Create<University>() }
        );
      await _careerService.EditCareer(_fixture.Create<Guid>(), _fixture.Create<EditCareerDto>());
    }

    [Fact]
    public async Task Should_Fail_When_AddingSubjectsToNonFoundCareer()
    {
      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
        _careerService.AddSubjects(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
          _fixture.Create<List<Guid>>())
      );
    }


    [Fact]
    public async Task Should_Fail_When_AddingNonExistentsSubjects()
    {
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityAdministrators)
          .Without(p => p.Notices)
        .Without(u => u.SubjectTemplates)
        .Without(u => u.UniversityPollPublications)
      );
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Career { Id = Guid.NewGuid(), Name = _fixture.Create<string>(), University = _fixture.Create<University>() }
        );

      _subjectRepository
        .Setup(repository => repository.FindSubjectByIdWithCareers(It.IsAny<Guid>(), It.IsAny<List<Guid>>()))
        .ReturnsAsync(
          () => new EditableList<Subject>()
        );
      await Assert.ThrowsAsync<ValidationException>(() =>
        _careerService.AddSubjects(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
          _fixture.Create<List<Guid>>())
      );
    }


    [Fact]
    public async Task Should_Fail_When_AddingRepeatedSubjects()
    {
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityAdministrators)
          .Without(p => p.Notices)
        .Without(u => u.UniversityPollPublications)
        .Without(u => u.SubjectTemplates)
      );
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Career { Id = Guid.NewGuid(), Name = _fixture.Create<string>(), University = _fixture.Create<University>() }
        );

      var careerId = Guid.NewGuid();
      _subjectRepository
        .Setup(repository => repository.FindSubjectByIdWithCareers(It.IsAny<Guid>(), It.IsAny<List<Guid>>()))
        .ReturnsAsync(
          new List<Subject>
          {
            new Subject
            {
              Id = Guid.Empty,
              Name = "SomeName",
              CareerSubjects = new List<CareerSubject> {new CareerSubject {Career = new Career {Id = careerId}}}
            }
          }
        );
      await Assert.ThrowsAsync<ValidationException>(() =>
        _careerService.AddSubjects(_fixture.Create<Guid>(), careerId, new List<Guid> { Guid.Empty })
      );
    }


    [Fact]
    public async Task Should_Pass_When_AddingSubjects()
    {
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityAdministrators)
        .Without(u => u.UniversityPollPublications)
        .Without(u => u.SubjectTemplates)
          .Without(p => p.Notices)
      );
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Career { Id = Guid.NewGuid(), Name = _fixture.Create<string>(), University = _fixture.Create<University>() }
        );

      _subjectRepository
        .Setup(repository => repository.FindSubjectByIdWithCareers(It.IsAny<Guid>(), It.IsAny<List<Guid>>()))
        .ReturnsAsync(
          new List<Subject> { new Subject { Id = Guid.Empty, Name = "SomeName" } }
        );
      await _careerService.AddSubjects(_fixture.Create<Guid>(), _fixture.Create<Guid>(), new List<Guid> { Guid.Empty });
    }

    [Fact]
    public async Task Should_Fail_When_RemovingNonExistentSubjects()
    {
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityAdministrators)
        .Without(u => u.UniversityPollPublications)
          .Without(p => p.Notices)
        .Without(u => u.SubjectTemplates)
      );
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Career { Id = Guid.NewGuid(), Name = _fixture.Create<string>(), University = _fixture.Create<University>() }
        );

      _subjectRepository
        .Setup(repository => repository.FindSubjectByIdWithCareers(It.IsAny<Guid>(), It.IsAny<List<Guid>>()))
        .ReturnsAsync(
          () => new EditableList<Subject>()
        );
      await Assert.ThrowsAsync<ValidationException>(() =>
        _careerService.RemoveSubjects(_fixture.Create<Guid>(), _fixture.Create<Guid>(),
          _fixture.Create<List<Guid>>())
      );
    }


    [Fact]
    public async Task Should_Pass_When_RemovingSubjects()
    {
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityAdministrators)
        .Without(u => u.UniversityPollPublications)
        .Without(u => u.SubjectTemplates)
          .Without(p => p.Notices)
      );
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Career { Id = Guid.NewGuid(), Name = _fixture.Create<string>(), University = _fixture.Create<University>() }
        );

      var careerId = Guid.NewGuid();
      _subjectRepository
        .Setup(repository => repository.FindSubjectByIdWithCareers(It.IsAny<Guid>(), It.IsAny<List<Guid>>()))
        .ReturnsAsync(
          new List<Subject>
          {
            new Subject
            {
              Id = Guid.Empty,
              Name = "SomeName",
              CareerSubjects = new List<CareerSubject> {new CareerSubject {Career = new Career {Id = careerId}}}
            }
          }
        );
      await _careerService.RemoveSubjects(_fixture.Create<Guid>(), careerId, new List<Guid> { Guid.Empty });
    }

    [Fact]
    public async Task Should_Fail_When_RemovingNonExistentsSubjects()
    {
      _fixture.Behaviors.Add(new OmitOnRecursionBehavior());
      _fixture.Customize<University>(c => c.Without(u => u.Careers)
        .Without(u => u.UniversityPollPublications)
        .Without(u => u.UniversityAdministrators)
        .Without(u => u.SubjectTemplates)
          .Without(p => p.Notices)
      );
      _careerRepository
        .Setup(repository => repository.FindUniversityCareerById(It.IsAny<Guid>(), It.IsAny<Guid>()))
        .ReturnsAsync(
          new Career { Id = Guid.NewGuid(), Name = _fixture.Create<string>(), University = _fixture.Create<University>() }
        );

      var careerId = Guid.NewGuid();
      _subjectRepository
        .Setup(repository => repository.FindSubjectByIdWithCareers(It.IsAny<Guid>(), It.IsAny<List<Guid>>()))
        .ReturnsAsync(
          new List<Subject>
          {
            new Subject
            {
              Id = Guid.Empty,
              Name = "SomeName",
              CareerSubjects = new List<CareerSubject> {new CareerSubject {Career = new Career {Id = careerId}}}
            }
          }
        );
      await _careerService.RemoveSubjects(_fixture.Create<Guid>(), careerId, new List<Guid> { Guid.Empty });
    }

    [Fact]
    public async Task Should_Fail_When_UpdateCareerAndIdDoesExist()
    {

      await Assert.ThrowsAsync<EntityNotFoundException>(() =>
      _careerService.RemoveSubjects(_fixture.Create<Guid>(), _fixture.Create<Guid>(), new List<Guid> { Guid.Empty })
      );
    }
  }
}
