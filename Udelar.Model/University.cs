﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Udelar.Model.Poll.Publications;

namespace Udelar.Model
{
  //Facultad
  public class University : BaseModel
  {
    [Required] public string Name { get; set; }
    [Required] public string AccessPath { get; set; }

    [StringLength(9)] [Required] public string PrimaryColor { get; set; }
    [StringLength(9)] [Required] public string SecondaryColor { get; set; }
    [StringLength(250)] [Required] public string Logo { get; set; }
    [Required] public string Description { get; set; }

    public List<UniversityPollPublication> UniversityPollPublications { get; set; } =
      new List<UniversityPollPublication>();

    public ICollection<UniversityAdministrator> UniversityAdministrators { get; set; } =
      new HashSet<UniversityAdministrator>();

    public ICollection<Career> Careers { get; set; } = new HashSet<Career>();
    public ICollection<SubjectTemplate> SubjectTemplates { get; set; } = new List<SubjectTemplate>();

    public ICollection<Notice> Notices { get; set; } = new HashSet<Notice>();
  }
}
