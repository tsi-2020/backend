﻿namespace Udelar.Model
{
  public abstract class DeliveredAssignment : BaseModel
  {
    public float Score { get; set; }

    public string FileUrl { get; set; }

    public string FileName { get; set; }

    public string ProfessorComments { get; set; }

  }
}
