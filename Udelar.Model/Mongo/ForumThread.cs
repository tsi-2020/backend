﻿using System;
using System.ComponentModel.DataAnnotations;
using MongoDB.Entities;

namespace Udelar.Model.Mongo
{
  public class ForumThread : Entity, ICreatedOn, IModifiedOn
  {
    private readonly bool _testMode;

    public ForumThread(bool testMode)
    {
      _testMode = testMode;
    }

    public ForumThread()
    {
      this.InitOneToMany(() => Messages);
    }

    public Many<Message> Messages { get; set; }

    [Required] public Guid TenantId { get; set; }

    public One<Forum> Forum { get; set; }
    public string Title { get; set; }
    public DateTime? DeletedOn { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime ModifiedOn { get; set; }
  }
}
