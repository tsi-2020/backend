﻿using System;
using System.ComponentModel.DataAnnotations;
using MongoDB.Entities;

namespace Udelar.Model.Mongo
{
  public class Calendar : Entity, ICreatedOn, IModifiedOn
  {
    [Required] public Guid SubjectId { get; set; }
    [Required] public Guid TenantId { set; get; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public string Event { get; set; }
    public DateTime? DeletedOn { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime ModifiedOn { get; set; }
  }
}
