﻿using System;
using System.ComponentModel.DataAnnotations;
using MongoDB.Entities;

namespace Udelar.Model.Mongo
{
  public class Forum : Entity, ICreatedOn, IModifiedOn
  {

    private readonly bool _test;

    public Forum(bool test)
    {
      _test = test;
    }

    public Forum()
    {
      this.InitOneToMany(() => ForumThreads);
    }

    [Required] public Guid SubjectId { get; set; }
    [Required] public Guid TenantId { get; set; }
    public string Name { get; set; }
    public Many<ForumThread> ForumThreads { get; set; }
    public DateTime? DeletedOn { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime ModifiedOn { get; set; }
  }
}
