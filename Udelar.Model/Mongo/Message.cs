﻿using System;
using MongoDB.Entities;

namespace Udelar.Model.Mongo
{
  public class Message : Entity, ICreatedOn, IModifiedOn
  {
    public One<ForumThread> ForumThread { get; set; }
    public Guid UserId { get; set; }
    public string Text { get; set; }
    public string FullName { get; set; }
    public DateTime? DeletedOn { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime ModifiedOn { get; set; }
    public Guid TenantId { get; set; }
  }
}
