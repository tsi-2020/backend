﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  public abstract class BaseModel
  {
    [Key] public Guid Id { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime ModifiedOn { get; set; }
    public DateTime? DeletedOn { get; set; }
  }
}
