﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  public class Group : BaseModel
  {
    public ICollection<UserGroup> UserGroups { get; set; } = new HashSet<UserGroup>();
    [Required] public Subject Subject { get; set; }
    public ICollection<GroupDelivery> GrupalAssignments { get; set; } = new HashSet<GroupDelivery>();
  }
}
