﻿namespace Udelar.Model
{
  public class UserGroup : BaseModel
  {
    public User User { get; set; }
    public Group Group { get; set; }
  }
}
