﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  public class CareerSubject : BaseModel
  {
    [Required] public Career Career { get; set; }
    [Required] public Subject Subject { get; set; }
  }
}
