﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  //Carrera
  public class Career : Tenant
  {
    [Required] public string Name { get; set; }
    [Required] public ICollection<CareerSubject> CareerSubjects { get; set; } = new HashSet<CareerSubject>();
  }
}
