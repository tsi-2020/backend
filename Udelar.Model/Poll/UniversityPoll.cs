﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model.Poll
{
  public class UniversityPoll : Poll
  {
    [Required] public University University { get; set; }

    public bool IsInitialized { get; set; } = true;
  }
}
