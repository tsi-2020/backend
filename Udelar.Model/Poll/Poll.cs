﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Udelar.Model.Poll.Publications;
using Udelar.Model.Poll.Questions;

namespace Udelar.Model.Poll
{
  //Encuesta 
  public abstract class Poll : BaseModel
  {
    [Required] public string Title { get; set; }

    //Preguntas de la encuesta
    public HashSet<PollQuestion> Questions { get; set; } = new HashSet<PollQuestion>();

    //Respuestas publicadas por los alumnos
    public HashSet<PollResponse> PollResponses { get; set; } = new HashSet<PollResponse>();

    public HashSet<SubjectPollPublication> SubjectPollPublications { get; set; } =
      new HashSet<SubjectPollPublication>();

    public HashSet<UniversityPollPublication> UniversityPollPublications { get; set; } =
      new HashSet<UniversityPollPublication>();
  }
}
