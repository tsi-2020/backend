﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model.Poll.Questions
{
  public class PollChoice : BaseModel
  {
    [Required] public string Text { get; set; }
  }
}
