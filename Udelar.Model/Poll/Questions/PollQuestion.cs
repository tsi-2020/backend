﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model.Poll.Questions
{
  public abstract class PollQuestion : BaseModel
  {
    [Required] public string Text { get; set; }
    public int Position { get; set; }
  }
}
