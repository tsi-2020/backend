﻿using System.Collections.Generic;

namespace Udelar.Model.Poll.Questions
{
  public class MultipleChoicePollQuestion : PollQuestion
  {
    public bool IsMultipleSelect { get; set; } = false;
    public HashSet<PollChoice> Choices { get; set; } = new HashSet<PollChoice>();
  }
}
