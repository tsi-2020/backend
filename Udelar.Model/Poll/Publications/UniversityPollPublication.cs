using System.ComponentModel.DataAnnotations;

namespace Udelar.Model.Poll.Publications
{
  public class UniversityPollPublication: BaseModel
  {
    [Required] public Poll Poll { get; set; }
    [Required] public University University { get; set; }
  }
}
