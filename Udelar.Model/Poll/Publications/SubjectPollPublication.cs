using System.ComponentModel.DataAnnotations;

namespace Udelar.Model.Poll.Publications
{
  //Encuesta publicada
  public class SubjectPollPublication: BaseModel
  {
    [Required] public Poll Poll { get; set; }
    [Required] public Subject Subject { get; set; }
  }
}
