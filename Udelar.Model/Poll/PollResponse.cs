﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Udelar.Model.Poll.Answers;

namespace Udelar.Model.Poll
{
  //Solucion hecha por el usuario a un cuestionario
  public class PollResponse : BaseModel
  {
    //Respuestas del usuario
    public ICollection<PollAnswer> Answers { get; set; } = new List<PollAnswer>();

    //Que poll esta respondiendo
    [Required] public Poll Poll { get; set; }

    //Quien la responde
    [Required] public User User { get; set; }
  }
}
