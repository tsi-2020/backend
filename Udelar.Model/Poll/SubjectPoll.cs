﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model.Poll
{
  //Encuesta de un curso
  public class SubjectPoll : Poll
  {
    [Required] public Subject Subject { get; set; }
  }
}
