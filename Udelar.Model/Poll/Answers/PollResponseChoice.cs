﻿using Udelar.Model.Poll.Questions;

namespace Udelar.Model.Poll.Answers
{
  public class PollResponseChoice : BaseModel
  {
    public MultipleChoicePollAnswer MultipleChoicePollAnswer { get; set; }
    public PollChoice Choice { get; set; }

  }
}
