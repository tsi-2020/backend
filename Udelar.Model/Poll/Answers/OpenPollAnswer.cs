﻿using System.ComponentModel.DataAnnotations;
using Udelar.Model.Poll.Questions;

namespace Udelar.Model.Poll.Answers
{
  public class OpenPollAnswer : PollAnswer
  {
    public string Text { get; set; }
    [Required] public OpenPollQuestion OpenPollQuestion { get; set; }
  }
}
