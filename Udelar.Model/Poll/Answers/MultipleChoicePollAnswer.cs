﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Udelar.Model.Poll.Questions;

namespace Udelar.Model.Poll.Answers
{
  public class MultipleChoicePollAnswer : PollAnswer
  {
    [Required] public MultipleChoicePollQuestion MultipleChoicePollQuestion { get; set; }
    public ICollection<PollResponseChoice> Choices { get; set; } = new HashSet<PollResponseChoice>();
  }
}
