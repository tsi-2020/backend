﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  public class UserRole : BaseModel
  {
    [Required] public Role Role { get; set; }
    [Required] public User User { get; set; }
  }
}
