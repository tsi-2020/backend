﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Udelar.Model.Component;
using Udelar.Model.Mongo;
using Udelar.Model.Poll;
using Udelar.Model.Poll.Publications;

namespace Udelar.Model
{
  //Curso
  public class Subject : Tenant
  {
    [Required] public string Name { get; set; }
    [Required] public ICollection<CareerSubject> CareerSubjects { get; set; } = new HashSet<CareerSubject>();
    [Required] public bool ValidateWithBedelia { get; set; }
    [Required] public bool IsRemote { get; set; }
    public ICollection<Notice> Notices { get; set; } = new HashSet<Notice>();
    public ICollection<Enrollment> Enrollments { get; set; } = new HashSet<Enrollment>();
    public ICollection<Quiz.Quiz> Quizzes { get; set; } = new HashSet<Quiz.Quiz>();
    public ICollection<SubjectPollPublication> SubjectPollPublications { get; set; } = new HashSet<SubjectPollPublication>();
    public ICollection<Assignment> Assignments { get; set; } = new HashSet<Assignment>();
    public ICollection<EducationalUnit> Components { get; set; } = new List<EducationalUnit>();
    [NotMapped] public ICollection<Forum> Forums { get; set; } = new HashSet<Forum>();
  }
}
