﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  public abstract class Tenant : BaseModel
  {
    [Required] public University University { get; set; }
  }
}
