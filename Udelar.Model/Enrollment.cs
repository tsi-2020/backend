﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  public class Enrollment : BaseModel
  {
    [Required] public User User { get; set; }
    [Required] public Subject Subject { get; set; }
  }

  public class StudentEnrollment : Enrollment
  {
    public float Score { get; set; }
  }

  public class ProfessorEnrollment : Enrollment
  {
    public bool IsResponsible { get; set; }
  }
}
