﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  public class GroupDelivery : DeliveredAssignment
  {
    [Required] public Group Group { get; set; }
  }
}
