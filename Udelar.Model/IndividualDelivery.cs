﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  public class IndividualDelivery : DeliveredAssignment
  {
    [Required] public User User { get; set; }
  }
}
