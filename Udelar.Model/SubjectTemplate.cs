﻿using System.Collections.Generic;
using Udelar.Model.Component;

namespace Udelar.Model
{
  //Plantilla para visualizacion de curso
  public class SubjectTemplate : Tenant
  {
    public string Title { get; set; }

    public string Description { get; set; }

    public ICollection<EducationalUnit> Components { get; set; } = new List<EducationalUnit>();
  }
}
