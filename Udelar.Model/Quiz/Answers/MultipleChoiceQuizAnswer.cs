﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Udelar.Model.Quiz.Questions;

namespace Udelar.Model.Quiz.Answers
{
  public class MultipleChoiceQuizAnswer : QuizAnswer
  {
    [Required] public MultipleChoiceQuizQuestion MultipleChoiceQuizQuestion { get; set; }
    public ICollection<QuizChoice> Choices { get; set; } = new HashSet<QuizChoice>();
  }
}
