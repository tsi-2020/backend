﻿using System.ComponentModel.DataAnnotations;
using Udelar.Model.Quiz.Questions;

namespace Udelar.Model.Quiz.Answers
{
  public class TrueFalseQuizAnswer : QuizAnswer
  {
    [Required] public TrueFalseQuizQuestion TrueFalseQuizQuestion { get; set; }
    [Required] public bool SelectedAnswer { get; set; }
  }
}
