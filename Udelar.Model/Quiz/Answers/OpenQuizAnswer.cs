﻿using System.ComponentModel.DataAnnotations;
using Udelar.Model.Quiz.Questions;

namespace Udelar.Model.Quiz.Answers
{
  public class OpenQuizAnswer : QuizAnswer
  {
    public string Text { get; set; }
    [Required] public OpenQuizQuestion OpenQuizQuestion { get; set; }
  }
}
