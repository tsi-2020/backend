﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Udelar.Model.Quiz.Answers;

namespace Udelar.Model.Quiz
{
  //Solucion hecha por el usuario a un cuestionario
  public class QuizResponse : BaseModel
  {
    //Sus respuestas
    public ICollection<QuizAnswer> Answers { get; set; }

    //Que cuestionario esta haciendo
    [Required] public Quiz Quiz { get; set; }

    //Quien la responde
    [Required] public User User { get; set; }
    public float Score { get; set; }
  }
}
