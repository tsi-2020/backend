﻿using System.Collections.Generic;

namespace Udelar.Model.Quiz.Questions
{
  public class MultipleChoiceQuizQuestion : QuizQuestion
  {
    public bool MultipleSelect { get; set; } = false;
    public ICollection<QuizChoice> Choices { get; set; } = new HashSet<QuizChoice>();
  }
}
