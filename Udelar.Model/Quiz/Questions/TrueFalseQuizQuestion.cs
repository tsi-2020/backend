﻿namespace Udelar.Model.Quiz.Questions
{
  public class TrueFalseQuizQuestion : QuizQuestion
  {
    public bool IsCorrect { get; set; }
  }
}
