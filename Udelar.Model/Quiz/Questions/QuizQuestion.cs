﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model.Quiz.Questions
{
  public abstract class QuizQuestion : BaseModel
  {
    [Required] public string Text { get; set; }
    public float Score { get; set; }
  }
}
