﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model.Quiz.Questions
{
  public class QuizChoice : BaseModel
  {
    public bool IsCorrect { get; set; } = false;
    [Required] public string Text { get; set; }
  }
}
