﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Udelar.Model.Quiz.Questions;

namespace Udelar.Model.Quiz
{
  //Cuestionario publicado
  public class Quiz : BaseModel
  {
    //Preguntas del cuestioario
    public ICollection<QuizQuestion> Questions { get; set; } = new HashSet<QuizQuestion>();

    //Respuestas publicadas por los alumnos
    public ICollection<QuizResponse> QuizResponses { get; set; } = new HashSet<QuizResponse>();

    //Materia a la que pertecene
    [Required] public Subject Subject { get; set; }
    [Required] public DateTime AvailableFrom { get; set; }

    [Required] public DateTime AvailableUntil { get; set; }

    // if 0 = no limit
    // in seconds
    public int TimeLimit { get; set; }
  }
}
