﻿namespace Udelar.Model
{
  public abstract class Administrator : BaseModel
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Password { get; set; }
    public string RefreshToken { get; set; }
    public string Email { get; set; }
  }
}
