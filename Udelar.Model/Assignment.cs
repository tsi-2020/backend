﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  public class Assignment : BaseModel
  {
    [Required] public Subject Subject { get; set; }
    public ICollection<DeliveredAssignment> DeliveredAssignments { get; set; } = new HashSet<DeliveredAssignment>();
    [Required] public bool IsIndividual { get; set; }

    [Required] public DateTime AvailableFrom { get; set; }
    [Required] public DateTime AvailableUntil { get; set; }
    public float MaxScore { get; set; }

    public bool IsInitialized { get; set; } = true;
  }
}
