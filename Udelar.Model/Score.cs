﻿namespace Udelar.Model
{
  public class Score : BaseModel
  {
    public User User { get; set; }
    public Subject Subject { get; set; }
  }
}
