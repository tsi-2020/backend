﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Udelar.Model.Mongo;
using Udelar.Model.Poll;
using Udelar.Model.Quiz;

namespace Udelar.Model
{
  //Usuario de facultad
  public class User : Tenant
  {
    [Required] public string FirstName { get; set; }
    [Required] public string LastName { get; set; }
    [Required] public string Email { get; set; }
    [Required] public int CI { get; set; }
    public string Password { get; set; }
    public string RefreshToken { get; set; }

    public bool IsActive { get; set; }

    public string ResetPasswordCode { get; set; }

    public ICollection<UserRole> Roles { get; set; } = new HashSet<UserRole>();

    //Esta es la subscripcion la concha de tu madre
    public ICollection<Enrollment> Inscriptions { get; set; } = new HashSet<Enrollment>();

    //Evaluaciones hechas
    public ICollection<QuizResponse> QuizResponses { get; set; } = new HashSet<QuizResponse>();

    //Cuestionarios hechos
    public ICollection<PollResponse> PollsResponses { get; set; } = new HashSet<PollResponse>();

    public ICollection<UserGroup> UserGroups { get; set; } = new HashSet<UserGroup>();
    public ICollection<IndividualDelivery> Assignments { get; set; } = new HashSet<IndividualDelivery>();

    //Mensajes del foro
    [NotMapped] public ICollection<Message> Messages { get; set; } = new HashSet<Message>();
  }
}
