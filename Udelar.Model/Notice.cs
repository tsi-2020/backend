﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  //Comunicado
  public class Notice : BaseModel
  {
    [Required]
    public string Title { get; set; }
    public string Message { get; set; }
    public Subject Subject { get; set; }
    public University University { get; set; }
  }
}
