﻿namespace Udelar.Model.Component
{
  //componente de sala virtual
  public class VirtualMeetingComponent : Component
  {


    public VirtualMeetingComponent(VirtualMeetingComponent templateComponent) : base(templateComponent)
    {
      Title = templateComponent.Title;
      Url = templateComponent.Url;
    }

    public VirtualMeetingComponent()
    {
    }

    public string Title { get; set; }

    public string Url { get; set; }
  }
}
