﻿namespace Udelar.Model.Component
{
  //componente de video
  public class VideoComponent : Component
  {
    public VideoComponent()
    {
    }

    public VideoComponent(VideoComponent templateComponent) : base(templateComponent)
    {
      Title = templateComponent.Title;
      Url = templateComponent.Url;
    }

    public string Title { get; set; }

    public string Url { get; set; }
  }
}
