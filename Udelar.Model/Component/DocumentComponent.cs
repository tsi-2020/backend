﻿namespace Udelar.Model.Component
{
  //Componente para documentos
  public class DocumentComponent : Component
  {
    public DocumentComponent()
    {
    }

    public DocumentComponent(DocumentComponent templateComponent) : base(templateComponent)
    {
      Title = templateComponent.Title;
      Url = templateComponent.Url;
    }

    

    public string Title { get; set; }

    public string Url { get; set; }
  }
}
