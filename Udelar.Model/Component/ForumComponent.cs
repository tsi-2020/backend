﻿namespace Udelar.Model.Component
{
  //componente para foro
  public class ForumComponent : Component
  {

    public ForumComponent(string title, int position) : base(position)
    {
      Title = title;
    }

    public ForumComponent(ForumComponent templateComponent) : base(templateComponent)
    {
      Title = templateComponent.Title;
    }

    public ForumComponent()
    {

    }

    public string Title { get; set; }

    public string ForumId { get; set; }
  }
}
