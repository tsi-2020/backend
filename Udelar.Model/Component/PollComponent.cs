﻿namespace Udelar.Model.Component
{
  //componente para encuestas
  public class PollComponent : Component
  {
    public PollComponent()
    {
    }

    public PollComponent(PollComponent templateComponent) : base(templateComponent)
    {
      Title = templateComponent.Title;
    }

    public string Title { get; set; }

    public Poll.Poll Poll { get; set; }
  }
}
