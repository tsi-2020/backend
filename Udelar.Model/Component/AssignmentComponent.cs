﻿namespace Udelar.Model.Component
{
  //Componente para trabajos entregables
  public class AssignmentComponent : Component
  {
    public AssignmentComponent()
    {
    }

    public AssignmentComponent(AssignmentComponent templateComponent) : base(templateComponent)
    {
      Title = templateComponent.Title;
      Text = templateComponent.Text;
    }

    public string Title { get; set; }

    public string Text { get; set; }

    public Assignment Assignment { get; set; }
  }
}
