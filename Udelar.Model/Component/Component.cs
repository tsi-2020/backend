﻿namespace Udelar.Model.Component
{
  //Componente para armado de visualizacion de curso
  public class Component : BaseModel
  {
    protected Component(Component component)
    {
      Position = component.Position;
    }

    protected Component()
    {
    }

    protected Component(in int position)
    {
      Position = position;
    }

    public int Position { get; set; }
  }
}
