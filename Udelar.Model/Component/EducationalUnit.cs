﻿using System.Collections.Generic;

namespace Udelar.Model.Component
{
  //Unidad educativa, agrupa componentes
  public class EducationalUnit : BaseModel
  {
    public EducationalUnit()
    {
    }

    public EducationalUnit(EducationalUnit educationalUnit)
    {
      Position = educationalUnit.Position;
      Title = educationalUnit.Title;
      Text = educationalUnit.Text;
    }

    public int Position { get; set; }

    public string Title { get; set; }

    public string Text { get; set; }

    public List<Component> Components { get; set; } = new List<Component>();
  }
}
