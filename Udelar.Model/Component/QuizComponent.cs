﻿namespace Udelar.Model.Component
{
  //componente para cuestionarios
  public class QuizComponent : Component
  {
    public QuizComponent()
    {
    }

    public QuizComponent(QuizComponent templateComponent) : base(templateComponent)
    {
      Title = templateComponent.Title;
      Text = templateComponent.Text;
    }

    public string Title { get; set; }

    public string Text { get; set; }

    public Quiz.Quiz Quiz { get; set; }
  }
}
