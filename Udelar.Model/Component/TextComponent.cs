﻿namespace Udelar.Model.Component
{
  //Componente de texto
  public class TextComponent : Component
  {
    public TextComponent()
    {
    }

    public TextComponent(TextComponent templateComponent) : base(templateComponent)
    {
      Text = templateComponent.Text;
    }

    public string Text { get; set; }
  }
}
