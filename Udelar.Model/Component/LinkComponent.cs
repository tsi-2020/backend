﻿namespace Udelar.Model.Component
{
  //componente para enlaces
  public class LinkComponent : Component
  {
    public LinkComponent()
    {
    }

    public LinkComponent(LinkComponent templateComponent) : base(templateComponent)
    {
      Title = templateComponent.Title;
      Url = templateComponent.Url;
    }

    public string Title { get; set; }

    public string Url { get; set; }
  }
}
