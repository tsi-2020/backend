﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Model
{
  public class UniversityAdministrator : Administrator
  {
    [Required] public University University { get; set; }
  }
}
