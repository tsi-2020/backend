﻿using Udelar.Common.Dtos.User.Response;

namespace Udelar.Common.Dtos.Califications.response
{
  public class ReadCalificationDto
  {
    public ReadUserDto Student { get; set; }
    public float Score { get; set; }
  }
}
