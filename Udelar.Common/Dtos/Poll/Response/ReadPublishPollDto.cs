﻿using System;
using System.Collections.Generic;

namespace Udelar.Common.Dtos.Poll.Response
{
  public class ReadPublishPollDto
  {
    public bool IsAnswered { get; set; }
    public string Title { get; set; }
    public Guid Id { get; set; }
    public DateTime CreatedOn { get; set; }
    public List<ListPollOpenQuestionDto> OpenQuestions { get; set; }
    public IEnumerable<ListPollMultipleChoiceQuestionDto> MultipleChoiceQuestions { get; set; }
  }

  public class ListPollMultipleChoiceQuestionDto
  {
    public Guid Id { set; get; }
    public int Position { get; set; }
    public string Text { set; get; }
    public List<ListPollQuestionChoicesDto> Choices { set; get; }
  }

  public class ListPollQuestionChoicesDto
  {
    public Guid Id { set; get; }
    public string Text { set; get; }
  }

  public class ListPollOpenQuestionDto
  {
    public Guid Id { set; get; }
    public int Position { get; set; }
    public string Text { set; get; }
  }
}
