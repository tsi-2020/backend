﻿using System;

namespace Udelar.Common.Dtos.Poll.Response
{
  public class ReadPollDto
  {
    public string Id { get; set; }

    public string Title { get; set; }

    public DateTime CreatedOn { get; set; }
  }
}
