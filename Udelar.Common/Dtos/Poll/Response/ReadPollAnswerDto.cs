﻿using System;
using System.Collections.Generic;

namespace Udelar.Common.Dtos.Poll.Response
{
  public class ReadPaginatedPollAnswerDto
  {
    public int Count { get; set; }
    public List<ReadPollAnswerDto> ReadPollAnswerDtos { get; set; }
  }

  public class ReadPollAnswerDto
  {
    public string Title { get; set; }
    public DateTime CreatedOn { get; set; }
    public List<ListPollMultipleChoiceAnswerDto> MultipleChoiceAnswerDtos { get; set; }
    public List<ListPollOpenAnswerDto> OpenAnswerDtos { get; set; }
  }

  public class ListPollMultipleChoiceAnswerDto
  {
    public int Position { get; set; }
    public string Question { set; get; }
    public List<string> Choices { set; get; }
  }

  public class ListPollOpenAnswerDto
  {
    public int Position { get; set; }
    public string Question { set; get; }
    public string Response { set; get; }
  }
}
