﻿using System;
using System.Collections.Generic;

namespace Udelar.Common.Dtos.Poll.Request
{
  public class ReplyPollDto
  {
    public List<ReplyPollOpenQuestionDto> OpenQuestions { get; set; }
    public List<ReplyPollMultipleChoiceQuestionDto> MultipleChoiceQuestions { get; set; }
  }

  public class ReplyPollMultipleChoiceQuestionDto
  {
    public Guid MultipleChoiceQuestionId { set; get; }
    public List<Guid> Choices { set; get; }
  }

  public class ReplyPollOpenQuestionDto
  {
    public Guid OpenQuestionId { set; get; }
    public string Response { set; get; }
  }
}
