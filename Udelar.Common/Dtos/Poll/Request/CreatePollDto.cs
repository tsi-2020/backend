﻿using System.Collections.Generic;
using FluentValidation;

namespace Udelar.Common.Dtos.Poll.Request
{
  public class CreatePollDto
  {
    public string Title { get; set; }
    public List<CreatePollOpenQuestionDto> OpenQuestions { get; set; }
    public List<CreatePollMultipleChoiceQuestionDto> MultipleChoiceQuestions { get; set; }
  }

  public class CreatePollValidator : AbstractValidator<CreatePollDto>
  {
    public CreatePollValidator()
    {
      RuleFor(dto => dto.Title).NotEmpty().WithMessage("can not be empty");

      RuleFor(dto => dto.OpenQuestions).NotNull().WithMessage("Must be defined");

      RuleFor(dto => dto.MultipleChoiceQuestions).NotNull().WithMessage("Must be defined");

      When(dto => dto.OpenQuestions.Count == 0, () =>
        RuleFor(dto => dto.MultipleChoiceQuestions).NotEmpty()
          .WithMessage("At least 1 open OR multi choice question must be provided")
      );

      When(dto => dto.MultipleChoiceQuestions.Count == 0, () =>
        RuleFor(dto => dto.OpenQuestions).NotEmpty()
          .WithMessage("At least 1 open OR multi choice question must be provided")
      );

      RuleForEach(dto => dto.MultipleChoiceQuestions).SetValidator(new CreatePollMultipleChoiceQuestionValidator());
    }
  }
}
