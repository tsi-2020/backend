﻿using FluentValidation;

namespace Udelar.Common.Dtos.Poll.Request
{
  public class CreatePollChoiceDto
  {
    public string Text { get; set; }
  }

  public class CreatePollChoiceValidator : AbstractValidator<CreatePollChoiceDto>
  {
    public CreatePollChoiceValidator()
    {
      RuleFor(dto => dto.Text)
        .NotEmpty().WithMessage("can not be empty");
    }
  }
}
