﻿using System.Collections.Generic;
using FluentValidation;

namespace Udelar.Common.Dtos.Poll.Request
{
  public class CreatePollMultipleChoiceQuestionDto
  {
    public string Text { get; set; }
    public bool IsMultipleSelect { get; set; } = false;
    public ICollection<CreatePollChoiceDto> Choices { get; set; }
    public int Position { get; set; }
  }

  public class CreatePollMultipleChoiceQuestionValidator : AbstractValidator<CreatePollMultipleChoiceQuestionDto>
  {
    public CreatePollMultipleChoiceQuestionValidator()
    {
      RuleFor(dto => dto.Text)
        .NotEmpty().WithMessage("can not be empty");

      RuleFor(dto => dto.Choices).NotEmpty().NotNull().WithMessage("At leat one choice must be provided");
      RuleFor(dto => dto.Position).GreaterThanOrEqualTo(0);
    }
  }
}

