﻿using FluentValidation;

namespace Udelar.Common.Dtos.Poll.Request
{
  public class CreatePollOpenQuestionDto
  {
    public string Text { get; set; }
    public int Position { get; set; }
  }

  public class CreatePollOpenQuestionValidator : AbstractValidator<CreatePollOpenQuestionDto>
  {
    public CreatePollOpenQuestionValidator()
    {
      RuleFor(dto => dto.Text).NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.Position).GreaterThanOrEqualTo(0);
    }
  }
}
