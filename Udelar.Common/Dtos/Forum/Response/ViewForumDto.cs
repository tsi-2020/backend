﻿using System;
using System.Collections.Generic;
using System.Text;
using Udelar.Common.Dtos.Thread.Response;

namespace Udelar.Common.Dtos.Forum.Response
{
  public class ViewForumDto
  {
    public string Id { get; set; }
    public string Name { get; set; }

    public List<ReadThreadDto> Threads { get; set; }
  }
}
