﻿namespace Udelar.Common.Dtos.Forum.Response
{
  public class ReadForumDto
  {
    public string Id { get; set; }
    public string Name { get; set; }
  }
}
