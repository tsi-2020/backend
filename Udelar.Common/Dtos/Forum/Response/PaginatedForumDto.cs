﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Udelar.Common.Dtos.Forum.Response
{
  public class PaginatedForumDto
  {
    public long Count { get; set; }
    public List<ReadForumDto> ForumList { get; set; } = new List<ReadForumDto>();

  }
}
