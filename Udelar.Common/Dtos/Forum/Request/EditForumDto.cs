﻿using FluentValidation;

namespace Udelar.Common.Dtos.Forum.Request
{
  public class EditForumDto
  {
    public string Name { get; set; }
  }

  public class EditForumValidator : AbstractValidator<EditForumDto>
  {
    public EditForumValidator()
    {
      RuleFor(dto => dto.Name).NotEmpty().WithMessage("Can not be empty");
    }
  }
}
