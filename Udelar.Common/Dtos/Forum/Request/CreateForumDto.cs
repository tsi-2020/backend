﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Forum.Request
{
  public class CreateForumDto
  {
    public Guid SubjectId { get; set; }
    public string Name { get; set; }
  }

  public class CreateForumValidator : AbstractValidator<CreateForumDto>
  {
    public CreateForumValidator()
    {
      RuleFor(dto => dto.SubjectId).NotEmpty().WithMessage("Can not be empty");
      RuleFor(dto => dto.Name).NotEmpty().WithMessage("Can not be empty");
    }
  }
}
