﻿using FluentValidation;

namespace Udelar.Common.Dtos.Enrollment.request
{
  public class UpdateProfessorEnrollmentDto
  {
    public bool IsResponsible { get; set; }
  }

  public class UpdateProfessorEnrollmentValidator : AbstractValidator<UpdateProfessorEnrollmentDto>
  {
    public UpdateProfessorEnrollmentValidator()
    {
      RuleFor(dto => dto.IsResponsible).NotNull().WithMessage("Can not be empty");
    }
  }
}
