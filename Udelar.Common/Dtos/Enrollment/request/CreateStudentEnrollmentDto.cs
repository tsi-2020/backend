﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Enrollment.request
{
  public class CreateStudentEnrollmentDto
  {
    public Guid UserId { get; set; }
  }

  public class CreateStudentEnrollmentValidator : AbstractValidator<CreateStudentEnrollmentDto>
  {
    public CreateStudentEnrollmentValidator()
    {
      RuleFor(dto => dto.UserId).NotEmpty().WithMessage("Can not be empty");
    }
  }
}
