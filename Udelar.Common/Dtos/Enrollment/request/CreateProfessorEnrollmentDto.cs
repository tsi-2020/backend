﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Enrollment.request
{
  public class CreateProfessorEnrollmentDto
  {
    public Guid UserId { get; set; }

    public bool IsResponsible { get; set; }
  }

  public class CreateProfessorEnrollmentValidator : AbstractValidator<CreateProfessorEnrollmentDto>
  {
    public CreateProfessorEnrollmentValidator()
    {
      RuleFor(dto => dto.UserId).NotEmpty().WithMessage("Can not be empty");
      RuleFor(dto => dto.IsResponsible).NotNull().WithMessage("Can not be empty");
    }
  }
}
