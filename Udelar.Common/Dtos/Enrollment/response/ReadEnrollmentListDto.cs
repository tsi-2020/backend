﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Udelar.Common.Dtos.Enrollment.response
{
  public class ReadEnrollmentListDto
  {
    public List<ReadProfessorEnrollmentDto> Professors { get; set; } = new List<ReadProfessorEnrollmentDto>();

    public List<ReadStudentEnrollmentDto> Students { get; set; } = new List<ReadStudentEnrollmentDto>();
  }
}
