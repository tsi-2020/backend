﻿using Udelar.Common.Dtos.User.Response;

namespace Udelar.Common.Dtos.Enrollment.response
{
  public class ReadStudentEnrollmentDto
  {
    public string Id { get; set; }
    public ReadUserDto User { get; set; }
  }
}
