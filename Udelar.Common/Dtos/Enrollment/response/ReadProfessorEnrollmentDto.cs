﻿using Udelar.Common.Dtos.User.Response;

namespace Udelar.Common.Dtos.Enrollment.response
{
  public class ReadProfessorEnrollmentDto
  {
    public string Id { get; set; }

    public ReadUserDto User { get; set; }

    public bool IsResponsible { get; set; } = false;
  }
}
