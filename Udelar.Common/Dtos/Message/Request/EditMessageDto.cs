﻿using FluentValidation;

namespace Udelar.Common.Dtos.Message.Request
{
  public class EditMessageDto
  {
    public string Text { get; set; }
  }

  public class EditMessageDtoValidator : AbstractValidator<EditMessageDto>
  {
    public EditMessageDtoValidator()
    {
      RuleFor(dto => dto.Text)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");
    }
  }
}
