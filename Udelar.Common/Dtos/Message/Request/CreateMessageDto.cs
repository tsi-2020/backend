﻿using FluentValidation;

namespace Udelar.Common.Dtos.Message.Request
{
  public class CreateMessageDto
  {
    public string Text { get; set; }
  }
  public class CreateMessageValidator : AbstractValidator<CreateMessageDto>
  {
    public CreateMessageValidator()
    {
      RuleFor(dto => dto.Text).NotEmpty().WithMessage("Can not be empty");
    }
  }

}
