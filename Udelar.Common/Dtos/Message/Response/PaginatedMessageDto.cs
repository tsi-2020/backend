﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Udelar.Common.Dtos.Message.Response
{
  public class PaginatedMessageDto
  {
    public long Count { get; set; }

    public List<ReadMessageDto> MessageList { get; set; } = new List<ReadMessageDto>();

  }
}
