﻿using System;

namespace Udelar.Common.Dtos.Message.Response
{
  public class ReadMessageDto
  {
    public string Id { get; set; }
    public string UserId { get; set; }
    public string Text { get; set; }
    public string FullName { get; set; }
    public DateTime CreatedOn { get; set; }
  }
}
