﻿using FluentValidation;

namespace Udelar.Common.Dtos.Career.Request
{
  public class CreateCareerDto
  {
    public string Name { get; set; }
  }

  public class CreateCareerDtoValidator : AbstractValidator<CreateCareerDto>
  {
    public CreateCareerDtoValidator()
    {
      RuleFor(dto => dto.Name)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");
    }
  }
}
