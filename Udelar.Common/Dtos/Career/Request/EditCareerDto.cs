﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Career.Request
{
  public class EditCareerDto
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
  }

  public class EditCareerDtoValidator : AbstractValidator<EditCareerDto>
  {
    public EditCareerDtoValidator()
    {
      RuleFor(dto => dto.Name)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.Id)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");
    }
  }
}
