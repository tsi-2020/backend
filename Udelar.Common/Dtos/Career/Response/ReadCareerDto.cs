﻿using System.Collections.Generic;
using Udelar.Common.Dtos.Subject.Response;

namespace Udelar.Common.Dtos.Career.Response
{
  public class ReadCareerDto
  {
    public string Id { get; set; }

    public string Name { get; set; }
    public List<ReadSubjectLiteDto> Subjects { get; set; }

  }
}
