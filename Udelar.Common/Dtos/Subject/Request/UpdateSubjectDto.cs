﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Subject.Request
{
  public class UpdateSubjectDto
  {
    public string Name { get; set; }

    public bool ValidateWithBedelia { get; set; }

    public bool IsRemote { get; set; }
  }

  public class UpdateSubjectValidator : AbstractValidator<UpdateSubjectDto>
  {
    public UpdateSubjectValidator()
    {
      RuleFor(dto => dto.Name)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");

      RuleFor(dto => dto.ValidateWithBedelia)
        .NotNull().WithMessage("can not be empty");

      RuleFor(dto => dto.IsRemote)
        .NotNull().WithMessage("can not be empty");
    }
  }
}
