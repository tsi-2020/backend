﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Subject.Request
{
  public class CreateSubjectDto
  {
    public string Name { get; set; }

    public Guid ResponsibleTeacherId { get; set; }

    public Guid? SubjectTemplateId { get; set; }

    public bool ValidateWithBedelia { get; set; }

    public bool IsRemote { get; set; }
  }

  public class CreateSubjectValidator : AbstractValidator<CreateSubjectDto>
  {
    public CreateSubjectValidator()
    {
      RuleFor(dto => dto.Name)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");

      RuleFor(dto => dto.ResponsibleTeacherId)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");

      RuleFor(dto => dto.ValidateWithBedelia)
        .NotNull().WithMessage("can not be empty");

      RuleFor(dto => dto.IsRemote)
        .NotNull().WithMessage("can not be empty");
    }
  }
}
