﻿using System.Collections.Generic;
using Udelar.Common.Dtos.Component.Response;

namespace Udelar.Common.Dtos.Subject.Response
{
  public class ReadSubjectDto
  {
    public string Id { get; set; }

    public string Name { get; set; }

    public bool ValidateWithBedelia { get; set; }

    public bool IsRemote { get; set; }

    public List<ReadEducationalUnitDto> Components { get; set; } = new List<ReadEducationalUnitDto>();
  }
}
