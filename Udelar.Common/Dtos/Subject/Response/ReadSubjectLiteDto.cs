﻿namespace Udelar.Common.Dtos.Subject.Response
{
  public class ReadSubjectLiteDto
  {
    public string Id { get; set; }

    public string Name { get; set; }

    public bool ValidateWithBedelia { get; set; }

    public bool IsRemote { get; set; }
  }
}
