﻿using FluentValidation;

namespace Udelar.Common.Dtos.Auth.Rquest
{
  public class FrontOfficeLoginDto
  {
    public int CI { get; set; }
    public string Password { get; set; }
  }

  public class LoginUniversityValidator : AbstractValidator<FrontOfficeLoginDto>
  {
    public LoginUniversityValidator()
    {
      RuleFor(dto => dto.CI)
        .Must(ci => ci.ToString().Length == 7 || ci.ToString().Length == 8)
        .WithMessage("must be valid CI");
      RuleFor(dto => dto.Password)
        .NotEmpty().WithMessage("can not be empty");
    }
  }
}
