﻿using FluentValidation;

namespace Udelar.Common.Dtos.Auth.Rquest
{
  public class RefreshTokenDto
  {
    public string RefreshToken { get; set; }
  }

  public class RefreshTokenValidator : AbstractValidator<RefreshTokenDto>
  {
    public RefreshTokenValidator()
    {
      RuleFor(dto => dto.RefreshToken).NotEmpty().WithMessage("can not be empty");
    }
  }
}
