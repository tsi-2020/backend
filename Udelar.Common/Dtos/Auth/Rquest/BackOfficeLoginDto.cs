﻿using FluentValidation;

namespace Udelar.Common.Dtos.Auth.Rquest
{
  public class BackOfficeLoginDto
  {
    public string Email { get; set; }
    public string Password { get; set; }
  }

  public class LoginValidation : AbstractValidator<BackOfficeLoginDto>
  {
    public LoginValidation()
    {
      RuleFor(dto => dto.Email)
        .NotEmpty().WithMessage("can not be empty")
        .EmailAddress().WithMessage("must be valid email");
      RuleFor(dto => dto.Password)
        .NotEmpty().WithMessage("can not be empty");
    }
  }
}
