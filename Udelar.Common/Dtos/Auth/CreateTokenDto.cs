﻿using System.Collections.Generic;

namespace Udelar.Common.Dtos.Auth
{
  public class CreateTokenDto
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public IEnumerable<string> Roles { get; set; }
    public string Id { get; set; }
    public string Tenant { get; set; }
  }
}
