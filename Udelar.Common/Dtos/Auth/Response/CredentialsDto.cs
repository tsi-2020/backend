﻿namespace Udelar.Common.Dtos.Auth.Response
{
  public class CredentialsDto
  {
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
  }
}
