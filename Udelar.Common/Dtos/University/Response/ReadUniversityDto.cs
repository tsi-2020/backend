﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Udelar.Common.Dtos.UniversityAdministrator.Reponse;

namespace Udelar.Common.Dtos.University.Response
{
  public class ReadUniversityDto
  {
    [Required] public string Id { get; set; }

    [Required] public string Name { get; set; }

    [Required] public string AccessPath { get; set; }
    [Required] public string PrimaryColor { get; set; }
    [Required] public string SecondaryColor { get; set; }
    [Required] public string Logo { get; set; }
    [Required] public string Description { get; set; }

    public List<ReadUniversityAdminDto> Administrators { get; set; } = new List<ReadUniversityAdminDto>();
  }
}
