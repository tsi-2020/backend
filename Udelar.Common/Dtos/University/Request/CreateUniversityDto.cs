﻿using FluentValidation;
using Udelar.Common.Dtos.UniversityAdministrator.Request;

namespace Udelar.Common.Dtos.University.Request
{
  public class CreateUniversityDto
  {
    public string Name { get; set; }
    public string AccessPath { get; set; }
    public string PrimaryColor { get; set; } = "#2196F3";
    public string SecondaryColor { get; set; } = "#00bcd4";
    public string Logo { get; set; }
    public string Description { get; set; }

    public CreateUniversityAdminDto DefaultAdmin { get; set; }
  }

  public class CreateUniversityValidator : AbstractValidator<CreateUniversityDto>
  {
    public CreateUniversityValidator()
    {
      When(dto => !string.IsNullOrEmpty(dto.PrimaryColor), () =>
        RuleFor(dto => dto.PrimaryColor)
          .MaximumLength(9)
          .WithMessage("Max length should be 9 chars")
      );
      When(dto => !string.IsNullOrEmpty(dto.SecondaryColor), () =>
        RuleFor(dto => dto.SecondaryColor).MaximumLength(9)
          .MaximumLength(9)
          .WithMessage("Max length should be 9 chars")
      );
      When(dto => !string.IsNullOrEmpty(dto.Logo), () =>
        RuleFor(dto => dto.Logo).MaximumLength(250)
          .WithMessage("Max length should be 250 chars")
      );
      RuleFor(dto => dto.Name)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(4).WithMessage("minimun lenght must be 4");
      RuleFor(dto => dto.AccessPath)
        .Matches("^\\/[a-z]{4,15}$").WithMessage(@"Must match the regex ^\/[a-z]{4,15}$");
      RuleFor(dto => dto.DefaultAdmin).SetValidator(new CreateUniversityAdminValidator());
    }
  }
}
