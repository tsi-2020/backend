﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.University.Request
{
  public class UpdateUniversityDto
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public string AccessPath { get; set; }
    public string PrimaryColor { get; set; }
    public string SecondaryColor { get; set; }
    public string Logo { get; set; }
    public string Description { get; set; }
  }

  public class UpdateUniversityValidator : AbstractValidator<UpdateUniversityDto>
  {
    public UpdateUniversityValidator()
    {

      When(dto => !string.IsNullOrEmpty(dto.PrimaryColor), () =>
        RuleFor(dto => dto.PrimaryColor)
          .MaximumLength(9)
          .WithMessage("Max length should be 9 chars")
      );
      When(dto => !string.IsNullOrEmpty(dto.SecondaryColor), () =>
        RuleFor(dto => dto.SecondaryColor).MaximumLength(9)
          .MaximumLength(9)
          .WithMessage("Max length should be 9 chars")
      );
      When(dto => !string.IsNullOrEmpty(dto.Logo), () =>
        RuleFor(dto => dto.Logo).MaximumLength(250)
          .WithMessage("Max length should be 250 chars")
      );
      RuleFor(dto => dto.Id)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");

      RuleFor(dto => dto.Name)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");

      RuleFor(dto => dto.AccessPath)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");

      RuleFor(dto => dto.Description)
        .NotNull().WithMessage("can not be empty");
    }
  }
}
