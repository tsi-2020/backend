﻿using System;

namespace Udelar.Common.Dtos.Calendar.Response
{
  public class ReadEventDto
  {
    public string ID { get; set; }
    public Guid SubjectId { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public string Event { get; set; }
    public DateTime? DeletedOn { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime ModifiedOn { get; set; }
  }
}
