﻿using System;

namespace Udelar.Common.Dtos.Calendar.Request
{
  public class CreateEventDto
  {
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public string Event { get; set; }
  }
}
