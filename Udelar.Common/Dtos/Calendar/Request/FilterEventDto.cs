﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Calendar.Request
{
  public class FilterEventsDto
  {
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
  }

  public class FilterEventsValidation : AbstractValidator<FilterEventsDto>
  {
    public FilterEventsValidation()
    {
      RuleFor(dto => dto.StartDate).NotNull().WithMessage("Start Date must be provided");
      RuleFor(dto => dto.EndDate).NotNull().WithMessage("End Date must be provided");
    }
  }
}
// 1022ef98-d119-46d8-ab59-12b2aaf73817
// {
//   "accessToken": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJnaXZlbl9uYW1lIjoiTWFyaWFubyBadW5pbm8iLCJlbWFpbCI6Im1hcmlhbm96QHpvaG8uY29tIiwibmFtZWlkIjoiYzg5ZmVmM2UtYzQzNC00YmEwLTgzNmUtNjhhMmZjNTA2MDA3IiwiVGVuYW50IjoiMzNiNzc0MzQtYWI2MC00Yzg4LTkwMTgtYjE2MjkxZDdhOGYwIiwicm9sZSI6WyJTdHVkZW50IiwiUHJvZmVzc29yIl0sIm5iZiI6MTYwNTM3MjI0OSwiZXhwIjoxNjA1NDAxMDQ5LCJpYXQiOjE2MDUzNzIyNDl9.6BM0q89ZYKlKVL-cnqGF11Tt7d1M0v-LEDgejiC79PykIylhJ1MoHAd1Gx53ysUxatzwfZlXT6TQ8-LwAflxsw",
//   "refreshToken": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJnaXZlbl9uYW1lIjoic3RyaW5nIHN0cmluZyIsImVtYWlsIjoibWFyaWFub3pAem9oby5jb20iLCJuYW1laWQiOiJjODlmZWYzZS1jNDM0LTRiYTAtODM2ZS02OGEyZmM1MDYwMDciLCJUZW5hbnQiOiIzM2I3NzQzNC1hYjYwLTRjODgtOTAxOC1iMTYyOTFkN2E4ZjAiLCJyb2xlIjoiU3R1ZGVudCIsIm5iZiI6MTYwNTA0MTMyNSwiZXhwIjoxNjA1NjQ2MTI1LCJpYXQiOjE2MDUwNDEzMjV9.LmkUGLjhfTRj9Kzg9Q12ixoBIz1nRwkxGFfy41XkDSACJbrrspMUaNlaNBBMa_Lcb8uxFwY29rhd5JF_4FMhMg"
// }
