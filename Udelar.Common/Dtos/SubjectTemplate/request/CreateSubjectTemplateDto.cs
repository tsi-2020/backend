﻿using System.Collections.Generic;
using FluentValidation;
using Udelar.Common.Dtos.Component.Request;

namespace Udelar.Common.Dtos.SubjectTemplate.request
{
  public class CreateSubjectTemplateDto
  {
    public string Title { get; set; }

    public string Description { get; set; }

    public List<CreateEducationalUnitDto> EducationalUnits { get; set; } = new List<CreateEducationalUnitDto>();
  }

  public class CreateSubjectTemplateValidator : AbstractValidator<CreateSubjectTemplateDto>
  {
    public CreateSubjectTemplateValidator()
    {
      RuleFor(dto => dto.Title)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");

      RuleFor(dto => dto.Description)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(50).WithMessage("minimun lenght must be 50");
    }
  }
}
