﻿using System.Collections.Generic;
using Udelar.Common.Dtos.Component.Response;

namespace Udelar.Common.Dtos.SubjectTemplate.response
{
  public class ReadSubjectTemplateDto
  {
    public string Id { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }

    public List<ReadEducationalUnitDto> EducationalUnits { get; set; } = new List<ReadEducationalUnitDto>();
  }
}
