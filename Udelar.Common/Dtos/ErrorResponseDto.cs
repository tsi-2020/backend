﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Udelar.Common.Dtos
{
  public class ErrorResponseDto
  {
    public List<ErrorDto> Errors { get; set; } = new List<ErrorDto>();
    public string Message { get; set; }
    public int StatusCode { get; set; }

    public override string ToString()
    {
      return JsonConvert.SerializeObject(this);
    }
  }

  public class ErrorDto
  {
    public string Property { get; set; }
    public string Error { get; set; }
  }
}
