﻿namespace Udelar.Common.Dtos.Notice.Response
{
  public class ReadNoticeDto
  {
    public string Id { get; set; }
    public string Message { get; set; }
    public string CreatedOn { get; set; }
    public string Title { get; set; }
  }
}
