﻿using FluentValidation;

namespace Udelar.Common.Dtos.Notice.Request
{
  public class CreateNoticeDto
  {
    public string Message { get; set; }
    public string Title { get; set; }
  }

  public class CreateNoticeValidator : AbstractValidator<CreateNoticeDto>
  {
    public CreateNoticeValidator()
    {
      RuleFor(dto => dto.Message).NotEmpty()
        .NotNull().WithMessage("Can not be null")
        .NotEmpty().WithMessage("Can not be empty");
      RuleFor(dto => dto.Title)
        .NotNull().WithMessage("Can not be null")
        .NotEmpty().WithMessage("Can not be empty");
    }
  }
}
