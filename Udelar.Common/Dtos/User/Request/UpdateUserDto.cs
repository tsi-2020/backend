﻿using FluentValidation;

namespace Udelar.Common.Dtos.User.Request
{
  public class UpdateUserDto
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
  }

  public class UpdateUserRoleValidator : AbstractValidator<UpdateUserDto>
  {
    public UpdateUserRoleValidator()
    {
      RuleFor(dto => dto.FirstName).NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.LastName).NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.Email).EmailAddress().WithMessage("must be a email").NotEmpty()
        .WithMessage("can not be empty");
    }
  }
}
