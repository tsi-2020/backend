﻿using FluentValidation;
using Udelar.Model;

namespace Udelar.Common.Dtos.User.Request
{
  public class FilterUserDto
  {
    public string RoleFilter { get; set; }
    public string FullNameFilter { get; set; } = "";
    public int Page { get; set; } = 1;
    public int Limit { get; set; } = 25;
  }

  public class FilterUserValidation : AbstractValidator<FilterUserDto>
  {
    public FilterUserValidation()
    {
      RuleFor(dto => dto.RoleFilter).IsEnumName(typeof(Role)).WithMessage("Invalid Role (Student, Professor)");
      RuleFor(dto => dto.Page)
        .Must(page => page >= 1).WithMessage("Minimum page is 0");
      RuleFor(dto => dto.Limit)
        .Must(limit => limit <= 25).WithMessage("Max limit is 25")
        .Must(limit => limit >= 1).WithMessage("Minimum limit 1");
    }
  }
}
