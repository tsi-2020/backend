﻿using System.Collections.Generic;
using FluentValidation;
using Udelar.Model;

namespace Udelar.Common.Dtos.User.Request
{
  public class PatchUserRoleDto
  {
    public List<string> Roles { get; set; }
  }

  public class PatchUserRoleValidator : AbstractValidator<PatchUserRoleDto>
  {
    public PatchUserRoleValidator()
    {
      RuleForEach(dto => dto.Roles).IsEnumName(typeof(Role)).WithMessage("Invalid Role (Student, Professor)");
    }
  }
}
