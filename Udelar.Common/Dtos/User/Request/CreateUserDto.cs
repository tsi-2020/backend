﻿using System.Collections.Generic;
using FluentValidation;
using Udelar.Model;

namespace Udelar.Common.Dtos.User.Request
{
  public class CreateUserDto
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public int CI { get; set; }

    public List<string> Roles { get; set; }
  }

  public class CreateUserValidator : AbstractValidator<CreateUserDto>
  {
    public CreateUserValidator()
    {
      RuleForEach(dto => dto.Roles).IsEnumName(typeof(Role)).WithMessage("Invalid Role (Student, Professor)");
      RuleFor(dto => dto.CI.ToString()).Matches("^[0-9]{8}$").WithMessage(@"Must match the regex ^\d{8}$");
      RuleFor(dto => dto.FirstName).NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.LastName).NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.Email).EmailAddress().WithMessage("must be a email").NotEmpty()
        .WithMessage("can not be empty");
    }
  }
}
