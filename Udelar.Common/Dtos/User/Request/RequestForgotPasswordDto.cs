﻿using FluentValidation;

namespace Udelar.Common.Dtos.User.Request
{
  public class RequestForgotPasswordDto
  {
    public int CI { get; set; }
  }

  public class RequestForgotPasswordValidator : AbstractValidator<RequestForgotPasswordDto>
  {
    public RequestForgotPasswordValidator()
    {
      RuleFor(dto => dto.CI.ToString()).Matches("^[0-9]{8}$").WithMessage(@"Must match the regex ^\d{8}$");
    }
  }
}
