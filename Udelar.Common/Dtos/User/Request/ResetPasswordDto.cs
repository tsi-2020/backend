﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.User.Request
{
  public class ResetPasswordDto
  {
    public string Password { get; set; }
  }

  public class ChangePasswordValidator : AbstractValidator<ResetPasswordDto>
  {
    public ChangePasswordValidator()
    {
      RuleFor(dto => dto.Password).MinimumLength(8).WithMessage("At least 8 chars");
    }
  }
}
