﻿
namespace Udelar.Common.Dtos.User.Response
{
  public class ReadUserEnrollmentsDto
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public bool IsProfessorEnrollment { get; set; }
    public bool IsResponsible { get; set; }
    public float Score { get; set; }
  }
}
