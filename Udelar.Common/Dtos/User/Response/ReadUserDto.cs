﻿using System.Collections.Generic;

namespace Udelar.Common.Dtos.User.Response
{
  public class ReadUserDto
  {
    public string Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public int CI { get; set; }
    public List<string> Roles { get; set; } = new List<string>();
  }
}
