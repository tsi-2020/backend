﻿using System.Collections.Generic;

namespace Udelar.Common.Dtos.User.Response
{
  public class PaginatedUsersDto
  {
    public int Count { get; set; }
    public List<ReadUserDto> UserList { get; set; } = new List<ReadUserDto>();
  }
}
