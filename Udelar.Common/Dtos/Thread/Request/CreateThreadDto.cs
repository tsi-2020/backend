﻿namespace Udelar.Common.Dtos.Thread.Request
{
  public class CreateThreadDto
  {
    public string Title { get; set; }
  }
}
