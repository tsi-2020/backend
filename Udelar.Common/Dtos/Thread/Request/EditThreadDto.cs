﻿using FluentValidation;

namespace Udelar.Common.Dtos.Thread.Request
{
  public class EditThreadDto
  {
    public string Title { get; set; }
  }

  public class EditThreadDtoValidator : AbstractValidator<EditThreadDto>
  {
    public EditThreadDtoValidator()
    {
      RuleFor(dto => dto.Title)
        .NotNull().WithMessage("Can not be empty")
        .NotEmpty().WithMessage("Can not be empty");
    }
  }

}
