﻿using FluentValidation;

namespace Udelar.Common.Dtos.Thread.Request
{
  public class FilterThreadDto
  {
    public int Page { get; set; } = 1;
    public int Limit { get; set; } = 25;
  }

  public class FilterThreadValidation : AbstractValidator<FilterThreadDto>
  {
    public FilterThreadValidation()
    {
      RuleFor(dto => dto.Page)
        .Must(page => page >= 1).WithMessage("Minimum page is 0");
      RuleFor(dto => dto.Limit)
        .Must(limit => limit <= 25).WithMessage("Max limit is 25")
        .Must(limit => limit >= 1).WithMessage("Minimum limit 1");
    }
  }
}
