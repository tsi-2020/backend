﻿using System;
using System.Collections.Generic;
using System.Text;
using Udelar.Common.Dtos.Message.Response;

namespace Udelar.Common.Dtos.Thread.Response
{
  public class ViewThreadDto
  {
    public List<ReadMessageDto> Messages { get; set; }
  }
}
