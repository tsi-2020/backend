﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Udelar.Common.Dtos.Thread.Response
{
  public class PaginatedThreadDto
  {
    public long Count { get; set; }
    public List<ReadThreadDto> ThreadList { get; set; } = new List<ReadThreadDto>();
  }
}
