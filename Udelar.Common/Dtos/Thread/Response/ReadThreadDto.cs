﻿namespace Udelar.Common.Dtos.Thread.Response
{
  public class ReadThreadDto
  {
    public string Id { get; set; }
    public string Title { get; set; }
  }
}
