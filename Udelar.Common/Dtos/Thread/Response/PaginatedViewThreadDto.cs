﻿using System;
using System.Collections.Generic;
using System.Text;
using Udelar.Common.Dtos.Message.Response;

namespace Udelar.Common.Dtos.Thread.Response
{
  public class PaginatedViewThreadDto
  {
    public long Count { get; set; }

    public List<ReadMessageDto> MessageList { get; set; } = new List<ReadMessageDto>();
  }
}
