﻿namespace Udelar.Common.Dtos.UdelarAdministrator.Request
{
  public class CreateUdelarAdminDto
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
  }
}
