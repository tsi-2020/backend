﻿using FluentValidation;

namespace Udelar.Common.Dtos.UdelarAdministrator.Request
{
  public class UpdateUdelarAdminPasswordDto
  {
    public string OldPassword { get; set; }
    public string NewPassword { get; set; }
  }

  public class CreateUdelarAdminValidation : AbstractValidator<CreateUdelarAdminDto>
  {
    public CreateUdelarAdminValidation()
    {
      RuleFor(dto => dto.Email)
        .NotEmpty().WithMessage("can not be empty")
        .EmailAddress().WithMessage("must be valid email");
      RuleFor(dto => dto.Password)
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(8).WithMessage("minimun lenght must be 8");
      RuleFor(dto => dto.LastName)
        .NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.FirstName)
        .NotEmpty().WithMessage("can not be empty");
    }
  }
}
