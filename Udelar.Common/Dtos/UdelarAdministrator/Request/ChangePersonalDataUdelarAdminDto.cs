﻿using FluentValidation;

namespace Udelar.Common.Dtos.UdelarAdministrator.Request
{
  public class ChangePersonalDataUdelarAdminDto
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
  }

  public class ChangePersonalDataUdelarAdminValidator : AbstractValidator<ChangePersonalDataUdelarAdminDto>
  {
    public ChangePersonalDataUdelarAdminValidator()
    {
      RuleFor(dto => dto.FirstName)
        .NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.LastName)
        .NotEmpty().WithMessage("can not be empty");
    }
  }
}
