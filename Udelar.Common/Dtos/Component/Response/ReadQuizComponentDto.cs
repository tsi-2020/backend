﻿namespace Udelar.Common.Dtos.Component.Response
{
  public class ReadQuizComponentDto
  {
    public string id { get; set; }

    public int Position { get; set; }

    public string Title { get; set; }

    public string Text { get; set; }

    public string QuizId { get; set; }
  }
}
