﻿namespace Udelar.Common.Dtos.Component.Response
{
  public class ReadLinkComponentDto
  {
    public string id { get; set; }

    public int Position { get; set; }

    public string Title { get; set; }

    public string Url { get; set; }
  }
}
