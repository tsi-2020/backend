﻿namespace Udelar.Common.Dtos.Component.Response
{
  public class ReadTextComponentDto
  {
    public string id { get; set; }

    public int Position { get; set; }

    public string Text { get; set; }
  }
}
