﻿namespace Udelar.Common.Dtos.Component.Response
{
  public class ReadAssigmentComponentDto
  {
    public string id { get; set; }

    public int Position { get; set; }

    public string Title { get; set; }

    public string Text { get; set; }

    public string AssignmentId { get; set; }
  }
}
