﻿using System.Collections.Generic;

namespace Udelar.Common.Dtos.Component.Response
{
  public class ReadEducationalUnitDto
  {
    public string id { get; set; }

    public int Position { get; set; }

    public string Title { get; set; }

    public string Text { get; set; }

    public List<ReadAssigmentComponentDto> AssigmentComponents { get; set; } = new List<ReadAssigmentComponentDto>();

    public List<ReadDocumentComponentDto> DocumentComponents { get; set; } = new List<ReadDocumentComponentDto>();

    public List<ReadForumComponentDto> ForumComponents { get; set; } = new List<ReadForumComponentDto>();

    public List<ReadLinkComponentDto> LinkComponents { get; set; } = new List<ReadLinkComponentDto>();

    public List<ReadPollComponentDto> PollComponents { get; set; } = new List<ReadPollComponentDto>();

    public List<ReadQuizComponentDto> QuizComponents { get; set; } = new List<ReadQuizComponentDto>();

    public List<ReadTextComponentDto> TextComponents { get; set; } = new List<ReadTextComponentDto>();

    public List<ReadVideoComponentDto> VideoComponents { get; set; } = new List<ReadVideoComponentDto>();

    public List<ReadVirtualMeetingComponentDto> VirtualMeetingComponents { get; set; } =
      new List<ReadVirtualMeetingComponentDto>();
  }
}
