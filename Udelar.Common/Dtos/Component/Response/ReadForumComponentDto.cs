﻿namespace Udelar.Common.Dtos.Component.Response
{
  public class ReadForumComponentDto
  {
    public string id { get; set; }

    public string Title { get; set; }

    public int Position { get; set; }

    public string ForumId { get; set; }
  }
}
