﻿namespace Udelar.Common.Dtos.Component.Response
{
  public class ReadVideoComponentDto
  {
    public string id { get; set; }

    public int Position { get; set; }

    public string Title { get; set; }

    public string Url { get; set; }
  }
}
