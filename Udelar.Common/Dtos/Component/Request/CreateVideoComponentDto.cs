﻿using FluentValidation;

namespace Udelar.Common.Dtos.Component.Request
{
  public class CreateVideoComponentDto
  {
    public int Position { get; set; }

    public string Title { get; set; }

    public string Url { get; set; }
  }

  public class CreateVideoComponentValidator : AbstractValidator<CreateVideoComponentDto>
  {
    public CreateVideoComponentValidator()
    {
      RuleFor(dto => dto.Position)
        .NotNull().WithMessage("can not be empty")
        .GreaterThanOrEqualTo(0).WithMessage("value greater than 0");

      RuleFor(dto => dto.Title)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");

      RuleFor(dto => dto.Url)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");
    }
  }
}
