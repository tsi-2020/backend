﻿using FluentValidation;

namespace Udelar.Common.Dtos.Component.Request
{
  public class CreateTextComponentDto
  {
    public int Position { get; set; }

    public string Text { get; set; }
  }

  public class CreateTextComponentValidator : AbstractValidator<CreateTextComponentDto>
  {
    public CreateTextComponentValidator()
    {
      RuleFor(dto => dto.Position)
        .NotNull().WithMessage("can not be empty")
        .GreaterThanOrEqualTo(0).WithMessage("value greater than 0");

      RuleFor(dto => dto.Text)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");
    }
  }
}
