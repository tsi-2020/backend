﻿using FluentValidation;

namespace Udelar.Common.Dtos.Component.Request
{
  public class CreateLinkComponentDto
  {
    public int Position { get; set; }

    public string Title { get; set; }

    public string Url { get; set; }
  }

  public class CreateLinkComponentValidator : AbstractValidator<CreateLinkComponentDto>
  {
    public CreateLinkComponentValidator()
    {
      RuleFor(dto => dto.Position)
        .NotNull().WithMessage("can not be empty")
        .GreaterThanOrEqualTo(0).WithMessage("value greater than 0");

      RuleFor(dto => dto.Title)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");

      RuleFor(dto => dto.Url)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");
    }
  }
}
