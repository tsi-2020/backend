﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Component.Request
{
  public class UpdateLinkComponentDto
  {
    public Guid Id { get; set; }
    public int Position { get; set; }

    public string Title { get; set; }

    public string Url { get; set; }
  }

  public class UpdateLinkComponentValidator : AbstractValidator<UpdateLinkComponentDto>
  {
    public UpdateLinkComponentValidator()
    {
      RuleFor(dto => dto.Id)
        .NotNull().WithMessage("can not be empty");

      RuleFor(dto => dto.Position)
        .NotNull().WithMessage("can not be empty")
        .GreaterThanOrEqualTo(0).WithMessage("value greater than 0");

      RuleFor(dto => dto.Title)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");

      RuleFor(dto => dto.Url)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");
    }
  }
}
