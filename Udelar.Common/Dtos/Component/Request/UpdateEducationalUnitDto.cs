﻿using System.Collections.Generic;
using FluentValidation;

namespace Udelar.Common.Dtos.Component.Request
{
  public class UpdateEducationalUnitDto
  {
    public int Position { get; set; }
    public string Title { get; set; }

    public string Text { get; set; }

    public List<UpdateAssigmentComponentDto> AssigmentComponents { get; set; } =
      new List<UpdateAssigmentComponentDto>();

    public List<UpdateDocumentComponentDto> DocumentComponents { get; set; } = new List<UpdateDocumentComponentDto>();

    public List<UpdateForumComponentDto> ForumComponents { get; set; } = new List<UpdateForumComponentDto>();

    public List<UpdateLinkComponentDto> LinkComponents { get; set; } = new List<UpdateLinkComponentDto>();

    public List<UpdatePollComponentDto> PollComponents { get; set; } = new List<UpdatePollComponentDto>();

    public List<UpdateQuizComponentDto> QuizComponents { get; set; } = new List<UpdateQuizComponentDto>();

    public List<UpdateTextComponentDto> TextComponents { get; set; } = new List<UpdateTextComponentDto>();

    public List<UpdateVideoComponentDto> VideoComponents { get; set; } = new List<UpdateVideoComponentDto>();

    public List<UpdateVirtualMeetingComponentDto> VirtualMeetingComponents { get; set; } =
      new List<UpdateVirtualMeetingComponentDto>();
  }

  public class UpdateEducationalUnitValidator : AbstractValidator<UpdateEducationalUnitDto>
  {
    public UpdateEducationalUnitValidator()
    {
      RuleFor(dto => dto.Position)
        .NotNull().WithMessage("can not be empty")
        .GreaterThanOrEqualTo(0).WithMessage("value greater than 0");

      RuleFor(dto => dto.Title)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");
    }
  }
}
