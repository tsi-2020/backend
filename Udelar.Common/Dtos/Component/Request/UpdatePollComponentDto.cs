﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Component.Request
{
  public class UpdatePollComponentDto
  {
    public Guid Id { get; set; }
    public int Position { get; set; }

    public string Title { get; set; }

    public string Text { get; set; }

    public Guid PollId { get; set; }
  }

  public class UpdatePollComponentValidator : AbstractValidator<UpdatePollComponentDto>
  {
    public UpdatePollComponentValidator()
    {
      RuleFor(dto => dto.Id)
        .NotNull().WithMessage("can not be empty");

      RuleFor(dto => dto.Position)
        .NotNull().WithMessage("can not be empty")
        .GreaterThanOrEqualTo(0).WithMessage("value greater than 0");

      RuleFor(dto => dto.Title)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");

      RuleFor(dto => dto.Text)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");

      RuleFor(dto => dto.PollId)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");
    }

    public int Position { get; set; }
  }
}
