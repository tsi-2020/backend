﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Component.Request
{
  public class CreateAssigmentComponentDto
  {
    public int Position { get; set; }
    public string Title { get; set; }
    public string Text { get; set; }
    public DateTime AvailableUntil { get; set; }
    public Guid? AssignmentId { get; set; }
  }

  public class CreateAssigmentComponentValidator : AbstractValidator<CreateAssigmentComponentDto>
  {
    public CreateAssigmentComponentValidator()
    {
      RuleFor(dto => dto.AvailableUntil)
        .NotNull().WithMessage("can not be empty")
        .GreaterThan(DateTime.Today)
        .WithMessage("value greater than today");

      RuleFor(dto => dto.Position)
        .NotNull().WithMessage("can not be empty")
        .GreaterThanOrEqualTo(0).WithMessage("value greater than 0");

      RuleFor(dto => dto.Title)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");

      RuleFor(dto => dto.Text)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");
    }
  }
}
