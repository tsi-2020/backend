﻿using System.Collections.Generic;
using FluentValidation;

namespace Udelar.Common.Dtos.Component.Request
{
  public class CreateEducationalUnitDto
  {
    public int Position { get; set; }
    public string Title { get; set; }

    public string Text { get; set; }

    public List<CreateAssigmentComponentDto> AssigmentComponents { get; set; } = new List<CreateAssigmentComponentDto>();

    public List<CreateDocumentComponentDto> DocumentComponents { get; set; } = new List<CreateDocumentComponentDto>();

    public List<CreateForumComponentDto> ForumComponents { get; set; } = new List<CreateForumComponentDto>();

    public List<CreateLinkComponentDto> LinkComponents { get; set; } = new List<CreateLinkComponentDto>();

    public List<CreatePollComponentDto> PollComponents { get; set; } = new List<CreatePollComponentDto>();

    public List<CreateQuizComponentDto> QuizComponents { get; set; } = new List<CreateQuizComponentDto>();

    public List<CreateTextComponentDto> TextComponents { get; set; } = new List<CreateTextComponentDto>();

    public List<CreateVideoComponentDto> VideoComponents { get; set; } = new List<CreateVideoComponentDto>();

    public List<CreateVirtualMeetingComponentDto> VirtualMeetingComponents { get; set; } =
      new List<CreateVirtualMeetingComponentDto>();
  }

  public class CreateEducationalUnitValidator : AbstractValidator<CreateEducationalUnitDto>
  {
    public CreateEducationalUnitValidator()
    {
      RuleFor(dto => dto.Position)
        .NotNull().WithMessage("can not be empty")
        .GreaterThanOrEqualTo(0).WithMessage("value greater than 0");

      RuleFor(dto => dto.Title)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(5).WithMessage("minimun lenght must be 5");
    }
  }
}
