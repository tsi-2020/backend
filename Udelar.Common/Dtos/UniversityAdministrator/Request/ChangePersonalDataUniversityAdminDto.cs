﻿using FluentValidation;

namespace Udelar.Common.Dtos.UniversityAdministrator.Request
{
  public class ChangePersonalDataUniversityAdminDto
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
  }

  public class ChangePersonalDataUniversityAdminValidator : AbstractValidator<ChangePersonalDataUniversityAdminDto>
  {
    public ChangePersonalDataUniversityAdminValidator()
    {
      RuleFor(dto => dto.FirstName)
        .NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.LastName)
        .NotEmpty().WithMessage("can not be empty");
    }
  }
}
