﻿using FluentValidation;

namespace Udelar.Common.Dtos.UniversityAdministrator.Request
{
  public class CreateUniversityAdminDto
  {
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
  }


  public class CreateUniversityAdminValidator : AbstractValidator<CreateUniversityAdminDto>
  {
    public CreateUniversityAdminValidator()
    {
      RuleFor(dto => dto.Password).MinimumLength(8).WithMessage("At least 8 chars");
      RuleFor(dto => dto.FirstName).NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.LastName).NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.Email).NotEmpty().WithMessage("can not be empty");
    }
  }
}
