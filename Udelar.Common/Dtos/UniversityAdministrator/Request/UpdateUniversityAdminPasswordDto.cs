﻿using FluentValidation;

namespace Udelar.Common.Dtos.UniversityAdministrator.Request
{
  public class UpdateUniversityAdminPasswordDto
  {
    public string NewPassword { get; set; }
  }

  public class UpdateUniversityAdminPasswordValidator : AbstractValidator<UpdateUniversityAdminPasswordDto>
  {
    public UpdateUniversityAdminPasswordValidator()
    {
      RuleFor(dto => dto.NewPassword)
        .NotEmpty().WithMessage("can not be empty")
        .MinimumLength(8).WithMessage("minimum length must be 8");
    }
  }
}
