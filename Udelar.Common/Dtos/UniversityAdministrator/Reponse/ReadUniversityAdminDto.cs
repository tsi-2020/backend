﻿namespace Udelar.Common.Dtos.UniversityAdministrator.Reponse
{
  public class ReadUniversityAdminDto
  {
    public string Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Email { get; set; }
  }
}
