﻿using System;

namespace Udelar.Common.Dtos.Assigment.response
{
  public class ReadAssignmentDto
  {
    public string Id { get; set; }
    public DateTime AvailableFrom { get; set; }
    public DateTime AvailableUntil { get; set; }
    public float MaxScore { get; set; }
    public string Title { get; set; }
    public string Text { get; set; }
    public ReadDeliveredAssigmentDto DeliveredAssigment { get; set; }
  }
}
