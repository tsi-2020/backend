﻿namespace Udelar.Common.Dtos.Assigment.response
{
  public class ReadDeliveredAssigmentDto
  {
    public string Id { get; set; }

    public float Score { get; set; }

    public string FileUrl { get; set; }

    public string FileName { get; set; }

    public string ProfessorComments { get; set; }
  }
}
