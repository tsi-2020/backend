﻿using System;
using FluentValidation;

namespace Udelar.Common.Dtos.Assigment.request
{
  public class UpdateAssignmentDto
  {
    public DateTime AvailableFrom { get; set; }
    public DateTime AvailableUntil { get; set; }
    public float MaxScore { get; set; }
  }

  public class UpdateAssignmentValidator : AbstractValidator<UpdateAssignmentDto>
  {
    public UpdateAssignmentValidator()
    {
      RuleFor(dto => dto.AvailableFrom)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.AvailableUntil)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");
      RuleFor(dto => dto.MaxScore)
        .NotNull().WithMessage("can not be empty")
        .NotEmpty().WithMessage("can not be empty");
    }
  }
}
