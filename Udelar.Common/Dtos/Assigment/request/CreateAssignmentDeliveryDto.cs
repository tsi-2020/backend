﻿using System.ComponentModel.DataAnnotations;

namespace Udelar.Common.Dtos.Assigment.request
{
  public class CreateAssignmentDeliveryDto
  {
    [Required] public string FileUrl { get; set; }

    [Required] public string FileName { get; set; }
  }
}
