﻿using System;

namespace Udelar.Common.Dtos.Report
{
  public class UniversityUsersReportDto
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public long ProfessorsQuantity { get; set; }
    public long StudentsQuantity { get; set; }
  }
}
