﻿using System;

namespace Udelar.Common.Dtos.Report
{
  public class SubjectEnrollmentsReportDto
  {
    public Guid Id { get; set; }
    public string Name { get; set; }
    public long ProfessorsQuantity { get; set; }
    public long StudentsQuantity { get; set; }
    public string UniversityName { get; set; }
  }
}
