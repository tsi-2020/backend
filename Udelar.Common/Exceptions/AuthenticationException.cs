﻿using System;

namespace Udelar.Common.Exceptions
{
  public class AuthenticationException : Exception
  {
    public AuthenticationException(string name) : base(name)
    {
    }
  }
}
