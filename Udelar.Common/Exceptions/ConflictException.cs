﻿using System;

namespace Udelar.Common.Exceptions
{
  public class ConflictException : Exception
  {
    public ConflictException(string name) : base(name)
    {
    }
  }
}
