﻿using System;

namespace Udelar.Common.Exceptions
{
  public class EntityNotFoundException : Exception
  {
    public EntityNotFoundException(string name) : base(name)
    {
    }
  }
}
