﻿using System;

namespace Udelar.Common.Exceptions
{
  public class AuthorizationException : Exception
  {
    public AuthorizationException(string name) : base(name)
    {
    }
  }
}
