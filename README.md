# Udelar Backend

## Installation

```bash
git clone https://gitlab.com/tsi-2020/backend
git checkout development
cd backend
```

## Requirements

- .NET Core 3.1
- Postgres
- npm (to configure git hooks)
- make (to use makefile shorcuts)
- entity framework:

```console
dotnet tool install --global dotnet-ef
```

- Create a connection string secret:

```console
 dotnet user-secrets set connectionStrings:Default "Server=localhost;Database=UDELAR;Username=**changeme**;Password=**changeme**;" --project Udelar.API/
```

## Run It

First download dependencies, build the project and run migrations.

```console
make setup
```

- standard mode

```console
make run
```

- hot reload

```console
make watch
```
