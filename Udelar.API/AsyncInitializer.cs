﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Extensions.Hosting.AsyncInitialization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using MongoDB.Driver;
using MongoDB.Entities;
using Udelar.Model;
using Udelar.Model.Component;
using Udelar.Persistence;

namespace Udelar.APi
{
  public class AsyncInitializer : IAsyncInitializer
  {
    private readonly IConfigurationRoot _config;
    private readonly DataContext _ctx;

    public AsyncInitializer(DataContext context, IHostEnvironment env)
    {
      var builder = new ConfigurationBuilder()
        .SetBasePath(env.ContentRootPath)
        .AddJsonFile("appsettings.json", true, true)
        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
        .AddJsonFile("secrets/appsettings.secrets.json", true, true)
        .AddEnvironmentVariables();
      builder.AddUserSecrets<Program>();
      _ctx = context;
      _config = builder.Build();
    }

    public async Task InitializeAsync()
    {
      var connectionString = string.IsNullOrEmpty(_config["ConnectionStrings:Mongo"])
        ? MongoClientSettings.FromConnectionString("mongodb://localhost:27017")
        : MongoClientSettings.FromConnectionString(_config["ConnectionStrings:Mongo"]);
      await DB.InitAsync("udelar", connectionString);
      await InitializeTemplates();
    }

    private async Task InitializeTemplates()
    {
      var universities = await _ctx.Universities
        .Include(u => u.SubjectTemplates)
        .ToListAsync();

      foreach (var university in universities)
      {
        if (university.SubjectTemplates.Count != 0)
        {
          continue;
        }

        //TEMPLATE 1
        var forum1 = new ForumComponent { Position = 0, Title = "Foro General" };

        var forum2 = new ForumComponent { Position = 1, Title = "Noticias" };


        var educationalUnits1 = new List<EducationalUnit>();

        educationalUnits1.Add(new EducationalUnit
        {
          Position = 0,
          Title = "General",
          Text = "Aqui va una descripcion del curso",
          Components = new List<Component> { forum1, forum2 }
        });

        for (var i = 1; i <= 8; i++)
        {
          educationalUnits1.Add(
            new EducationalUnit { Position = i, Title = "Modulo " + i, Text = "Descripcion opcional" });
        }


        var template1 = new SubjectTemplate
        {
          University = university,
          Title = "Basico",
          Description = "Crea un Foro general y uno de noticias",
          Components = educationalUnits1
        };

        university.SubjectTemplates.Add(template1);

        //TEMPLATE 2

        var forum3 = new ForumComponent { Position = 0, Title = "Foro General" };

        var forum4 = new ForumComponent { Position = 1, Title = "Noticias" };


        var educationalUnits2 = new List<EducationalUnit>();

        educationalUnits2.Add(new EducationalUnit
        {
          Position = 0,
          Title = "General",
          Text = "Aqui va una descripcion del curso",
          Components = new List<Component> { forum3, forum4 }
        });

        for (var i = 1; i <= 4; i++)
        {
          educationalUnits2.Add(
            new EducationalUnit { Position = i, Title = "Modulo " + i, Text = "Descripcion opcional" });
        }

        var assignment = new AssignmentComponent { Position = 0, Title = "Trabajo final" };

        educationalUnits2.Add(new EducationalUnit
        {
          Position = 5,
          Title = "Entregas finales",
          Components = new List<Component> { assignment }
        });


        var template2 = new SubjectTemplate
        {
          University = university,
          Title = "Taller",
          Description = "Con trabajo final",
          Components = educationalUnits2
        };

        university.SubjectTemplates.Add(template2);

        //TEMPLATE 3

        var forum5 = new ForumComponent { Position = 0, Title = "Foro General" };

        var forum6 = new ForumComponent { Position = 1, Title = "Noticias" };

        var virtualMetting = new VirtualMeetingComponent
        {
          Position = 2,
          Title = "Salon virual",
          Url = "https://zoom.us/"
        };


        var educationalUnits3 = new List<EducationalUnit>();

        educationalUnits3.Add(new EducationalUnit
        {
          Position = 0,
          Title = "General",
          Text = "Aqui va una descripcion del curso",
          Components = new List<Component> { forum5, forum6, virtualMetting }
        });

        for (var i = 1; i <= 8; i++)
        {
          educationalUnits3.Add(
            new EducationalUnit { Position = i, Title = "Modulo " + i, Text = "Descripcion opcional" });
        }


        var template3 = new SubjectTemplate
        {
          University = university,
          Title = "Con salon virtual",
          Description = "Con salon virtual",
          Components = educationalUnits3
        };

        university.SubjectTemplates.Add(template3);

        await _ctx.SaveChangesAsync();
      }
    }
  }
}
