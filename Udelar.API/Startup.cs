﻿using System;
using System.IO;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Obl.API.Middlewares;
using Udelar.APi.Middleware;
using Udelar.Common.Dtos;
using Udelar.Persistence;
using Udelar.Persistence.Repositories;
using Udelar.Services;
using Udelar.Services.MapperProfiles;

namespace Udelar.APi
{
  public class Startup
  {
    public Startup(IHostEnvironment env)
    {
      var builder = new ConfigurationBuilder()
        .SetBasePath(env.ContentRootPath)
        .AddJsonFile("appsettings.json", true, true)
        .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
        .AddJsonFile("secrets/appsettings.secrets.json", true, true)
        .AddEnvironmentVariables();
      builder.AddUserSecrets<Program>();
      Configuration = builder.Build();
      var message = $"Host: {Environment.MachineName}\n" +
                    $"EnvironmentName: {env.EnvironmentName}\n" +
                    $"Secret value: {Configuration["ConnectionStrings:Default"]}";
      Console.WriteLine(message);
    }

    public IConfiguration Configuration { get; set; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddAsyncInitializer<AsyncInitializer>();
      services.AddScoped<ILoggerManager, LoggerManager>();
      //Services

      services.AddScoped<IUniversityAdminService, UniversityAdminService>();
      services.AddScoped<IPollService, PollService>();
      services.AddScoped<IAuthService, AuthService>();
      services.AddScoped<IUniversitiesService, UniversitiesService>();
      services.AddScoped<ISubjectTemplateService, SubjectTemplateService>();
      services.AddScoped<ICareerService, CareerService>();
      services.AddScoped<IUserService, UserService>();
      services.AddScoped<ISubjectService, SubjectService>();
      services.AddScoped<IForumService, ForumService>();
      services.AddScoped<IThreadService, ThreadService>();
      services.AddScoped<INoticeService, NoticeService>();
      services.AddScoped<IEnrollmentService, EnrollmentService>();
      services.AddScoped<IMessageService, MessageService>();
      services.AddScoped<ICalendarService, CalendarService>();
      services.AddScoped<IAssignmentService, AssignmentService>();
      services.AddScoped<ICalificationService, CalificationService>();
      services.AddScoped<IBedeliasService, BedeliasService>();

      //Repositories
      services.AddScoped<IUserRepository, UserRepository>();
      services.AddScoped<IPollRepository, PollRepository>();
      services.AddScoped<IAdminRepository, AdminRepository>();
      services.AddScoped<IUniversitiesRepository, UniversitiesRepository>();
      services.AddScoped<ISubjectTemplateRepository, SubjectTemplateRepository>();
      services.AddScoped<ICareerRepository, CareerRepository>();
      services.AddScoped<ISubjectRepository, SubjectRepository>();
      services.AddScoped<IForumRepository, ForumRepository>();
      services.AddScoped<IThreadRepository, ThreadRepository>();
      services.AddScoped<INoticeRepository, NoticeRepository>();
      services.AddScoped<IMessageRepository, MessageRepository>();
      services.AddScoped<ICalendarRepository, CalendarRepository>();
      services.AddScoped<IAssignmentRepository, AssignmentRepository>();

      // services.AddScoped<IEmailSender, EmailSender>();
      services.AddHttpClient<IEmailSender, EmailSender>(cfg =>
      {
        cfg.BaseAddress = new Uri("https://api.mailgun.net");
        cfg.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
          Convert.ToBase64String(Encoding.UTF8.GetBytes("api:ac6a00e5757ac6eafc9cc868e7c9d6c2-0afbfc6c-7f61e53b")));
      });

      services.AddDbContext<DataContext>(
        optionsBuilder => optionsBuilder.UseNpgsql(
          Configuration["ConnectionStrings:Default"],
          contextOptionsBuilder => contextOptionsBuilder.MigrationsAssembly("Udelar.Persistence")
        ));
      services.AddControllers(options => options.Filters.Add<ValidationFilter>())
        .AddFluentValidation(options =>
          options.RegisterValidatorsFromAssemblyContaining<ErrorResponseDto>())
        .AddNewtonsoftJson();
      services.AddSwaggerGen(options =>
      {
        options.OperationFilter<AddAuthHeaderOperationFilter>();
        options.AddSecurityDefinition("bearer",
          new OpenApiSecurityScheme
          {
            Description = "`Token only` without `Bearer ` prefix",
            Type = SecuritySchemeType.Http,
            BearerFormat = "JWT",
            In = ParameterLocation.Header,
            Scheme = "bearer"
          });
      });
      services.AddSwaggerGen(c =>
      {
        // Set the comments path for the Swagger JSON and UI.
        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
        c.IncludeXmlComments(xmlPath);
      });
      services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
      services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
        options.TokenValidationParameters = new TokenValidationParameters
        {
          ValidateAudience = false,
          ValidateIssuer = false,
          ValidateIssuerSigningKey = true,
          IssuerSigningKey =
            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration.GetSection("AppSettings:Token").Value))
        });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerManager logger)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseSwagger();
      app.UseSwaggerUI(uiOptions => { uiOptions.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"); });

      app.UseCors(policyBuilder => policyBuilder
        .AllowAnyOrigin()
        .AllowAnyMethod()
        .AllowAnyHeader());


      // app.UseHttpsRedirection();

      app.UseRouting();
      app.UseMiddleware<RequestLoggerMiddleware>();
      app.ConfigureExceptionHandler(logger);

      app.UseAuthentication();
      app.UseAuthorization();

      app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
  }
}
