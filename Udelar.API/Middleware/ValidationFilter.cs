﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Udelar.Common.Dtos;

namespace Udelar.APi.Middleware
{
  public class ValidationFilter : IResultFilter
  {
    public void OnResultExecuted(ResultExecutedContext context)
    {
    }

    public void OnResultExecuting(ResultExecutingContext context)
    {
      if (context.ModelState.IsValid)
      {
        return;
      }

      var errorsInModel = context.ModelState
        .Where(x => x.Value.Errors.Count > 0)
        .ToDictionary(kvp => kvp.Key,
          kvp => kvp.Value.Errors.Select(
            x => x.ErrorMessage)).ToArray();
      var errorResponse = new ErrorResponseDto { StatusCode = 400, Message = "Validation Error" };

      foreach (var (key, value) in errorsInModel)
      {
        foreach (var error in value)
        {
          errorResponse.Errors.Add(new ErrorDto { Property = key, Error = error });
        }
      }

      context.Result = new BadRequestObjectResult(errorResponse);
    }
  }
}
