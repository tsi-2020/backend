﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Obl.API.Middlewares;
using Udelar.Common.Dtos;
using Udelar.Common.Exceptions;

namespace Udelar.APi.Middleware
{
  public static class ExceptionMiddlewareExtensions
  {
    public static void ConfigureExceptionHandler(this IApplicationBuilder app, ILoggerManager logger)
    {
      app.UseExceptionHandler(appError =>
      {
        appError.Run(async context =>
        {
          var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
          context.Response.StatusCode = GetErrorCode(contextFeature.Error);
          context.Response.ContentType = "application/json";
          logger.LogError($"Something went wrong: {contextFeature.Error}");

          var errorDto = new ErrorResponseDto
          {
            StatusCode = context.Response.StatusCode,
            Message = contextFeature.Error.Message
            // todo production
            // Message =  context.Response.StatusCode == 500 ? "Internal Server Error" : contextFeature.Error.Message
          };
          var json = JsonConvert.SerializeObject(errorDto,
            new JsonSerializerSettings
            {
              ContractResolver = new DefaultContractResolver { NamingStrategy = new CamelCaseNamingStrategy() },
              Formatting = Formatting.Indented
            });
          await context.Response.WriteAsync(json);
        });
      });
    }

    private static int GetErrorCode(Exception exception)
    {
      return exception switch
      {
        EntityNotFoundException _ => (int)HttpStatusCode.NotFound,
        ValidationException _ => (int)HttpStatusCode.BadRequest,
        ConflictException _ => (int)HttpStatusCode.Conflict,
        AuthenticationException _ => (int)HttpStatusCode.Unauthorized,
        AuthorizationException _ => (int)HttpStatusCode.Forbidden,
        NotImplementedException _ => (int)HttpStatusCode.NotImplemented,
        _ => (int)HttpStatusCode.InternalServerError
      };
    }
  }
}
