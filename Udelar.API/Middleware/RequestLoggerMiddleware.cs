﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Udelar.APi.Middleware
{
  public class RequestLoggerMiddleware
  {
    private readonly ILogger _logger;
    private readonly RequestDelegate _next;

    public RequestLoggerMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
    {
      _next = next;
      _logger = loggerFactory.CreateLogger<RequestLoggerMiddleware>();
    }

    public async Task Invoke(HttpContext context)
    {
      var bodyStream = context.Response.Body;

      var responseBodyStream = new MemoryStream();
      context.Response.Body = responseBodyStream;

      await _next(context);

      responseBodyStream.Seek(0, SeekOrigin.Begin);
      var responseBody = new StreamReader(responseBodyStream).ReadToEnd();
      _logger.LogInformation(
        "Request {method} {url} => {statusCode}",
        context.Request?.Method,
        context.Request?.Path.Value,
        context.Response?.StatusCode);
      if (context.Response.StatusCode > 299)
      {
        _logger.Log(LogLevel.Information, $"RESPONSE LOG: {responseBody}", null);
      }

      responseBodyStream.Seek(0, SeekOrigin.Begin);
      await responseBodyStream.CopyToAsync(bodyStream);
    }
  }
}
