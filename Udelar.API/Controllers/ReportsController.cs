﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Report;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class ReportsController : ControllerBase
  {
    private IUserService _userService;
    private ISubjectService _subjectService;

    public ReportsController(IUserService userService, ISubjectService subjectService)
    {
      _userService = userService;
      _subjectService = subjectService;
    }


    /// <summary>
    /// [UdelarAdmin,UniversityAdmin]
    /// </summary>
    [HttpGet("SubjectEnrollments")]
    [Authorize(Roles = "UdelarAdmin, UniversityAdmin")]
    public async Task<ActionResult<List<SubjectEnrollmentsReportDto>>> GetSubjectEnrollmentsReportGroupByUniversity()
    {
      if (User.IsInRole("UniversityAdmin"))
      {
        var tenantAsString = User.FindFirstValue("Tenant");
        if (!Guid.TryParse(tenantAsString, out var tenantId))
        {
          return BadRequest("Invalid Tenant Id");
        }

        return await _subjectService.GetSubjectEnrollmentsReportFilterByUniversityId(tenantId);
      }
      return await _subjectService.GetSubjectEnrollmentsReportFilterByUniversityId(Guid.Empty);
    }


    /// <summary>
    /// [UdelarAdmin]
    /// </summary>
    [HttpGet("UniversityUsers")]
    [Authorize(Roles = "UdelarAdmin")]
    public async Task<ActionResult<List<UniversityUsersReportDto>>> GetUniversitiesUsersQuantityReport()
    {
      return await _userService.GetUniversitiesUsersQuantityReport();
    }
  }
}
