﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Poll.Request;
using Udelar.Common.Dtos.Poll.Response;
using Udelar.Common.Exceptions;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class PollsController : ControllerBase
  {
    private readonly IPollService _pollService;

    public PollsController(IPollService pollService)
    {
      _pollService = pollService;
    }

    /// <summary>
    /// Crea una poll de UDELAR [UdelarAdmin]
    /// </summary>
    [HttpPost]
    [Authorize(Roles = "UdelarAdmin")]
    public async Task<IActionResult> CreateUdelarPoll(CreatePollDto createPollDto)
    {
      await _pollService.CreateUdelarPoll(createPollDto);
      return NoContent();
    }


    /// <summary>
    /// Crea una poll de Universidad [UniversityAdmin]
    /// </summary>
    [HttpPost("/api/Universities/{universityId}/Polls")]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<IActionResult> CreateUniversityPoll(CreatePollDto createPollDto, Guid universityId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        throw new AuthorizationException("Permisos insuficientes");
      }

      await _pollService.CreateUniversityPoll(createPollDto, universityId);

      return NoContent();
    }


    /// <summary>
    /// Crea una poll de Curso [Professor]
    /// </summary>
    [HttpPost("/api/Universities/{universityId}/Subjects/{subjectId}/Polls")]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> CreateSubjectPoll(CreatePollDto createPollDto, Guid subjectId, Guid universityId)
    {
      {
        var tenantAsString = User.FindFirstValue("Tenant");
        if (Guid.TryParse(tenantAsString, out var tenantId))
        {
          await _pollService.CreateSubjectPoll(createPollDto, subjectId, universityId);
        }

        return NoContent();
      }
    }


    /// <summary>
    /// Publica una poll en un Curso [Professor,UniversityAdmin]
    /// </summary>
    [HttpPost("/api/Universities/{universityId}/Subjects/{subjectId}/Polls/{pollId}")]
    [Authorize(Roles = "UniversityAdmin,Professor")]
    public async Task<IActionResult> PublishPollIntoSubject(Guid universityId, Guid subjectId, Guid pollId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      Guid.TryParse(tenantAsString, out var tenantId);
      if (!tenantId.Equals(universityId))
      {
        throw new AuthorizationException("Permisos insuficientes.");
      }

      if (User.IsInRole("Professor"))
      {
        await _pollService.PublishPollIntoSubjectAsProfessor(universityId, subjectId, pollId);
      }
      else
      {
        await _pollService.PublishPollIntoSubjectAsUniversityAdmin(universityId, subjectId, pollId);
      }

      return Ok();
    }


    /// <summary>
    /// Publica una Poll en una Universidad [UdelarAdmin]
    /// </summary>
    [HttpPost("/api/Universities/{universityId}/Polls/{pollId}")]
    [Authorize(Roles = "UdelarAdmin")]
    public async Task<IActionResult> PublishPollIntoUniversity(Guid universityId, Guid pollId)
    {
      await _pollService.PublishPollIntoUniversity(universityId, pollId);
      return Ok();
    }

    /// <summary>
    /// [Student,Professor]
    /// </summary>
    [HttpPost("/api/Subjects/{subjectId}/Polls/{pollId}/Reply")]
    [Authorize(Roles = "Student,Professor")]
    public async Task<IActionResult> ReplySubjectPoll(Guid subjectId, Guid pollId, ReplyPollDto replyPollDto)
    {
      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var userId))
      {
        return BadRequest("Invalid User Id");
      }

      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        throw new AuthorizationException("Permisos insuficientes.");
      }

      await _pollService.ReplySubjectPoll(tenantId, subjectId, pollId, userId, replyPollDto);
      return Ok();
    }

    /// <summary>
    /// [Student,Professor]
    /// </summary>
    [HttpPost("/api/Universities/{universityId}/Polls/{pollId}/Reply")]
    [Authorize(Roles = "Student,Professor")]
    public async Task<IActionResult> ReplyUniversityPoll(Guid universityId, Guid pollId, ReplyPollDto replyPollDto)
    {
      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var userId))
      {
        return BadRequest("Invalid User Id");
      }

      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        throw new AuthorizationException("Permisos insuficientes.");
      }

      await _pollService.ReplyUniversityPoll(universityId, pollId, userId, replyPollDto);
      return Ok();
    }

    /// <summary>
    /// Lee las polls de un Curso [Professor]
    /// </summary>
    [HttpGet("/api/Subjects/{subjectId}/Polls")]
    [Authorize(Roles = "Professor")]
    public async Task<ActionResult<List<ReadPollDto>>> ListSubjectPolls(Guid subjectId)
    {
      return Ok(await _pollService.ReadSubjectPolls(subjectId));
    }

    /// <summary>
    /// Lista las polls de Udelar o Universidad segun el ROL [UdelarAdmin,UniversityAdmin]
    /// </summary>
    [HttpGet("/api/Polls")]
    [Authorize(Roles = "UniversityAdmin,UdelarAdmin")]
    public async Task<ActionResult<List<ReadPollDto>>> ListPolls()
    {
      if (User.IsInRole("UdelarAdmin"))
      {
        return Ok(await _pollService.ReadUdelarPolls());
      }

      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        throw new AuthorizationException("Permisos insuficientes.");
      }

      return Ok(await _pollService.ReadUniversityPolls(tenantId));
    }


    /// <summary>
    /// Lee las polls Publicadas en un Curso [Professor,Student]
    /// </summary>
    [HttpGet("/api/Subjects/{subjectId}/PublishedPolls")]
    // [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult<List<ReadPublishPollDto>>> ListPublishedSubjectPolls(Guid subjectId, [FromQuery] Guid userId)
    {
      return Ok(await _pollService.ListPublishedSubjectPolls(subjectId, userId));
    }

    /// <summary>
    /// Lee las polls Publicadas en una Universidad [Professor,Student]
    /// </summary>
    [HttpGet("/api/University/{universityId}/PublishedPolls")]
    // [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult<List<ReadPublishPollDto>>> ListPublishedUniversityPolls(Guid universityId, [FromQuery] Guid userId)
    {
      return Ok(await _pollService.ListPublishedUniversityPolls(universityId, userId));
    }

    /// <summary>
    /// Lee una poll Publicada [Professor,Student]
    /// </summary>
    [HttpGet("/api/University/{universityId}/PublishedPolls/{pollId}")]
    [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult<List<ReadPublishPollDto>>> ReadPublishedPoll(Guid pollId)
    {
      return Ok(await _pollService.GetPublishedPollById(pollId));
    }

    /// <summary>
    /// Lee Respuestas de una poll Publicada [Professor,UniversityAdmin,UdelarAdmin]
    /// </summary>
    [HttpGet("/api/Polls/{pollId}/Answers")]
    [Authorize(Roles = "Professor,UniversityAdmin,UdelarAdmin")]
    public async Task<ActionResult<ReadPaginatedPollAnswerDto>> ReadPollAnswers(Guid pollId, [FromQuery] int page = 1, [FromQuery] int limit = 25)
    {
      return Ok(await _pollService.GetPollAnswers(pollId, page, limit));
    }
  }
}
