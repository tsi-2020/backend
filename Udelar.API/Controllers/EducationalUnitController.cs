﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Component.Request;
using Udelar.Common.Dtos.Component.Response;
using Udelar.Common.Dtos.Enrollment.request;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/Subjects/{subjectId}/EducationalUnit")]
  public class EducationalUnitController : ControllerBase
  {
    private readonly ISubjectService _subjectService;

    public EducationalUnitController(ISubjectService subjectService)
    {
      _subjectService = subjectService;
    }

    /// <summary>
    /// [Professor]
    /// </summary>
    [HttpPost]
    [Authorize(Roles = "Professor")]
    public async Task<ActionResult<ReadEducationalUnitDto>> AddNewEducationalUnit(Guid subjectId, CreateEducationalUnitDto unitDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var responsibleUserId))
      {
        return BadRequest("Invalid User Id");
      }

      return Ok(await _subjectService.AddNewEducationalUnit(tenantId, subjectId, unitDto, responsibleUserId));
    }

    /// <summary>
    /// [Professor]
    /// </summary>
    [HttpDelete("{unitId}")]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> RemoveEducationalUnit(Guid subjectId, Guid unitId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var responsibleUserId))
      {
        return BadRequest("Invalid User Id");
      }

      await _subjectService.RemoveEducationalUnit(tenantId, subjectId, unitId, responsibleUserId);
      return Ok();
    }

    /// <summary>
    /// [Professor]
    /// </summary>
    [HttpPut("{unitId}")]
    [Authorize(Roles = "Professor")]
    public async Task<ActionResult<ReadEducationalUnitDto>> UpdateEducationalUnit(Guid subjectId, Guid unitId, UpdateEducationalUnitDto unitDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var responsibleUserId))
      {
        return BadRequest("Invalid User Id");
      }


      return Ok(await _subjectService.UpdateEducationalUnit(tenantId, subjectId, unitId, unitDto, responsibleUserId));
    }
  }
}
