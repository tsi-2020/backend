﻿using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.University.Request;
using Udelar.Common.Dtos.University.Response;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class UniversitiesController : ControllerBase
  {
    private readonly IUniversitiesService _universitiesService;

    public UniversitiesController(IUniversitiesService universitiesService)
    {
      _universitiesService = universitiesService;
    }

    /// <summary>
    /// [UdelarAdmin]
    /// </summary>
    [HttpPost]
    [Authorize(Roles = "UdelarAdmin")]
    public async Task<ActionResult<ReadUniversityDto>> CreateUniversity(CreateUniversityDto createUniversity)
    {
      return await _universitiesService.CreateUniversity(createUniversity);
    }

    [HttpGet]
    public async Task<ActionResult<List<ReadUniversityDto>>> GetUniversities([FromQuery(Name = "path")] string path)
    {
      if (!string.IsNullOrEmpty(path))
      {
        return await _universitiesService.GetUniversityByPath(path);
      }
      return await _universitiesService.GetUniversities();
    }

    /// <summary>
    /// [UdelarAdmin,UniversityAdmin]
    /// </summary>
    [HttpPut]
    [Authorize(Roles = "UdelarAdmin, UniversityAdmin")]
    public async Task<ActionResult<ReadUniversityDto>> EditUniversity(UpdateUniversityDto updateUniversity)
    {
      return await _universitiesService.EditUniversity(updateUniversity);
    }

    /// <summary>
    /// [UdelarAdmin]
    /// </summary>
    [HttpDelete("{id}")]
    [Authorize(Roles = "UdelarAdmin")]
    public async Task<IActionResult> DeleteUniversity(Guid id)
    {
      await _universitiesService.DeleteUniversity(id);
      return Ok();
    }

    /// <summary>
    /// [UdelarAdmin,UniversityAdmin]
    /// </summary>
    [HttpGet("{id}")]
    [Authorize(Roles = "UniversityAdmin,UdelarAdmin")]
    public async Task<ActionResult<ReadUniversityDto>> GetUniversityById(Guid id)
    {
      if (User.IsInRole("UdelarAdmin"))
      {
        return await _universitiesService.GetUniversityById(id);
      }

      var currentUserId = User.FindFirstValue("Tenant");
      if (string.IsNullOrEmpty(currentUserId) || !currentUserId.Equals(id.ToString()))
      {
        throw new AuthenticationException();
      }

      return await _universitiesService.GetUniversityById(id);
    }
  }
}
