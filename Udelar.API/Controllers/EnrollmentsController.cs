﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Enrollment.request;
using Udelar.Common.Dtos.Enrollment.response;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/Subjects/{subjectId}")]
  public class EnrollmentsController : ControllerBase
  {
    private readonly IEnrollmentService _enrollmentService;

    public EnrollmentsController(IEnrollmentService enrollmentService)
    {
      _enrollmentService = enrollmentService;
    }

    /// <summary>
    /// Devuelve todas las inscripciones de un curso [Student,Professor,UniversityAdmin]
    /// </summary>
    [HttpGet("Enrollments")]
    [Authorize(Roles = "Student,Professor,UniversityAdmin")]
    public async Task<ActionResult<ReadEnrollmentListDto>> GetSubjectEnrollments(Guid subjectId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var userId))
      {
        return BadRequest("Invalid User Id");
      }

      if (User.IsInRole("UniversityAdmin"))
      {
        userId = Guid.Empty;
      }

      var result = await _enrollmentService.GetSubjectEnrollments(tenantId, subjectId, userId);

      return Ok(result);
    }

    /// <summary>
    /// Actualiza los docentes de un curso y su rol dentro del curso [Professor, UniveristyAdmin]
    /// </summary>
    [HttpPost("Professors")]
    [Authorize(Roles = "Professor,UniversityAdmin")]
    public async Task<IActionResult> EnrollProfessors(Guid subjectId, List<CreateProfessorEnrollmentDto> enrollmentDtoList)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var role = User.FindFirst(ClaimTypes.Role).Value;

      var responsibleUserId = Guid.Empty;

      if (role != "UniversityAdmin")
      {
        var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
        if (!Guid.TryParse(userIdAsString, out responsibleUserId))
        {
          return BadRequest("Invalid User Id");
        }
      }

      await _enrollmentService.UpdateProfessorEnrollments(tenantId, subjectId, enrollmentDtoList, responsibleUserId);
      return Ok();
    }

    /// <summary>
    /// Agrega un alumno a un curso [Professor]
    /// </summary>
    [HttpPost("Students")]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> EnrollStudent(Guid subjectId, CreateStudentEnrollmentDto enrollmentDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var responsibleUserId))
      {
        return BadRequest("Invalid User Id");
      }

      await _enrollmentService.EnrollStudent(tenantId, subjectId, enrollmentDto.UserId, responsibleUserId);
      return Ok();
    }

    /// <summary>
    /// Automatriculacion a curso [Student]
    /// </summary>
    [HttpPost("enroll")]
    [Authorize(Roles = "Student")]
    public async Task<IActionResult> AutoEnrollStudent(Guid subjectId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var userId))
      {
        userId = Guid.Empty;
      }

      await _enrollmentService.AutoEnrollStudent(tenantId, subjectId, userId);
      return Ok();
    }

    /// <summary>
    /// Actualiza en rol de un docente dentro de un curso [Professor]
    /// </summary>
    [HttpPut("Professors/{enrollmentId}")]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> UpdateProfessorRole(Guid subjectId, Guid enrollmentId,
      UpdateProfessorEnrollmentDto enrollmentDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var responsibleUserId))
      {
        return BadRequest("Invalid User Id");
      }

      await _enrollmentService.UpdateProfessorRole(tenantId, subjectId, enrollmentId, enrollmentDto.IsResponsible,
        responsibleUserId);
      return Ok();
    }

    /// <summary>
    /// Elimina un inscripcion dentro de un curso [Professor]
    /// </summary>
    /// 
    [HttpDelete("Enrollments/{enrollmentId}")]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> DeleteEnrollment(Guid subjectId, Guid enrollmentId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var responsibleUserId))
      {
        return BadRequest("Invalid User Id");
      }

      await _enrollmentService.DeleteEnrollment(tenantId, subjectId, enrollmentId, responsibleUserId);
      return Ok();
    }
  }
}
