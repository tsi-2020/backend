﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.UniversityAdministrator.Reponse;
using Udelar.Common.Dtos.UniversityAdministrator.Request;
using Udelar.Common.Exceptions;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class UniversityAdministratorsController : ControllerBase
  {
    private readonly IUniversityAdminService _universityAdminService;

    public UniversityAdministratorsController(IUniversityAdminService universityAdminService)
    {
      _universityAdminService = universityAdminService;
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [Authorize(Roles = "UniversityAdmin")]
    [HttpGet]
    public async Task<ActionResult<List<ReadUniversityAdminDto>>> ListAdmins()
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      return Ok(await _universityAdminService.ListAdmins(tenantId));
    }

    /// <summary>
    /// [UniversityAdmin,UdelarAdmin]
    /// </summary>
    [Authorize(Roles = "UniversityAdmin, UdelarAdmin")]
    [HttpPost("/api/Universities/{universityId}/UniversityAdministrators")]
    public async Task<ActionResult<ReadUniversityAdminDto>> CreateAdmin(
      CreateUniversityAdminDto createUniversityAdminDto, Guid universityId)
    {
      var tenantId = User.FindFirstValue("Tenant");
      if (tenantId != "" && !tenantId.Equals(universityId.ToString()))
      {
        throw new AuthorizationException("Permisos insuficientes.");
      }

      var adminDto = await _universityAdminService.CreateAdmin(createUniversityAdminDto, universityId);
      return CreatedAtRoute("GetUniversityAdminById", new { UserId = adminDto.Id, UniversityId = universityId },
        adminDto);
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [Authorize(Roles = "UniversityAdmin")]
    [HttpGet("{userId}", Name = nameof(GetUniversityAdminById))]
    public async Task<ActionResult<ReadUniversityAdminDto>> GetUniversityAdminById(Guid userId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      return Ok(await _universityAdminService.GetAdminById(userId, tenantId));
    }

    /// <summary>
    /// [UniversityAdmin,UdelarAdmin]
    /// </summary>
    [HttpPut("/api/Universities/{universityId}/UniversityAdministrators/{userId}")]
    [Authorize(Roles = "UniversityAdmin, UdelarAdmin")]
    public async Task<IActionResult> UpdateUniversityAdminPersonalData(Guid userId, Guid universityId,
      ChangePersonalDataUniversityAdminDto adminData)
    {
      var tenantId = User.FindFirstValue("Tenant");
      if (tenantId != "" && !tenantId.Equals(universityId.ToString()))
      {
        throw new AuthorizationException("Permisos insuficientes.");
      }

      await _universityAdminService.UpdateUniversityAdminPersonalData(universityId, userId, adminData);
      return Ok();
    }

    /// <summary>
    /// [UniversityAdmin,UdelarAdmin]
    /// </summary>
    [HttpPatch("/api/Universities/{universityId}/UniversityAdministrators/{userId}/password")]
    [Authorize(Roles = "UniversityAdmin, UdelarAdmin")]
    public async Task<IActionResult> ChangePassword(UpdateUniversityAdminPasswordDto passwordDto, Guid userId,
      Guid universityId)
    {
      if (User.IsInRole("UdelarAdmin"))
      {
        await _universityAdminService.ChangePassword(passwordDto, userId);
      }
      else
      {
        var tenantId = User.FindFirstValue("Tenant");
        if (string.IsNullOrEmpty(tenantId) || !tenantId.Equals(universityId.ToString()))
        {
          throw new AuthorizationException("Permisos insuficientes.");
        }
        await _universityAdminService.ChangePassword(passwordDto, userId);
      }

      return NoContent();
    }

    /// <summary>
    /// [UniversityAdmin,UdelarAdmin]
    /// </summary>
    [HttpDelete("/api/Universities/{universityId}/UniversityAdministrators/{userId}")]
    [Authorize(Roles = "UniversityAdmin,UdelarAdmin")]
    public async Task<IActionResult> DeleteUniversityAdmin(Guid userId, Guid universityId)
    {
      var tenantId = User.FindFirstValue("Tenant");
      if (tenantId != "" && !tenantId.Equals(universityId.ToString()))
      {
        throw new AuthorizationException("Permisos insuficientes.");
      }

      await _universityAdminService.DeleteUniversityAdmin(universityId, userId);
      return NoContent();
    }
  }
}
