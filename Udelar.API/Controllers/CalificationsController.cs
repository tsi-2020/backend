﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Califications.response;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/Subjects/{subjectId}/Califications")]
  public class CalificationsController : ControllerBase
  {
    private ICalificationService _calificationService;

    public CalificationsController(ICalificationService calificationService)
    {
      _calificationService = calificationService;
    }


    /// <summary>
    /// Informe de Cierre de Actas [Professor,UniversityAdmin]
    /// </summary>
    /// <param name="subjectId"></param>
    /// <returns></returns>
    [HttpGet]
    [Authorize(Roles = "Professor,UniversityAdmin")]
    public async Task<ActionResult<List<ReadCalificationDto>>> GetSubjectCalifications(Guid subjectId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var userId))
      {
        return BadRequest("Invalid User Id");
      }

      if (User.IsInRole("UniversityAdmin"))
      {
        userId = Guid.Empty;
      }

      return Ok(await _calificationService.GetSubjectCalifications(tenantId, subjectId, userId));
    }

    /// <summary>
    /// Generar Cierre de Actas [Professor,UniversityAdmin]
    /// </summary>
    /// <param name="subjectId"></param>
    /// <returns></returns>
    [HttpPost]
    [Authorize(Roles = "Professor,UniversityAdmin")]
    public async Task<ActionResult<List<ReadCalificationDto>>> GenerateSubjectCalifications(Guid subjectId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var userId))
      {
        return BadRequest("Invalid User Id");
      }

      if (User.IsInRole("UniversityAdmin"))
      {
        userId = Guid.Empty;
      }

      return Ok(await _calificationService.GenerateSubjectCalifications(tenantId, subjectId, userId));
    }
  }
}
