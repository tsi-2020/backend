﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos;
using Udelar.Common.Dtos.Career.Request;
using Udelar.Common.Dtos.Career.Response;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/[Controller]")]
  public class CareersController : ControllerBase
  {
    private readonly ICareerService _careerService;

    public CareersController(ICareerService careerService)
    {
      _careerService = careerService;
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpPost]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<ActionResult<ReadCareerDto>> CreateCareer(CreateCareerDto career)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      return Ok(await _careerService.CreateCareer(tenantId, career));
    }

    [HttpGet("/api/Universities/{universityId}/Careers")]
    public async Task<ActionResult<List<ReadCareerDto>>> GetAllCareers(Guid universityId, [FromQuery] string careerName)
    {
      return Ok(await _careerService.GetAllUniversityCareers(universityId, careerName));
    }

    [HttpGet("/api/Universities/{universityId}/Careers/{careerId}")]
    public async Task<ActionResult<ReadCareerDto>> GetCareerById(Guid careerId, Guid universityId)
    {
      return Ok(await _careerService.GetUniversityCareerById(universityId, careerId));
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpPut]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<IActionResult> EditCareer(EditCareerDto career)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      await _careerService.EditCareer(tenantId, career);
      return Ok();
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpDelete("{careerId}")]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<IActionResult> DeleteCareer(Guid careerId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      await _careerService.DeleteCareer(tenantId, careerId);
      return Ok();
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpPost("{careerId}/Subjects")]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<IActionResult> AddSubjectToCareer(Guid careerId, List<Guid> subjectIds)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      await _careerService.AddSubjects(tenantId, careerId, subjectIds);
      return Ok();
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpPatch("{careerId}/Subjects")]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<IActionResult> RemoveSubjectsFromCareer(Guid careerId, List<Guid> subjectIds)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      await _careerService.RemoveSubjects(tenantId, careerId, subjectIds);

      return Ok();

    }

  }
}
