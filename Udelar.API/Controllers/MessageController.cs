﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Message.Request;
using Udelar.Common.Dtos.Message.Response;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class MessageController : ControllerBase
  {
    private readonly IMessageService _messageService;

    public MessageController(IMessageService messageService)
    {
      _messageService = messageService;
    }

    /// <summary>
    /// [Professor,Student]
    /// </summary>
    [HttpPost("{threadId}")]
    [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult<ReadMessageDto>> CreateMessage(CreateMessageDto createMessageDto, string threadId)
    {
      var userIdAsString = User.FindFirstValue(ClaimTypes.NameIdentifier);
      var userFullName = User.FindFirstValue(ClaimTypes.GivenName);
      var tenantAsString = User.FindFirstValue("Tenant");

      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      if (!Guid.TryParse(userIdAsString, out var userId))
      {
        return BadRequest("Invalid User Id");
      }

      return Ok(await _messageService.CreateMessage(createMessageDto, threadId, userId, userFullName, tenantId));
    }

    /// <summary>
    /// [Professor,Student]
    /// </summary>
    [HttpPut("{messageId}")]
    [Authorize(Roles = "Professor,Student")]
    public async Task<IActionResult> EditMessage(EditMessageDto editMessageDto, string messageId)
    {
      var userIdAsString = User.FindFirstValue(ClaimTypes.NameIdentifier);
      var tenantAsString = User.FindFirstValue("Tenant");

      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      if (!Guid.TryParse(userIdAsString, out var userId))
      {
        return BadRequest("Invalid User Id");
      }

      await _messageService.EditMessage(editMessageDto, messageId, tenantId, userId);

      return Ok();
    }

  }
}
