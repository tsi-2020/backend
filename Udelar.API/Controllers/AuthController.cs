﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Auth.Response;
using Udelar.Common.Dtos.Auth.Rquest;
using Udelar.Common.Dtos.User.Request;
using Udelar.Model;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class AuthController : ControllerBase
  {
    private readonly IAuthService _authService;
    private readonly IUserService _userService;

    public AuthController(IAuthService authService, IUserService userService)
    {
      _authService = authService;
      _userService = userService;
    }

    [HttpPost("login")]
    public async Task<ActionResult<CredentialsDto>> AdminLogin(BackOfficeLoginDto backOfficeLoginDto)
    {
      return await _authService.AdminLogin(backOfficeLoginDto);
    }

    [HttpPost("refreshToken")]
    public async Task<ActionResult<CredentialsDto>> AdminRefreshToken(RefreshTokenDto refreshTokenDto)
    {
      return await _authService.RefreshToken(refreshTokenDto);
    }


    [HttpPost("{universityId}/register")]
    public async Task<NoContentResult> UserRegister(CreateUserDto createUserDto, Guid universityId)
    {
      await _userService.CreateUser(createUserDto, new List<Role> { Role.Student }, universityId);
      return NoContent();
    }

    [HttpPost("{universityId}/login")]
    public async Task<ActionResult<CredentialsDto>> UserLogin(FrontOfficeLoginDto frontOfficeLoginUdelarDto,
      Guid universityId)
    {
      return await _authService.UserLogin(frontOfficeLoginUdelarDto, universityId);
    }


    [HttpPost("{universityId}/ResetPassword")]
    public async Task<ActionResult> ResetPassword(RequestForgotPasswordDto requestForgotPassword, Guid universityId)
    {
      await _authService.RequestForgotPasswordLink(requestForgotPassword, universityId);
      return Ok();
    }
  }
}
