﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.User.Request;
using Udelar.Common.Dtos.User.Response;
using Udelar.Model;
using Udelar.Services;


namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class UsersController : ControllerBase
  {
    private readonly IUserService _userService;

    public UsersController(IUserService userService)
    {
      _userService = userService;
    }


    [HttpGet("ValidateToken/{token}")]
    public async Task<ActionResult<ReadUserDto>> PreActivate(string token)
    {
      var createdUser = await _userService.FindByResetToken(token);
      return StatusCode(200, createdUser);
    }

    [HttpPost("ResetPassword/{token}")]
    public async Task<ActionResult<ReadUserDto>> ResetPassword(string token, ResetPasswordDto resetPasswordDto)
    {
      var createdUser = await _userService.ResetPassword(token, resetPasswordDto);
      return StatusCode(200, createdUser);
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [Authorize(Roles = "UniversityAdmin")]
    [HttpPost]
    public async Task<ActionResult<ReadUserDto>> CreateUser(CreateUserDto createUserDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var roles = createUserDto.Roles.Select(Enum.Parse<Role>).ToHashSet();

      var createdUser = await _userService.CreateUser(createUserDto, roles.ToList(), tenantId);
      return StatusCode(201, createdUser);
    }

    /// <summary>
    /// [UniversityAdmin,Professor,Student]
    /// </summary>
    [HttpGet]
    [Authorize(Roles = "UniversityAdmin,Professor,Student")]
    public async Task<ActionResult<PaginatedUsersDto>> GetUniversityUsers([FromQuery] FilterUserDto filterUsersDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      return Ok(await _userService.GetUsers(tenantId, filterUsersDto));
    }

    /// <summary>
    /// [UdelarAdmin,UniversityAdmin,Professor,Student]
    /// </summary>
    [HttpGet("{userId}")]
    [Authorize(Roles = "UdelarAdmin,UniversityAdmin,Professor,Student")]
    public async Task<ActionResult<ReadUserDto>> GetUniversityUserById(Guid userId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      return Ok(await _userService.GetUserById(tenantId, userId));
    }

    /// <summary>
    /// [UniversityAdmin,Professor,Student]
    /// </summary>
    [HttpPut("{userId}")]
    [Authorize(Roles = "UniversityAdmin,Student,Professor")]
    public async Task<ActionResult> UpdateUniversityUserById(Guid userId, UpdateUserDto updateData)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      await _userService.UpdateUserData(tenantId, userId, updateData);
      return Ok();
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpPatch("{userId}/Roles")]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<ActionResult> ChangeUserRoles(Guid userId, PatchUserRoleDto patchUserRoleDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      await _userService.PatchUserRole(tenantId, userId, patchUserRoleDto);
      return Ok();
    }


    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpDelete("{userId}")]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<ActionResult> DeleteUser(Guid userId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      await _userService.DeleteUser(tenantId, userId);
      return Ok();
    }

    /// <summary>
    /// [Professor,Student]
    /// </summary>
    [HttpPut]
    [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult> UpdateMyUser(UpdateUserDto updateData)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var userId))
      {
        return BadRequest("Invalid User Id");
      }

      await _userService.UpdateUserData(tenantId, userId, updateData);
      return Ok();
    }

    /// <summary>
    /// [Professor,Student]
    /// </summary>
    [HttpGet("{userId}/Enrollments")]
    [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult<ReadUserEnrollmentsDto>> GetUserEnrollments(Guid userId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var parsedUserId))
      {
        return BadRequest("Invalid User Id");
      }

      if (userId != parsedUserId)
      {
        return BadRequest("Invalid User Id");
      }

      return Ok(await _userService.GetUserEnrollments(userId));
    }
  }
}
