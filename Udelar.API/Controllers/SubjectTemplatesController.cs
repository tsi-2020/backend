﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.SubjectTemplate.request;
using Udelar.Common.Dtos.SubjectTemplate.response;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/Templates")]
  public class SubjectTemplatesController : ControllerBase
  {
    private readonly ISubjectTemplateService _templateService;

    public SubjectTemplatesController(ISubjectTemplateService templateService)
    {
      _templateService = templateService;
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpPost]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<IActionResult> CreateSubjectTemplate(CreateSubjectTemplateDto subjectTemplate)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      await _templateService.CreateSubjectTemplate(tenantId, subjectTemplate);
      return Ok();
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpGet]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<ActionResult<List<ReadSubjectTemplateDto>>> GetAllSubjectTemplates()
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      return Ok(await _templateService.GetAllSubjectTemplates(tenantId));
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpGet("{templateId}")]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<ActionResult<List<ReadSubjectTemplateDto>>> GetSubjectTemplateById(Guid templateId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      return Ok(await _templateService.GetSubjectTemplateById(tenantId, templateId));
    }
  }
}
