﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Forum.Request;
using Udelar.Common.Dtos.Forum.Response;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class ForumsController : ControllerBase
  {
    private readonly IForumService _forumService;

    public ForumsController(IForumService forumService)
    {
      _forumService = forumService;
    }

    /// <summary>
    /// [Professor]
    /// </summary>
    [HttpPost]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> CreateForum(CreateForumDto createForumDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      await _forumService.CreateForum(createForumDto, tenantId);
      return Ok();
    }

    /// <summary>
    /// [Professor, Student]
    /// </summary>
    [HttpGet]
    [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult<PaginatedForumDto>> ListForums([FromQuery] FilterForumDto filterForumDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      return Ok(await _forumService.ListForum(tenantId, filterForumDto));
    }

    /// <summary>
    /// [Professor]
    /// </summary>
    [HttpPut("{forumId}")]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> EditForum(EditForumDto editForumDto, string forumId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      await _forumService.EditForum(editForumDto, tenantId, forumId);
      return Ok();
    }

    /// <summary>
    /// [Professor]
    /// </summary>
    [HttpDelete("{forumId}")]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> DeleteForum(string forumId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      await _forumService.DeleteForum(tenantId, forumId);
      return Ok();
    }

    /// <summary>
    /// [Professor,Student]
    /// </summary>
    [HttpGet("{forumId}")]
    [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult<ViewForumDto>> ReadForums(string forumId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      return Ok(await _forumService.ViewForum(tenantId, forumId));
    }
  }
}
