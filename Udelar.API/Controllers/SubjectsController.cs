﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Subject.Request;
using Udelar.Common.Dtos.Subject.Response;
using Udelar.Common.Exceptions;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class SubjectsController : ControllerBase
  {
    private readonly ISubjectService _subjectService;

    public SubjectsController(ISubjectService subjectService)
    {
      _subjectService = subjectService;
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpPost]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<ActionResult<ReadSubjectLiteDto>> CreateSubject(CreateSubjectDto subjectDto)
    {
      var tenantId = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantId, out var tenantGuid))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var subject = await _subjectService.CreateSubject(tenantGuid, subjectDto);
      return Ok(subject);
    }

    [HttpGet("/api/Universities/{universityId}/Subjects")]
    // [Authorize(Roles = "UdelarAdmin,UniversityAdmin,Student,Professor")]
    public async Task<ActionResult<List<ReadSubjectLiteDto>>> GetAllSubjects(Guid universityId, [FromQuery] string subjectName = "")
    {
      return Ok(await _subjectService.GetAllSubjects(subjectName, universityId));
    }


    /// <summary>
    /// [UniversityAdmin,Student,Professor]
    /// </summary>
    [HttpGet("{id}")]
    [Authorize(Roles = "UniversityAdmin,Student,Professor")]
    //TODO verificar que el usuario  con perfil docente o alumno pertenezca al curso
    public async Task<ActionResult<ReadSubjectDto>> GetSubjectById(Guid id)
    {
      var tenantId = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantId, out var tenantGuid))
      {
        return BadRequest("Invalid Tenant Id");
      }

      return Ok(await _subjectService.GetSubjectById(tenantGuid, id));
    }


    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpDelete("{id}")]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<IActionResult> RemoveSubject(Guid id)
    {
      var tenantId = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantId, out var tenantGuid))
      {
        return BadRequest("Invalid Tenant Id");
      }

      await _subjectService.RemoveSubject(tenantGuid, id);
      return Ok();
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [HttpPut("{id}")]
    [Authorize(Roles = "UniversityAdmin")]
    public async Task<ActionResult<ReadSubjectLiteDto>> UpdateSubject(Guid id, UpdateSubjectDto subjectDto)
    {
      var tenantId = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantId, out var tenantGuid))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var subject = await _subjectService.UpdateSubject(tenantGuid, id, subjectDto);
      return Ok(subject);
    }
  }

}
