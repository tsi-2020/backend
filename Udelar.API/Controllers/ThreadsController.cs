﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Udelar.Common.Dtos.Thread.Request;
using Udelar.Common.Dtos.Thread.Response;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class ThreadsController : ControllerBase
  {
    private readonly IThreadService _threadService;

    public ThreadsController(IThreadService threadService)
    {
      _threadService = threadService;
    }

    /// <summary>
    /// [Professor,Student]
    /// </summary>
    [HttpPost("/api/Forums/{forumId}/Threads")]
    [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult<ReadThreadDto>> CreateThread(CreateThreadDto createThreadDto, string forumId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      return Ok(await _threadService.CreateThread(createThreadDto, forumId, tenantId));
    }

    /// <summary>
    /// [Professor]
    /// </summary>
    [HttpPut("{threadId}")]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> EditThread(EditThreadDto editThreadDto, string threadId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      await _threadService.EditThread(editThreadDto, threadId, tenantId);
      return Ok();
    }

    /// <summary>
    /// [Professor]
    /// </summary>
    [HttpDelete("{threadId}")]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> DeleteThread(string threadId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      await _threadService.DeleteThread(threadId, tenantId);
      return Ok();
    }

    /// <summary>
    /// [Professor,Student]
    /// </summary>
    [HttpGet("{threadId}/messages")]
    [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult<PaginatedViewThreadDto>> ReadThreads(string threadId, [FromQuery] FilterThreadDto filterThreadDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      return Ok(await _threadService.ViewThread(tenantId, threadId, filterThreadDto));

    }

    /// <summary>
    /// [Professor,Student]
    /// </summary>
    [HttpGet("{forumId}")]
    [Authorize(Roles = "Professor,Student")]
    public async Task<ActionResult<PaginatedThreadDto>> ListThreads(String forumId, [FromQuery] FilterThreadDto filterThreadDto)
    {
      //Control de que forumID esté bien
      if (!ObjectId.TryParse(forumId, out var forumObjId))
      {
        return BadRequest("Invalid Forum Id");
      }

      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      return Ok(await _threadService.ListThreads(tenantId, forumId, filterThreadDto));
    }
  }
}
