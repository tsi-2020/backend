﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Calendar.Request;
using Udelar.Common.Dtos.Calendar.Response;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/Subjects/{subjectId}/CalendarEvents")]
  public class CalendarController : ControllerBase
  {
    private readonly ICalendarService _calendarService;

    public CalendarController(ICalendarService calendarService)
    {
      _calendarService = calendarService;
    }


    /// <summary>
    /// [Professor]
    /// </summary>
    [HttpPost]
    [Authorize(Roles = "Professor")]
    public async Task<IActionResult> CreateEvent(Guid subjectId, CreateEventDto createEventDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      return Ok(await _calendarService.CreateEvent(tenantId, subjectId, createEventDto));
    }

    /// <summary>
    /// [Professor, Student]
    /// </summary>
    [HttpGet]
    [Authorize(Roles = "Professor, Student")]
    public async Task<ActionResult<List<ReadEventDto>>> ReadEvents(Guid subjectId, [FromQuery] FilterEventsDto filterEventsDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      return Ok(await _calendarService.FilterEvents(tenantId, subjectId, filterEventsDto));
    }
  }
}
