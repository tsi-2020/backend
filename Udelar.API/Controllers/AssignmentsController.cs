﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Assigment.request;
using Udelar.Common.Dtos.Assigment.response;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/Subjects/{subjectId}/Assignments")]
  public class AssignmentsController : ControllerBase
  {
    private readonly IAssignmentService _assignmentService;

    public AssignmentsController(IAssignmentService assignmentService)
    {
      _assignmentService = assignmentService;
    }

    /// <summary>
    /// [Student,Professor]
    /// </summary>
    [HttpGet("{assignmentId}")]
    [Authorize(Roles = "Student,Professor")]
    public async Task<ActionResult<ReadAssignmentDto>> GetSubjectAssigmentById(Guid subjectId, Guid assignmentId)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var requesterUserId))
      {
        return BadRequest("Invalid User Id");
      }

      return Ok(await _assignmentService.GetSubjectAssignmentById(tenantId, subjectId, assignmentId, requesterUserId));
    }

    /// <summary>
    /// [Professor]
    /// </summary>
    [HttpPut("{assignmentId}")]
    [Authorize(Roles = "Professor")]
    public async Task<ActionResult<ReadAssignmentDto>> UpdateSubjectAssigment(Guid subjectId, Guid assignmentId, UpdateAssignmentDto assignmentDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var requesterUserId))
      {
        return BadRequest("Invalid User Id");
      }

      return Ok(await _assignmentService.UpdateSubjectAssignment(tenantId, subjectId, assignmentId, assignmentDto, requesterUserId));
    }

    /// <summary>
    /// [Student]
    /// </summary>
    [HttpPost("{assignmentId}/deliveries")]
    [Authorize(Roles = "Student")]
    public async Task<ActionResult<ReadAssignmentDto>> CreateAssignmentDelivery(Guid subjectId, Guid assignmentId,
      CreateAssignmentDeliveryDto deliveryDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }

      var userIdAsString = User.FindFirst(ClaimTypes.NameIdentifier).Value;
      if (!Guid.TryParse(userIdAsString, out var requesterUserId))
      {
        return BadRequest("Invalid User Id");
      }

      return Ok(await _assignmentService.CreateAssignmentDelivery(tenantId, subjectId, assignmentId, requesterUserId, deliveryDto));
    }
  }
}
