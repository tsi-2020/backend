﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.Notice.Request;
using Udelar.Common.Dtos.Notice.Response;
using Udelar.Common.Exceptions;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  public class NoticeController : ControllerBase
  {
    private readonly INoticeService _noticeService;

    public NoticeController(INoticeService iNoticeService)
    {
      _noticeService = iNoticeService;
    }

    /// <summary>
    /// [UdelarAdmin,UniversityAdmin]
    /// </summary>
    [HttpPost("/api/Universities/{universityId}/Subjects/{subjectId}/Notices")]
    [Authorize(Roles = "UdelarAdmin,UniversityAdmin")]
    public async Task<IActionResult> CreateNotice(CreateNoticeDto createNoticeDto, Guid universityId, Guid subjectId)
    {
      var tenantId = User.FindFirstValue("Tenant");
      if (!string.IsNullOrEmpty(tenantId) && !tenantId.Equals(universityId.ToString()))
      {
        throw new AuthorizationException("Permisos insuficientes.");
      }

      await _noticeService.CreateNotice(createNoticeDto, universityId, subjectId);
      return Ok();
    }


    /// <summary>
    /// [UdelarAdmin,UniversityAdmin]
    /// </summary>
    [HttpPost("/api/Universities/{universityId}/Notices")]
    [Authorize(Roles = "UdelarAdmin,UniversityAdmin")]
    public async Task<IActionResult> CreateUniversityNotice(CreateNoticeDto createNoticeDto, Guid universityId)
    {
      var tenantId = User.FindFirstValue("Tenant");
      if (!string.IsNullOrEmpty(tenantId) && !tenantId.Equals(universityId.ToString()))
      {
        throw new AuthorizationException("Permisos insuficientes.");
      }

      await _noticeService.CreateNotice(createNoticeDto, universityId);
      return Ok();
    }

    /// <summary>
    /// [Student,Professor]
    /// </summary>
    [HttpGet("/api/Universities/{universityId}/Subjects/{subjectId}/Notices")]
    [Authorize(Roles = "Student,Professor")]
    public async Task<ActionResult<List<ReadNoticeDto>>> ListNotices(Guid universityId, Guid subjectId)
    {
      var tenantId = User.FindFirstValue("Tenant");
      if (!string.IsNullOrEmpty(tenantId) && !tenantId.Equals(universityId.ToString()))
      {
        throw new AuthorizationException("Permisos insuficientes.");
      }
      return Ok(await _noticeService.ListNotices(universityId, subjectId));
    }


    [HttpGet("/api/Universities/{universityId}/Notices")]
    public async Task<ActionResult<List<ReadNoticeDto>>> ListNotices(Guid universityId)
    {
      return Ok(await _noticeService.ListNotices(universityId));
    }

    [HttpGet("/api/Notices/{noticeId}")]
    public async Task<ActionResult<ReadNoticeDto>> ReadNotice(Guid noticeId)
    {
      return Ok(await _noticeService.ReadNotice(noticeId));
    }
  }
}
