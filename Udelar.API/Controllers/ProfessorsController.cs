﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Udelar.Common.Dtos.User.Request;
using Udelar.Common.Dtos.User.Response;
using Udelar.Model;
using Udelar.Services;

namespace Udelar.APi.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  public class ProfessorsController : ControllerBase
  {
    private readonly IUserService _userService;

    public ProfessorsController(IUserService userService)
    {
      _userService = userService;
    }

    /// <summary>
    /// [UniversityAdmin]
    /// </summary>
    [Authorize(Roles = "UniversityAdmin")]
    [HttpPost]
    public async Task<ActionResult<ReadUserDto>> CreateProfessor(CreateUserDto createUserDto)
    {
      var tenantAsString = User.FindFirstValue("Tenant");
      if (!Guid.TryParse(tenantAsString, out var tenantId))
      {
        return BadRequest("Invalid Tenant Id");
      }
      var createdProfessor = await _userService.CreateUser(createUserDto, new List<Role> { Role.Professor }, tenantId);
      return StatusCode(201, createdProfessor);
    }
  }
}
