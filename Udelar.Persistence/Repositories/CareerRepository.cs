﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udelar.Common.Exceptions;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public class CareerRepository : ICareerRepository
  {
    private readonly DataContext _context;

    public CareerRepository(DataContext context)
    {
      _context = context;
    }

    public async Task AddCareerToUniversity(Guid universityId, Career career)
    {
      var university = await _context.Universities
        .Include(u => u.Careers)
        .FirstOrDefaultAsync(u => u.Id == universityId);
      if (university == default)
      {
        throw new EntityNotFoundException("No se encontro la facultad");
      }

      university.Careers.Add(career);

      await _context.SaveChangesAsync();
    }

    public async Task<List<Career>> FindUniversityCareers(Guid universityId, string careerName)
    {
      var university = await _context.Universities
        .FirstOrDefaultAsync(u => u.Id == universityId);
      if (university == default)
      {
        throw new EntityNotFoundException("No se encontro la facultad");
      }

      if (String.IsNullOrEmpty(careerName))
      {
        return await _context.Careers
        .Include(c => c.University)
        .Where(c => c.University.Id == universityId)
        .ToListAsync();
      }
      else
      {
        return await _context.Careers
        .Include(c => c.University)
        .Where(c => c.University.Id == universityId && c.Name.ToLower().Contains(careerName.ToLower()))
        .ToListAsync();
      }

    }

    public async Task<Career> FindUniversityCareerById(Guid universityId, Guid careerId)
    {
      var career = await _context.Careers
        .Include(c => c.University)
        .FirstOrDefaultAsync(c => c.Id == careerId && c.University.Id == universityId);
      if (career == default)
      {
        var university = await _context.Universities
          .FirstOrDefaultAsync(u => u.Id == universityId);
        if (university == default)
        {
          throw new EntityNotFoundException("No se encontro la facultad");
        }

        throw new EntityNotFoundException("No se encontro la carrera en la facultad");
      }

      return career;
    }

    public async Task UpdateChanges()
    {
      await _context.SaveChangesAsync();
    }

    public async Task RemoveUniversityCareer(Guid universityId, Guid careerId)
    {
      var career = await _context.Careers
        .Include(c => c.University)
        .FirstOrDefaultAsync(c => c.Id == careerId && c.University.Id == universityId);
      if (career == default)
      {
        var university = await _context.Universities
          .FirstOrDefaultAsync(u => u.Id == universityId);
        if (university == default)
        {
          throw new EntityNotFoundException("No se encontro la facultad");
        }

        throw new EntityNotFoundException("No se encontro la carrera en la facultad");
      }

      career.DeletedOn = DateTime.Now;
      await _context.SaveChangesAsync();
    }

    public async Task RemoveSubjectCareer(Guid universityId, Guid careerId, Guid subjectId)
    {

      var career = await _context.Careers
        .Include(c => c.University)
        .FirstOrDefaultAsync(c => c.Id == careerId && c.University.Id == universityId);

      if (career == default)
      {
        var university = await _context.Universities
          .FirstOrDefaultAsync(u => u.Id == universityId);
        if (university == default)
        {
          throw new EntityNotFoundException("No se encontro la facultad");
        }

        throw new EntityNotFoundException("No se encontro  la carrera en la facultad");
      }

      var subject = career.CareerSubjects.FirstOrDefault(s => s.Subject.Id == subjectId);

      //Controlo que el subject ya no esté eliminado
      if (subject == default)
      {

        throw new EntityNotFoundException("La asignatura no fue encontrada");

      }

      subject.DeletedOn = DateTime.Now;

      await _context.SaveChangesAsync();

    }

  }
}
