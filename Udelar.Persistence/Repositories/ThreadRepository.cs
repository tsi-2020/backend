﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Entities;
using Udelar.Common.Dtos.Thread.Request;
using Udelar.Model.Mongo;

namespace Udelar.Persistence.Repositories
{
  public class ThreadRepository : IThreadRepository
  {

    public async Task<ForumThread> Create(CreateThreadDto createThreadDto, Forum forum, Guid tenantId)
    {
      var thread = new ForumThread { Forum = forum, Title = createThreadDto.Title, TenantId = tenantId };
      await thread.SaveAsync();
      await forum.ForumThreads.AddAsync(thread);
      return thread;
    }

    public Task Save(ForumThread thread)
    {
      return DB.SaveAsync(thread);
    }

    public async Task<Tuple<long, List<ForumThread>>> List(Guid tenantId, Forum forum, int page, int limit)
    {
      var count = await forum.ForumThreads.ChildrenCountAsync();

      var threads = await forum.ForumThreads
        .ChildrenFluent()
        .Match(f => f.DeletedOn == null && f.TenantId == tenantId)
        .Skip((limit * page) - limit)
        .Limit(limit)
        .ToListAsync();

      return new Tuple<long, List<ForumThread>>(count, threads);
    }

    public Task<ForumThread> GetById(string threadId, Guid tenantId)
    {
      return DB.Find<ForumThread>()
        .Match(a => a.DeletedOn == null && a.ID == threadId && a.TenantId == tenantId)
        .ExecuteSingleAsync();
    }

    public async Task<Tuple<long, List<Message>>> GetThreadMessages(ForumThread thread, int page, int limit)
    {
      var count = await thread.Messages.ChildrenCountAsync();

      var messages = await thread.Messages
        .ChildrenFluent()
        .Match(f => f.DeletedOn == null)
        .Skip(limit * page - limit)
        .Limit(limit)
        .ToListAsync();

      return new Tuple<long, List<Message>>(count, messages);
    }
  }
}
