﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Model.Poll;

namespace Udelar.Persistence.Repositories
{
  public interface IPollRepository
  {
    Task<UdelarPoll> CreateUdelarPoll(UdelarPoll poll);
    Task<List<UdelarPoll>> ReadUdelarPolls();
    Task<UniversityPoll> CreateUniversityPoll(UniversityPoll poll);
    Task<List<UniversityPoll>> ReadUniversityPolls(Guid id);
    Task<Poll> FindUdelarPollById(Guid pollId);
    Task<Poll> FindUniversityPollById(Guid pollId, Guid universityId);
    Task<PollResponse> CreatePollResponse(PollResponse pollResponse);
    Task<PollResponse> FindUserPollResponse(Guid pollId, Guid userId);
    Task<SubjectPoll> CreateSubjectPoll(SubjectPoll poll);
    Task<Poll> FindSubjectPoll(Guid pollId, Guid subjectId);
    Task<List<SubjectPoll>> ReadSubjectPolls(Guid subjectId);
    Task<Poll> FindUniversityOrUdelarPoll(Guid pollId);
    Task<Tuple<List<SubjectPoll>, List<UniversityPoll>>> FindSubjectPublishedPolls(Guid subjectId);
    Task<List<UdelarPoll>> FindUniversityPublishedPolls(Guid universityId);
    Task<Poll> FindPollById(Guid pollId);
    Task<Tuple<int, List<PollResponse>>> GetPollAnswers(Guid pollId, int page, int limit);
  }
}
