﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public interface IUserRepository
  {
    Task<User> FindUserInUniversityById(Guid userId, Guid tenantId);
    Task<User> CreateUser(User user);
    Task SaveChanges(User user);
    Task<User> FindByCI(int ci, Guid tenantId);
    Task<User> GetUserById(Guid tenantId, Guid userId);
    Task<User> FindByRefreshToken(string refreshToken);
    Task<User> FindByResetToken(string token);
    Task<Tuple<int, List<User>>> GetUsersByRole(Guid tenantId, int page, int limit, Role parse, string fullNameFilter);
    Task<Tuple<int, List<User>>> GetUsers(Guid tenantId, int page, int limit, string fullNameFilter);
    Task<User> FindByEmail(string email, Guid universityId, Guid userId);
    Task<User> FindUserEnrollments(Guid userId);
    Task<List<User>> GetAllWithUniversityAndRoles();
  }
}
