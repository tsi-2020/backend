﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public interface ISubjectRepository
  {
    Task CreateSubject(Subject subject);
    Task<List<Subject>> FindAllSubjects(string subjectName, Guid tenantId);
    Task<Subject> FindSubjectById(Guid tenantId, Guid subjectId);
    Task<Subject> FindByIdAndUniversity(Guid subjectId, Guid tenantId);
    Task RemoveSubject(Subject subject);
    Task<List<Subject>> FindSubjectByIdWithCareers(Guid universityId, List<Guid> subjectIds);
    Task SaveChanges(Subject subject);
    Task<Subject> FindSubjectWithEnrollments(Guid tenantId, Guid subjectId);
    Task<Subject> FindSubjectWithEnrollmentsAndComponents(Guid tenantId, Guid subjectId);

    Task<List<Subject>> ListSubjectCareerById(Guid careerId);
    Task<List<Subject>> FindAllSubjectsWithEnrollmentsAndUniversity();
    Task<List<Subject>> FindAllSubjectsWithEnrollmentsByUniversity(Guid tenantId);
  }
}
