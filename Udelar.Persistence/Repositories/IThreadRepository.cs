﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Thread.Request;
using Udelar.Model.Mongo;

namespace Udelar.Persistence.Repositories
{
  public interface IThreadRepository
  {
    public Task Save(ForumThread thread);

    public Task<Tuple<long, List<ForumThread>>> List(Guid tenantId, Forum forum, int page, int limit);

    public Task<ForumThread> GetById(string threadId, Guid tenantId);

    public Task<Tuple<long, List<Message>>> GetThreadMessages(ForumThread thread, int page, int limit);
    Task<ForumThread> Create(CreateThreadDto createThreadDto, Forum forum, Guid tenantId);
  }
}
