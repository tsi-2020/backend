﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udelar.Common.Exceptions;
using Udelar.Model.Poll;
using Udelar.Model.Poll.Answers;
using Udelar.Model.Poll.Questions;

namespace Udelar.Persistence.Repositories
{
  public class PollRepository : IPollRepository
  {
    private readonly DataContext _context;

    public PollRepository(DataContext context)
    {
      _context = context;
    }

    public async Task<UdelarPoll> CreateUdelarPoll(UdelarPoll poll)
    {
      await _context.UdelarPolls.AddAsync(poll);
      await _context.SaveChangesAsync();
      return poll;
    }

    public async Task<UniversityPoll> CreateUniversityPoll(UniversityPoll poll)
    {
      await _context.UniversityPolls.AddAsync(poll);
      await _context.SaveChangesAsync();
      return poll;
    }

    public async Task<SubjectPoll> CreateSubjectPoll(SubjectPoll poll)
    {
      await _context.SubjectPolls.AddAsync(poll);
      await _context.SaveChangesAsync();
      return poll;
    }

    public async Task<Poll> FindSubjectPoll(Guid pollId, Guid subjectId)
    {
      var poll = await _context.SubjectPolls
        .Include(p => p.Subject)
        .Include(p => p.Questions)
        .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
        .Include(p => p.SubjectPollPublications)
        .ThenInclude(p => p.Poll)
        .FirstOrDefaultAsync(p => p.Id == pollId && p.Subject.Id == subjectId);

      if (poll != default)
      {
        return poll;
      }

      throw new EntityNotFoundException("No se encontro la encuesta");
    }

    public Task<List<SubjectPoll>> ReadSubjectPolls(Guid subjectId)
    {
      return _context.SubjectPolls
        .Include(poll => poll.Subject)
        .Where(u => u.Subject.Id == subjectId)
        .ToListAsync();
    }


    public Task<List<UdelarPoll>> ReadUdelarPolls()
    {
      return _context.UdelarPolls.ToListAsync();
    }

    public Task<List<UniversityPoll>> ReadUniversityPolls(Guid id)
    {
      return _context.UniversityPolls
        .Include(poll => poll.University)
        .Where(u => u.University.Id == id)
        .ToListAsync();
    }


    public async Task<Poll> FindUniversityOrUdelarPoll(Guid pollId)
    {
      Poll poll = await _context.UdelarPolls
                    .Include(p => p.Questions)
                    .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
                    .FirstOrDefaultAsync(p => p.Id == pollId) ??
                  (Poll)await _context.UniversityPolls
                    .Include(p => p.University)
                    .Include(p => p.Questions)
                    .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
                    .FirstOrDefaultAsync();
      return poll;
    }

    public async Task<Tuple<List<SubjectPoll>, List<UniversityPoll>>> FindSubjectPublishedPolls(Guid subjectId)
    {
      var subjectPolls = await _context.SubjectPolls
        .Where(sp => sp.Subject.Id.Equals(subjectId))
        .Include(p => p.PollResponses)
        .ThenInclude(p => p.User)
        .Include(p => p.SubjectPollPublications)
        .ThenInclude(s => s.Poll)
        .ThenInclude(p => p.Questions)
        .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
        .ToListAsync();

      var universityPolls = await _context.UniversityPolls
        .Where(sp => sp.SubjectPollPublications.Any(sp => sp.Subject.Id.Equals(subjectId)))
        .Include(p => p.PollResponses)
        .ThenInclude(p => p.User)
        .Include(p => p.SubjectPollPublications)
        .ThenInclude(s => s.Poll)
        .ThenInclude(p => p.Questions)
        .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
        .Include(s => s.SubjectPollPublications)
        .ThenInclude(s => s.Subject)
        .ToListAsync();

      return new Tuple<List<SubjectPoll>, List<UniversityPoll>>(subjectPolls, universityPolls);
    }

    public Task<List<UdelarPoll>> FindUniversityPublishedPolls(Guid universityId)
    {
      return _context.UdelarPolls
        .Where(sp => sp.UniversityPollPublications.Any(u => u.University.Id.Equals(universityId)))
        .Include(p => p.PollResponses)
        .ThenInclude(p => p.User)
        .Include(p => p.UniversityPollPublications)
        .ThenInclude(s => s.Poll)
        .ThenInclude(p => p.Questions)
        .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
        .Include(p => p.UniversityPollPublications)
        .ThenInclude(s => s.University)
        .ToListAsync();
    }

    public async Task<Poll> FindPollById(Guid pollId)
    {
      Poll poll = await _context.UdelarPolls
                    .Include(p => p.PollResponses)
                    .ThenInclude(p => p.User)
                    .Include(p => p.PollResponses)
                    .ThenInclude(p => p.User)
                    .Include(p => p.Questions)
                    .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
                    .FirstOrDefaultAsync(p => p.Id == pollId) ??
                  (Poll)await _context.UniversityPolls
                    .Include(p => p.PollResponses)
                    .ThenInclude(p => p.User)
                    .Include(p => p.Questions)
                    .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
                    .FirstOrDefaultAsync(p => p.Id == pollId) ??
                  await _context.SubjectPolls
                    .Include(p => p.PollResponses)
                    .ThenInclude(p => p.User)
                    .Include(p => p.Questions)
                    .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
                    .FirstOrDefaultAsync(p => p.Id == pollId);

      return poll;
    }

    public async Task<Tuple<int, List<PollResponse>>> GetPollAnswers(Guid pollId, int page, int limit)
    {
      var count = await
        _context.PollResponse.Where(p => p.Poll.Id.Equals(pollId))
          .CountAsync();
      var result = await _context.PollResponse.Where(p => p.Poll.Id.Equals(pollId))
        .Include(p => p.Poll)
        .Include(p => p.Answers)
        .ThenInclude(pa => (pa as MultipleChoicePollAnswer).Choices)
        .ThenInclude(choice => choice.Choice)
        .Include(p => p.Answers)
        .ThenInclude(pa => (pa as MultipleChoicePollAnswer).MultipleChoicePollQuestion)
        .Include(p => p.Answers)
        .ThenInclude(pa => (pa as OpenPollAnswer).OpenPollQuestion)
        .Skip((limit * page) - limit)
        .Take(limit)
        .OrderByDescending(response => response.CreatedOn)
        .ToListAsync();

      return new Tuple<int, List<PollResponse>>(count, result);
    }

    public async Task<Poll> FindUdelarPollById(Guid pollId)
    {
      var poll = await _context.UdelarPolls
        .Include(p => p.UniversityPollPublications)
        .ThenInclude(p => p.University)
        .Include(p => p.SubjectPollPublications)
        .ThenInclude(p => p.Subject)
        .Include(p => p.Questions)
        .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
        .FirstOrDefaultAsync(p => p.Id == pollId);

      if (poll != default)
      {
        return poll;
      }

      throw new EntityNotFoundException("No se encontro la encuesta");
    }


    public async Task<Poll> FindUniversityPollById(Guid pollId, Guid universityId)
    {
      var poll = await _context.UniversityPolls
        .Include(p => p.UniversityPollPublications)
        .ThenInclude(p => p.University)
        .Include(p => p.SubjectPollPublications)
        .ThenInclude(p => p.Subject)
        .Include(p => p.University)
        .Include(p => p.Questions)
        .ThenInclude(c => (c as MultipleChoicePollQuestion).Choices)
        .FirstOrDefaultAsync(p => p.Id == pollId && p.University.Id == universityId);

      if (poll != default)
      {
        return poll;
      }

      if (default == await _context.Universities.FirstOrDefaultAsync(u => u.Id == universityId))
      {
        throw new EntityNotFoundException("No se encontro la universidad");
      }

      throw new EntityNotFoundException("No se encontro la encuesta");
    }

    public async Task<PollResponse> CreatePollResponse(PollResponse pollResponse)
    {
      await _context.PollResponse.AddAsync(pollResponse);
      await _context.SaveChangesAsync();
      return pollResponse;
    }

    public Task<PollResponse> FindUserPollResponse(Guid pollId, Guid userId)
    {
      return _context.PollResponse.Include(pr => pr.User).Include(pr => pr.Poll)
        .FirstOrDefaultAsync(response => response.User.Id.Equals(userId) && response.Poll.Id.Equals(pollId));
    }
  }
}
