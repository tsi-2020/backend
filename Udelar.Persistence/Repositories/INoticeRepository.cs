﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public interface INoticeRepository
  {
    Task<Notice> CreateNotice(Notice notice);
    Task SaveChanges(Notice notice);
    Task<List<Notice>> ListNotices(Guid subjectId);
    Task<List<Notice>> ListUniversityNotices(Guid universityId);
    Task<Notice> GetNoticeById(Guid noticeId);
  }
}
