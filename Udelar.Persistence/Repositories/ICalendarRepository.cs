﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Calendar.Request;
using Udelar.Model.Mongo;

namespace Udelar.Persistence.Repositories
{
  public interface ICalendarRepository
  {
    public Task Save(Calendar calendar);

    public Task<List<Calendar>> List(Guid tenantId);
    Task<List<Calendar>> FilterByDates(Guid toString, Guid subjectId, FilterEventsDto filterEventsDto);
  }
}
