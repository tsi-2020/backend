﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Model.Component;

namespace Udelar.Persistence.Repositories
{
  public class SubjectRepository : ISubjectRepository
  {
    private readonly DataContext _context;

    public SubjectRepository(DataContext context)
    {
      _context = context;
    }

    public async Task CreateSubject(Subject subject)
    {
      await _context.Subjects.AddAsync(subject);
      await _context.SaveChangesAsync();
    }


    public async Task<Subject> FindByIdAndUniversity(Guid subjectId, Guid tenantId)
    {
      var subject = await _context.Subjects
        .Include(s => s.University)
        .FirstOrDefaultAsync(s =>
          s.Id == subjectId && s.University.Id == tenantId);
      if (subject != default)
      {
        return subject;
      }

      if (default == await _context.Universities.FirstOrDefaultAsync(u => u.Id == tenantId))
      {
        throw new EntityNotFoundException("No se encontro la universidad");
      }
      throw new EntityNotFoundException("No se encontro el curso");

    }

    public async Task RemoveSubject(Subject subject)
    {
      subject.DeletedOn = DateTime.Now;
      await _context.SaveChangesAsync();
    }

    public Task<List<Subject>> FindSubjectByIdWithCareers(Guid universityId, List<Guid> subjectIds)
    {
      return _context.Subjects
        .Include(s => s.University)
        .Where(s => s.University.Id == universityId && subjectIds.Contains(s.Id))
        .Include(s => s.CareerSubjects)
        .ThenInclude(cs => cs.Career)
        .ToListAsync();
    }

    public Task SaveChanges(Subject subject)
    {
      _context.Subjects.Update(subject);
      return _context.SaveChangesAsync();
    }

    public async Task<Subject> FindSubjectWithEnrollments(Guid tenantId, Guid subjectId)
    {
      var subject = await _context.Subjects
        .Include(s => s.University)
        .Where(s => s.University.Id == tenantId && s.Id == subjectId)
        .Include(s => s.Enrollments)
        .ThenInclude(i => i.User)
        .FirstOrDefaultAsync();
      if (subject == default)
      {
        throw new EntityNotFoundException("No se encontro el curso");
      }

      return subject;
    }

    public async Task<Subject> FindSubjectWithEnrollmentsAndComponents(Guid tenantId, Guid subjectId)
    {
      var subject = await _context.Subjects
        .Include(s => s.University)
        .Where(s => s.University.Id == tenantId && s.Id == subjectId)
        .Include(s => s.Enrollments)
        .ThenInclude(i => i.User)
        .Include(s => s.Components)
        .ThenInclude(e => e.Components)
        .ThenInclude(c => (c as AssignmentComponent).Assignment)
        .Include(s => s.Components)
        .ThenInclude(e => e.Components)
        .ThenInclude(c => (c as PollComponent).Poll)
        .Include(s => s.Components)
        .ThenInclude(e => e.Components)
        .ThenInclude(c => (c as QuizComponent).Quiz)
        .FirstOrDefaultAsync();

      if (subject == default)
      {
        throw new EntityNotFoundException("No se encontro el curso");
      }

      return subject;
    }

    public async Task<List<Subject>> FindAllSubjects(string subjectName, Guid tenantId)
    {
      return await _context.Subjects.Include(s => s.University)
        .Where(s => s.University.Id == tenantId && s.Name.ToLower().Contains(subjectName.ToLower()))
        .ToListAsync();
    }

    public async Task<Subject> FindSubjectById(Guid tenantId, Guid subjectId)
    {
      var subject = await _context.Subjects
        .Include(s => s.University)
        .Where(s => s.University.Id == tenantId && s.Id == subjectId)
        .Include(s => s.Components)
        .ThenInclude(e => e.Components)
        .ThenInclude(c => (c as AssignmentComponent).Assignment)
        .Include(s => s.Components)
        .ThenInclude(e => e.Components)
        .ThenInclude(c => (c as PollComponent).Poll)
        .Include(s => s.Components)
        .ThenInclude(e => e.Components)
        .ThenInclude(c => (c as QuizComponent).Quiz)
        .FirstOrDefaultAsync();

      if (subject == default)
      {
        throw new EntityNotFoundException("No se encontro el curso");
      }

      return subject;
    }

    public async Task<List<Subject>> ListSubjectCareerById(Guid careerId)
    {

      var subjects = await _context.Subjects
        .Include(s => s.CareerSubjects).ThenInclude(cs => cs.Career)
        .Where(s => s.CareerSubjects.Any(c => c.Career.Id == careerId)).ToListAsync();

      return subjects;

    }

    public async Task<List<Subject>> FindAllSubjectsWithEnrollmentsAndUniversity()
    {
      return await _context.Subjects
        .Include(s => s.Enrollments)
        .Include(s => s.University)
        .ToListAsync();
    }

    public async Task<List<Subject>> FindAllSubjectsWithEnrollmentsByUniversity(Guid tenantId)
    {
      return await _context.Subjects
        .Include(s => s.Enrollments)
        .Include(s => s.University)
        .Where(s => s.University.Id == tenantId)
        .ToListAsync();
    }
  }
}
