﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public interface ISubjectTemplateRepository
  {
    Task AddSubjectTemplate(SubjectTemplate subjectTemplate);
    Task<List<SubjectTemplate>> GetAllSubjectTemplates(Guid universityId);
    Task<SubjectTemplate> GetSubjectTemplateById(Guid universityId, Guid templateId);
  }
}
