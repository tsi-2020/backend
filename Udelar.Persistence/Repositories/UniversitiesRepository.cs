﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udelar.Common.Exceptions;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public class UniversitiesRepository : IUniversitiesRepository
  {
    private readonly DataContext _context;

    public UniversitiesRepository(DataContext context)
    {
      _context = context;
    }

    public async Task AddUniversity(University university)
    {
      await _context.Universities.AddAsync(university);
      await _context.SaveChangesAsync();
    }

    public Task<List<University>> FindUniversityAll()
    {
      return _context.Universities.ToListAsync();
    }

    public async Task<University> FindUniversityById(Guid id)
    {
      var university = await _context.Universities.Include(u => u.UniversityAdministrators)
        .FirstOrDefaultAsync(u => u.Id == id);
      if (university == default)
      {
        throw new EntityNotFoundException("No se encontro la facultad");
      }

      return university;
    }

    public Task UpdateUniversity(University university)
    {
      return _context.SaveChangesAsync();
    }

    public async Task RemoveUniversity(Guid id)
    {
      var university = await _context.Universities.FirstOrDefaultAsync(u => u.Id == id);
      if (university == default)
      {
        throw new EntityNotFoundException("No se encontro la facultad");
      }

      university.DeletedOn = DateTime.Now;
      await _context.SaveChangesAsync();
    }

    public Task<University> FindByPath(string path)
    {
      return _context.Universities.FirstOrDefaultAsync(university => university.AccessPath.ToUpper() == path.ToUpper());
    }

    public Task Save(University university)
    {
      return _context.SaveChangesAsync();
    }
  }
}
