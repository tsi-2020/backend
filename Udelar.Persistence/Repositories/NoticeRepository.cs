﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public class NoticeRepository : INoticeRepository
  {
    private readonly DataContext _ctx;

    public NoticeRepository(DataContext dataContext)
    {
      _ctx = dataContext;
    }

    public async Task<Notice> CreateNotice(Notice notice)
    {
      await _ctx.Notices.AddAsync(notice);
      await _ctx.SaveChangesAsync();
      return notice;
    }

    public Task SaveChanges(Notice notice)
    {
      return _ctx.SaveChangesAsync();
    }

    public Task<List<Notice>> ListNotices(Guid subjectId)
    {
      return _ctx.Notices
        .Include(notice => notice.Subject)
        .Where(notice => notice.Subject.Id == subjectId)
        .OrderByDescending(notice => notice.CreatedOn)
        .ToListAsync();
    }

    public Task<List<Notice>> ListUniversityNotices(Guid universityId)
    {
      return _ctx.Notices.Include(notice => notice.University)
        .Where(notice => notice.University.Id == universityId)
        .OrderByDescending(notice => notice.CreatedOn)
        .ToListAsync();
    }

    public Task<Notice> GetNoticeById(Guid noticeId)
    {
      return _ctx.Notices
        .Where(notice => notice.Id == noticeId)
        .FirstOrDefaultAsync();
    }
  }
}
