﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Entities;
using Udelar.Common.Dtos.Message.Request;
using Udelar.Model.Mongo;

namespace Udelar.Persistence.Repositories
{
  public class MessageRepository : IMessageRepository
  {
    public async Task<Message> Create(CreateMessageDto createMessageDto, string userName, Guid userId, ForumThread forumThread,
      Guid tenantId)
    {
      var message = new Message
      {
        Text = createMessageDto.Text,
        UserId = userId,
        FullName = userName,
        ForumThread = forumThread,
        TenantId = tenantId
      };
      await message.SaveAsync();
      await forumThread.Messages.AddAsync(message);

      return message;
    }

    public Task Save(Message message)
    {
      return DB.SaveAsync(message);
    }

    public Task<Message> GetById(string messageId, Guid tenantId)
    {
      return DB.Find<Message>()
        .Match(a => a.DeletedOn == null && a.ID == messageId && a.TenantId == tenantId)
        .ExecuteSingleAsync();
    }

    public async Task<Tuple<long, List<Message>>> List(ForumThread thread, int page, int limit)
    {
      var count = await thread
        .Messages
        .ChildrenCountAsync();

      var messages = await thread
        .Messages
        .ChildrenFluent()
        .Skip(limit * page - limit)
        .Limit(limit).ToListAsync();
      return new Tuple<long, List<Message>>(count, messages);
    }
  }
}
