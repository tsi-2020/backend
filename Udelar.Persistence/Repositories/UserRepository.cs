﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public class UserRepository : IUserRepository
  {
    private readonly DataContext _context;

    public UserRepository(DataContext dataContext)
    {
      _context = dataContext;
    }

    public Task<User> FindUserInUniversityById(Guid id, Guid tenantId)
    {
      return _context.Users.Include(user => user.University)
        .FirstOrDefaultAsync(user => id == user.Id && user.University.Id == tenantId);
    }

    public async Task<User> CreateUser(User user)
    {
      await _context.Users.AddAsync(user);
      await _context.SaveChangesAsync();
      return user;
    }

    public Task SaveChanges(User user)
    {
      return _context.SaveChangesAsync();
    }

    public Task<User> FindByCI(int ci, Guid universityId)
    {
      return _context.Users
        .Include(user => user.University)
        .Include(user => user.Roles)
        .FirstOrDefaultAsync(user => user.CI == ci && user.University.Id == universityId);
    }

    public async Task<User> GetUserById(Guid tenantId, Guid userId)
    {
      return await _context.Users
        .Include(u => u.University)
        .Where(u => u.University.Id == tenantId && u.Id == userId)
        .Include(u => u.Roles)
        .FirstOrDefaultAsync();
    }

    public Task<User> FindByRefreshToken(string refreshToken)
    {
      return _context.Users
        .Include(user => user.University)
        .Include(user => user.Roles)
        .FirstOrDefaultAsync(user => user.RefreshToken == refreshToken);
    }

    public Task<User> FindByResetToken(string token)
    {
      return _context.Users
        .FirstOrDefaultAsync(user => user.ResetPasswordCode == token);
    }

    public async Task<Tuple<int, List<User>>> GetUsersByRole(Guid tenantId, int page, int limit, Role role,
      string fullNameFilter)
    {
      var count = await _context.Users.Include(u => u.University)
        .Where(u => u.University.Id == tenantId)
        .Include(u => u.Roles)
        .Where(u => u.Roles.Any(r => r.Role == role))
        .Where(u => (u.FirstName + u.LastName).ToLower().Contains(fullNameFilter.ToLower()))
        .CountAsync();
      var users = await _context.Users
        .Include(u => u.University)
        .Where(u => u.University.Id == tenantId)
        .Include(u => u.Roles)
        .Where(u => u.Roles.Any(r => r.Role == role))
        .Where(u => (u.FirstName + " " + u.LastName).ToLower().Contains(fullNameFilter.ToLower()))
        .Skip((limit * page) - limit)
        .Take(limit)
        .ToListAsync();
      return new Tuple<int, List<User>>(count, users);
    }

    public async Task<Tuple<int, List<User>>> GetUsers(Guid tenantId, int page, int limit, string fullNameFilter)
    {
      var count = await _context.Users
        .Include(u => u.University)
        .Include(u => u.Roles)
        .Where(u => (u.FirstName + " " + u.LastName).ToLower().Contains(fullNameFilter.ToLower()))
        .Where(u => u.University.Id == tenantId).CountAsync();
      var users = await _context.Users
        .Include(u => u.University)
        .Include(u => u.Roles)
        .Where(u => u.University.Id == tenantId)
        .Where(u => (u.FirstName + " " + u.LastName).ToLower().Contains(fullNameFilter.ToLower()))
        .Skip((limit * page) - limit)
        .Take(limit)
        .ToListAsync();
      return new Tuple<int, List<User>>(count, users);
    }

    public Task<User> FindByEmail(string email, Guid universityId, Guid userId)
    {
      if (userId == default)
      {
        return _context.Users
          .Include(u => u.University)
          .Where(u => u.University.Id == universityId)
          .Where(u => u.Email.ToLower() == email.ToLower())
          .FirstOrDefaultAsync();
      }

      return _context.Users
        .Include(u => u.University)
        .Where(u => u.Id != userId)
        .Where(u => u.University.Id == universityId)
        .Where(u => u.Email.ToLower() == email.ToLower())
        .FirstOrDefaultAsync();
    }

    public Task<User> FindUserEnrollments(Guid userId)
    {
      return _context.Users.Include(u => u.Inscriptions)
        .ThenInclude(i => i.Subject)
        .Where(u => u.Id.Equals(userId)).FirstOrDefaultAsync();
    }

    public async Task<List<User>> GetAllWithUniversityAndRoles()
    {
      return await _context.Users
        .Include(u => u.University)
        .Include(u => u.Roles)
        .ToListAsync();
    }
  }
}
