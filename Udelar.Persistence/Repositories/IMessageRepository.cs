﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Message.Request;
using Udelar.Model.Mongo;

namespace Udelar.Persistence.Repositories
{
  public interface IMessageRepository
  {
    Task Save(Message message);
    Task<Message> GetById(string messageId, Guid tenantId);
    Task<Tuple<long, List<Message>>> List(ForumThread thread, int page, int limit);

    Task<Message>
      Create(CreateMessageDto createMessageDto, string userFullName, Guid userId, ForumThread thread, Guid tenantId);
  }
}
