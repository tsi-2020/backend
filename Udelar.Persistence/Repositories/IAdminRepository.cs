﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public interface IAdminRepository
  {
    Task<UniversityAdministrator> AddUniversityAdministrator(UniversityAdministrator admin);
    Task<Administrator> FindByEmail(string email);
    Task<UniversityAdministrator> FindByEmailAndUniversityId(string email, Guid universityId);
    Task<UniversityAdministrator> FindById(Guid userId);
    Task<List<UniversityAdministrator>> ListUniversityAdministrators(Guid universityId);
    Task SaveChanges(Administrator administrator);
    Task<Administrator> FindByRefreshToken(string refreshToken);
    Task<UniversityAdministrator> FindByIdAndUniversity(Guid userId, Guid universityId);
  }
}
