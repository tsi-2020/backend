﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udelar.Common.Exceptions;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public class AdminRepository : IAdminRepository
  {
    private readonly DataContext _context;

    public AdminRepository(DataContext context)
    {
      _context = context;
    }

    public async Task<UniversityAdministrator> AddUniversityAdministrator(UniversityAdministrator admin)
    {
      await _context.UniversityAdministrators.AddAsync(admin);
      await _context.SaveChangesAsync();
      return admin;
    }

    public Task<Administrator> FindByEmail(string email)
    {
      return _context.Administrators
        .Include(admin => (admin as UniversityAdministrator).University)
        .FirstOrDefaultAsync(admin => admin.Email == email);
    }

    public Task<UniversityAdministrator> FindByEmailAndUniversityId(string email, Guid universityId)
    {
      return _context.UniversityAdministrators
        .Include(admin => admin.University)
        .FirstOrDefaultAsync(admin => admin.Email == email && admin.University.Id == universityId);
    }

    public Task<UniversityAdministrator> FindById(Guid userId)
    {
      return _context.UniversityAdministrators
        .FirstOrDefaultAsync(admin => admin.Id == userId);
    }

    public Task<List<UniversityAdministrator>> ListUniversityAdministrators(Guid universityId)
    {
      return _context.UniversityAdministrators.Include(admin => admin.University)
        .Where(admin => admin.University.Id == universityId).ToListAsync();
    }

    public Task SaveChanges(Administrator administrator)
    {
      return _context.SaveChangesAsync();
    }

    public Task<Administrator> FindByRefreshToken(string refreshToken)
    {
      return _context.Administrators
        .Include(admin => (admin as UniversityAdministrator).University)
        .FirstOrDefaultAsync(admin => admin.RefreshToken == refreshToken);
    }

    public Task<UniversityAdministrator> FindByIdAndUniversity(Guid userId, Guid universityId)
    {
      return _context.UniversityAdministrators.Include(u => u.University)
        .FirstOrDefaultAsync(admin => admin.Id == userId
                                      && admin.University.Id == universityId);
    }
  }
}
