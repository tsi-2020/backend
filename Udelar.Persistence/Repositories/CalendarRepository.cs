﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Entities;
using Udelar.Common.Dtos.Calendar.Request;
using Udelar.Model.Mongo;

namespace Udelar.Persistence.Repositories
{
  public class CalendarRepository : ICalendarRepository
  {
    public Task Save(Calendar calendar)
    {
      return DB.SaveAsync(calendar);
    }

    public Task<List<Calendar>> List(Guid tenantId)
    {
      return DB.Queryable<Calendar>()
        .Where(f => f.DeletedOn == null && f.TenantId == tenantId)
        .ToListAsync();
    }

    public Task<List<Calendar>> FilterByDates(Guid tenantId, Guid subjectId, FilterEventsDto filterEventsDto)
    {
      return DB.Fluent<Calendar>()
        .Match(f => f.DeletedOn == null
                    && f.TenantId == tenantId
                    && f.StartDate >= filterEventsDto.StartDate
                    && f.EndDate <= filterEventsDto.EndDate)
        .SortBy(c => c.StartDate)
        .ToListAsync();
    }
  }
}
