﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Entities;
using Udelar.Common.Dtos.Forum.Request;
using Udelar.Model.Mongo;

namespace Udelar.Persistence.Repositories
{
  public class ForumRepository : IForumRepository
  {
    public async Task<Forum> CreateForum(CreateForumDto createForumDto, Guid tenantId)
    {
      var forum = new Forum { Name = createForumDto.Name, SubjectId = createForumDto.SubjectId, TenantId = tenantId };
      await forum.SaveAsync();
      return forum;
    }

    public Task<Forum> GetById(string forumId, Guid tenantId)
    {
      return DB.Find<Forum>()
        .Match(a => a.DeletedOn == null && a.ID == forumId && a.TenantId == tenantId)
        .ExecuteSingleAsync();
    }

    public async Task<Tuple<long, List<Forum>>> List(Guid tenantId, int page, int limit)
    {
      var count = await DB.CountAsync<Forum>(forum => forum.TenantId == tenantId);

      var forums = await DB.Fluent<Forum>()
        .Match(f => f.DeletedOn == null && f.TenantId == tenantId)
        .Skip(limit * page - limit)
        .Limit(limit)
        .ToListAsync();

      return new Tuple<long, List<Forum>>(count, forums);
    }

    public Task<List<ForumThread>> GetForumThreads(Forum forum)
    {
      return forum.ForumThreads
        .ChildrenQueryable()
        .Where(f => f.DeletedOn == null)
        .ToListAsync();
    }

    public async Task Save(Forum persistedForum)
    {
      await persistedForum.SaveAsync();
    }
  }
}
