﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Common.Dtos.Forum.Request;
using Udelar.Model.Mongo;

namespace Udelar.Persistence.Repositories
{
  public interface IForumRepository
  {
    Task<Forum> CreateForum(CreateForumDto createForumDto, Guid tenantId);

    Task<Forum> GetById(string forumId, Guid tenantId);

    Task<Tuple<long, List<Forum>>> List(Guid tenantId, int page, int limit);

    Task<List<ForumThread>> GetForumThreads(Forum forum);
    Task Save(Forum persistedForum);
  }
}
