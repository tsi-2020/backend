﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public interface IUniversitiesRepository
  {
    Task AddUniversity(University university);
    Task<List<University>> FindUniversityAll();
    Task<University> FindUniversityById(Guid updateUniversityId);
    Task UpdateUniversity(University university);
    Task RemoveUniversity(Guid id);
    Task<University> FindByPath(string path);
    Task Save(University university);
  }
}
