﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public interface ICareerRepository
  {
    Task AddCareerToUniversity(Guid universityId, Career career);
    Task<List<Career>> FindUniversityCareers(Guid universityId, string careerName);
    Task<Career> FindUniversityCareerById(Guid universityId, Guid careerId);
    Task UpdateChanges();
    Task RemoveUniversityCareer(Guid universityId, Guid careerId);
    Task RemoveSubjectCareer(Guid universityId, Guid careerId, Guid subjectId);
  }
}
