﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udelar.Common.Exceptions;
using Udelar.Model;

namespace Udelar.Persistence.Repositories
{
  public class SubjectTemplateRepository : ISubjectTemplateRepository
  {
    private readonly DataContext _context;

    public SubjectTemplateRepository(DataContext context)
    {
      _context = context;
    }

    public async Task AddSubjectTemplate(SubjectTemplate subjectTemplate)
    {
      await _context.SubjectTemplates.AddAsync(subjectTemplate);
      await _context.SaveChangesAsync();
    }

    public async Task<List<SubjectTemplate>> GetAllSubjectTemplates(Guid universityId)
    {
      var university = await _context.Universities.FirstOrDefaultAsync(u => u.Id == universityId);
      if (university == default)
      {
        throw new ConflictException("No se encontro la facultad");
      }

      return await _context.SubjectTemplates
        .Include(temp => temp.University)
        .Where(u => u.University.Id == universityId)
        .ToListAsync();
    }

    public async Task<SubjectTemplate> GetSubjectTemplateById(Guid universityId, Guid templateId)
    {
      var university = await _context.Universities.FirstOrDefaultAsync(u => u.Id == universityId);
      if (university == default)
      {
        throw new ConflictException("No se encontro la facultad");
      }

      var template = await _context.SubjectTemplates
        .Include(t => t.University)
        .Include(t => t.Components)
        .ThenInclude(c => c.Components)
        .FirstOrDefaultAsync(t => t.Id == templateId && t.University.Id == universityId);
      if (template == default)
      {
        throw new ConflictException("No se encontro la platilla");
      }

      return template;
    }
  }
}
