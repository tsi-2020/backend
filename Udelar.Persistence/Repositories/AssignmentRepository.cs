﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udelar.Common.Exceptions;
using Udelar.Model;
using Udelar.Model.Component;

namespace Udelar.Persistence.Repositories
{
  public class AssignmentRepository : IAssignmentRepository
  {
    private readonly DataContext _context;

    public AssignmentRepository(DataContext context)
    {
      _context = context;
    }

    public Task<AssignmentComponent> GetAssignmentComponentByAssignmentId(Guid assignmentId)
    {
      return _context.AssignmentComponents
        .Include(ac => ac.Assignment)
        .FirstOrDefaultAsync(ac => ac.Assignment.Id.Equals(assignmentId));
    }

    public async Task<Assignment> FindAssignmentById(Guid tenantId, Guid subjectId, Guid assignmentId)
    {
      var assignment = await _context.Assignments
        .Include(a => a.Subject)
        .ThenInclude(s => s.University)
        .FirstOrDefaultAsync(a =>
          a.Id == assignmentId && a.Subject.Id == subjectId && a.Subject.University.Id == tenantId);

      if (assignment == default)
      {
        throw new EntityNotFoundException("El trabajo entregable no existe");
      }

      return assignment;
    }

    public async Task<Assignment> FindAssignmentByIdWithDeliveries(Guid tenantId, Guid subjectId, Guid assignmentId)
    {
      var assignment = await _context.Assignments
        .Include(a => a.DeliveredAssignments)
        .ThenInclude(d => (d as IndividualDelivery).User)
        .Include(a => a.Subject)
        .ThenInclude(s => s.University)
        .FirstOrDefaultAsync(a =>
          a.Id == assignmentId && a.Subject.Id == subjectId && a.Subject.University.Id == tenantId);

      if (assignment == default)
      {
        throw new EntityNotFoundException("El trabajo entregable no existe");
      }

      return assignment;
    }

    public async Task SaveChanges()
    {
      await _context.SaveChangesAsync();
    }
  }
}
