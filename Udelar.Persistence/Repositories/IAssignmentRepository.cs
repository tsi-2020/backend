﻿using System;
using System.Threading.Tasks;
using Udelar.Model;
using Udelar.Model.Component;

namespace Udelar.Persistence.Repositories
{
  public interface IAssignmentRepository
  {
    Task<Assignment> FindAssignmentById(Guid tenantId, Guid subjectId, Guid assignmentId);
    Task<Assignment> FindAssignmentByIdWithDeliveries(Guid tenantId, Guid subjectId, Guid assignmentId);
    Task SaveChanges();
    Task<AssignmentComponent> GetAssignmentComponentByAssignmentId(Guid assignmentId);
  }
}
