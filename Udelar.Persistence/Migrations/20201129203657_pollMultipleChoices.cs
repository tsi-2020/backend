﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class pollMultipleChoices : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_PollsChoices_PollAnswer_MultipleChoicePollAnswerId",
          table: "PollsChoices");

      migrationBuilder.DropIndex(
          name: "IX_PollsChoices_MultipleChoicePollAnswerId",
          table: "PollsChoices");

      migrationBuilder.DropColumn(
          name: "MultipleChoicePollAnswerId",
          table: "PollsChoices");

      migrationBuilder.CreateTable(
          name: "PollResponseChoice",
          columns: table => new
          {
            Id = table.Column<Guid>(nullable: false),
            CreatedOn = table.Column<DateTime>(nullable: false),
            ModifiedOn = table.Column<DateTime>(nullable: false),
            DeletedOn = table.Column<DateTime>(nullable: true),
            MultipleChoicePollAnswerId = table.Column<Guid>(nullable: true),
            ChoiceId = table.Column<Guid>(nullable: true)
          },
          constraints: table =>
          {
            table.PrimaryKey("PK_PollResponseChoice", x => x.Id);
            table.ForeignKey(
                      name: "FK_PollResponseChoice_PollsChoices_ChoiceId",
                      column: x => x.ChoiceId,
                      principalTable: "PollsChoices",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Restrict);
            table.ForeignKey(
                      name: "FK_PollResponseChoice_PollAnswer_MultipleChoicePollAnswerId",
                      column: x => x.MultipleChoicePollAnswerId,
                      principalTable: "PollAnswer",
                      principalColumn: "Id",
                      onDelete: ReferentialAction.Restrict);
          });

      migrationBuilder.CreateIndex(
          name: "IX_PollResponseChoice_ChoiceId",
          table: "PollResponseChoice",
          column: "ChoiceId");

      migrationBuilder.CreateIndex(
          name: "IX_PollResponseChoice_MultipleChoicePollAnswerId",
          table: "PollResponseChoice",
          column: "MultipleChoicePollAnswerId");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
          name: "PollResponseChoice");

      migrationBuilder.AddColumn<Guid>(
          name: "MultipleChoicePollAnswerId",
          table: "PollsChoices",
          type: "uuid",
          nullable: true);

      migrationBuilder.CreateIndex(
          name: "IX_PollsChoices_MultipleChoicePollAnswerId",
          table: "PollsChoices",
          column: "MultipleChoicePollAnswerId");

      migrationBuilder.AddForeignKey(
          name: "FK_PollsChoices_PollAnswer_MultipleChoicePollAnswerId",
          table: "PollsChoices",
          column: "MultipleChoicePollAnswerId",
          principalTable: "PollAnswer",
          principalColumn: "Id",
          onDelete: ReferentialAction.Restrict);
    }
  }
}
