﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class enrollments : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
        name: "SubjectRole");

      migrationBuilder.CreateTable(
        name: "Enrollment",
        columns: table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          UserId = table.Column<Guid>(nullable: false),
          SubjectId = table.Column<Guid>(nullable: false),
          Discriminator = table.Column<string>(nullable: false),
          IsResponsible = table.Column<bool>(nullable: true),
          Score = table.Column<float>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Enrollment", x => x.Id);
          table.ForeignKey(
            name: "FK_Enrollment_Subjects_SubjectId",
            column: x => x.SubjectId,
            principalTable: "Subjects",
            principalColumn: "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            name: "FK_Enrollment_Users_UserId",
            column: x => x.UserId,
            principalTable: "Users",
            principalColumn: "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateIndex(
        name: "IX_Enrollment_SubjectId",
        table: "Enrollment",
        column: "SubjectId");

      migrationBuilder.CreateIndex(
        name: "IX_Enrollment_UserId",
        table: "Enrollment",
        column: "UserId");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
        name: "Enrollment");

      migrationBuilder.CreateTable(
        name: "SubjectRole",
        columns: table => new
        {
          Id = table.Column<Guid>(type: "uuid", nullable: false),
          CreatedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
          DeletedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
          Discriminator = table.Column<string>(type: "text", nullable: false),
          ModifiedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
          SubjectId = table.Column<Guid>(type: "uuid", nullable: false),
          UserId = table.Column<Guid>(type: "uuid", nullable: false),
          Score = table.Column<float>(type: "real", nullable: true),
          IsResponsible = table.Column<bool>(type: "boolean", nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_SubjectRole", x => x.Id);
          table.ForeignKey(
            name: "FK_SubjectRole_Subjects_SubjectId",
            column: x => x.SubjectId,
            principalTable: "Subjects",
            principalColumn: "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            name: "FK_SubjectRole_Users_UserId",
            column: x => x.UserId,
            principalTable: "Users",
            principalColumn: "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateIndex(
        name: "IX_SubjectRole_SubjectId",
        table: "SubjectRole",
        column: "SubjectId");

      migrationBuilder.CreateIndex(
        name: "IX_SubjectRole_UserId",
        table: "SubjectRole",
        column: "UserId");
    }
  }
}
