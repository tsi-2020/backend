﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class fixMuchasMierdas : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
        name: "SubjectPoll");

      migrationBuilder.AddColumn<Guid>(
        name: "SubjectId",
        table: "Poll",
        nullable: true);

      migrationBuilder.CreateTable(
        name: "SubjectPollPublication",
        columns: table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          PollId = table.Column<Guid>(nullable: false),
          SubjectId = table.Column<Guid>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_SubjectPollPublication", x => x.Id);
          table.ForeignKey(
            name: "FK_SubjectPollPublication_Poll_PollId",
            column: x => x.PollId,
            principalTable: "Poll",
            principalColumn: "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            name: "FK_SubjectPollPublication_Subjects_SubjectId",
            column: x => x.SubjectId,
            principalTable: "Subjects",
            principalColumn: "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        name: "UniversityPollPublication",
        columns: table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          PollId = table.Column<Guid>(nullable: false),
          UniversityId = table.Column<Guid>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_UniversityPollPublication", x => x.Id);
          table.ForeignKey(
            name: "FK_UniversityPollPublication_Poll_PollId",
            column: x => x.PollId,
            principalTable: "Poll",
            principalColumn: "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            name: "FK_UniversityPollPublication_Universities_UniversityId",
            column: x => x.UniversityId,
            principalTable: "Universities",
            principalColumn: "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateIndex(
        name: "IX_Poll_SubjectId",
        table: "Poll",
        column: "SubjectId");

      migrationBuilder.CreateIndex(
        name: "IX_SubjectPollPublication_PollId",
        table: "SubjectPollPublication",
        column: "PollId");

      migrationBuilder.CreateIndex(
        name: "IX_SubjectPollPublication_SubjectId",
        table: "SubjectPollPublication",
        column: "SubjectId");

      migrationBuilder.CreateIndex(
        name: "IX_UniversityPollPublication_PollId",
        table: "UniversityPollPublication",
        column: "PollId");

      migrationBuilder.CreateIndex(
        name: "IX_UniversityPollPublication_UniversityId",
        table: "UniversityPollPublication",
        column: "UniversityId");

      migrationBuilder.AddForeignKey(
        name: "FK_Poll_Subjects_SubjectId",
        table: "Poll",
        column: "SubjectId",
        principalTable: "Subjects",
        principalColumn: "Id",
        onDelete: ReferentialAction.Cascade);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
        name: "FK_Poll_Subjects_SubjectId",
        table: "Poll");

      migrationBuilder.DropTable(
        name: "SubjectPollPublication");

      migrationBuilder.DropTable(
        name: "UniversityPollPublication");

      migrationBuilder.DropIndex(
        name: "IX_Poll_SubjectId",
        table: "Poll");

      migrationBuilder.DropColumn(
        name: "SubjectId",
        table: "Poll");

      migrationBuilder.CreateTable(
        name: "SubjectPoll",
        columns: table => new
        {
          Id = table.Column<Guid>(type: "uuid", nullable: false),
          CreatedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
          DeletedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
          ModifiedOn = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
          PollId = table.Column<Guid>(type: "uuid", nullable: false),
          SubjectId = table.Column<Guid>(type: "uuid", nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_SubjectPoll", x => x.Id);
          table.ForeignKey(
            name: "FK_SubjectPoll_Poll_PollId",
            column: x => x.PollId,
            principalTable: "Poll",
            principalColumn: "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            name: "FK_SubjectPoll_Subjects_SubjectId",
            column: x => x.SubjectId,
            principalTable: "Subjects",
            principalColumn: "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateIndex(
        name: "IX_SubjectPoll_PollId",
        table: "SubjectPoll",
        column: "PollId");

      migrationBuilder.CreateIndex(
        name: "IX_SubjectPoll_SubjectId",
        table: "SubjectPoll",
        column: "SubjectId");
    }
  }
}
