﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class addUniversityTheme : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<string>(
        name: "Logo",
        table: "Universities",
        maxLength: 250,
        nullable: false,
        defaultValue: "");

      migrationBuilder.AddColumn<string>(
        name: "PrimaryColor",
        table: "Universities",
        maxLength: 9,
        nullable: false,
        defaultValue: "");

      migrationBuilder.AddColumn<string>(
        name: "SecondaryColor",
        table: "Universities",
        maxLength: 9,
        nullable: false,
        defaultValue: "");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropColumn(
        name: "Logo",
        table: "Universities");

      migrationBuilder.DropColumn(
        name: "PrimaryColor",
        table: "Universities");

      migrationBuilder.DropColumn(
        name: "SecondaryColor",
        table: "Universities");
    }
  }
}
