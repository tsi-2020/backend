﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class addScores : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
        name: "Scores",
        columns: table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          UserId = table.Column<Guid>(nullable: true),
          SubjectId = table.Column<Guid>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Scores", x => x.Id);
          table.ForeignKey(
            name: "FK_Scores_Subjects_SubjectId",
            column: x => x.SubjectId,
            principalTable: "Subjects",
            principalColumn: "Id",
            onDelete: ReferentialAction.Restrict);
          table.ForeignKey(
            name: "FK_Scores_Users_UserId",
            column: x => x.UserId,
            principalTable: "Users",
            principalColumn: "Id",
            onDelete: ReferentialAction.Restrict);
        });

      migrationBuilder.CreateIndex(
        name: "IX_Scores_SubjectId",
        table: "Scores",
        column: "SubjectId");

      migrationBuilder.CreateIndex(
        name: "IX_Scores_UserId",
        table: "Scores",
        column: "UserId");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
        name: "Scores");
    }
  }
}
