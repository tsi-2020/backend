﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class createNotices : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
        "FK_Notice_Subjects_SubjectId",
        "Notice");

      migrationBuilder.DropPrimaryKey(
        "PK_Notice",
        "Notice");


      migrationBuilder.RenameTable(
        "Notice",
        newName: "Notices");

      migrationBuilder.RenameIndex(
        "IX_Notice_SubjectId",
        table: "Notices",
        newName: "IX_Notices_SubjectId");

      migrationBuilder.AddPrimaryKey(
        "PK_Notices",
        "Notices",
        "Id");


      migrationBuilder.AddForeignKey(
        "FK_Notices_Subjects_SubjectId",
        "Notices",
        "SubjectId",
        "Subjects",
        principalColumn: "Id",
        onDelete: ReferentialAction.Restrict);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
        "FK_Notices_Subjects_SubjectId",
        "Notices");

      migrationBuilder.DropPrimaryKey(
        "PK_Notices",
        "Notices");

      migrationBuilder.RenameTable(
        "Notices",
        newName: "Notice");

      migrationBuilder.RenameIndex(
        "IX_Notices_SubjectId",
        table: "Notice",
        newName: "IX_Notice_SubjectId");

      migrationBuilder.AddPrimaryKey(
        "PK_Notice",
        "Notice",
        "Id");

      migrationBuilder.AddForeignKey(
        "FK_Notice_Subjects_SubjectId",
        "Notice",
        "SubjectId",
        "Subjects",
        principalColumn: "Id",
        onDelete: ReferentialAction.Restrict);
    }
  }
}
