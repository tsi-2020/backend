﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class addPollQuestionPosition : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<int>(
        name: "Position",
        table: "PollQuestion",
        nullable: false,
        defaultValue: 0);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropColumn(
        name: "Position",
        table: "PollQuestion");
    }
  }
}
