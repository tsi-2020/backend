﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class assigments : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_Assignment_Subjects_SubjectId",
          table: "Assignment");

      migrationBuilder.DropForeignKey(
          name: "FK_Component_Assignment_AssignmentId",
          table: "Component");

      migrationBuilder.DropForeignKey(
          name: "FK_DeliveredAssignment_Assignment_AssignmentId",
          table: "DeliveredAssignment");

      migrationBuilder.DropPrimaryKey(
        name: "PK_Assignment",
        table: "Assignment");

      migrationBuilder.RenameTable(
          name: "Assignment",
          newName: "Assignments");

      migrationBuilder.RenameColumn(
          name: "isInitialized",
          table: "Assignments",
          newName: "IsInitialized");

      migrationBuilder.RenameIndex(
          name: "IX_Assignment_SubjectId",
          table: "Assignments",
          newName: "IX_Assignments_SubjectId");

      migrationBuilder.AddColumn<string>(
          name: "FileName",
          table: "DeliveredAssignment",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "FileUrl",
          table: "DeliveredAssignment",
          nullable: true);

      migrationBuilder.AddColumn<string>(
          name: "ProfessorComments",
          table: "DeliveredAssignment",
          nullable: true);

      migrationBuilder.AddPrimaryKey(
          name: "PK_Assignments",
          table: "Assignments",
          column: "Id");

      migrationBuilder.AddForeignKey(
          name: "FK_Assignments_Subjects_SubjectId",
          table: "Assignments",
          column: "SubjectId",
          principalTable: "Subjects",
          principalColumn: "Id",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_Component_Assignments_AssignmentId",
          table: "Component",
          column: "AssignmentId",
          principalTable: "Assignments",
          principalColumn: "Id",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_DeliveredAssignment_Assignments_AssignmentId",
          table: "DeliveredAssignment",
          column: "AssignmentId",
          principalTable: "Assignments",
          principalColumn: "Id",
          onDelete: ReferentialAction.Restrict);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
          name: "FK_Assignments_Subjects_SubjectId",
          table: "Assignments");

      migrationBuilder.DropForeignKey(
          name: "FK_Component_Assignments_AssignmentId",
          table: "Component");

      migrationBuilder.DropForeignKey(
          name: "FK_DeliveredAssignment_Assignments_AssignmentId",
          table: "DeliveredAssignment");

      migrationBuilder.DropPrimaryKey(
          name: "PK_Assignments",
          table: "Assignments");

      migrationBuilder.DropColumn(
          name: "FileName",
          table: "DeliveredAssignment");

      migrationBuilder.DropColumn(
          name: "FileUrl",
          table: "DeliveredAssignment");

      migrationBuilder.DropColumn(
          name: "ProfessorComments",
          table: "DeliveredAssignment");

      migrationBuilder.RenameTable(
          name: "Assignments",
          newName: "Assignment");

      migrationBuilder.RenameColumn(
          name: "IsInitialized",
          table: "Assignment",
          newName: "isInitialized");

      migrationBuilder.RenameIndex(
          name: "IX_Assignments_SubjectId",
          table: "Assignment",
          newName: "IX_Assignment_SubjectId");

      migrationBuilder.AddPrimaryKey(
          name: "PK_Assignment",
          table: "Assignment",
          column: "Id");

      migrationBuilder.AddForeignKey(
          name: "FK_Assignment_Subjects_SubjectId",
          table: "Assignment",
          column: "SubjectId",
          principalTable: "Subjects",
          principalColumn: "Id",
          onDelete: ReferentialAction.Cascade);

      migrationBuilder.AddForeignKey(
          name: "FK_Component_Assignment_AssignmentId",
          table: "Component",
          column: "AssignmentId",
          principalTable: "Assignment",
          principalColumn: "Id",
          onDelete: ReferentialAction.Restrict);

      migrationBuilder.AddForeignKey(
          name: "FK_DeliveredAssignment_Assignment_AssignmentId",
          table: "DeliveredAssignment",
          column: "AssignmentId",
          principalTable: "Assignment",
          principalColumn: "Id",
          onDelete: ReferentialAction.Restrict);
    }
  }
}
