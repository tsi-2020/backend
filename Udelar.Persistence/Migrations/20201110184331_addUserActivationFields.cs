﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class addUserActivationFields : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DeleteData(
          table: "Administrators",
          keyColumn: "Id",
          keyValue: new Guid("70bae65b-a208-4446-8da2-11417c835c9d"));

      migrationBuilder.AddColumn<string>(
          name: "ActivationCode",
          table: "Users",
          nullable: true);

      migrationBuilder.AddColumn<bool>(
          name: "IsActive",
          table: "Users",
          nullable: false,
          defaultValue: false);

      migrationBuilder.AddColumn<string>(
          name: "ResetPasswordCode",
          table: "Users",
          nullable: true);

      migrationBuilder.InsertData(
          table: "Administrators",
          columns: new[] { "Id", "CreatedOn", "DeletedOn", "Discriminator", "Email", "FirstName", "LastName", "ModifiedOn", "Password", "RefreshToken" },
          values: new object[] { new Guid("b3a1efac-de6d-4a2c-ac72-7bc3cf9bffd0"), new DateTime(2020, 11, 10, 15, 43, 30, 790, DateTimeKind.Local).AddTicks(5547), null, "UdelarAdministrator", "admin@udelar.uy", "Administrador", "Administrador", new DateTime(2020, 11, 10, 15, 43, 30, 794, DateTimeKind.Local).AddTicks(1360), "$2a$11$VokKNNfKC2uxE.xN74C62eL703Yf3f1/Ma4xfePyhOsD8Apj0g/Ie", null });
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DeleteData(
          table: "Administrators",
          keyColumn: "Id",
          keyValue: new Guid("b3a1efac-de6d-4a2c-ac72-7bc3cf9bffd0"));

      migrationBuilder.DropColumn(
          name: "ActivationCode",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "IsActive",
          table: "Users");

      migrationBuilder.DropColumn(
          name: "ResetPasswordCode",
          table: "Users");

      migrationBuilder.InsertData(
          table: "Administrators",
          columns: new[] { "Id", "CreatedOn", "DeletedOn", "Discriminator", "Email", "FirstName", "LastName", "ModifiedOn", "Password", "RefreshToken" },
          values: new object[] { new Guid("70bae65b-a208-4446-8da2-11417c835c9d"), new DateTime(2020, 11, 7, 15, 11, 1, 323, DateTimeKind.Local).AddTicks(6214), null, "UdelarAdministrator", "admin@udelar.uy", "Administrador", "Administrador", new DateTime(2020, 11, 7, 15, 11, 1, 327, DateTimeKind.Local).AddTicks(916), "$2a$11$db81P/jSJMA6Cf/h/XT0cO8Mvff.zRpTqfCHNOj2lU6togEFsfa0u", null });
    }
  }
}
