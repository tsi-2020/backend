﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class renameIsAsyncToCheckWithBedelia : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.RenameColumn("IsAsync", "Subjects", "ValidateWithBedelia");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {

      migrationBuilder.RenameColumn("ValidateWithBedelia", "Subjects", "IsAsync");
    }
  }
}
