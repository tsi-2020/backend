﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class removeCi : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DeleteData(
        "Administrators",
        "Id",
        new Guid("45f0f175-e333-4627-9be7-93eecc355b91"));

      migrationBuilder.DeleteData(
        "Administrators",
        "Id",
        new Guid("12a8b1c3-2d30-4ca9-9960-e771960ca9ff"));

      migrationBuilder.DeleteData(
        "Universities",
        "Id",
        new Guid("c43970c3-ebfb-4747-9683-d9fe8b168b03"));

      migrationBuilder.DropColumn(
        "CI",
        "Administrators");

      migrationBuilder.InsertData(
        "Administrators",
        new[]
        {
          "Id", "CreatedOn", "DeletedOn", "Discriminator", "Email", "FirstName", "LastName", "ModifiedOn", "Password",
          "RefreshToken"
        },
        new object[]
        {
          new Guid("dbbd20f4-7e41-4834-86c5-b1ce97538426"),
          new DateTime(2020, 10, 12, 13, 33, 11, 378, DateTimeKind.Local).AddTicks(4942), null, "UdelarAdministrator",
          "admin@udelar.uy", "Administrador", "Administrador",
          new DateTime(2020, 10, 12, 13, 33, 11, 381, DateTimeKind.Local).AddTicks(8890),
          "$2a$11$MUS01ht/yWYkvUoJHOU6NuQN.sKVJAgBxrOI/4E6VzpD6DZdIp9OG", null
        });

      migrationBuilder.InsertData(
        "Universities",
        new[] { "Id", "AccessPath", "CreatedOn", "DeletedOn", "ModifiedOn", "Name" },
        new object[]
        {
          new Guid("f96b860a-1fef-4afb-8e1c-6dcfc8c4c547"), "/FING",
          new DateTime(2020, 10, 12, 13, 33, 11, 383, DateTimeKind.Local).AddTicks(2917), null,
          new DateTime(2020, 10, 12, 13, 33, 11, 383, DateTimeKind.Local).AddTicks(2926), "FING"
        });

      migrationBuilder.InsertData(
        "Administrators",
        new[]
        {
          "Id", "CreatedOn", "DeletedOn", "Discriminator", "Email", "FirstName", "LastName", "ModifiedOn", "Password",
          "RefreshToken", "UniversityId"
        },
        new object[]
        {
          new Guid("51f02e96-4cc6-4b27-bb14-2aec79f92440"),
          new DateTime(2020, 10, 12, 13, 33, 11, 546, DateTimeKind.Local).AddTicks(2276), null,
          "UniversityAdministrator", "admin@fing.uy", "Administrador Fing", "Administrador Fing",
          new DateTime(2020, 10, 12, 13, 33, 11, 546, DateTimeKind.Local).AddTicks(2427),
          "$2a$11$kJVZMpVsRNpyaeaX7Kj63.6TeXszf0hGxV9vbq7wzxAVDwGA459L.", null,
          new Guid("f96b860a-1fef-4afb-8e1c-6dcfc8c4c547")
        });
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DeleteData(
        "Administrators",
        "Id",
        new Guid("dbbd20f4-7e41-4834-86c5-b1ce97538426"));

      migrationBuilder.DeleteData(
        "Administrators",
        "Id",
        new Guid("51f02e96-4cc6-4b27-bb14-2aec79f92440"));

      migrationBuilder.DeleteData(
        "Universities",
        "Id",
        new Guid("f96b860a-1fef-4afb-8e1c-6dcfc8c4c547"));

      migrationBuilder.AddColumn<int>(
        "CI",
        "Administrators",
        "integer",
        nullable: false,
        defaultValue: 0);

      migrationBuilder.InsertData(
        "Administrators",
        new[]
        {
          "Id", "CI", "CreatedOn", "DeletedOn", "Discriminator", "Email", "FirstName", "LastName", "ModifiedOn",
          "Password", "RefreshToken"
        },
        new object[]
        {
          new Guid("45f0f175-e333-4627-9be7-93eecc355b91"), 12345678,
          new DateTime(2020, 9, 30, 0, 49, 11, 422, DateTimeKind.Local).AddTicks(8485), null, "UdelarAdministrator",
          "admin@udelar.uy", "Administrador", "Administrador",
          new DateTime(2020, 9, 30, 0, 49, 11, 427, DateTimeKind.Local).AddTicks(9168),
          "$2a$11$HbEgSE1JLRb8bGXiPz/2NOwWttv86YP71rmIsknBshcj7YihDYKt6", null
        });

      migrationBuilder.InsertData(
        "Universities",
        new[] { "Id", "AccessPath", "CreatedOn", "DeletedOn", "ModifiedOn", "Name" },
        new object[]
        {
          new Guid("c43970c3-ebfb-4747-9683-d9fe8b168b03"), "/FING",
          new DateTime(2020, 9, 30, 0, 49, 11, 429, DateTimeKind.Local).AddTicks(9289), null,
          new DateTime(2020, 9, 30, 0, 49, 11, 429, DateTimeKind.Local).AddTicks(9301), "FING"
        });

      migrationBuilder.InsertData(
        "Administrators",
        new[]
        {
          "Id", "CI", "CreatedOn", "DeletedOn", "Discriminator", "Email", "FirstName", "LastName", "ModifiedOn",
          "Password", "RefreshToken", "UniversityId"
        },
        new object[]
        {
          new Guid("12a8b1c3-2d30-4ca9-9960-e771960ca9ff"), 12345679,
          new DateTime(2020, 9, 30, 0, 49, 11, 596, DateTimeKind.Local).AddTicks(5037), null,
          "UniversityAdministrator", "admin@fing.uy", "Administrador Fing", "Administrador Fing",
          new DateTime(2020, 9, 30, 0, 49, 11, 596, DateTimeKind.Local).AddTicks(5074),
          "$2a$11$JmCLlH5YbWHcHLj/j8.KDuh52bK4IGSzsVUNAD7X5JKxk0DXn03H.", null,
          new Guid("c43970c3-ebfb-4747-9683-d9fe8b168b03")
        });
    }
  }
}
