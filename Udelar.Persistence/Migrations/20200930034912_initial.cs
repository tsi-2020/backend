﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class initial : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.CreateTable(
        "Universities",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Name = table.Column<string>(nullable: false),
          AccessPath = table.Column<string>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Universities", x => x.Id);
        });

      migrationBuilder.CreateTable(
        "Administrators",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          CI = table.Column<int>(nullable: false),
          FirstName = table.Column<string>(nullable: true),
          LastName = table.Column<string>(nullable: true),
          Password = table.Column<string>(nullable: true),
          RefreshToken = table.Column<string>(nullable: true),
          Email = table.Column<string>(nullable: true),
          Discriminator = table.Column<string>(nullable: false),
          UniversityId = table.Column<Guid>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Administrators", x => x.Id);
          table.ForeignKey(
            "FK_Administrators_Universities_UniversityId",
            x => x.UniversityId,
            "Universities",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "Careers",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          UniversityId = table.Column<Guid>(nullable: false),
          Name = table.Column<string>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Careers", x => x.Id);
          table.ForeignKey(
            "FK_Careers_Universities_UniversityId",
            x => x.UniversityId,
            "Universities",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "Poll",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Title = table.Column<string>(nullable: false),
          Discriminator = table.Column<string>(nullable: false),
          UniversityId = table.Column<Guid>(nullable: true),
          IsInitialized = table.Column<bool>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Poll", x => x.Id);
          table.ForeignKey(
            "FK_Poll_Universities_UniversityId",
            x => x.UniversityId,
            "Universities",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "Subjects",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          UniversityId = table.Column<Guid>(nullable: false),
          Name = table.Column<string>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Subjects", x => x.Id);
          table.ForeignKey(
            "FK_Subjects_Universities_UniversityId",
            x => x.UniversityId,
            "Universities",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "SubjectTemplates",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          UniversityId = table.Column<Guid>(nullable: false),
          Title = table.Column<string>(nullable: true),
          Description = table.Column<string>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_SubjectTemplates", x => x.Id);
          table.ForeignKey(
            "FK_SubjectTemplates_Universities_UniversityId",
            x => x.UniversityId,
            "Universities",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "Users",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          UniversityId = table.Column<Guid>(nullable: false),
          FirstName = table.Column<string>(nullable: false),
          LastName = table.Column<string>(nullable: false),
          Email = table.Column<string>(nullable: false),
          CI = table.Column<int>(nullable: false),
          Password = table.Column<string>(nullable: true),
          RefreshToken = table.Column<string>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Users", x => x.Id);
          table.ForeignKey(
            "FK_Users_Universities_UniversityId",
            x => x.UniversityId,
            "Universities",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "PollQuestion",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Text = table.Column<string>(nullable: false),
          Discriminator = table.Column<string>(nullable: false),
          PollId = table.Column<Guid>(nullable: true),
          IsMultipleSelect = table.Column<bool>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_PollQuestion", x => x.Id);
          table.ForeignKey(
            "FK_PollQuestion_Poll_PollId",
            x => x.PollId,
            "Poll",
            "Id",
            onDelete: ReferentialAction.Restrict);
        });

      migrationBuilder.CreateTable(
        "Assignment",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          SubjectId = table.Column<Guid>(nullable: false),
          IsIndividual = table.Column<bool>(nullable: false),
          AvailableFrom = table.Column<DateTime>(nullable: false),
          AvailableUntil = table.Column<DateTime>(nullable: false),
          MaxScore = table.Column<float>(nullable: false),
          isInitialized = table.Column<bool>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Assignment", x => x.Id);
          table.ForeignKey(
            "FK_Assignment_Subjects_SubjectId",
            x => x.SubjectId,
            "Subjects",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "CareerSubject",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          CareerId = table.Column<Guid>(nullable: false),
          SubjectId = table.Column<Guid>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_CareerSubject", x => x.Id);
          table.ForeignKey(
            "FK_CareerSubject_Careers_CareerId",
            x => x.CareerId,
            "Careers",
            "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            "FK_CareerSubject_Subjects_SubjectId",
            x => x.SubjectId,
            "Subjects",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "Group",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          SubjectId = table.Column<Guid>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Group", x => x.Id);
          table.ForeignKey(
            "FK_Group_Subjects_SubjectId",
            x => x.SubjectId,
            "Subjects",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "Notice",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Message = table.Column<string>(nullable: true),
          SubjectId = table.Column<Guid>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Notice", x => x.Id);
          table.ForeignKey(
            "FK_Notice_Subjects_SubjectId",
            x => x.SubjectId,
            "Subjects",
            "Id",
            onDelete: ReferentialAction.Restrict);
        });

      migrationBuilder.CreateTable(
        "Quiz",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          SubjectId = table.Column<Guid>(nullable: false),
          AvailableFrom = table.Column<DateTime>(nullable: false),
          AvailableUntil = table.Column<DateTime>(nullable: false),
          TimeLimit = table.Column<int>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Quiz", x => x.Id);
          table.ForeignKey(
            "FK_Quiz_Subjects_SubjectId",
            x => x.SubjectId,
            "Subjects",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "SubjectPoll",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          PollId = table.Column<Guid>(nullable: false),
          SubjectId = table.Column<Guid>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_SubjectPoll", x => x.Id);
          table.ForeignKey(
            "FK_SubjectPoll_Poll_PollId",
            x => x.PollId,
            "Poll",
            "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            "FK_SubjectPoll_Subjects_SubjectId",
            x => x.SubjectId,
            "Subjects",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "EducationalUnits",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Position = table.Column<int>(nullable: false),
          Title = table.Column<string>(nullable: true),
          Text = table.Column<string>(nullable: true),
          SubjectId = table.Column<Guid>(nullable: true),
          SubjectTemplateId = table.Column<Guid>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_EducationalUnits", x => x.Id);
          table.ForeignKey(
            "FK_EducationalUnits_Subjects_SubjectId",
            x => x.SubjectId,
            "Subjects",
            "Id",
            onDelete: ReferentialAction.Restrict);
          table.ForeignKey(
            "FK_EducationalUnits_SubjectTemplates_SubjectTemplateId",
            x => x.SubjectTemplateId,
            "SubjectTemplates",
            "Id",
            onDelete: ReferentialAction.Restrict);
        });

      migrationBuilder.CreateTable(
        "PollResponse",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          PollId = table.Column<Guid>(nullable: false),
          UserId = table.Column<Guid>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_PollResponse", x => x.Id);
          table.ForeignKey(
            "FK_PollResponse_Poll_PollId",
            x => x.PollId,
            "Poll",
            "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            "FK_PollResponse_Users_UserId",
            x => x.UserId,
            "Users",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "SubjectRole",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          UserId = table.Column<Guid>(nullable: false),
          SubjectId = table.Column<Guid>(nullable: false),
          Discriminator = table.Column<string>(nullable: false),
          Score = table.Column<float>(nullable: true),
          IsResponsible = table.Column<bool>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_SubjectRole", x => x.Id);
          table.ForeignKey(
            "FK_SubjectRole_Subjects_SubjectId",
            x => x.SubjectId,
            "Subjects",
            "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            "FK_SubjectRole_Users_UserId",
            x => x.UserId,
            "Users",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "UserRole",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Role = table.Column<string>(nullable: false),
          UserId = table.Column<Guid>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_UserRole", x => x.Id);
          table.ForeignKey(
            "FK_UserRole_Users_UserId",
            x => x.UserId,
            "Users",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "DeliveredAssignment",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Score = table.Column<float>(nullable: false),
          AssignmentId = table.Column<Guid>(nullable: true),
          Discriminator = table.Column<string>(nullable: false),
          GroupId = table.Column<Guid>(nullable: true),
          UserId = table.Column<Guid>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_DeliveredAssignment", x => x.Id);
          table.ForeignKey(
            "FK_DeliveredAssignment_Assignment_AssignmentId",
            x => x.AssignmentId,
            "Assignment",
            "Id",
            onDelete: ReferentialAction.Restrict);
          table.ForeignKey(
            "FK_DeliveredAssignment_Group_GroupId",
            x => x.GroupId,
            "Group",
            "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            "FK_DeliveredAssignment_Users_UserId",
            x => x.UserId,
            "Users",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "UserGroup",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          UserId = table.Column<Guid>(nullable: true),
          GroupId = table.Column<Guid>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_UserGroup", x => x.Id);
          table.ForeignKey(
            "FK_UserGroup_Group_GroupId",
            x => x.GroupId,
            "Group",
            "Id",
            onDelete: ReferentialAction.Restrict);
          table.ForeignKey(
            "FK_UserGroup_Users_UserId",
            x => x.UserId,
            "Users",
            "Id",
            onDelete: ReferentialAction.Restrict);
        });

      migrationBuilder.CreateTable(
        "QuizQuestion",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Text = table.Column<string>(nullable: false),
          Score = table.Column<float>(nullable: false),
          Discriminator = table.Column<string>(nullable: false),
          QuizId = table.Column<Guid>(nullable: true),
          MultipleSelect = table.Column<bool>(nullable: true),
          IsCorrect = table.Column<bool>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_QuizQuestion", x => x.Id);
          table.ForeignKey(
            "FK_QuizQuestion_Quiz_QuizId",
            x => x.QuizId,
            "Quiz",
            "Id",
            onDelete: ReferentialAction.Restrict);
        });

      migrationBuilder.CreateTable(
        "QuizResponse",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          QuizId = table.Column<Guid>(nullable: false),
          UserId = table.Column<Guid>(nullable: false),
          Score = table.Column<float>(nullable: false)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_QuizResponse", x => x.Id);
          table.ForeignKey(
            "FK_QuizResponse_Quiz_QuizId",
            x => x.QuizId,
            "Quiz",
            "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            "FK_QuizResponse_Users_UserId",
            x => x.UserId,
            "Users",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "Component",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Position = table.Column<int>(nullable: false),
          Discriminator = table.Column<string>(nullable: false),
          EducationalUnitId = table.Column<Guid>(nullable: true),
          Title = table.Column<string>(nullable: true),
          Text = table.Column<string>(nullable: true),
          AssignmentId = table.Column<Guid>(nullable: true),
          DocumentComponent_Title = table.Column<string>(nullable: true),
          Url = table.Column<string>(nullable: true),
          ForumComponent_Title = table.Column<string>(nullable: true),
          ForumId = table.Column<string>(nullable: true),
          LinkComponent_Title = table.Column<string>(nullable: true),
          LinkComponent_Url = table.Column<string>(nullable: true),
          PollComponent_Title = table.Column<string>(nullable: true),
          PollId = table.Column<Guid>(nullable: true),
          QuizComponent_Title = table.Column<string>(nullable: true),
          QuizComponent_Text = table.Column<string>(nullable: true),
          QuizId = table.Column<Guid>(nullable: true),
          TextComponent_Text = table.Column<string>(nullable: true),
          VideoComponent_Title = table.Column<string>(nullable: true),
          VideoComponent_Url = table.Column<string>(nullable: true),
          VirtualMeetingComponent_Title = table.Column<string>(nullable: true),
          VirtualMeetingComponent_Url = table.Column<string>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_Component", x => x.Id);
          table.ForeignKey(
            "FK_Component_Assignment_AssignmentId",
            x => x.AssignmentId,
            "Assignment",
            "Id",
            onDelete: ReferentialAction.Restrict);
          table.ForeignKey(
            "FK_Component_EducationalUnits_EducationalUnitId",
            x => x.EducationalUnitId,
            "EducationalUnits",
            "Id",
            onDelete: ReferentialAction.Restrict);
          table.ForeignKey(
            "FK_Component_Poll_PollId",
            x => x.PollId,
            "Poll",
            "Id",
            onDelete: ReferentialAction.Restrict);
          table.ForeignKey(
            "FK_Component_Quiz_QuizId",
            x => x.QuizId,
            "Quiz",
            "Id",
            onDelete: ReferentialAction.Restrict);
        });

      migrationBuilder.CreateTable(
        "PollAnswer",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Discriminator = table.Column<string>(nullable: false),
          PollResponseId = table.Column<Guid>(nullable: true),
          MultipleChoicePollQuestionId = table.Column<Guid>(nullable: true),
          Text = table.Column<string>(nullable: true),
          OpenPollQuestionId = table.Column<Guid>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_PollAnswer", x => x.Id);
          table.ForeignKey(
            "FK_PollAnswer_PollQuestion_MultipleChoicePollQuestionId",
            x => x.MultipleChoicePollQuestionId,
            "PollQuestion",
            "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            "FK_PollAnswer_PollQuestion_OpenPollQuestionId",
            x => x.OpenPollQuestionId,
            "PollQuestion",
            "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            "FK_PollAnswer_PollResponse_PollResponseId",
            x => x.PollResponseId,
            "PollResponse",
            "Id",
            onDelete: ReferentialAction.Restrict);
        });

      migrationBuilder.CreateTable(
        "QuizAnswer",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Discriminator = table.Column<string>(nullable: false),
          QuizResponseId = table.Column<Guid>(nullable: true),
          MultipleChoiceQuizQuestionId = table.Column<Guid>(nullable: true),
          Text = table.Column<string>(nullable: true),
          OpenQuizQuestionId = table.Column<Guid>(nullable: true),
          TrueFalseQuizQuestionId = table.Column<Guid>(nullable: true),
          SelectedAnswer = table.Column<bool>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_QuizAnswer", x => x.Id);
          table.ForeignKey(
            "FK_QuizAnswer_QuizQuestion_MultipleChoiceQuizQuestionId",
            x => x.MultipleChoiceQuizQuestionId,
            "QuizQuestion",
            "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            "FK_QuizAnswer_QuizQuestion_OpenQuizQuestionId",
            x => x.OpenQuizQuestionId,
            "QuizQuestion",
            "Id",
            onDelete: ReferentialAction.Cascade);
          table.ForeignKey(
            "FK_QuizAnswer_QuizResponse_QuizResponseId",
            x => x.QuizResponseId,
            "QuizResponse",
            "Id",
            onDelete: ReferentialAction.Restrict);
          table.ForeignKey(
            "FK_QuizAnswer_QuizQuestion_TrueFalseQuizQuestionId",
            x => x.TrueFalseQuizQuestionId,
            "QuizQuestion",
            "Id",
            onDelete: ReferentialAction.Cascade);
        });

      migrationBuilder.CreateTable(
        "PollsChoices",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          Text = table.Column<string>(nullable: false),
          MultipleChoicePollAnswerId = table.Column<Guid>(nullable: true),
          MultipleChoicePollQuestionId = table.Column<Guid>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_PollsChoices", x => x.Id);
          table.ForeignKey(
            "FK_PollsChoices_PollAnswer_MultipleChoicePollAnswerId",
            x => x.MultipleChoicePollAnswerId,
            "PollAnswer",
            "Id",
            onDelete: ReferentialAction.Restrict);
          table.ForeignKey(
            "FK_PollsChoices_PollQuestion_MultipleChoicePollQuestionId",
            x => x.MultipleChoicePollQuestionId,
            "PollQuestion",
            "Id",
            onDelete: ReferentialAction.Restrict);
        });

      migrationBuilder.CreateTable(
        "QuizzesChoices",
        table => new
        {
          Id = table.Column<Guid>(nullable: false),
          CreatedOn = table.Column<DateTime>(nullable: false),
          ModifiedOn = table.Column<DateTime>(nullable: false),
          DeletedOn = table.Column<DateTime>(nullable: true),
          IsCorrect = table.Column<bool>(nullable: false),
          Text = table.Column<string>(nullable: false),
          MultipleChoiceQuizAnswerId = table.Column<Guid>(nullable: true),
          MultipleChoiceQuizQuestionId = table.Column<Guid>(nullable: true)
        },
        constraints: table =>
        {
          table.PrimaryKey("PK_QuizzesChoices", x => x.Id);
          table.ForeignKey(
            "FK_QuizzesChoices_QuizAnswer_MultipleChoiceQuizAnswerId",
            x => x.MultipleChoiceQuizAnswerId,
            "QuizAnswer",
            "Id",
            onDelete: ReferentialAction.Restrict);
          table.ForeignKey(
            "FK_QuizzesChoices_QuizQuestion_MultipleChoiceQuizQuestionId",
            x => x.MultipleChoiceQuizQuestionId,
            "QuizQuestion",
            "Id",
            onDelete: ReferentialAction.Restrict);
        });

      migrationBuilder.InsertData(
        "Administrators",
        new[]
        {
          "Id", "CI", "CreatedOn", "DeletedOn", "Discriminator", "Email", "FirstName", "LastName", "ModifiedOn",
          "Password", "RefreshToken"
        },
        new object[]
        {
          new Guid("45f0f175-e333-4627-9be7-93eecc355b91"), 12345678,
          new DateTime(2020, 9, 30, 0, 49, 11, 422, DateTimeKind.Local).AddTicks(8485), null, "UdelarAdministrator",
          "admin@udelar.uy", "Administrador", "Administrador",
          new DateTime(2020, 9, 30, 0, 49, 11, 427, DateTimeKind.Local).AddTicks(9168),
          "$2a$11$HbEgSE1JLRb8bGXiPz/2NOwWttv86YP71rmIsknBshcj7YihDYKt6", null
        });

      migrationBuilder.InsertData(
        "Universities",
        new[] { "Id", "AccessPath", "CreatedOn", "DeletedOn", "ModifiedOn", "Name" },
        new object[]
        {
          new Guid("c43970c3-ebfb-4747-9683-d9fe8b168b03"), "/FING",
          new DateTime(2020, 9, 30, 0, 49, 11, 429, DateTimeKind.Local).AddTicks(9289), null,
          new DateTime(2020, 9, 30, 0, 49, 11, 429, DateTimeKind.Local).AddTicks(9301), "FING"
        });

      migrationBuilder.InsertData(
        "Administrators",
        new[]
        {
          "Id", "CI", "CreatedOn", "DeletedOn", "Discriminator", "Email", "FirstName", "LastName", "ModifiedOn",
          "Password", "RefreshToken", "UniversityId"
        },
        new object[]
        {
          new Guid("12a8b1c3-2d30-4ca9-9960-e771960ca9ff"), 12345679,
          new DateTime(2020, 9, 30, 0, 49, 11, 596, DateTimeKind.Local).AddTicks(5037), null,
          "UniversityAdministrator", "admin@fing.uy", "Administrador Fing", "Administrador Fing",
          new DateTime(2020, 9, 30, 0, 49, 11, 596, DateTimeKind.Local).AddTicks(5074),
          "$2a$11$JmCLlH5YbWHcHLj/j8.KDuh52bK4IGSzsVUNAD7X5JKxk0DXn03H.", null,
          new Guid("c43970c3-ebfb-4747-9683-d9fe8b168b03")
        });

      migrationBuilder.CreateIndex(
        "IX_Administrators_UniversityId",
        "Administrators",
        "UniversityId");

      migrationBuilder.CreateIndex(
        "IX_Assignment_SubjectId",
        "Assignment",
        "SubjectId");

      migrationBuilder.CreateIndex(
        "IX_Careers_UniversityId",
        "Careers",
        "UniversityId");

      migrationBuilder.CreateIndex(
        "IX_CareerSubject_CareerId",
        "CareerSubject",
        "CareerId");

      migrationBuilder.CreateIndex(
        "IX_CareerSubject_SubjectId",
        "CareerSubject",
        "SubjectId");

      migrationBuilder.CreateIndex(
        "IX_Component_AssignmentId",
        "Component",
        "AssignmentId");

      migrationBuilder.CreateIndex(
        "IX_Component_EducationalUnitId",
        "Component",
        "EducationalUnitId");

      migrationBuilder.CreateIndex(
        "IX_Component_PollId",
        "Component",
        "PollId");

      migrationBuilder.CreateIndex(
        "IX_Component_QuizId",
        "Component",
        "QuizId");

      migrationBuilder.CreateIndex(
        "IX_DeliveredAssignment_AssignmentId",
        "DeliveredAssignment",
        "AssignmentId");

      migrationBuilder.CreateIndex(
        "IX_DeliveredAssignment_GroupId",
        "DeliveredAssignment",
        "GroupId");

      migrationBuilder.CreateIndex(
        "IX_DeliveredAssignment_UserId",
        "DeliveredAssignment",
        "UserId");

      migrationBuilder.CreateIndex(
        "IX_EducationalUnits_SubjectId",
        "EducationalUnits",
        "SubjectId");

      migrationBuilder.CreateIndex(
        "IX_EducationalUnits_SubjectTemplateId",
        "EducationalUnits",
        "SubjectTemplateId");

      migrationBuilder.CreateIndex(
        "IX_Group_SubjectId",
        "Group",
        "SubjectId");

      migrationBuilder.CreateIndex(
        "IX_Notice_SubjectId",
        "Notice",
        "SubjectId");

      migrationBuilder.CreateIndex(
        "IX_Poll_UniversityId",
        "Poll",
        "UniversityId");

      migrationBuilder.CreateIndex(
        "IX_PollAnswer_MultipleChoicePollQuestionId",
        "PollAnswer",
        "MultipleChoicePollQuestionId");

      migrationBuilder.CreateIndex(
        "IX_PollAnswer_OpenPollQuestionId",
        "PollAnswer",
        "OpenPollQuestionId");

      migrationBuilder.CreateIndex(
        "IX_PollAnswer_PollResponseId",
        "PollAnswer",
        "PollResponseId");

      migrationBuilder.CreateIndex(
        "IX_PollQuestion_PollId",
        "PollQuestion",
        "PollId");

      migrationBuilder.CreateIndex(
        "IX_PollResponse_PollId",
        "PollResponse",
        "PollId");

      migrationBuilder.CreateIndex(
        "IX_PollResponse_UserId",
        "PollResponse",
        "UserId");

      migrationBuilder.CreateIndex(
        "IX_PollsChoices_MultipleChoicePollAnswerId",
        "PollsChoices",
        "MultipleChoicePollAnswerId");

      migrationBuilder.CreateIndex(
        "IX_PollsChoices_MultipleChoicePollQuestionId",
        "PollsChoices",
        "MultipleChoicePollQuestionId");

      migrationBuilder.CreateIndex(
        "IX_Quiz_SubjectId",
        "Quiz",
        "SubjectId");

      migrationBuilder.CreateIndex(
        "IX_QuizAnswer_MultipleChoiceQuizQuestionId",
        "QuizAnswer",
        "MultipleChoiceQuizQuestionId");

      migrationBuilder.CreateIndex(
        "IX_QuizAnswer_OpenQuizQuestionId",
        "QuizAnswer",
        "OpenQuizQuestionId");

      migrationBuilder.CreateIndex(
        "IX_QuizAnswer_QuizResponseId",
        "QuizAnswer",
        "QuizResponseId");

      migrationBuilder.CreateIndex(
        "IX_QuizAnswer_TrueFalseQuizQuestionId",
        "QuizAnswer",
        "TrueFalseQuizQuestionId");

      migrationBuilder.CreateIndex(
        "IX_QuizQuestion_QuizId",
        "QuizQuestion",
        "QuizId");

      migrationBuilder.CreateIndex(
        "IX_QuizResponse_QuizId",
        "QuizResponse",
        "QuizId");

      migrationBuilder.CreateIndex(
        "IX_QuizResponse_UserId",
        "QuizResponse",
        "UserId");

      migrationBuilder.CreateIndex(
        "IX_QuizzesChoices_MultipleChoiceQuizAnswerId",
        "QuizzesChoices",
        "MultipleChoiceQuizAnswerId");

      migrationBuilder.CreateIndex(
        "IX_QuizzesChoices_MultipleChoiceQuizQuestionId",
        "QuizzesChoices",
        "MultipleChoiceQuizQuestionId");

      migrationBuilder.CreateIndex(
        "IX_SubjectPoll_PollId",
        "SubjectPoll",
        "PollId");

      migrationBuilder.CreateIndex(
        "IX_SubjectPoll_SubjectId",
        "SubjectPoll",
        "SubjectId");

      migrationBuilder.CreateIndex(
        "IX_SubjectRole_SubjectId",
        "SubjectRole",
        "SubjectId");

      migrationBuilder.CreateIndex(
        "IX_SubjectRole_UserId",
        "SubjectRole",
        "UserId");

      migrationBuilder.CreateIndex(
        "IX_Subjects_UniversityId",
        "Subjects",
        "UniversityId");

      migrationBuilder.CreateIndex(
        "IX_SubjectTemplates_UniversityId",
        "SubjectTemplates",
        "UniversityId");

      migrationBuilder.CreateIndex(
        "IX_UserGroup_GroupId",
        "UserGroup",
        "GroupId");

      migrationBuilder.CreateIndex(
        "IX_UserGroup_UserId",
        "UserGroup",
        "UserId");

      migrationBuilder.CreateIndex(
        "IX_UserRole_UserId",
        "UserRole",
        "UserId");

      migrationBuilder.CreateIndex(
        "IX_Users_UniversityId",
        "Users",
        "UniversityId");
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropTable(
        "Administrators");

      migrationBuilder.DropTable(
        "CareerSubject");

      migrationBuilder.DropTable(
        "Component");

      migrationBuilder.DropTable(
        "DeliveredAssignment");

      migrationBuilder.DropTable(
        "Notice");

      migrationBuilder.DropTable(
        "PollsChoices");

      migrationBuilder.DropTable(
        "QuizzesChoices");

      migrationBuilder.DropTable(
        "SubjectPoll");

      migrationBuilder.DropTable(
        "SubjectRole");

      migrationBuilder.DropTable(
        "UserGroup");

      migrationBuilder.DropTable(
        "UserRole");

      migrationBuilder.DropTable(
        "Careers");

      migrationBuilder.DropTable(
        "EducationalUnits");

      migrationBuilder.DropTable(
        "Assignment");

      migrationBuilder.DropTable(
        "PollAnswer");

      migrationBuilder.DropTable(
        "QuizAnswer");

      migrationBuilder.DropTable(
        "Group");

      migrationBuilder.DropTable(
        "SubjectTemplates");

      migrationBuilder.DropTable(
        "PollQuestion");

      migrationBuilder.DropTable(
        "PollResponse");

      migrationBuilder.DropTable(
        "QuizQuestion");

      migrationBuilder.DropTable(
        "QuizResponse");

      migrationBuilder.DropTable(
        "Poll");

      migrationBuilder.DropTable(
        "Quiz");

      migrationBuilder.DropTable(
        "Users");

      migrationBuilder.DropTable(
        "Subjects");

      migrationBuilder.DropTable(
        "Universities");
    }
  }
}
