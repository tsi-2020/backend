﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class subjectTemplate : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<bool>(
        name: "IsAsync",
        table: "Subjects",
        nullable: false,
        defaultValue: false);

      migrationBuilder.AddColumn<bool>(
        name: "IsRemote",
        table: "Subjects",
        nullable: false,
        defaultValue: false);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropColumn(
        name: "IsAsync",
        table: "Subjects");

      migrationBuilder.DropColumn(
        name: "IsRemote",
        table: "Subjects");
    }
  }
}
