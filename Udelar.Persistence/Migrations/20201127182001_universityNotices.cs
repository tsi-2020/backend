﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Udelar.Persistence.Migrations
{
  public partial class universityNotices : Migration
  {
    protected override void Up(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.AddColumn<Guid>(
        name: "UniversityId",
        table: "Notices",
        nullable: true);

      migrationBuilder.CreateIndex(
        name: "IX_Notices_UniversityId",
        table: "Notices",
        column: "UniversityId");

      migrationBuilder.AddForeignKey(
        name: "FK_Notices_Universities_UniversityId",
        table: "Notices",
        column: "UniversityId",
        principalTable: "Universities",
        principalColumn: "Id",
        onDelete: ReferentialAction.Restrict);
    }

    protected override void Down(MigrationBuilder migrationBuilder)
    {
      migrationBuilder.DropForeignKey(
        name: "FK_Notices_Universities_UniversityId",
        table: "Notices");

      migrationBuilder.DropIndex(
        name: "IX_Notices_UniversityId",
        table: "Notices");


      migrationBuilder.DropColumn(
        name: "UniversityId",
        table: "Notices");
    }
  }
}
