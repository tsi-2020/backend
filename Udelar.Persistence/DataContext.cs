﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Udelar.Model;
using Udelar.Model.Component;
using Udelar.Model.Poll;
using Udelar.Model.Poll.Answers;
using Udelar.Model.Poll.Questions;
using Udelar.Model.Quiz.Answers;
using Udelar.Model.Quiz.Questions;

namespace Udelar.Persistence
{
  public class DataContext : DbContext
  {
    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }

    public DbSet<Notice> Notices { get; set; }
    public DbSet<UniversityAdministrator> UniversityAdministrators { get; set; }
    public DbSet<UdelarAdministrator> UdelarAdministrators { get; set; }
    public DbSet<User> Users { get; set; }
    public DbSet<University> Universities { get; set; }
    public DbSet<Career> Careers { get; set; }
    public DbSet<SubjectTemplate> SubjectTemplates { get; set; }
    public DbSet<Subject> Subjects { get; set; }
    public DbSet<Assignment> Assignments { get; set; }
    public DbSet<Administrator> Administrators { get; set; }
    public DbSet<Score> Scores { get; set; }
    public DbSet<GroupDelivery> GroupDeliveries { get; set; }
    public DbSet<IndividualDelivery> IndividualAssignments { get; set; }
    public DbSet<StudentEnrollment> StudentSubjectRoles { get; set; }
    public DbSet<ProfessorEnrollment> TeacherSubjectRoles { get; set; }

    //POLL
    public DbSet<OpenPollQuestion> PollOpenQuestions { get; set; }
    public DbSet<MultipleChoicePollQuestion> PollsMultipleChoiceQuestions { get; set; }
    public DbSet<PollChoice> PollsChoices { get; set; }
    public DbSet<MultipleChoicePollAnswer> PollsMultipleChoiceAnswers { get; set; }
    public DbSet<OpenPollAnswer> PollsOpenAnswers { get; set; }
    public DbSet<PollResponse> PollResponse { get; set; }

    public DbSet<SubjectPoll> SubjectPolls { get; set; }
    public DbSet<UniversityPoll> UniversityPolls { get; set; }
    public DbSet<UdelarPoll> UdelarPolls { get; set; }

    //QUIZZ
    public DbSet<OpenQuizQuestion> QuizzesOpenQuestions { get; set; }
    public DbSet<MultipleChoiceQuizQuestion> QuizzesMultipleChoiceQuestions { get; set; }
    public DbSet<TrueFalseQuizQuestion> QuizzesTrueFalseQuestions { get; set; }
    public DbSet<QuizChoice> QuizzesChoices { get; set; }

    public DbSet<MultipleChoiceQuizAnswer> QuizzesMultipleChoiceAnswers { get; set; }
    public DbSet<OpenQuizAnswer> QuizzesOpenAnswers { get; set; }
    public DbSet<TrueFalseQuizAnswer> QuizzesTrueFalseAnswers { get; set; }

    //Components
    public DbSet<AssignmentComponent> AssignmentComponents { get; set; }
    public DbSet<DocumentComponent> DocumentComponents { get; set; }
    public DbSet<EducationalUnit> EducationalUnits { get; set; }
    public DbSet<ForumComponent> ForumComponents { get; set; }
    public DbSet<LinkComponent> LinkComponents { get; set; }
    public DbSet<PollComponent> PollComponents { get; set; }
    public DbSet<QuizComponent> QuizComponents { get; set; }
    public DbSet<TextComponent> TextComponents { get; set; }
    public DbSet<VideoComponent> VideoComponents { get; set; }
    public DbSet<VirtualMeetingComponent> VirtualMeetingComponents { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      Seed(builder);
      builder.Entity<UserRole>()
        .Property(userRole => userRole.Role)
        .HasConversion<string>();

      builder.Entity<User>().HasQueryFilter(b => EF.Property<DateTime>(b, "DeletedOn").Date == null);
      builder.Entity<University>().HasQueryFilter(b => EF.Property<DateTime>(b, "DeletedOn").Date == null);
      builder.Entity<Administrator>().HasQueryFilter(b => EF.Property<DateTime>(b, "DeletedOn").Date == null);
      builder.Entity<Career>().HasQueryFilter(b => EF.Property<DateTime>(b, "DeletedOn").Date == null);
      builder.Entity<Subject>().HasQueryFilter(b => EF.Property<DateTime>(b, "DeletedOn").Date == null);
      builder.Entity<CareerSubject>().HasQueryFilter(b => EF.Property<DateTime>(b, "DeletedOn").Date == null);
      base.OnModelCreating(builder);
    }

    private static void Seed(ModelBuilder builder)
    {
      builder.Entity<UdelarAdministrator>().HasData(
        new
        {
          Id = Guid.NewGuid(),
          FirstName = "Administrador",
          LastName = "Administrador",
          Email = "admin@udelar.uy",
          Password = BCrypt.Net.BCrypt.HashPassword("admin"),
          CI = 12345678,
          CreatedOn = DateTime.Now,
          ModifiedOn = DateTime.Now
        }
      );
    }


    public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
      CancellationToken cancellationToken = default)
    {
      var entries = ChangeTracker
        .Entries()
        .Where(e => e.Entity is BaseModel && (
          e.State == EntityState.Added
          || e.State == EntityState.Modified));

      foreach (var entityEntry in entries)
      {
        ((BaseModel)entityEntry.Entity).ModifiedOn = DateTime.Now;

        if (entityEntry.State == EntityState.Added)
        {
          ((BaseModel)entityEntry.Entity).CreatedOn = DateTime.Now;
        }
      }

      return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
    }
  }
}
